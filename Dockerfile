FROM atlas/athanalysis:21.2.137

ADD . /xampp/XAMPPtmva
WORKDIR /xampp/build
RUN source ../XAMPPtmva/HDF5Writing/test/setup_ci_release.sh &&  \
    sudo chown -R atlas /xampp && \
    cmake ../XAMPPtmva && \
    make 
