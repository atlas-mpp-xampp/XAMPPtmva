Welcome to XAMPPtmva! 
===

This package is the extension for machine learning of [XAMPPplotting](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPplotting). XAMPPtmva includes the support for TMVA and to the most common machine learning packages based on python interfaces like SciKitLearn, Keras & Theano.

How to download the code
===
We recommend you making your own branch or forking the project before you download the code. XAMPPtmva already includes the XAMPPplotitng as GIT submodule. So as long as you do not develop XAMPPplotting at the same time, just execute the following sequence of commands.
```
mkdir MyXAMPPtmva
cd MyXAMPPtmva/
git clone ssh://git@gitlab.cern.ch:7999/atlas-mpp-xampp/XAMPPtmva.git --recursive source -b <Your branch>
setupATLAS -c sl6+batch
asetup AthAnalysis,21.2.67,here
mkdir build
cd build
cmake ../source/
make -j 42
```

_Attention_: Numpy does not work properly on centos7 because of missing OpenBlas references. Therefore, it's extremely important that you run XAMPPtmva from inside the singularity containers. The setup downloads and installs several python packages in the backend -- in earlier days also HDF5, which requires a lot of disk space (~2-3GB). If there are any problems during compilation, please check *before hand* if you do not exceed your quota. Otherwise feel free to contact the developers which are willing to help. It'also extremely important that you do not setup `AthAnalysis` inside the source area, otherwise the dedicated `CMakeLists.txt` is overwritten, leading to compilation errors!!!!

After the compilation is done, do:
```
source build/${AthAnalysis_PLATFORM}/setup.sh
```
How to run a training using TMVA
===
Like in XAMPPplotting the steering of the job is based on config files, namely InputConfigs and TrainingConfigs. The InputConfigs are following essentially the same logic as in XAMPPplotting, but have been extended for some special use-cases as you can see below. Now let's get started with the TrainingConfigs.
``` 
### At the very beginning we must define the input, like in XAMPPplotting. 
### Avoiding to rewrite RunConfigs for the training and the later application this step is outsourced
### to the well known RunConfig files in order to load one just type
RunConfig <PAth to my run config where ever it is>
```
The RunConfig also satisfies the purpose to define the criteria of events which are suitable for training. If you like to train on two different topologies in one go, it's also possible you can do then the following:
```
RunConfig <Config path to my first topology>
RunConfig <Config path to my second topology>
....
```
Events satisying at least one of the topologies are then used for training. After we've loaded all the objects and criteria we want to train on, we need to define which features we want to use from the events. A new training variable is defined via the following blog
```
New_MVAVar
    Name <fancy name inside the training>
    <Then add the reader following the rules of XAMPPplotting>
End_MVAVar
```
Please be aware XAMPPtmva distinguishes between four types of training 
* Event classificaiton: That is the standard type. All training variables given to the training must be of scalar type. I.e it's explicitly checked for each event if the variable is valid and only contains one entry. Something like,
 ```
 ParReader MyElectrons pt
 ```
 is not possible. You must parse each particle momentum explicitly to the training. If the criteria are not satisfied the training is aborted immediately.

* Object classficaiton: This training type allows for some CP work to do some particle ID (like di-tau reconstruction, that was the motivation to which we came it least up with it). In order to tell XAMPPtmva that you want to perform some object-classifcation problems, please add `ObjectTraining`
 to the very beginning of your config file. Similary to the event classification, all training variable must hold foreach particle. Vector like variables must be converted to a flat-tuple like shape.

* Event/Object regression. In these cases the variable must be explicitly marked as the true-value you want to target to. You can do it if you add `isTargetVariable` to you variable block. I.e.:
```
New_MVAVar
   Name <I want to predict something>
   <Reader you want to predict>
   isTargetVariable
End_MVAVar
```
A side remark about regression problems: At the moment this is only supported for the python base machine-learning algorithms. For the TMVA based ones,  I still need to find out how to declare a variable as regression variable in the factory. If you know it already please let me know and I add this immediately to the framework. 
*Important*: XAMPPtmva bails out if you add targetVariables to classificaiton trainings or you do not add any targetVariable to regression problems.

Configuring the Training/Testing sample
==
By default XAMPPtmva splits the labelled events into Training and Testing. Training events are parsed to the MVA algorithm, testing events are later used to evaluate the performance and spot over-training, etc. By default the events are split in an alternate order. XAMPPtmva allows for some modifications of this splitting.

1) The most straightforward option is exchanging the Training and Testing events. In order to do so just add `InvertSplitting` somewhere in your TrainingConfig.
2) Changing the `SplitMode` either into `Random` or `Block`: If you use one of the two options, another quantity become important, the `TrainingFraction`. It determines how many event out of the block -- currently of size 100 - are piped to  training and testing. For the `Block` mode the first `N_{fraction}` events are piped to the training, the rest to testing. For the `Random` mode at each event a coin is thrown and the decision is made analogously.
3) Assigning DSIDs to training and testing: This option is very useful if you want to study the impact of the choice of your event generator to your training performance. In these cases you can define logical TrainValidSamples via.
```
New_TrainValidSample
   Name <Name of the logical sample e.g : ttbar>
   TrainingDSID <type all of the DSIDs which should be used for training. If the DSIDs are consecutevly then you can do start - end>
   ValidDSID < Datasets which should belong to the validation>
End_TrainValidSample
```
For training events from these samples the standard logic of testing and training does not apply, they're piped to the corresponding category.
4) Limiting the amount of events from a given sample: Sometimes the user needs a fixed amount of events from a given DSID in his training. One way would be to produce a dedicated slimmed tree or to use the funcionality provided by XAMPPtmva:
```
LimitEvents <Training/Testing> <DSID> <Limit>
```
As soon as the amount of used events is exceeded. The user is notified ones and all following events are ignored from that particular DSID.
5) Excluding datasets from contributing to the signal or the background. This feature has been motivated by the CP studies, where we wanted to rejected potential particle candidates from a given physics process. The you need to type either 
```
ExcludeFromSignal <DSID>
```
or 
```
ExcludeFromBackground <DSID>
```

Defining what's actually signal or background
==
Thus far we've learned how to select labled events, how to define the  event/object features and taylored what's going to be defined as a training and testing events. To conclude the meta section, we still need to tell XAMPPtmva what's actually signal or not. In case of event classification, it's fairly simpe, we've the DSIDs. You can define a range of dsids to become signal via:
```
SignalDSID <list of DSIDs you like to have>
```
Note: This keyword is one of the extensions of the inputconfigs. You can add it either there or in the training configs themselves.
Now for object classification, the DSID is not really a good choice or if you want to train on specific event features. Therefore, the other way is actually to define Cuts in the training config. Here kicks an very important subtlety in:
* Cut in the RunConfig file --> Which events are actually used for my MVA algorithm
* Cut in the TrainingConfig file --> What are the features to distinguish between background and signal

Also IMPORTANT: If you decide to parse both options. The logical *AND* is taken as descision boundary.

Configure the MVA algorithm itself.
==
Finally, we need to define what MVA we'd like to use. XAMPPtmva distinshuishes between two big groups, `TMVA` and `ScitKitLearn` or the python based ones. Their fundamental difference is that for `TMVA` everything is kept inside c++ while for `SciKitLearn`  we need to transform the data first into a python readable file format, which is in our case the HDF5 format (c.f. [HDF5 developer's](https://support.hdfgroup.org/HDF5/whatishdf5.html) page for more information).

Adding a method using TMVA
=
TMVA has been the first framework we tried out for machine learning. In order to configure a TMVA method you need to add a block like this to your training config-file
```
New_TMVAMethod
   Name <How do you call your method>
   Type <What's the fundamental algorithm you'd like to use. Currently supported BDT/Fisher/Cuts/Likelihood/PDERS/PDEFoam/KNN/HMatrix/LD/FDA/MLP/CFMlpANN/TMlpANN/SVM/RuleFit>
   Option <Define the hypertraining parameters of this algorithm>
End_TMVAMethod
```
About the pros and limitations of each method as well as about their hyperparameters to configure, please consult the [TMVA](https://root.cern.ch/tmva)' page. The common TMVA examples have been already added to the [TMVAExample.conf](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPtmva/blob/master/XAMPPtmva/data/TrainingConf/TMVAExample.conf) file. You can include this file as a starting point. There can be as many methods defined as you like in a config file. Finally we need to tell XAMPPtmva which algorithms we'd like to train with. In other words we need to book them via
```
BookMethod <Name of your method defined above>
```

Adding a method using SciKitLearn
= 
TMVA is entirely based on `C++`. Therefore all data is kept transient in the memory and kept within one instance of the programme. Thus we need to assemble, the configuration of the features to use and the hyperparameters of the machine-learning algorthims in one go.  As already mentioned above, the basic key for the python based machine-learning algorithms is to transform the data into the HDF5 file format. From this we can use python scripts for further processing and application. If this is left entirely to the user it might become messy and not so comfortable. Also scans of hyperparameters or comparisons of different trainings might end up in a book-keeping nightmare. Therefore, XAMPPtmva comes along with a basic set of python libraries allowing to steer your trainings in a more userfriendly way. Let's have first a look at the TrainingConfig side and then enlight the python side a bit. Currently, XAMPPtmva supports the `BDT` and `MLP` classification from the [SciKitLearn](scikit-learn.org). Let's say you'd like to define a new BDT based on the SciKitLearn package via:
```
New_SciKitMethod
   Name <My new method>
   Module <The python module to use with no .py ending>
   Option <The possible training options>
End_SciKitMethod
```
You can find the available Modules in the [python/SciKitLearn](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPtmva/tree/master/XAMPPtmva/python/SciKitLearn) directory of the package. These are just the "executables". This block assembles then the python commmand to execute and dumps it to a text-file. By default the cross-validation command is added to the file as well. Theses methods have then the same name with a `_swapped` appended. Note: Here is a slight trap you might tap into. You are not allowed to define methods ending with `swapped` for this reason.

Looking up the options & adding a new algortihm
=
Let's take the [BDT](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPtmva/blob/master/XAMPPtmva/python/SciKitLearn/Classifier/SciKitBDTClassifier.py) as an introductory example to explain the `Options` and also how to add a new machine-learning algorthim.  Our guideline is to configure the hyperparameters using the `argument` parser coming along with some default values ([BDT -options] (https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPtmva/blob/master/XAMPPtmva/python/SciKitLearn/Classifier/SciKitBDTClassifier.py#L306) which is nothing more than an extension of the basic I/O parser of the package. The list of parsable arguments is of course given by each algorithm - in case of SCiKitLearn usually by the constructor. You can have a look at the top of the file where we define a the [SciKitBDTClassifier](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPtmva/blob/master/XAMPPtmva/python/SciKitLearn/Classifier/SciKitBDTClassifier.py#L94) to setup the BDT from scikit learn and parse it to our skeleton which handles the dataset, does some basic check on the data and executes finally the training. New algorthims can be easily added by new classes inherting from the `SciKitClassfier` - the class also handles the regression problems. In principle you'll neeed only to create the instances of the MVA-classes. Everytrhing else should be handled by the mother class.

Note: You can have TMVA and SciKit methods defined in the same file. The execution macro picks the right ones.


How to run the code
===
XAMPPtmva is executed via three main macros `TrainTMVA` for TMVA training, `WriteHDF5` if you only like to write the HDF5 file and `TrainSciKitLearn` which writes the HDF5 file and then executes all of the training followed by a set of diagnostic plots. To each of the macros you need provide
1) An input config containing *all* samples
2) A training config.

To train [Scikitlearn](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPtmva/blob/master/XAMPPtmva/util/TrainSciKitLearn.cxx) just type
```
TrainSciKitLearn -i <Path to your input config> -t <Path to your training config> -d <Output directory default: SciKitLearnfiles/>
```
into your console. You can optionally add the arguments `--nThreads <x>` how many training threads are executed simultaneously and `--noPlots` then you do not get some nice summary pdfs with basic diagnositc plots. 
By default it's assumed that you want to parse a `classification` problem. If you aim for a regression problem please add the `--Regression` argument to the command.

To train [TMVA](https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPtmva/blob/master/XAMPPtmva/util/TrainTMVA.cxx) just type
```
TrainTMVA -i <Path to your input config> -t <Path to your training config> -d <Output directory default: TrainingFiles/> -o <ROOT outfile name in that directory default: output_tmva.root>
```


Apply the training and first diagnostics
===

Diagnostics
==
XAMPPtmva writes a lot of diagnostic histograms by default which is inspired by the `TMVA` framework. The histograms inclue:
1) Normalized distributions of the training & testing data split into background & signal
2) The correlation matrices between the input variables for signal & background
3) The normalized MVA score for training&testing background split into background & signal
4) The ordinary ROC curve - i.e. Signal efficiency against 1- background efficiency - as well as the background rejection ROC - signal efficiency against 1/ (1- background efficiency)
5) The Kolmogorov smirnov score which is a statistical test to measure the amount of overtraining by comparing the test & training ROC-curve [Wikipedia]().

You can plot these quantities in a nice way by using the python script stored in [python/TMVA]{} or [python/SciKitPlotting]{}. For more information please consult their parser help functions.

Apply the obtained training
==
Any machine learning is useless if it cannot be applied to new data or some nice plots can be

Miscellaneous
===


Extension of the input config files & file-format
==
By default XAPMPtmva assumes to recieve trees produced by XAMPPbase or one of its analysis-specific extensions. But there are exceptions to this rule. For instance it's  possible to parse derived HistFitter trees or any tree satisfying these simple formating rules:
1) All weights stored in the tree are stored as `dobule` branches.  There is no large restriction to their naming. You cannot parse 'NormWeight' , 'muWeight' and 'PeriodWeight' to the Weight string. By default XAMPPtmva assumes that there is a branch `NormWeight` present in the tree, otherwise it's throws an error which can be safely ignored.
2) The tree contains a branch named `runNumber` of type `unsigned int`, `mcChannelNumber`, `SUSYFinalState` both of type int present in the tree. The weight & training services inside the framework use the information from these branches to assign DSID, Normalization and training-testing information
If your files satisfy these criteria, they can safely parsed. You just need to add the `IsHistFitter` keyword to your input config. The assumption that the tree ends with `Nominal` can be disabled by the `SetNominalName` keyword in combination with `IsHistFitter`. 

XAMPPplotting itself does not allow to mingle `data` and `Monte-Carlo` events in one job. Unfortunately, we had some use cases where we took information directly from the data to have a better training performance. If you're facing also such a problem with the ``XAMPP`` trees then just add the `IsHybridInput` to your input config. Trees not produced with the XAMPP framework should satisfy the criteria described above and then it should be fine.


