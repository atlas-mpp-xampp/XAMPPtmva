#ifndef XAMPPtmva_TMVAFactoryUtils_H
#define XAMPPtmva_TMVAFactoryUtils_H

#include <XAMPPtmva/FactoryUtils.h>

#include <TMVA/DataLoader.h>
#include <TMVA/Factory.h>

#include <memory>
#include <string>
#include <vector>
namespace TMVA {
    class DataLoader;
    class Factory;
}  // namespace TMVA
namespace XAMPP {

    // This class stores the methods for the factory
    class TMVAFactoryMethod {
    public:
        TMVAFactoryMethod(const TMVA::Types::EMVA& Type, const std::string& Name, const std::vector<std::string>& Options);
        std::string name() const;
        bool BookInFactory(std::shared_ptr<TMVA::Factory> Factory);

    private:
        TMVA::Types::EMVA m_Type;
        std::string m_Name;
        std::vector<std::string> m_Options;
    };
    class TMVACacheProvider : public MVACacheProvider {
    public:
        static TMVACacheProvider* getProvider();
        virtual ~TMVACacheProvider();

        bool setupFactory() override;
        std::shared_ptr<TMVA::DataLoader> dataLoader() const;
        bool prepareAndExecuteTraining() override;
        void setOutFile(std::shared_ptr<TFile> file) override;
        void setOutDirectory(const std::string& outDir) override;

        bool addTrainingMethod(const TMVA::Types::EMVA& Type, const std::string& name, const std::vector<std::string>& Options);

    protected:
        TMVACacheProvider();
        TMVACacheProvider(const TMVACacheProvider&) = delete;
        void operator=(const TMVACacheProvider&) = delete;
        MVACacheHandler_Ptr createHandler(MVALogicalSmp_Ptr Smp, unsigned int TrainSize, unsigned int ValidSize,
                                          bool isEvHandler = true) const override;
        bool IsMethodKnown(const std::string& method) const override;

    private:
        std::shared_ptr<TMVA::DataLoader> m_DataLoader;
        std::shared_ptr<TMVA::Factory> m_Factory;
        std::vector<std::shared_ptr<TMVAFactoryMethod> > m_TrainingMethods;
        std::shared_ptr<TFile> m_outfile;
        std::string m_outDir;
    };
    // General Implementation to use the TMVA-DataLoader interface
    class TMVACache : public MVABuffer {
    public:
        TMVACache(unsigned int T, unsigned int Size);
        virtual ~TMVACache();

    private:
        bool FillEvent(const Cache& Event, const double& Weight) override;
        std::shared_ptr<TMVA::DataLoader> m_Loader;
    };
    //################################
    //  Implementation for training of entire events
    //################################
    class TMVAEventCache : public TMVACache {
    public:
        TMVAEventCache(unsigned int T, unsigned int Size);
        virtual ~TMVAEventCache();

    protected:
        bool PushBack(ITreeVarReader* Reader, Cache& Vec) override;
    };
    class TMVAEventClassification : public MVACacheHandler {
    public:
        TMVAEventClassification(MVALogicalSmp_Ptr Sample, unsigned int TrainSize = 1, unsigned int ValidSize = 1);
        virtual ~TMVAEventClassification();

    protected:
        std::shared_ptr<MVATrainTestSplitting> CreateCache(unsigned int Type) override;
    };
    class TMVAEventTrainTestHandler : public MVATrainTestSplitting {
    public:
        TMVAEventTrainTestHandler(unsigned int Type, MVACacheHandler* Handler);
        ~TMVAEventTrainTestHandler();

    protected:
        std::shared_ptr<MVABuffer> CreateCache(unsigned int T, unsigned int Size) override;
    };

    //###################
    //  Implementation for the training of particular objects
    //###################
    class TMVAObjectClassification : public MVACacheHandler {
    public:
        TMVAObjectClassification(MVALogicalSmp_Ptr Sample, unsigned int TrainSize = 1, unsigned int ValidSize = 1);

        using MVACacheHandler::CacheEvent;
        CachingStatus CacheEvent(const std::vector<ITreeVarReader*>& Readers) override;

    protected:
        std::shared_ptr<MVATrainTestSplitting> CreateCache(unsigned int Type) override;
    };
    class TMVAObjectTrainTestHandler : public MVATrainTestSplitting {
    public:
        TMVAObjectTrainTestHandler(unsigned int Type, TMVAObjectClassification* Handler);
        unsigned int TargetEntry() const;
        ~TMVAObjectTrainTestHandler();

    protected:
        std::shared_ptr<MVABuffer> CreateCache(unsigned int T, unsigned int Size) override;
        TMVAObjectClassification* m_parent;
    };
    class TMVAObjectCache : public TMVACache {
    public:
        TMVAObjectCache(TMVAObjectTrainTestHandler* Handler, unsigned int T, unsigned int Size);
        virtual ~TMVAObjectCache();

    protected:
        bool PushBack(ITreeVarReader* Reader, Cache& Vec) override;

    private:
        TMVAObjectTrainTestHandler* m_ObjectHandler;
    };

}  // namespace XAMPP
#endif
