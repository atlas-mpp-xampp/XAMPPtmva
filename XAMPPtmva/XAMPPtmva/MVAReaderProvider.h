#ifndef XAMPPTMVA_TMVAREADERPROVIDER_H_
#define XAMPPTMVA_TMVAREADERPROVIDER_H_

#include <XAMPPplotting/ReaderProvider.h>
namespace XAMPP {
    class MVAReaderProvider : public ReaderProvider {
    public:
        static ReaderProvider* GetInstance(bool DisableGeVToMeV = false);

        virtual ITreeVarReader* CreateReader(std::stringstream& sstr);
        virtual ITreeVarReader* CreateReader(std::ifstream& inf, std::stringstream& sstr);

        virtual ParticleReader* GetParticle(XAMPP::TimeComponent t, const std::string& Name, const std::string& Coll = "");

        virtual ~MVAReaderProvider();

    protected:
        MVAReaderProvider(bool DisableGeVToMeV);
        MVAReaderProvider(const MVAReaderProvider&) = delete;
        void operator=(const MVAReaderProvider&) = delete;

    private:
        ITreeVarReader* CreateEventTMVAReader(std::stringstream& sstr);
        ITreeVarReader* CreateEventSciKitReader(std::stringstream& sstr);

        ITreeVarReader* CreateParticleTMVAReader(std::stringstream& sstr);

        ITreeVarReader* CreateSciKitRegressionReader(std::stringstream& sstr);
        bool m_DisableGeVToMeV;
    };

}  // namespace XAMPP

#endif /* XAMPPTMVA_TMVAREADERPROVIDER_H_ */
