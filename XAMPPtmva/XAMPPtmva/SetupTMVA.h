#ifndef XAMPPTMVA_SETUPTMVA_H
#define XAMPPTMVA_SETUPTMVA_H

#include <XAMPPplotting/AnalysisSetup.h>
#include <XAMPPtmva/MVAvariable.h>
#include <memory>
namespace TMVA {
    class Factory;
}
namespace XAMPP {

    class MVATreeMaker;
    class Condition;
    class SetupTMVA : public AnalysisSetup {
    public:
        SetupTMVA();
        virtual ~SetupTMVA();
        bool ConfigureTraining(const std::string& InputCfg, const std::string& TrainingCfg, const std::string& OutputDir,
                               const std::string& FileName);

        bool ReadTrainConfig(const std::string& train_cfg);

        void CloseFile();
        void setBatchMode();
        void RegressionTraining();

    protected:
        typedef bool (SetupTMVA::*ConfigReader)(std::ifstream& inf);

        bool ImportFile(const std::string& Config, ConfigReader Parser, bool always = false);

        virtual int CheckAnaConfigProperties(std::ifstream& inf, const std::string& line);
        virtual int CheckInputConfigProperties(std::ifstream& inf, const std::string& line);

    private:
        bool CreateOutFile(const std::string& File);

        bool ReadTMVAConfig(std::ifstream& inf);
        bool AssignNewTrainValidSample(std::ifstream& inf);
        bool BookMVAMethods(std::stringstream& sstr, bool BookFactory = true);

        // Methods to run machine learning on SciKitLearn and TMVA
        bool NewTMVATrainingMethod(std::ifstream& inf);
        bool NewSciKitTrainingMethod(std::ifstream& inf);

        bool RunTreeMaker();

        bool AddMVAVariable(std::ifstream& inf, bool AssignToFactory = false);

        bool ConfigureMVAReader(std::ifstream& inf);

        bool CreateSciKitReader(std::ifstream& inf, bool EvClass);

        bool CreateTMVAReader(std::ifstream& inf, bool EvClass);

        bool m_classification;
        bool m_BatchMode;
        // I/O related stuff
        std::shared_ptr<TFile> m_outFile;
        bool m_IsHistFitInput;

        std::vector<int> m_SignalDSIDs;
        bool m_SkipNegWeight;

        int m_applicationToSetup;
        std::vector<std::string> m_BookedMethods;

        std::vector<std::shared_ptr<Condition>> m_SignalConditions;

        bool m_doObjectTraining;

        MVATrainingReader* m_CurrentHelper;
    };

}  // namespace XAMPP
#endif  // ANALYSISSETUP_H
