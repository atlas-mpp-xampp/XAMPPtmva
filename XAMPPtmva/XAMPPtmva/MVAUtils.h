#ifndef XAMPPtmva_MVAUtils_H
#define XAMPPtmva_MVAUtils_H

#include <XAMPPplotting/FinalPlotHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
class TGraphAsymmErrors;

namespace XAMPP {

    class MVAPerformanceHelper {
    public:
        MVAPerformanceHelper(const std::shared_ptr<TH1>& background_train, const std::shared_ptr<TH1>& background_test,
                             const std::shared_ptr<TH1>& signal_train, const std::shared_ptr<TH1>& signal_test);

        MVAPerformanceHelper(const TH1* background_train, const TH1* background_test, const TH1* signal_train, const TH1* signal_test);

        static std::shared_ptr<TH1> createGiniHisto(const std::shared_ptr<TH1>& signal, const std::shared_ptr<TH1>& background);
        static std::shared_ptr<TH1> createGiniHisto(const TH1* signal, const TH1* background);

        static double calculateGiniIndex(const std::shared_ptr<TH1>& signal, const std::shared_ptr<TH1>& background);
        static double calculateGiniIndex(const TH1* signal, const TH1* background);

        static std::pair<double, double> fitResolution(std::shared_ptr<TH1> histo, double x_min = -1, double x_max = -1);
        static std::pair<double, double> fitResolution(TH1* histo, double x_min = -1, double x_max = -1);

    private:
        std::shared_ptr<TGraphAsymmErrors> generate_ROC(const std::shared_ptr<TH1>& bkg, const std::shared_ptr<TH1>& sig,
                                                        bool is_train = true, bool inv_bkg_eff = false);
        std::shared_ptr<TH1> createCulmulative(const std::shared_ptr<TH1>& H);

        std::shared_ptr<TH1> m_background_train;
        std::shared_ptr<TH1> m_background_test;

        std::shared_ptr<TH1> m_signal_train;
        std::shared_ptr<TH1> m_signal_test;

        // Classical ROC curves
        std::shared_ptr<TGraphAsymmErrors> m_train_roc;
        std::shared_ptr<TGraphAsymmErrors> m_test_roc;
        // signal efficiency against inverse background rejection
        std::shared_ptr<TGraphAsymmErrors> m_train_eff;
        std::shared_ptr<TGraphAsymmErrors> m_test_eff;
    };
}  // namespace XAMPP
#endif
