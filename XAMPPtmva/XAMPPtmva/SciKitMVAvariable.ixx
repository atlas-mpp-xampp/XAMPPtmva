#ifndef XAMPPtmva_SciKitReaderUtils_IXX
#define XAMPPtmva_SciKitReaderUtils_IXX

#include <XAMPPtmva/SciKitMVAvariable.h>
namespace XAMPP {
    //######################################################
    //              SciKiyPythonObjectService
    //######################################################
    template <typename T>
    std::shared_ptr<np::ndarray> SciKiyPythonObjectService::new_numpy_array(unsigned int rows, unsigned int cols, unsigned int shelfs) {
        if (!m_hasErrors) {
            if (rows == 0) {
                Error("SciKiyPythonObjectService::new_numpy_array()", "You want to initialize an array with 0 rows?");
                m_hasErrors = true;
            } else {
                try {
                    if (cols == 0) {
                        return std::make_shared<np::ndarray>(np::zeros(p::make_tuple(rows), np::dtype::get_builtin<T>()));
                    } else if (shelfs == 0) {
                        return std::make_shared<np::ndarray>(np::zeros(p::make_tuple(rows, cols), np::dtype::get_builtin<T>()));
                    } else {
                        return std::make_shared<np::ndarray>(np::zeros(p::make_tuple(rows, cols, shelfs), np::dtype::get_builtin<T>()));
                    }
                } catch (...) { setInErrorState(); }
            }
        }
        return std::shared_ptr<np::ndarray>();
    }
    //######################################################
    //              SciKitNumPyArray
    //######################################################
    template <class T>
    SciKitNumPyArray<T>::SciKitNumPyArray(unsigned int r, unsigned int c, unsigned int s) :
        m_array(SciKiyPythonObjectService::getService()->new_numpy_array<T>(r, c, s)),
        m_rows(r),
        m_cols(c),
        m_shelfs(s),
        m_read_only(false) {}
    template <class T>
    SciKitNumPyArray<T>::SciKitNumPyArray(const np::ndarray& other) :
        m_array(std::make_shared<np::ndarray>(other)),
        m_rows(other.shape(0)),
        m_cols(other.get_nd() > 1 ? other.shape(1) : 0),
        m_shelfs(other.get_nd() > 2 ? other.shape(2) : 0),
        m_read_only(true) {}
    template <class T> bool SciKitNumPyArray<T>::set(unsigned int row, const T& val) const {
        if (m_read_only || rows() <= row || !m_array || cols() > 0 || shelfs() > 0) return false;
        (*reinterpret_cast<T*>(m_array->get_data() + row * m_array->get_strides()[0])) = val;
        return true;
    }
    template <class T> bool SciKitNumPyArray<T>::set(unsigned int row, unsigned int col, const T& val) const {
        if (m_read_only || rows() <= row || !m_array || shelfs() > 0)
            return false;
        else if (col == 0 && cols() == 0)
            return set(row, val);
        else if (col >= cols())
            return false;
        (*reinterpret_cast<T*>(m_array->get_data() + row * m_array->get_strides()[0] + col * m_array->get_strides()[1])) = val;
        return true;
    }
    template <class T> bool SciKitNumPyArray<T>::set(unsigned int row, unsigned int col, unsigned int shelf, const T& val) const {
        if (m_read_only || rows() <= row || !m_array)
            return false;
        else if (shelf == 0 && shelfs() == 0)
            return set(row, col, val);
        else if (col >= cols() || shelf >= shelfs())
            return false;
        (*reinterpret_cast<T*>(m_array->get_data() + row * m_array->get_strides()[0] + col * m_array->get_strides()[1] +
                               shelf * m_array->get_strides()[2])) = val;
        return true;
    }
    template <class T> const T SciKitNumPyArray<T>::get(unsigned int row, unsigned int col, unsigned int shelf) const {
        if (!m_array || row >= rows() || (col != 0 && cols() <= col) || (shelf != 0 && shelfs() <= shelf)) return std::nan("nan");

        return (*reinterpret_cast<T*>(
            m_array->get_data() + row * m_array->get_strides()[0] +
            (cols() > 0 ? col * m_array->get_strides()[1] + (shelfs() > 0 ? shelf * m_array->get_strides()[2] : 0) : 0)));
    }
    template <class T> unsigned int SciKitNumPyArray<T>::rows() const { return m_rows; }
    template <class T> unsigned int SciKitNumPyArray<T>::cols() const { return m_cols; }
    template <class T> unsigned int SciKitNumPyArray<T>::shelfs() const { return m_shelfs; }
    template <class T> std::shared_ptr<np::ndarray> SciKitNumPyArray<T>::get_data() const { return m_array; }

}  // namespace XAMPP
#endif
