#ifndef XAMPPTMVA_XAMPPTMVADICT_H
#define XAMPPTMVA_XAMPPTMVADICT_H

#include <XAMPPtmva/FactoryUtils.h>
#include <XAMPPtmva/MVAParticleReader.h>
#include <XAMPPtmva/MVAReaderProvider.h>
#include <XAMPPtmva/MVATreeMaker.h>
#include <XAMPPtmva/MVAUtils.h>
#include <XAMPPtmva/MetaDataTreeReader.h>
#include <XAMPPtmva/SciKitFactoryUtils.h>
#include <XAMPPtmva/SciKitFactoryUtilsRegression.h>
#include <XAMPPtmva/SciKitMVAvariable.h>
#include <XAMPPtmva/SetupTMVA.h>
#include <XAMPPtmva/TMVAFactoryUtils.h>
#include <XAMPPtmva/TMVAvariable.h>
#include <XAMPPtmva/TemplateFitReader.h>

#endif
