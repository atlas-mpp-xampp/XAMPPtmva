#ifndef XAMPPtmva_TMVAParticleReader_H
#define XAMPPtmva_TMVAParticleReader_H
#include <XAMPPplotting/TreeVarReader.h>

namespace XAMPP {
    class MVAParticleReader : public ParticleReader {
    public:
        static MVAParticleReader* GetReader(const std::string& Name, const std::string& Coll = "");

    protected:
        MVAParticleReader(const std::string& Name, const std::string& Coll = "");
        MVAParticleReader(const MVAParticleReader&) = delete;
        void operator=(const MVAParticleReader&) = delete;

        ITreeVectorReader* AddVariable(std::stringstream& sstr) override;
    };

    class MVAParticleReaderM : public MVAParticleReader {
    public:
        static MVAParticleReaderM* GetReader(const std::string& Name, std::string Coll = "");
        const TLorentzVector& P4() const override;
        TLorentzVector GetP4(size_t E) override;

    protected:
        MVAParticleReaderM(std::string Name, std::string Coll = "");
        MVAParticleReaderM(const MVAParticleReaderM&) = delete;
        void operator=(const MVAParticleReaderM&) = delete;
        std::string time_component() const override;
    };
}  // namespace XAMPP
#endif
