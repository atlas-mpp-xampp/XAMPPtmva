#ifndef XAMPPtmva_SciKitReaderUtils_H
#define XAMPPtmva_SciKitReaderUtils_H

#include <Python.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1.h>
#include <XAMPPtmva/MVAvariable.h>
#include <boost/python.hpp>
#include <boost/python/numpy.hpp>
#include <memory>

namespace XAMPP {
    namespace np = boost::python::numpy;
    namespace p = boost::python;
    typedef boost::python::object py_obj;

    class SciKitScoreParser;
    class SciKitDataReadBuffer;
    //###############################################################################
    //      The SciKiyPythonObjectService class interfaces the MVAReaderInterface   #
    //      to python. Since only one python thread shall be present in the entire  #
    //      enviroment, this class is written as a service                          #
    //###############################################################################
    class SciKiyPythonObjectService {
    public:
        static SciKiyPythonObjectService* getService();
        ~SciKiyPythonObjectService();

        /// Creates an instance of the SciKitClassReader to read out a numpy training from
        /// the disk an apply it on unkown data
        std::shared_ptr<py_obj> initSciKitClassReader(const std::string& training_path, const std::string& train_name);

        /// Creates an instance of the SciKitRegressoinReader to apply regression to unkown data
        /// from a SciKit training.
        std::shared_ptr<py_obj> initSciKitRegressionReader(const std::string& training_path, const std::string& train_name);

        /// Creates a numpy array with zeros initialized.
        template <typename T>
        std::shared_ptr<np::ndarray> new_numpy_array(unsigned int rows, unsigned int cols = 0, unsigned int shelfs = 0);

        /// Returns a shared_ptr to a new python list
        std::shared_ptr<boost::python::list> newPyList();

        /// Returns the function attribute to evaluate the classification MVA score from the data
        std::shared_ptr<py_obj> evaluationClassObj() const;
        /// Returns the function attribute to check if the variable names are consistent
        /// with the ones used in the training
        std::shared_ptr<py_obj> consistencyClassObj() const;
        /// Returns the function attribute to obtain the regression for a given variable from the data
        std::shared_ptr<py_obj> evaluationRegressionObj() const;
        /// Returns the function attribute to check the consistency of the regression training
        std::shared_ptr<py_obj> consistencyRegressionObj() const;

        /// Returns the function attribute to obtain the number of targets in the regression training
        std::shared_ptr<py_obj> regressionTargetsObj() const;
        /// Returns the function attribute to find out the place of a specific target variable
        std::shared_ptr<py_obj> regressionTargetIdxObj() const;

        bool importTrainingModule(const std::string& module);
        /// Check the error state of the service
        bool hasErrors() const;
        /// If any exception from python is thrown this service is set into the error state
        /// The service will not return anything useful anymore from this point.
        void setInErrorState();

    private:
        // Basic Singleton constructor
        SciKiyPythonObjectService();
        SciKiyPythonObjectService(const SciKiyPythonObjectService&) = delete;
        void operator=(const SciKiyPythonObjectService&) = delete;

        static SciKiyPythonObjectService* m_Inst;
        // Pointer to the SciKitReader python module
        // This module states the python site of
        //  the SciKitLearn -- XAMPPtmva module
        std::shared_ptr<py_obj> m_read_module;

        //##########################################################
        //  The following objects are meant to be assigned to      #
        //  read classification trainings from the file            #
        //##########################################################
        // Pointer to store the table to the SciKitReader
        std::shared_ptr<py_obj> m_scikit_classReader_table;
        // Pointer to store the function pointer to the evaluate score method
        std::shared_ptr<py_obj> m_sciKit_class_evaluate;
        // Pointer to store the function pointer to the consistency check method
        std::shared_ptr<py_obj> m_sciKit_class_consistency;

        //##########################################################
        //    SciKitLearn also supports regression problems.       #
        //    The access to the python classes are initialzed here #
        //##########################################################
        std::shared_ptr<py_obj> m_scikit_classRegression_table;
        std::shared_ptr<py_obj> m_scikit_regression_evaluate;
        std::shared_ptr<py_obj> m_scikit_regression_consistency;
        std::shared_ptr<py_obj> m_scikit_regression_nTargets;
        std::shared_ptr<py_obj> m_scikit_regression_targetIdx;

        std::map<std::string, std::shared_ptr<py_obj>> m_scikit_class;
        bool m_hasErrors;
    };
    template <class T> class SciKitNumPyArray {
    public:
        SciKitNumPyArray(unsigned int rows, unsigned int cols = 0, unsigned int shelfs = 0);
        SciKitNumPyArray(const np::ndarray& other);

        inline bool set(unsigned int row, const T& val) const;
        inline bool set(unsigned int row, unsigned int col, const T& val) const;
        inline bool set(unsigned int row, unsigned int col, unsigned int shelf, const T& val) const;

        inline const T get(unsigned int row, unsigned int col = 0, unsigned int shelf = 0) const;
        inline unsigned int rows() const;
        inline unsigned int cols() const;
        inline unsigned int shelfs() const;

        std::shared_ptr<np::ndarray> get_data() const;

    private:
        std::shared_ptr<np::ndarray> m_array;
        unsigned int m_rows;
        unsigned int m_cols;
        unsigned int m_shelfs;
        bool m_read_only;
    };
    //  Translate the std::vector to a python list parsable to
    //  the SciKit evaluation algorithm
    //  The parsing of vectors on an event by event basis is horrible slow, even if we use the NumPy arrays.
    //  However, if we try to chunk the data, that might speed up the job
    //
    class SciKitChunkBufferService {
    public:
        static SciKitChunkBufferService* getService();
        ~SciKitChunkBufferService();
        bool fill_buffer();
        unsigned int buffer_size() const;
        Long64_t last_updated() const;

        void Register(SciKitDataReadBuffer* B);

    private:
        bool renew_buffer() const;
        static SciKitChunkBufferService* m_Inst;
        SciKitChunkBufferService();
        SciKitChunkBufferService(const SciKitChunkBufferService&) = delete;
        void operator=(const SciKitChunkBufferService&) = delete;

        EventService* m_eventService;
        unsigned int m_treeNumber;

        Long64_t m_updated_last_time;
        unsigned int m_buffer_size;
        std::vector<SciKitDataReadBuffer*> m_score_buffers;
    };

    class SciKitDataReadBuffer {
    public:
        SciKitDataReadBuffer(const std::vector<std::shared_ptr<SciKitScoreParser>>& T);
        void append(MVAParserInterface* I);
        // create an ordinary data-buffer for event
        // classification/ regression

        // Creates the begin buffer object storing
        // How many objects to expect in each event
        void newSizeBuffer(unsigned int num_ev_to_buffer);
        bool getNumObjects(unsigned int pos);

        void newDataBuffer();

        bool addToBuffer(unsigned int pos);
        bool propagate_update();

        unsigned int needed_buffer_space() const;

    private:
        unsigned int min_size() const;
        SciKiyPythonObjectService* m_python_service;
        SciKitChunkBufferService* m_buffer_service;
        std::unique_ptr<SciKitNumPyArray<float>> m_data_buffer;
        // Cache whether the event was good or not
        std::shared_ptr<SciKitNumPyArray<unsigned int>> m_size_buffer;

        std::vector<MVAParserInterface*> m_interfaces;
        const std::vector<std::shared_ptr<SciKitScoreParser>>& m_trainings;
        unsigned int m_N;
        unsigned int m_Space;
    };
    //
    // The SciKitScoreParser communicates with the python ScikitReader
    // and buffers the MVA scores for a given range of events
    //
    class SciKitScoreParser {
    public:
        SciKitScoreParser(const std::string& method);
        virtual ~SciKitScoreParser();
        std::string method() const;
        bool updateScores(std::shared_ptr<np::ndarray> data_buffer);
        void setSizeInformation(std::shared_ptr<SciKitNumPyArray<unsigned int>> buffer);
        // Retrieve the information from the SciKit score parser
        // The first argument represents the entry of the vector
        // to pick-up and the second one the i-th variable
        double evaluate(unsigned int n = 0, unsigned int col = 0);
        virtual bool checkConsistency(const std::vector<std::string>& train_varNames);
        unsigned int nColumns() const;

        unsigned int entries() const;

    protected:
        virtual std::shared_ptr<py_obj> train_obj() const = 0;
        virtual std::shared_ptr<py_obj> evaluation_obj() const = 0;
        virtual std::shared_ptr<py_obj> consistency_obj() const = 0;
        // In case one needs to apply multi-dimensional regressions
        void setColumns(unsigned int c);

    private:
        SciKiyPythonObjectService* m_python_service;
        SciKitChunkBufferService* m_buffer_service;
        EventService* m_event_service;
        std::string m_method;
        std::unique_ptr<SciKitNumPyArray<double>> m_score_buffer;
        std::shared_ptr<SciKitNumPyArray<unsigned int>> m_size_buffer;
        unsigned int m_cols;
    };
    class SciKitScoreClassParser : public SciKitScoreParser {
    public:
        SciKitScoreClassParser(const std::string& train_path, const std::string& method);

    protected:
        virtual std::shared_ptr<py_obj> train_obj() const;
        virtual std::shared_ptr<py_obj> evaluation_obj() const;
        virtual std::shared_ptr<py_obj> consistency_obj() const;

    private:
        SciKiyPythonObjectService* m_python_service;
        std::shared_ptr<py_obj> m_py_reader;
    };
    class SciKitScoreRegressionParser : public SciKitScoreParser {
    public:
        SciKitScoreRegressionParser(const std::string& train_path, const std::string& method);
        virtual bool checkConsistency(const std::vector<std::string>& train_varNames);

        unsigned int tagetIndex(const std::string& var_name) const;
        // Use the diagnostic file to retrieve the relative resolution histogram
        // of the MVA regression algorithm. The histogram is then fitted to a gaussian
        // function and then returned.

        std::shared_ptr<TF1> resolution(const std::string& var_name) const;

    protected:
        virtual std::shared_ptr<py_obj> train_obj() const;
        virtual std::shared_ptr<py_obj> evaluation_obj() const;
        virtual std::shared_ptr<py_obj> consistency_obj() const;

    private:
        SciKiyPythonObjectService* m_python_service;
        std::shared_ptr<py_obj> m_py_reader;
        std::unique_ptr<TFile> m_diag_file;
    };

    class SciKitMVATrainingReader : public MVATrainingReader {
    public:
        SciKitMVATrainingReader(const std::string& training, const std::string& file_path,
                                MVATrainingReader::TrainingType T = MVATrainingReader::TrainingType::EventClass);
        virtual bool loadMethods();
        std::shared_ptr<SciKitScoreParser> getMethod(const std::string& method) const;

    protected:
        std::shared_ptr<MVAParserInterface> newParserInterface(const std::string& var_name, ITreeVarReader* tree_reader);
        virtual std::shared_ptr<SciKitScoreParser> addScoreParser(const std::string& method);
        virtual bool parse_toML();

    private:
        std::vector<std::shared_ptr<SciKitScoreParser>> m_trainings;
        std::shared_ptr<SciKitDataReadBuffer> m_data_buffer;
    };

    class SciKitMVAParserInterface : public MVAParserInterface {
    public:
        SciKitMVAParserInterface(const std::string& variable_name, ITreeVarReader* reader, std::shared_ptr<SciKitDataReadBuffer> cache);
    };

    class SciKitEventScoreVarReader : public IScalarReader {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double read() const;
        virtual bool update();
        virtual size_t entries() const;
        virtual ~SciKitEventScoreVarReader();
        static ITreeVarReader* GetReader(const std::string& Method, const std::string& ClassName = "");

    protected:
        SciKitEventScoreVarReader(const std::string& Method, const std::string& ClassName = "");
        SciKitEventScoreVarReader(const SciKitEventScoreVarReader&) = delete;
        void operator=(const SciKitEventScoreVarReader&) = delete;

        std::string m_Method;
        SciKitMVATrainingReader* m_helper;
        EventService* m_eventService;
        std::shared_ptr<SciKitScoreParser> m_MVA;

        bool m_Registered;
        mutable unsigned int m_EventNumber;
        mutable double m_Cache;

        bool m_Booked;
    };

    class SciKitEventRegressionVarReader;
    //##################################
    //  It's conceptional not the cleanest solution but the easiest one to propagate
    //  systematics of the regression algorithm to the XAMPP framework
    class SciKitRegressionSystHandler {
    public:
        std::string target_variable() const;
        SciKitMVATrainingReader* get_helper() const;
        inline double scale_syst() const { return m_syst; }
        inline SciKitScoreRegressionParser* get_parser() const { return m_MVA; }
        inline double get_mean() const { return m_res_func ? m_res_func->GetParameter(1) : 0; }

        friend SciKitEventRegressionVarReader;

        bool is_good() const;

    protected:
        SciKitRegressionSystHandler(const std::string& method, const std::string& className, const std::string& target_var);
        SciKitRegressionSystHandler(const SciKitRegressionSystHandler&) = delete;
        void operator=(const SciKitRegressionSystHandler&) = delete;

        class SciKitRegressionSystmatic : public IKinematicSyst {
        public:
            SciKitRegressionSystmatic(SciKitRegressionSystHandler* instance, bool is_up);
            virtual std::string syst_name() const;
            virtual std::string tree_name() const;
            virtual bool is_nominal() const;
            virtual bool apply_syst();
            virtual void reset();

        private:
            SciKitRegressionSystHandler* m_reference;
            bool m_is_up;
        };

    private:
        std::string syst_prestring() const;
        SciKitMVATrainingReader* m_helper;
        SciKitScoreRegressionParser* m_MVA;
        std::string m_target_variable;
        std::shared_ptr<TF1> m_res_func;

        std::shared_ptr<SciKitRegressionSystmatic> m_up_syst;
        std::shared_ptr<SciKitRegressionSystmatic> m_down_syst;

        double m_syst;
    };

    class SciKitEventRegressionVarReader : public IScalarReader {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double read() const;
        virtual bool update();
        virtual size_t entries() const;
        virtual ~SciKitEventRegressionVarReader();

        static ITreeVarReader* GetReader(const std::string& Method, const std::string& ClassName, const std::string& target_var);

    private:
        SciKitEventRegressionVarReader(const std::string& method, const std::string& className, const std::string& target_var);

        std::shared_ptr<SciKitRegressionSystHandler> m_systHandler;
        EventService* m_eventService;
        unsigned int m_targetIdx;

        bool m_Registered;
        mutable unsigned int m_EventNumber;
        mutable double m_Cache;
    };

    class SciKitObjectScoreVarReader : public IVectorReader {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;

        static ITreeVectorReader* GetReader(const std::string& Method, const std::string& ClassName);

    private:
        SciKitObjectScoreVarReader(const std::string& Method, const std::string& ClassName = "");
        SciKitObjectScoreVarReader(const SciKitObjectScoreVarReader&) = delete;
        void operator=(const SciKitObjectScoreVarReader&) = delete;

        std::string m_Method;
        SciKitMVATrainingReader* m_helper;
        std::shared_ptr<SciKitScoreParser> m_MVA;

        bool m_Registered;
        bool m_Booked;
    };
}  // namespace XAMPP
#include <XAMPPtmva/SciKitMVAvariable.ixx>
#endif
