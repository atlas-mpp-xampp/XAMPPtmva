#ifndef XAMPPtmva_SciKitLearnFactory_H
#define XAMPPtmva_SciKitLearnFactory_H

#include <HDF5Writing/HDF5ClassFactory.h>

namespace XAMPP {
    class SciKitLearnFactory : public HDF5ClassFactory {
    public:
        SciKitLearnFactory();

    protected:
        bool initializeFile() override;
        // Write into the meta-data whether the HDF5 file is meant for classification
        // or for regression. The integer is derived from EventType in the Factor utils
        virtual unsigned int data_type() const;
    };

    class SciKitRegressionFactory : public HDF5RegressionFactory, public SciKitLearnFactory {
    public:
        SciKitRegressionFactory();
        using HDF5RegressionFactory::AddTestingEvent;
        using HDF5RegressionFactory::AddTrainingEvent;

    protected:
        bool initializeFile() override;
        unsigned int data_type() const override;
    };
}  // namespace XAMPP
#endif
