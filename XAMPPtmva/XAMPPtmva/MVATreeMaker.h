#ifndef XAMPPTMVA_TMVAINPUTTREEMAKER_H
#define XAMPPTMVA_TMVAINPUTTREEMAKER_H
#include <XAMPPplotting/Selector.h>
#include <XAMPPtmva/FactoryUtils.h>

#include <TString.h>
#include <map>
namespace XAMPP {
    enum SplitMode {
        Alternate = 0,
        Block,
        Random,
    };
    class ITreeVarReader;
    class MVATreeMaker : public XAMPP::Selector {
    public:
        MVATreeMaker(const std::string &Name, const std::string &TreeName, const std::vector<std::string> &Files);
        virtual ~MVATreeMaker();

        void SetOutputLocation(std::shared_ptr<TFile> File);
        void RequirePosWeights();
        void ObjectTraining();
        void RegressionProblem();
        virtual void FinalizeOutput();
        virtual void SetOutputLocation(const std::string &);

        long long unsigned int GetNumTrainSig() const;
        long long unsigned int GetNumTrainBkg() const;

        long long unsigned int GetNumTestSig() const;
        long long unsigned int GetNumTestBkg() const;

        bool CheckTraining() const;

    protected:
        // Here is the place to implement your actual analysis. The job is aborted if false is returned
        virtual bool AnalyzeEvent();
        using Selector::AppendReader;
        void AppendReader(const std::vector<ITreeVarReader *> &readers);

        // Called once per Each systematic.. Essential to reset the counter per each loop
        virtual bool initBranches(const std::string &Syst);
        long long unsigned int GetEvents(MVACacheHandler::Events Ev) const;

        // The list of ITreeVarReaders to train TMVA...
        // This list is a subset of the ones initialized by SetReaders();
        std::vector<ITreeVarReader *> m_trainVariables;
        // In case of regression we need to parse the true values of the event to the
        // MVA training
        std::vector<ITreeVarReader *> m_targetVariables;
        // Skip events with negative weights...
        bool m_DoNegative;
        // Do we have Object classification (i.e. DiTau) or event
        bool m_doObjTraining;
        // Do we have regression problem or a classification problem
        bool m_doClassification;

        std::vector<MVACacheHandler_Ptr> m_SampleCaches;
        MVACacheHandler_Ptr m_GeneralCache;
        MVATrainingService *m_trainService;
    };
}  // namespace XAMPP

#endif
