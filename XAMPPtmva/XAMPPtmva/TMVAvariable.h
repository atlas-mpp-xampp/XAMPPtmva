#ifndef XAMPPtmva_TMVAvariable_H
#define XAMPPtmva_TMVAvariable_H

#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPtmva/MVAvariable.h>
#include <memory>
#include <string>

namespace TMVA {
    class Reader;
    class IMethod;

}  // namespace TMVA

namespace XAMPP {
    class Weight;
    class MVALogicalTrainTestSample;
    // This class constitutes the common interface between XAMPPlotting and the TMVAReader.
    // It manages the TMVA::Reader and all variables needed for a proper evaluation of the
    // obtained MVA training on MC/Data. Once per event the NewEvent method checks if the
    // variables could be successfully parsed from the ITreeVarReader -> TMVA::Reader
    class TMVAReaderHelper : public MVATrainingReader {
    public:
        std::shared_ptr<TMVA::Reader> GetTMVAReader() const;
        std::shared_ptr<TMVA::IMethod> GetMethod(const std::string& method_name) const;

        virtual bool loadMethods();
        TMVAReaderHelper(const std::string& Name, const std::string& XMLPath, MVATrainingReader::TrainingType T);
        virtual ~TMVAReaderHelper();

    protected:
        virtual std::shared_ptr<MVAParserInterface> newParserInterface(const std::string& var_name, ITreeVarReader* tree_reader);
        virtual bool parse_toML();

    private:
        TMVAReaderHelper(const TMVAReaderHelper&) = delete;
        void operator=(const TMVAReaderHelper&) = delete;
        std::shared_ptr<TMVA::Reader> m_Reader;
        std::vector<std::shared_ptr<TMVA::IMethod>> m_tmva_methods;
    };

    class TMVAParserInterface : public MVAParserInterface {
    public:
        TMVAParserInterface(std::shared_ptr<TMVA::Reader> TMVAReader, const std::string& Name, ITreeVarReader* InputReader);
        TMVAParserInterface(const TMVAParserInterface&) = delete;
        void operator=(const TMVAParserInterface&) = delete;

        bool update(size_t target = 0) override;

    private:
        std::shared_ptr<TMVA::Reader> m_tmva_reader;
    };

    class TMVAEventMethodScore : public IScalarReader {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double read() const;
        virtual bool update();
        virtual ~TMVAEventMethodScore();
        static TMVAEventMethodScore* GetReader(const std::string& Method, const std::string& ClassName = "");

    protected:
        TMVAEventMethodScore(const std::string& Method, const std::string& ClassName = "");
        TMVAEventMethodScore(const TMVAEventMethodScore&) = delete;
        void operator=(const TMVAEventMethodScore&) = delete;

        std::string m_method_name;
        TMVAReaderHelper* m_helper;
        TMVA::IMethod* m_Method;
        bool m_Registered;
        mutable unsigned int m_EventNumber;

        mutable double m_Cache;
    };
    class TMVAObjectMethodScore : public IVectorReader {
    public:
        virtual std::string name() const;
        virtual bool init(TTree* t);
        virtual double readEntry(const size_t I) const;
        virtual size_t entries() const;

        static ITreeVectorReader* GetReader(const std::string& Method, const std::string& ClassName);

    private:
        TMVAObjectMethodScore(const std::string& Method, const std::string& ClassName = "");
        TMVAObjectMethodScore(const TMVAObjectMethodScore&) = delete;
        void operator=(const TMVAObjectMethodScore&) = delete;

        std::string m_method_name;
        TMVAReaderHelper* m_helper;
        TMVA::IMethod* m_Method;

        bool m_Registered;
    };
}  // namespace XAMPP
#endif
