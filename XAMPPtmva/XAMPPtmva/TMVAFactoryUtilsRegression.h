#ifndef XAMPPtmva_TMVAFactoryUtilsRegression_H
#define XAMPPtmva_TMVAFactoryUtilsRegression_H

#include <XAMPPtmva/TMVAFactoryUtils.h>

#include <TMVA/DataLoader.h>
#include <TMVA/Factory.h>

#include <memory>
#include <string>
#include <vector>

namespace XAMPP {
    class TMVARegressionCacheProvider : public TMVACacheProvider {
    public:
        static TMVACacheProvider* getProvider();
        virtual ~TMVARegressionCacheProvider();
        virtual bool addTargetVariable(ITreeVarReader* Reader, const std::string& varName);

    protected:
        TMVARegressionCacheProvider();
        TMVARegressionCacheProvider(const TMVARegressionCacheProvider&) = delete;
        void operator=(const TMVARegressionCacheProvider&) = delete;

        //       virtual MVACacheHandler_Ptr createHandler(MVALogicalSmp_Ptr Smp, unsigned int TrainSize,
        //                                                 unsigned int ValidSize,
        //                                                 bool isEvHandler = true) const;
    };

}  // namespace XAMPP
#endif
