#ifndef XAMPPtmva_MetaDataTreeReader_H
#define XAMPPtmva_MetaDataTreeReader_H

#include <TFile.h>
#include <TTree.h>
#include <XAMPPplotting/HistFitterMetaReader.h>

namespace XAMPP {
    class TMVANormDataBase : public HistFitterNormDataBase {
    public:
        static NormalizationDataBase *getDataBase();
        virtual ~TMVANormDataBase();

    protected:
        TMVANormDataBase();
        TMVANormDataBase(const TMVANormDataBase &) = delete;
        void operator=(const TMVANormDataBase &) = delete;
    };
    // Sometimes people want to pass data and mc within a single job. This is insane
    // But let's make it possible
    class TMVAHybridNormDataBase : public HybridNormDataBase {
    public:
        static NormalizationDataBase *getDataBase();
        virtual ~TMVAHybridNormDataBase();

    protected:
        TMVAHybridNormDataBase();
        TMVAHybridNormDataBase(const TMVAHybridNormDataBase &) = delete;
        void operator=(const TMVAHybridNormDataBase &) = delete;
    };
}  // namespace XAMPP
#endif
