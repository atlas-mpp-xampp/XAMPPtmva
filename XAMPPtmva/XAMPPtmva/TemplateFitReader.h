#ifndef XAMPPtmva_TemplatefitReader_H
#define XAMPPtmva_TemplatefitReader_H

#include <TRandom3.h>
#include <XAMPPplotting/DataDrivenWeights.h>
#include <XAMPPplotting/TreeVarReader.h>
namespace XAMPP {

    // The chi2 histo slicer calculates a fictional chi2 from a histogram
    // plotting a known input variable against an unknown variable. In each event
    // the corresponding slice of the unknown variable is choosen depending on the
    // value of the known input variable. A random number generator draws a uniform
    // random number ranging between 0 and the maximum of the distribution's slice
    // once per event. This random number is then interpreted as the true probability
    // value....

    class Chi2HistoLikeliHood {
    public:
        Chi2HistoLikeliHood();

        bool LoadHistogramFromFile(std::stringstream& sstr);
        bool LoadHistogramFromFile(const std::string& file, const std::string& histo_path);

        ITreeVarReader* reader() const;
        TAxis* axis() const;

        int find_bin(double assumption) const;
        bool set_reader(ITreeVarReader* r);

        bool is_valid() const;

        std::shared_ptr<TH1> log_likelihood();
        std::shared_ptr<TH1> likelihood();

    private:
        void pick_slice();
        void construct_slices();

        TH1LookUp m_TH1;
        ITreeVarReader* m_variable;
        EventService* m_service;

        std::map<int, std::shared_ptr<TH1>> m_slices;
        std::map<int, std::shared_ptr<TH1>> m_log_slices;

        Long64_t m_current_event;
        std::shared_ptr<TH1> m_current_slice;
        std::shared_ptr<TH1> m_current_log_slice;

        int m_current_bin;
    };

    class TemplateFitReader : public IScalarReader {
    public:
        virtual std::string name() const;
        virtual double read() const;
        virtual bool init(TTree* t);

        bool setupTemplates(std::ifstream& inf);

        static TemplateFitReader* GetReader(const std::string& r);

    private:
        TemplateFitReader(const std::string& r_name);
        void operator=(const TemplateFitReader&) = delete;
        TemplateFitReader(const TemplateFitReader&) = delete;

        std::string m_name;
        bool m_Registered;
        std::vector<std::shared_ptr<Chi2HistoLikeliHood>> m_templates;

        EventService* m_service;
        mutable Long64_t m_last_event;
        mutable double m_cache;
        unsigned int m_n_iter;
        std::unique_ptr<TRandom> m_rnd_generator;
    };

}  // namespace XAMPP
#endif
