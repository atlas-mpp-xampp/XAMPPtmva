#ifndef XAMPPtmva_FactoryUtils_H
#define XAMPPtmva_FactoryUtils_H

#include <memory>
#include <string>
#include <vector>

class TFile;

namespace XAMPP {
    enum EventType {
        Training = 1 << 1,
        Testing = 1 << 2,
        Background = 1 << 3,
        Signal = 1 << 4,
        Classification = 1 << 5,
        Regression = 1 << 6,
        Target = 1 << 7,
        Total = Background | Signal | Training | Testing | Classification | Regression | Target,

    };
    enum CachingStatus { NotResponsible, Done, Failure };

    class Weight;
    class Condition;
    class ITreeVarReader;
    class IParticleReader;

    class MVATrainTestSplitting;

    class MVACacheHandler;
    typedef std::shared_ptr<MVACacheHandler> MVACacheHandler_Ptr;

    class MVAEventCounter;
    typedef std::shared_ptr<MVAEventCounter> EventCounterPtr;

    class MVALogicalTrainTestSample;
    typedef std::shared_ptr<MVALogicalTrainTestSample> MVALogicalSmp_Ptr;

    struct MVAFeature {
        MVAFeature(ITreeVarReader* R, const std::string& var_name, const std::string& var_label) {
            reader = R;
            name = var_name;
            label = var_label;
        };
        ITreeVarReader* reader;
        std::string name;
        std::string label;
    };

    // Common class to keep track how many events of a given DSID ended up in
    // signal/background - training/testing -> needed for summary later
    class MVAEventCounter {
    public:
        MVAEventCounter(unsigned int dsid);

        unsigned int DSID() const;
        long long unsigned nSignalTrain() const;
        long long unsigned nBackgroundTrain() const;

        long long unsigned nSignalTest() const;
        long long unsigned nBackgroundTest() const;
        bool AddEvent(unsigned int T);

        void LimitTestingEvents(long long unsigned Limit);
        void LimitTrainingEvents(long long unsigned Limit);
        void Reset();

        void LockSignal(bool B = true);
        void LockBackground(bool B = true);

        // Returns true if no limit on training or testing events is given
        // or if the limit is not yet reached
        bool hasCapacity() const;

    private:
        unsigned int m_DSID;
        // Count how many events per sample
        long long unsigned m_NTrainSignalEvents;
        long long unsigned m_NTrainBkgEvents;

        long long unsigned m_NTestSignalEvents;
        long long unsigned m_NTestBkgEvents;

        // Limit the maximum number testing and training events
        bool m_LimitTesting;
        bool m_LimitTraining;
        long long unsigned m_LimitTrainEvents;
        long long unsigned m_LimitTestEvents;

        // Remove one type of events from the classification
        // It does not make sense to constrain a subsplit of events
        // individually.
        bool m_LockSignalEvents;
        bool m_LockBkgEvents;

        // Keep track of messages. If the class has
        // prompted the message once then it is enough
        bool m_msgLockSignal;
        bool m_msgLockBkg;
        bool m_msgLimitTest;
        bool m_msgLimitTrain;
    };

    // Class to build up a logical combination of training and test samples
    // The user can assign a DSID pair for training and testing. This might be useful
    // If the MVA method shall be trained and tested on two different samples. E.g.
    // PowHeg ttbar vs Sherpa ttbar
    class MVALogicalTrainTestSample {
    public:
        MVALogicalTrainTestSample(const std::string& Name);
        ~MVALogicalTrainTestSample();

        std::string name() const;

        bool AddTrainSample(int DSID);
        bool AddTestSample(int DSID);

        bool AddTrainSample(const std::vector<int>& Smp);
        bool AddTestSample(const std::vector<int>& Smp);

        void AddTrainingEvent();
        void AddTestingEvent();

        bool IsTrainDSID(int DSID) const;
        bool IsTestDSID(int DSID) const;

        const std::vector<int>& TrainDSIDs() const;
        const std::vector<int>& TestDSIDs() const;

        bool Copy(const MVALogicalTrainTestSample& ToCopy);

        void LimitTestingEvents(long long unsigned Limit);
        void LimitTrainingEvents(long long unsigned Limit);

    private:
        void LockSamples(std::vector<int>& dsids);
        std::string m_Name;
        std::vector<int> m_Train;
        std::vector<int> m_Test;

        // Limit the maximum number testing and training events
        bool m_LimitTesting;
        bool m_LimitTraining;
        long long unsigned m_LimitTrainEvents;
        long long unsigned m_LimitTestEvents;
        // How many events have been seen
        long long unsigned m_NTrainEvents;
        long long unsigned m_NTestEvents;
    };

    // This class handles the splitting of the events into training and background
    // during the training.
    class MVATrainingService {
    public:
        enum SplitMode { Undefined = -1, Alternate = 0, Block, Random, Fraction };
        // Checks whether the current event is a training or validation event
        // The event counter is incremented if the function is called, so the
        // return value musy be cached in the calling method
        bool isTrainingEvent(unsigned int T);
        bool isTrainingBkgEvent();
        bool isTrainingSignalEvent();

        // Checks whether the event is signal or not
        // Counter is kept constant at each call
        bool isSignalEvent() const;
        bool isSignalDSID(int DSID) const;

        // Set the DSIDs of the signal samples in the MVA taining
        void SetSignals(const std::vector<int>& DSIDs);
        // Alternatively, you can pass cuts on the event topology to declare an event as signal
        void SetEventSignalRequirements(const std::vector<std::shared_ptr<Condition>>& SignalRequirement);
        bool HasSignalDefinition() const;

        // In a general MVA training the events are split into training and testing. XAMPPtmva provides various
        // ways to assign this splitting
        //  1) Alternate: T V T V T V T V T V
        //  2) Block: V V V V V V V T T T T T T T
        //          The sum of N_{V} + N_{T} is cached in SplitBlockSize() / SetSplitBlockSize()
        //          while N_{T} is defined via Splitting()/ SetTrainingFraction()
        bool SetSplitMode(MVATrainingService::SplitMode Mode);
        bool SetSplitMode(const std::string& Mode);

        bool SetSplitBlockSize(unsigned int Size);
        bool SetTrainingFraction(unsigned int Split);

        unsigned int SplitBlockSize() const;
        unsigned int Splitting() const;
        // Swap training with testing
        void InvertSplitting(bool B);
        bool InvertedSplitting() const;

        // Reset the event counters of all samples
        void Reset();

        // This method is the analgous to the analysis regions
        // in SciKit learn the topology name is interpreted as a region label
        std::string topologyLabel() const;
        void setTopologyLabel(const std::string& name);

        static MVATrainingService* getService();

        virtual ~MVATrainingService();

        // Retrieves the Training/Testing event counter of the current event
        EventCounterPtr getCounter() const;
        // Retrieves a specific event counter
        EventCounterPtr getCounter(unsigned int dsid) const;

        void PromptSampleStats() const;
        // Retrieve the dataset ID of the current event
        unsigned int DSID() const;

        // Searches for a logical Training/Testing sample like ttbar, background-jets, etc.
        // The names are defined by the users
        MVALogicalSmp_Ptr getLogicalSample(const std::string& smp_name);

        // Consistency check of all training-validation samples at the beginning. The check fails if a
        // DSID is either assigned in two logical training samples, assigned as training and testing at the same time,
        // or logical samples lack of training or testing dsids
        bool TainSamplesConsistent() const;

        std::vector<MVALogicalSmp_Ptr> getAllSamples() const;

    protected:
        MVATrainingService();
        MVATrainingService(const MVATrainingService&) = delete;
        void operator=(const MVATrainingService&) = delete;
        static MVATrainingService* m_Inst;

    private:
        unsigned int GetUsedEvents(unsigned int T) const;

        // Methods to form the summary in the end of the day
        std::string FormStrIntoTable(const std::string& str_toprint, unsigned int columnsize) const;
        std::string GetWhiteSpaces(const std::string& str_toprint, size_t maxsize, bool Front = true) const;
        void maxLength(const std::string& str, unsigned int& Length) const;

        std::string m_topology;
        std::vector<MVALogicalSmp_Ptr> m_LogicTrainSmp;
        Weight* m_Weighter;

        // Splitting of the events into training and validation
        // Has to assign a value between 1- 100
        // The first portion of each 100 events is assigned as training the other as validation
        bool m_InvertSplitting;
        unsigned int m_SplitBase;
        unsigned int m_SplitBlock;

        // Events used for signal and background in total
        unsigned int m_NumEvSignal;
        unsigned int m_NumEvBkg;
        enum SplitMode m_Mode;
        // Signal requirements

        // The vector of Signal DSIDs to discriminate background/
        // Any signal DSID has to be on this list..
        std::vector<int> m_Signals;
        std::shared_ptr<Condition> m_IsSignalEvent;

        mutable std::vector<EventCounterPtr> m_UsedSamples;
        mutable EventCounterPtr m_currentSample;
        // What was the last event when the regression method has been called
        unsigned int m_LastEvent;
    };
    //
    //
    //
    class MVACacheProvider {
    public:
        static MVACacheProvider* getProvider();
        virtual ~MVACacheProvider();

        MVACacheHandler_Ptr GeneralEventCache(unsigned int TrainSize, unsigned int TestSize) const;
        MVACacheHandler_Ptr GeneralObjectCache(unsigned int TrainSize, unsigned int TestSize) const;

        std::vector<MVACacheHandler_Ptr> createEventCaches(unsigned int TrainSize, unsigned int TestSize) const;
        std::vector<MVACacheHandler_Ptr> createObjectCaches(unsigned int TrainSize, unsigned int TestSize) const;

        bool addTrainingVariable(ITreeVarReader* Reader, const std::string& varName, const std::string& label = "");
        bool addTargetVariable(ITreeVarReader* Reader, const std::string& varName, const std::string& label = "");

        virtual bool setupFactory();

        virtual bool prepareAndExecuteTraining();
        virtual void setOutFile(std::shared_ptr<TFile> file);
        virtual void setOutFileName(const std::string& fileName);
        virtual void setOutDirectory(const std::string& outDir);

        bool bookMethods(const std::vector<std::string>& Methods);

        std::vector<ITreeVarReader*> getTrainingVariables() const;
        std::vector<ITreeVarReader*> getTargetVariables() const;

        const std::vector<MVAFeature>& getTrainFeatures() const;
        const std::vector<MVAFeature>& getTargetFeatures() const;

        const std::vector<std::string>& getBookedMethods() const;
        bool bookMethod(const std::string& Method);
        void clearBookedMethods();

        void setTrainingObject(IParticleReader* particle);
        IParticleReader* getTrainingObject() const;

    protected:
        std::vector<MVACacheHandler_Ptr> createCaches(unsigned int TrainSize, unsigned int TestSize, bool isEvHandler = true) const;
        virtual MVACacheHandler_Ptr createHandler(MVALogicalSmp_Ptr LogicalSmp, unsigned int TrainSize, unsigned int TestSize,
                                                  bool isEvHandler = true) const;

        virtual bool IsMethodKnown(const std::string& method) const;

        static MVACacheProvider* m_Inst;
        MVACacheProvider();
        MVACacheProvider(const MVACacheProvider&) = delete;
        void operator=(const MVACacheProvider&) = delete;

    private:
        std::vector<MVAFeature> m_TrainingVariables;
        std::vector<MVAFeature> m_TargetVariables;

        std::vector<std::string> m_BookedMethods;
        IParticleReader* m_trainParticle;
    };
    //
    // For machine learning the data must be parsed from the XAMPP ntuples to the machine learning
    // ToolKits. In all cases, the events are transformed to an intermediate file-format containing only information
    // relevant for the training and which is useable by the algorithm of course. Since events are split into training
    // and testing as well as they are grouped into blocks. They must be buffered first before they are written to the new
    // file format. The MVABuffer class provides the basic interface to do this buffering.
    class MVABuffer {
    public:
        // Basic constuctor of the class telling which event type is buffered (Signal/Background - training/testing)  and
        // the size of the buffer before it is dumped to the intermediate file format.
        MVABuffer(unsigned int T, unsigned int Size);
        // Cache the information of the event from the ITreeVarReaders of XAMPPplotting
        virtual bool CacheEvent(const std::vector<ITreeVarReader*>& Readers);
        // Is the Cache/Buffer full. I.e. the size argument exceeded
        bool IsCacheFull() const;
        // Write everything to the intermediate file-format.
        bool FillFactory(unsigned int max_elements = -1);
        // How many events have been buffered in total?
        long long unsigned int Events() const;
        // How many events are currently in the buffer
        size_t currently_buffered() const;

        void Clear();
        virtual ~MVABuffer();

    protected:
        typedef std::vector<double> Cache;
        typedef std::vector<Cache> CacheVector;
        // Create a new empty space in the cache vector to buffer the event with n different
        // input variables.
        virtual CacheVector::iterator NewEvent(unsigned int n_vars);
        // Maximum cache size
        unsigned int CacheSize() const;
        // Parse the filled event to the final buffer
        virtual bool FillEvent(const Cache& Event, const double& Weight) = 0;
        // Extract the information from the ITreeVarReader to the event-cache.
        // Check if the numbers are finite and the ITreeVarReader is well behaved.
        virtual bool PushBack(ITreeVarReader* Reader, Cache& Vec) = 0;
        // What is the classification type of the current buffer
        unsigned int type() const;

    private:
        unsigned int m_Type;
        unsigned int m_CacheSize;

        Weight* m_weight;
        CacheVector m_CachedEvents;
        Cache m_CachedWeights;
        long long unsigned int m_NumEvents;
    };

    // The MVATrainTestSplitting class is the first complex building block after the MVABuffer. It
    // manages the grouping of training and testing events of a certain classification type.
    class MVATrainTestSplitting {
    public:
        MVATrainTestSplitting(unsigned int Type, MVACacheHandler* Handler);
        virtual bool init();
        CachingStatus CacheEvent(const std::vector<ITreeVarReader*>& Readers);
        CachingStatus CacheEvent(const std::vector<ITreeVarReader*>& train_vars, const std::vector<ITreeVarReader*>& target_vars);

        bool DrainBuffers();
        virtual ~MVATrainTestSplitting();

        long long unsigned int TrainingEvents() const;
        long long unsigned int TestingEvents() const;
        unsigned int type() const;

    protected:
        virtual std::shared_ptr<MVABuffer> CreateCache(unsigned int T, unsigned int Size) = 0;

        EventType assign_cache();

        unsigned int m_Type;
        MVACacheHandler* m_Handler;
        MVATrainingService* m_Service;

        std::shared_ptr<MVABuffer> m_TrainingCache;
        std::shared_ptr<MVABuffer> m_TestCache;
    };

    class MVACacheHandler {
    public:
        MVACacheHandler(MVALogicalSmp_Ptr sample, unsigned int TrainSize, unsigned int TestSize);
        virtual ~MVACacheHandler();
        virtual bool init();
        bool isGeneralCache() const;
        std::string name() const;

        virtual CachingStatus CacheEvent(const std::vector<ITreeVarReader*>& classificationvars);
        virtual CachingStatus CacheEvent(const std::vector<ITreeVarReader*>& train_vars, const std::vector<ITreeVarReader*>& target_vars);

        long long unsigned int BkgTrainEvents() const;
        long long unsigned int BkgTestEvents() const;

        long long unsigned int SigTrainEvents() const;
        long long unsigned int SigTestEvents() const;

        typedef long long unsigned int (MVACacheHandler::*Events)() const;

        bool DrainBuffers();

        bool CheckTraining() const;

        unsigned int TrainSize() const;
        unsigned int TestSize() const;

        MVALogicalSmp_Ptr logicalSample() const;

        /// The target entry is of importances
        unsigned int TargetEntry() const;
        bool setTargetEntry(unsigned int target);

    protected:
        bool isResponsible() const;
        virtual std::shared_ptr<MVATrainTestSplitting> CreateCache(unsigned int Type) = 0;

    private:
        MVALogicalSmp_Ptr m_Sample;
        unsigned int m_TrainSize;
        unsigned int m_TestSize;
        MVATrainingService* m_Service;
        unsigned int m_targetEntry;
        IParticleReader* m_trainParticle;

    protected:
        std::shared_ptr<MVATrainTestSplitting> m_SignalCache;
        std::shared_ptr<MVATrainTestSplitting> m_BackgroundCache;
    };

}  // namespace XAMPP
#endif
