#ifndef XAMPPtmva_MVAvariable_H
#define XAMPPtmva_MVAvariable_H

#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/Weight.h>
#include <memory>
#include <string>

namespace XAMPP {
    typedef std::pair<std::string, std::string> StringPair;
    enum MachineFrameWork { TMVA = 1, SciKitLearn = 2 };
    // Basic class to establish the connection between the ITreeVarReaders from XAMPPplotting &
    // the machine-learning classifier's to label unknown events
    class MVAParserInterface {
    public:
        MVAParserInterface(const std::string& variable_name, ITreeVarReader* reader);
        MVAParserInterface(const MVAParserInterface&) = delete;
        void operator=(const MVAParserInterface&) = delete;
        virtual ~MVAParserInterface() = default;

        virtual size_t Size() const;

        std::string name() const;
        ITreeVarReader* variable() const;

        virtual bool update(size_t target = 0);
        const float* getCache() const;
        // TMVA is retarded
        float* getCache();

    private:
        std::string m_name;
        ITreeVarReader* m_reader;
        float m_variable;
    };
    //  The MVA training reader loads the final training from the disk & ensures that, the given variable names are consistent with
    //  the ones used in the training session, the current values of the variables are updated accordingly to have a proper classification
    //  of the event or of the particle.
    class MVATrainingReader {
    public:
        enum TrainingType { EventClass, ObjectClass, EventRegression, ObjectRegression };
        MVATrainingReader(const std::string& training, const std::string& file_path, MachineFrameWork M,
                          MVATrainingReader::TrainingType T = MVATrainingReader::TrainingType::EventClass);
        MVATrainingReader(const MVATrainingReader&) = delete;
        void operator=(const MVATrainingReader&) = delete;

        virtual ~MVATrainingReader();
        // Name of the class of trainings
        std::string name() const;
        // In which directory are the trainings located?
        std::string getFilePath() const;
        // Connect the ITreeVarReader with a training-variable.
        bool addVariable(const std::string& var_name, ITreeVarReader* reader);
        // For particle ID we know that the IParticleReader contains all needed information.
        // we can parse it together with a list of training variables matching the proper branches from
        // the tree.
        bool addVariable(const std::vector<StringPair>& train_var_name, ParticleReader* particle);
        // Has the algorithm variables at all?
        bool hasVariables() const;
        // a new event has been loaded or we want to cache the information of another particle in the
        // event.
        virtual bool newEvent(size_t target = 0);

        void BookMethod(const std::string& method);
        bool IsBooked(const std::string& Method) const;
        void clearBookedMethods();

        // Book a specific MVA method from the training (i.e. BDTD, BDTB). The method must be trained in the training session.
        // If the files cannot be loaded an error is thrown.
        virtual bool loadMethods() = 0;

        MachineFrameWork framework() const;
        MVATrainingReader::TrainingType train_type() const;
        bool isEventClassifier() const;
        bool isObjectClassifier() const;
        bool isEventRegression() const;
        bool isObjectRegression() const;

        bool isClassifier() const;
        bool isRegressor() const;
        bool isEventBased() const;
        bool isObjectBased() const;

        std::vector<std::string> trainVarNames() const;

        size_t Size() const;

    protected:
        virtual std::shared_ptr<MVAParserInterface> newParserInterface(const std::string& var_name, ITreeVarReader* tree_reader) = 0;
        virtual bool parse_toML();

        const std::vector<std::shared_ptr<MVAParserInterface>>& getInterfaces() const;
        const std::vector<std::string>& getBookedMethods() const;

    private:
        bool isVariableUsed(const std::string& Name, ITreeVarReader* R) const;

        std::string m_training;
        std::string m_path;
        MachineFrameWork m_framework;
        TrainingType m_classType;
        std::vector<std::shared_ptr<MVAParserInterface>> m_interfaces;
        std::vector<std::string> m_methods_to_book;
        // caching
        unsigned int m_event_number;
        EventService* m_eventService;
        size_t m_target;
    };

}  // namespace XAMPP
#endif
