#ifndef XAMPPtmva_SciKitFactoryUtilsRegression_H
#define XAMPPtmva_SciKitFactoryUtilsRegression_H

#include <XAMPPtmva/SciKitFactoryUtils.h>
namespace XAMPP {

    //#############################################################
    //          General interface to the SciKiLearnFactory
    //############################################################
    class SciKitRegressionAuxillaryBuffer;
    typedef std::shared_ptr<SciKitRegressionAuxillaryBuffer> SciKitRegressionAuxillaryBuffer_Ptr;
    class SciKitRegressionBuffer : public MVABuffer {
    public:
        SciKitRegressionBuffer(SciKitRegressionAuxillaryBuffer_Ptr Aux, unsigned int T, unsigned int Size);
        virtual ~SciKitRegressionBuffer();

    protected:
        virtual bool FillEvent(const Cache& Event, const double& Weight);

    private:
        SciKitRegressionAuxillaryBuffer_Ptr m_auxBuffer;
    };
    class SciKitRegressionAuxillaryBuffer {
    public:
        SciKitRegressionAuxillaryBuffer();
        bool BufferTraining(const std::vector<double>& train, const double weight);
        bool BufferTarget(const std::vector<double>& target);
        bool ParseToFactory(unsigned int event_type);

    private:
        std::vector<double> m_train;
        std::vector<double> m_target;
        double m_weight;
        SciKitRegressionFactory* m_factory;
    };
    class SciKitRegressionEventBuffer : public SciKitRegressionBuffer {
    public:
        SciKitRegressionEventBuffer(SciKitRegressionAuxillaryBuffer_Ptr Aux, unsigned int T, unsigned int Size);

    protected:
        virtual bool PushBack(ITreeVarReader* Reader, Cache& Vec);
    };

    class SciKitRegressionBufferHandler : public MVATrainTestSplitting {
    public:
        SciKitRegressionBufferHandler(unsigned int Type, MVACacheHandler* Handler);
        virtual bool init();

    protected:
        virtual std::shared_ptr<MVABuffer> CreateCache(unsigned int T, unsigned int Size);

    private:
        SciKitRegressionAuxillaryBuffer_Ptr m_auxBuffer;
        MVACacheHandler* m_Handler;
    };

    class ScikitRegressionCacheHandler : public MVACacheHandler {
    public:
        ScikitRegressionCacheHandler(MVALogicalSmp_Ptr sample, unsigned int TrainSize, unsigned int ValidSize);
        virtual bool init();

    protected:
        virtual std::shared_ptr<MVATrainTestSplitting> CreateCache(unsigned int Type);
    };

    class SciKitRegressionCacheProvider : public SciKitCacheProvider {
    public:
        static SciKitRegressionCacheProvider* getProvider();
        virtual ~SciKitRegressionCacheProvider();

        SciKitRegressionFactory* regressionFactory() const;

        bool setupFactory() override;

    protected:
        SciKitRegressionCacheProvider();
        SciKitRegressionCacheProvider(const SciKitRegressionCacheProvider&) = delete;
        void operator=(const SciKitRegressionCacheProvider&) = delete;
        MVACacheHandler_Ptr createHandler(MVALogicalSmp_Ptr Smp, unsigned int TrainSize, unsigned int ValidSize,
                                          bool isEvHandler = true) const override;
    };

}  // namespace XAMPP
#endif
