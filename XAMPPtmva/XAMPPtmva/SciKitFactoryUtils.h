#ifndef XAMPPtmva_SciKitFactoryUtils_H
#define XAMPPtmva_SciKitFactoryUtils_H

#include <XAMPPtmva/FactoryUtils.h>
#include <XAMPPtmva/SciKitLearnFactory.h>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace XAMPP {

    //#############################################################
    //          General interface to the SciKiLearnFactory
    //############################################################
    class SciKitLearnCache : public MVABuffer {
    public:
        SciKitLearnCache(unsigned int T, unsigned int Size);
        virtual ~SciKitLearnCache();

    protected:
        virtual bool FillEvent(const Cache& Event, const double& Weight);

    private:
        std::shared_ptr<SciKitLearnFactory> m_Factory;
    };
    //###########################################################################################################
    //     SciKitLearn is a python based machine learning framework. The SciKitLearnFactory provided by         #
    //     XAMPPtmva is basically nothing else than a HDF5 file-maker. In order to train the machine learning   #
    //     algorithm the user needs to write an python script interfacing the HDF5 file to the algorithm and    #
    //     file it in the 'XAMPPtmva/python/SciKitLearn directory. The Script shall use the `SciKitClassifier`  #
    //     and `SciKitDataSet` classes to interface the machine learning algorithm.                             #
    //            (C.f. XAMPPtmva/python/SciKitLearn/BDT.py) as a working example.                              #
    //     Now, if the user wants to perform a scan over training setting parameters, test different machine    #
    //     learning classifiers, it might be a bit laborious, if he needs to call the scripts by hand from      #
    //     the command-line. There is a lack of documentation what parameter setting was used, too. To mitigate #
    //     this drawback XAMPPtmva provides the service to define SciKitLearn training methods in a similar     #
    //     way to the TMVA methods. The SciKitLearnMethod class saves the name of the classifier script to use  #
    //     together with a specific class-name and the considered options of the training. If XAMPPtmva runs    #
    //     successful a text file is written.                                                                   #
    //###########################################################################################################

    class SciKitLearnMethod {
    public:
        SciKitLearnMethod(const std::string& to_execute, const std::string& className, const std::vector<std::string>& options);
        std::string name() const;
        std::string script() const;
        void write(std::ofstream& stream);
        void writeIOArgs(std::ofstream& stream, bool swapped = false);

    private:
        void writeCmd(std::ofstream& stream, bool swapped = false);

        std::string m_pyscript;
        std::string m_name;
        std::vector<std::string> m_options;
    };
    typedef std::shared_ptr<SciKitLearnMethod> SciKitLearnMethod_Ptr;

    class SciKitCacheProvider : public MVACacheProvider {
    public:
        static SciKitCacheProvider* getProvider();
        virtual ~SciKitCacheProvider();

        std::shared_ptr<SciKitLearnFactory> getFactory() const;
        void setOutDirectory(const std::string& outDir) override;
        void setOutFileName(const std::string& filename) override;
        bool prepareAndExecuteTraining() override;

        bool setupFactory() override;
        // Returns a vecor of methods exist and are actually booked
        bool addTrainingMethod(const std::string& py_script, const std::string& name, const std::vector<std::string>& options);

    protected:
        SciKitCacheProvider();
        SciKitCacheProvider(const SciKitCacheProvider&) = delete;
        void operator=(const SciKitCacheProvider&) = delete;

        MVACacheHandler_Ptr createHandler(MVALogicalSmp_Ptr Smp, unsigned int TrainSize, unsigned int ValidSize,
                                          bool isEvHandler = true) const override;
        bool IsMethodKnown(const std::string& method) const override;

    protected:
        std::shared_ptr<SciKitLearnFactory> m_SciKitFactory;

    private:
        std::vector<SciKitLearnMethod_Ptr> m_trainMethods;
    };

    class SciKitLearnEventCache : public SciKitLearnCache {
    public:
        SciKitLearnEventCache(unsigned int T, unsigned int Size);
        virtual ~SciKitLearnEventCache();

    protected:
        virtual bool PushBack(ITreeVarReader* Reader, Cache& Vec);
    };

    class SciKitLearnEventClassification : public MVACacheHandler {
    public:
        SciKitLearnEventClassification(MVALogicalSmp_Ptr Sample, unsigned int TrainSize = 1, unsigned int ValidSize = 1);
        virtual ~SciKitLearnEventClassification();

    protected:
        virtual std::shared_ptr<MVATrainTestSplitting> CreateCache(unsigned int Type);
    };

    class SciKitLearnEventTrainTestHandler : public MVATrainTestSplitting {
    public:
        SciKitLearnEventTrainTestHandler(unsigned int Type, MVACacheHandler* Handler);
        ~SciKitLearnEventTrainTestHandler();

    protected:
        virtual std::shared_ptr<MVABuffer> CreateCache(unsigned int T, unsigned int Size);
    };

    class SciKitLearnObjectClassification : public MVACacheHandler {
    public:
        SciKitLearnObjectClassification(MVALogicalSmp_Ptr Sample, unsigned int TrainSize = 1, unsigned int ValidSize = 1);
        virtual ~SciKitLearnObjectClassification();
        using MVACacheHandler::CacheEvent;
        virtual CachingStatus CacheEvent(const std::vector<ITreeVarReader*>& Readers);

    protected:
        virtual std::shared_ptr<MVATrainTestSplitting> CreateCache(unsigned int Type);
    };
    class SciKitLearnObjectTrainTestHandler : public MVATrainTestSplitting {
    public:
        SciKitLearnObjectTrainTestHandler(unsigned int Type, SciKitLearnObjectClassification* Handler);
        unsigned int TargetEntry() const;
        ~SciKitLearnObjectTrainTestHandler();

    protected:
        virtual std::shared_ptr<MVABuffer> CreateCache(unsigned int T, unsigned int Size);
        SciKitLearnObjectClassification* m_parent;
    };
    class SciKitLearnObjectCache : public SciKitLearnCache {
    public:
        SciKitLearnObjectCache(SciKitLearnObjectTrainTestHandler* Handler, unsigned int T, unsigned int Size);
        virtual ~SciKitLearnObjectCache();

    protected:
        virtual bool PushBack(ITreeVarReader* Reader, Cache& Vec);

    private:
        SciKitLearnObjectTrainTestHandler* m_ObjectHandler;
    };

}  // namespace XAMPP
#endif
