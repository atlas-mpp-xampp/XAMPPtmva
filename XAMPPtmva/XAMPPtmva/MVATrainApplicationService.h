#ifndef XAMPPtmva_MVAAppClassifierService_H
#define XAMPPtmva_MVAAppClassifierService_H

#include <XAMPPtmva/MVAvariable.h>
#include <XAMPPtmva/SciKitMVAvariable.h>
#include <XAMPPtmva/TMVAvariable.h>

namespace XAMPP {
    class MVATrainApplicationService {
    public:
        static MVATrainApplicationService* getService();

        std::shared_ptr<MVATrainingReader> getTraining(const std::string& name, MachineFrameWork M) const;
        TMVAReaderHelper* getTMVAClassifier(const std::string& name) const;

        TMVAReaderHelper* TMVAEventClassifier(const std::string& name, const std::string& xml_path);
        TMVAReaderHelper* TMVAObjectClassifier(const std::string& name, const std::string& xml_path);

        SciKitMVATrainingReader* getSciKitTraining(const std::string& name) const;

        SciKitMVATrainingReader* SciKitEventClassifier(const std::string& name, const std::string& train_path);
        SciKitMVATrainingReader* SciKitObjectClassifier(const std::string& name, const std::string& train_path);
        SciKitMVATrainingReader* SciKitEventRegressor(const std::string& name, const std::string& train_path);

        ~MVATrainApplicationService();

    private:
        static MVATrainApplicationService* m_Inst;
        MVATrainApplicationService();
        MVATrainApplicationService(const MVATrainApplicationService&) = delete;
        void operator=(const MVATrainApplicationService&) = delete;
        std::vector<std::shared_ptr<MVATrainingReader>> m_classifiers;
    };
}  // namespace XAMPP
#endif
