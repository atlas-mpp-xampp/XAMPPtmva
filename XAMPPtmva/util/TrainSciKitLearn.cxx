#include <PathResolver/PathResolver.h>
#include <TSystem.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

int main(int argc, char* argv[]) {
    // Some variables for configuration
    std::string HDF5Cfg = "";
    std::string InputCfg = "";
    std::string outDir = "SciKitLearnFiles/";
    std::string outFile = "output_scikit.root";
    bool BatchMode = false;
    bool Classification = true;
    bool CreateQuickPlots = true;
    unsigned int nThreads = 4;

    // read the config provided by the user
    for (int k = 1; k < argc; ++k) {
        if (std::string(argv[k]) == "--BatchMode") {
            BatchMode = true;
        } else if (std::string(argv[k]) == "--Regression") {
            Classification = false;
        } else if (std::string(argv[k]).find("-t") == 0) {
            HDF5Cfg = argv[k + 1];
            ++k;
        } else if (std::string(argv[k]).find("-i") == 0) {
            InputCfg = argv[k + 1];
            ++k;
        } else if (std::string(argv[k]).find("-d") == 0) {
            outDir = argv[k + 1];
            ++k;
        } else if (std::string(argv[k]).find("-o") == 0) {
            outFile = argv[k + 1];
            ++k;
        } else if (std::string(argv[k]) == "--nThreads") {
            nThreads = atoi(argv[k + 1]);
            ++k;
        } else if (std::string(argv[k]) == "--noPlots") {
            CreateQuickPlots = false;
        } else {
            XAMPP::Error("TrainSciKitLearn()", Form("Invalid argument %s.", argv[k]));
            XAMPP::Info("TrainSciKitLearn()", "Allowed arguments are:");
            XAMPP::Info("TrainSciKitLearn()", "    *** -i  <input_config>");
            XAMPP::Info("TrainSciKitLearn()", "    *** -t <train_cfg>");
            XAMPP::Info("TrainSciKitLearn()", "    *** -d <destination_dir>");
            XAMPP::Info("TrainSciKitLearn()", "    *** -o <out_file name> ");
            XAMPP::Info("TrainSciKitLearn()", "    *** --Regression <the training is meant for regression instead of classification>");
            XAMPP::Info("TrainSciKitLearn()", "    *** --nThreads <how many python threads shall be opened>");
            XAMPP::Info("TrainSciKitLearn()", "    *** --noPlots <Disable the summary plots>");
            return EXIT_FAILURE;
        }
    }
    if (BatchMode) nThreads = 0;
    std::string log_dir = Form("%s/Log_Files", outDir.c_str());

    if (nThreads > 0) { gSystem->mkdir(log_dir.c_str(), true); }
    std::string hdf5_cmd = Form("WriteHDF5 -i %s -t %s -d %s -o %s %s %s %s",
                                InputCfg.c_str(),  // Incfg file
                                HDF5Cfg.c_str(),   // Equivalent to the Train config
                                outDir.c_str(),    // where to store the HDF5
                                outFile.c_str(), std::string(BatchMode ? "--BatchMode" : "").c_str(),
                                std::string(Classification ? "" : "--Regression").c_str(),
                                nThreads > 0 ? Form("2>&1 | tee %s/HDF5_Making.log", log_dir.c_str()) : "");

    if (system(hdf5_cmd.c_str()) != EXIT_SUCCESS) return EXIT_FAILURE;
    if (nThreads > 0) {
        XAMPP::Info("TrainSciKitLearn()", "Calculate the covariance matrices");
        std::this_thread::sleep_for(std::chrono::milliseconds(5000));

        std::string cov_cmd = Form("python %s --HDF5File %s/%s.H5 --outDir %s/ --nAuxillaryThreads %d 2>&1 | tee %s/Covariance_Making.log",
                                   PathResolverFindCalibFile("XAMPPtmva/SciKitLearn/Utils/CalculateCovariance.py").c_str(), outDir.c_str(),
                                   outFile.substr(0, outFile.find(".root")).c_str(), outDir.c_str(), nThreads, log_dir.c_str());

        if (system(cov_cmd.c_str()) != EXIT_SUCCESS) return EXIT_FAILURE;
        XAMPP::Info("TrainSciKitLearn()", "Start the MVA training.");

        std::string cmd_in_file = Form("%s/%s_trainCmds.txt", outDir.c_str(), outFile.substr(0, outFile.rfind(".")).c_str());
        std::ifstream inf(cmd_in_file);
        std::vector<std::string> cmds;

        /// Run a dummy processing on the input data to obtain a first overview of the
        /// input variables correlation matrices, etc..
        std::string first_diag_cmd = Form(
            "python %s  --runVariablesOnly --HDF5File %s/%s.H5 --outDir %s/Diagnostic/ --nAuxillaryThreads %d --dataPreProcessing "
            "%s/Covariance_Matrix.pkl  ",
            PathResolverFindCalibFile("XAMPPtmva/SciKitLearn/Utils/RunDiagnostics.py").c_str(), outDir.c_str(),
            outFile.substr(0, outFile.rfind(".")).c_str(), outDir.c_str(), 2, outDir.c_str());

        /// The processing needs to be piped through the FastPlots script
        std::string first_plot_cmd =
            Form("python %s -i %s/Diagnostic/ --output %s/Plots --nThreads 1",
                 PathResolverFindCalibFile("XAMPPtmva/SciKitPlotting/SciKitFastPlots.py").c_str(), outDir.c_str(), outDir.c_str());

        cmds.push_back(first_diag_cmd + " ;" + first_plot_cmd + " 2>&1 | tee " + log_dir + "/VariableDiagnostics.log");

        if (inf.good()) {
            std::string line;
            while (XAMPP::GetLine(inf, line)) {
                cmds.push_back(line + std::string(" --nAuxillaryThreads 1 --dataPreProcessing ") + outDir +
                               std::string("/Covariance_Matrix.pkl 2>&1 | tee ") + log_dir + Form("/Train_%lu.log", cmds.size()));
            }

            std::vector<std::future<int>> threads;
            threads.reserve(cmds.size());
            XAMPP::Info("TrainSciKitLearn()", "Will execute %lu trainings with %u threads at the same time", cmds.size(), nThreads);
            for (const auto& cmd : cmds) {
                while (XAMPP::count_active(threads) >= nThreads) { std::this_thread::sleep_for(std::chrono::milliseconds(5000)); }
                XAMPP::Info("TrainSciKitLearn()", "Start training %s (%lu/%lu).", cmd.c_str(), threads.size(), cmds.size());
                threads.push_back(std::async(std::launch::async, system, cmd.c_str()));
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
            unsigned int n = XAMPP::count_active(threads);
            while (n > 0) {
                XAMPP::Info("TrainSciKitLearn()", "Wait until the last %u trainings are going to finish", n);
                std::this_thread::sleep_for(std::chrono::milliseconds(5000));
                n = XAMPP::count_active(threads);
            }
            // count how many trainings succeeded.
            n = 0;
            unsigned int i = 0;
            for (auto& th : threads) {
                int code = th.get();
                if (code == 0)
                    ++n;
                else {
                    XAMPP::Warning("TrainSciKitLearn()", "Training %s failed", cmds[i].c_str());
                }
            }
            XAMPP::Info("TrainSciKitLearn()", "Trainings are done. %u/%lu trainings ran successfully", n, threads.size());

            // Execute the quick plots as well
            if (CreateQuickPlots) {
                XAMPP::Info("TrainSciKitLearn()", "Run the quick diagnostic summary");
                std::string quick_plots =
                    Form("python %s --inputPath %s/Trainings/ --output %s/Plots --nThreads %u %s 2>&1 | tee %s/QuickPlots.log",
                         PathResolverFindCalibFile("XAMPPtmva/SciKitPlotting/SciKitFastPlots.py").c_str(), outDir.c_str(), outDir.c_str(),
                         nThreads, Classification ? "" : "--regression", log_dir.c_str());
                system(quick_plots.c_str());
            }
        }
    }
    return EXIT_SUCCESS;
}
