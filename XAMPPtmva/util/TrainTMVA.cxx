#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TStopwatch.h>
#include <TTree.h>
#include "TSystem.h"

#include <cstring>
#include <iostream>
#include <string>

#include <XAMPPtmva/SetupTMVA.h>
#include <XAMPPtmva/TMVAFactoryUtils.h>
#include <XAMPPtmva/TMVAFactoryUtilsRegression.h>

int main(int argc, char* argv[]) {
    // Usage
    // TrainTMVA -i <Path to your input config containing signal and backgrond > -t <Path to your config with the training parameters> -d
    // <Where to store the output> -o <What's the name of the ROOT file> Some variables for configuration
    std::string TMVACfg = "";
    std::string InputCfg = "";
    std::string outDir = "TrainingFiles/";
    std::string outFile = "output_tmva.root";
    bool BatchMode = false;
    bool Classification = true;
    // read the config provided by the user
    for (int k = 1; k < argc; ++k) {
        if (std::string(argv[k]) == "--BatchMode") {
            BatchMode = true;
        } else if (std::string(argv[k]).find("-t") == 0) {
            TMVACfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-i") == 0) {
            InputCfg = argv[k + 1];
        } else if (std::string(argv[k]).find("-d") == 0) {
            outDir = argv[k + 1];
        } else if (std::string(argv[k]).find("-o") == 0) {
            outFile = argv[k + 1];
        } else if (std::string(argv[k]) == "--Regression") {
            Classification = false;
        }
    }
    if (Classification) {
        XAMPP::TMVACacheProvider::getProvider();
    } else {
        XAMPP::TMVARegressionCacheProvider::getProvider();
    }
    XAMPP::SetupTMVA setup;

    if (BatchMode) setup.setBatchMode();
    if (!Classification) setup.RegressionTraining();

    if (!setup.ConfigureTraining(InputCfg, TMVACfg, outDir, outFile)) { return EXIT_FAILURE; }

    return EXIT_SUCCESS;
}
