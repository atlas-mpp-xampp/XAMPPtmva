#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TStopwatch.h>
#include <TTree.h>
#include "TSystem.h"

#include <cstring>
#include <iostream>
#include <string>

#include <XAMPPtmva/SciKitFactoryUtils.h>
#include <XAMPPtmva/SciKitFactoryUtilsRegression.h>
#include <XAMPPtmva/SetupTMVA.h>

int main(int argc, char* argv[]) {
    // Some variables for configuration

    std::string HDF5Config = "";
    std::string InputCfg = "";
    std::string outDir = "SciKitLearnFiles/";
    std::string outFile = "output_scikit.root";
    bool BatchMode = false;
    bool Classification = true;
    // read the config provided by the user
    for (int k = 1; k < argc; ++k) {
        if (std::string(argv[k]) == "--BatchMode") {
            BatchMode = true;
        } else if (std::string(argv[k]) == "--Regression") {
            Classification = false;
        } else if (std::string(argv[k]).find("-t") == 0) {
            HDF5Config = argv[k + 1];
            ++k;
        } else if (std::string(argv[k]).find("-i") == 0) {
            InputCfg = argv[k + 1];
            ++k;
        } else if (std::string(argv[k]).find("-d") == 0) {
            outDir = argv[k + 1];
            ++k;
        } else if (std::string(argv[k]).find("-o") == 0) {
            outFile = argv[k + 1];
            ++k;
        }
    }
    if (Classification)
        XAMPP::SciKitCacheProvider::getProvider();
    else
        XAMPP::SciKitRegressionCacheProvider::getProvider();
    std::unique_ptr<XAMPP::SetupTMVA> setup = std::make_unique<XAMPP::SetupTMVA>();
    if (BatchMode) setup->setBatchMode();
    if (!Classification) setup->RegressionTraining();
    if (!setup->ConfigureTraining(InputCfg, HDF5Config, outDir, outFile)) {
        XAMPP::Error("WriteHDF5", "File making failed");
        system(Form("rm %s/%s", outDir.c_str(), outFile.c_str()));
        system(Form("rm %s/%s.H5", outDir.c_str(), outFile.substr(0, outFile.rfind(".")).c_str()));
        return EXIT_FAILURE;
    }
    setup.reset();
    system(Form("rm %s/%s", outDir.c_str(), outFile.c_str()));
    return EXIT_SUCCESS;
}
