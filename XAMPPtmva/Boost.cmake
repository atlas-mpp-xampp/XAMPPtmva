# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Boost as part of the offline / analysis release.
#

# Tell the user what's happening:
message( STATUS "Building Boost as part of this project" )

# The source code of Boost:
set( _boostSource
   "http://cern.ch/lcgpackages/tarFiles/sources/boost_1_69_0.tar.gz" )
set( _boostMd5 "b50944c0c13f81ce2c006802a1186f5a" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/BoostBuild )


# Decide where to take Python from:
find_package( PythonInterp 2.7 REQUIRED )
find_package( PythonLibs 2.7 REQUIRED )
set( _extraConf " --show-libraries --with-python=${PYTHON_EXECUTABLE} --with-libraries='python'") #--without-libraries='addr2line,zlib,bzip2,atomic,chrono,container,context,coroutine,date_time,exception,fiber,filesystem,graph,graph_parallel,iostreams,locale,log,math,mpi,program_options,random,regex,serialization,signals,stacktrace,system,test,thread,timer,type_erasure,wave' " )

message( STATUS "Export the python library path to install numpy properly")
# Only add debug symbols in Debug build mode:
set( _extraOpt )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
   list( APPEND _extraOpt "variant=debug" )
endif()
# Set a compiler toolset explicitly. For the cases where multiple compiler
# types are available in the environment at the same time.
if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" )
   list( APPEND _extraOpt "toolset=clang" )
elseif( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" )
   list( APPEND _extraOpt "toolset=gcc" )
endif()

set( BOOST_PYTHON_LIBARIES )
foreach( lib boost_python27 boost_numpy27 )
   list( APPEND BOOST_PYTHON_LIBARIES
      $<BUILD_INTERFACE:${_buildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}${lib}${CMAKE_SHARED_LIBRARY_SUFFIX}>
      $<INSTALL_INTERFACE:lib/${CMAKE_SHARED_LIBRARY_PREFIX}${lib}${CMAKE_SHARED_LIBRARY_SUFFIX}> )
endforeach()

# Build Boost:
ExternalProject_Add( Boost_python
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_boostSource}
   URL_MD5 ${_boostMd5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ./bootstrap.sh --prefix=${_buildDir} ${_extraConf}
   BUILD_COMMAND ./b2 ${_extraOpt} 
   COMMAND ./b2 ${_extraOpt} install
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/
   <INSTALL_DIR> )

#add_dependencies( Boost_python numpy )

# Install Boost:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

unset ( _buildDir)
unset ( _boostMd5)
unset (_boostSource)
unset (_extraOpt)
unset (_extraConf)
