#include <XAMPPplotting/AnalysisSetup.h>
#include <XAMPPplotting/ArithmetricReaders.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPtmva/MVAReaderProvider.h>
#include <XAMPPtmva/MVATrainApplicationService.h>
#include <XAMPPtmva/MVATreeMaker.h>
#include <XAMPPtmva/MetaDataTreeReader.h>
#include <XAMPPtmva/SciKitFactoryUtils.h>
#include <XAMPPtmva/SciKitMVAvariable.h>
#include <XAMPPtmva/SetupTMVA.h>
#include <XAMPPtmva/TMVAFactoryUtils.h>
#include <XAMPPtmva/TMVAvariable.h>

#include <TMVA/DataLoader.h>
#include <TMVA/Factory.h>
#include <TMVA/ROCCalc.h>
#include <TMVA/Reader.h>
#include <TMVA/Tools.h>

#include <TFile.h>
#include <TSystem.h>

#include <dirent.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

namespace XAMPP {
    SetupTMVA::SetupTMVA() :
        AnalysisSetup(),
        m_classification(true),
        m_BatchMode(false),
        m_outFile(nullptr),
        m_IsHistFitInput(false),
        m_SignalDSIDs(),
        m_SkipNegWeight(false),
        m_applicationToSetup(-1),
        m_BookedMethods(),
        m_SignalConditions(),
        m_doObjectTraining(false),
        m_CurrentHelper(nullptr) {}
    void SetupTMVA::setBatchMode() { m_BatchMode = true; }
    void SetupTMVA::RegressionTraining() { m_classification = false; }
    SetupTMVA::~SetupTMVA() {
        delete MVATrainingService::getService();
        // No full training session has been executed. I'm pretty sure that the
        // class  was initiated from python and it's not a good idea to delete the
        // python interpreter while it's running. It's somekind of pissed about it.
        if (!loadWeightsOnly()) delete MVATrainApplicationService::getService();

        CloseFile();
    }
    void SetupTMVA::CloseFile() {
        if (m_outFile) {
            m_outFile->cd();
            m_outFile->Write("", TObject::kOverwrite);
            m_outFile->Close();
            m_outFile.reset();
        }
    }
    bool SetupTMVA::CreateOutFile(const std::string& File) {
        if (m_outFile) {
            Warning("SetupTMVA::SetOutputLocation()", "Close existing file " + std::string(m_outFile->GetName()) + ".");
            CloseFile();
        }
        if (File.rfind("/") < File.size()) {
            std::string OutDir = File.substr(0, File.rfind("/"));
            Info("SetupTMVA::SetOutputLocation()", "Output file will be stored in " + OutDir);
            gSystem->mkdir(OutDir.c_str(), true);
        }
        m_outFile = std::make_shared<TFile>(File.c_str(), "RECREATE");
        if (!m_outFile || !m_outFile->IsOpen()) {
            Error("SetupTMVA::SetOutputLocation()", "Could not create file " + File);
            return false;
        }
        MVACacheProvider::getProvider()->setOutFile(m_outFile);
        return true;
    }
    bool SetupTMVA::ImportFile(const std::string& C, ConfigReader Parser, bool always) {
        std::ifstream Import;
        std::string Path = ResolvePath(C);
        if (Path.empty()) {
            Error("SetupTMVA::ImportFile()", "No config path was given");
            return false;
        }
        if (!always && IsElementInList(m_ImportedConfigs, Path)) {
            Info("SetupTMVA::ImportFile()", "The file " + Path + " has already been imported. It will not imported again.");
            return true;
        }
        m_ImportedConfigs.push_back(Path);
        Info("SetupTMVA::ImportFile()", "Import new file " + Path);
        Import.open(Path);
        return (this->*Parser)(Import);
    }
    bool SetupTMVA::ReadTrainConfig(const std::string& train_cfg) {
        LoadWeightsOnly();
        return ImportFile(train_cfg, &SetupTMVA::ReadTMVAConfig);
    }
    bool SetupTMVA::ConfigureTraining(const std::string& InputCfg, const std::string& TrainingCfg, const std::string& OutputDir,
                                      const std::string& FileName) {
        MVACacheProvider::getProvider()->setOutDirectory(OutputDir);
        MVACacheProvider::getProvider()->setOutFileName(FileName);
        if (!CreateOutFile(OutputDir + std::string(OutputDir.empty() ? "" : "/") + FileName)) return false;
        if (!ImportFile(InputCfg, &SetupTMVA::ReadInputFiles) || !InitMetaData()) { return false; }
        if (!ImportFile(TrainingCfg, &SetupTMVA::ReadTMVAConfig)) { return false; }
        Info("SetupTMVA::ConfigureTraining()", "Will create TMVA input tree first");
        if (!RunTreeMaker()) return false;
        Info("SetupTMVA::ConfigureTraining()", "Successfully created the TMVA input");
        if (!MVACacheProvider::getProvider()->prepareAndExecuteTraining()) return false;
        CloseFile();
        return true;
    }
    int SetupTMVA::CheckInputConfigProperties(std::ifstream& inf, const std::string& line) {
        std::stringstream sstr(line);
        if (IsKeyWordSatisfied(sstr, "IsHybridInput")) {
            HybridNormDataBase::getDataBase();
        } else if (IsKeyWordSatisfied(sstr, "IsHistFitter")) {
            TMVANormDataBase::getDataBase();
            m_IsHistFitInput = true;
        } else if (IsKeyWordSatisfied(sstr, "SignalDSID")) {
            ExtractListFromStream(sstr, m_SignalDSIDs);
        } else if (IsKeyWordSatisfied(sstr, "SetNominalName")) {
            m_NominalName = GetWordFromStream(sstr);
            m_SetNominalName = true;
        } else
            return AnalysisSetup::CheckInputConfigProperties(inf, line);

        if (m_SetNominalName && m_IsHistFitInput)
            dynamic_cast<TMVANormDataBase*>(TMVANormDataBase::getDataBase())->SetNominalName(m_NominalName);

        return PropertyStatus::New;
    }
    int SetupTMVA::CheckAnaConfigProperties(std::ifstream& inf, const std::string& line) {
        MVAReaderProvider::GetInstance(false);
        int prop = AnalysisSetup::CheckAnaConfigProperties(inf, line);
        if (prop != PropertyStatus::NothingFound) return prop;
        std::stringstream sstr(line);
        if (IsKeyWordSatisfied(sstr, "New_TMVAEventClass") && !CreateTMVAReader(inf, true))
            return PropertyStatus::Failure;
        else if (IsKeyWordSatisfied(sstr, "New_TMVAObjectClass") && !CreateTMVAReader(inf, false))
            return PropertyStatus::Failure;
        else if (IsKeyWordSatisfied(sstr, "New_SciKitEventClass") && !CreateSciKitReader(inf, true))
            return PropertyStatus::Failure;
        else if (IsKeyWordSatisfied(sstr, "New_SciKitObjectClass") && !CreateSciKitReader(inf, false))
            return PropertyStatus::Failure;
        else if (IsKeyWordSatisfied(sstr, "setTrainingLabel"))
            MVATrainingService::getService()->setTopologyLabel(line.substr(line.find(" ") + 1, std::string::npos));
        else
            return PropertyStatus::NothingFound;
        return PropertyStatus::New;
    }
    bool SetupTMVA::ReadTMVAConfig(std::ifstream& inf) {
        std::string line;
        MVAReaderProvider::GetInstance(false);
        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "Import") && !ImportFile(GetWordFromStream(sstr), &SetupTMVA::ReadTMVAConfig))
                return false;
            else if (IsKeyWordSatisfied(sstr, "RunConfig") && !ImportFile(GetWordFromStream(sstr), &SetupTMVA::ReadAnaConfig))
                return false;
            //
            //  define signal classification criteria
            //
            else if (GetWordFromStream(sstr, true).find("Cut") == GetWordFromStream(sstr, true).size() - 3) {
                std::shared_ptr<Condition> C = nullptr;
                if (IsKeyWordSatisfied(sstr, "CombCut")) {
                    C = CreateCombinedCut(inf, m_Readers, line);
                } else
                    C = CreateCut(line, m_Readers);
                if (!C) return false;
                m_SignalConditions.push_back(C);
            } else if (IsKeyWordSatisfied(sstr, "SignalDSID"))
                ExtractListFromStream(sstr, m_SignalDSIDs);
            //
            //  Options to assign training and testing events
            //
            else if (IsKeyWordSatisfied(sstr, "TrainingFraction") &&
                     !MVATrainingService::getService()->SetTrainingFraction(GetIntegerFromStream(sstr)))
                return false;
            else if (IsKeyWordSatisfied(sstr, "SplitMode") && !MVATrainingService::getService()->SetSplitMode(GetWordFromStream(sstr)))
                return false;
            else if (IsKeyWordSatisfied(sstr, "SkipNegativeWeights"))
                m_SkipNegWeight = true;
            else if (IsKeyWordSatisfied(sstr, "InvertSplitting"))
                MVATrainingService::getService()->InvertSplitting(true);

            // Constrain the number of testing and training events per sample
            // Syntax LimitEvents <Training/Testing> <DSID> <N>
            else if (IsKeyWordSatisfied(sstr, "LimitEvents")) {
                bool Limit_Training;
                if (IsKeyWordSatisfied(sstr, "Training"))
                    Limit_Training = true;
                else if (IsKeyWordSatisfied(sstr, "Testing"))
                    Limit_Training = false;
                else {
                    Error("ReadTMVAConfig()", "If you want to limit a type of events please decide which one, 'Training' or 'Testing'.");
                    return false;
                }
                int DSID(-1);
                long long unsigned Limit(0);
                sstr >> DSID >> Limit;
                EventCounterPtr Counter = MVATrainingService::getService()->getCounter(DSID);
                if (Limit_Training)
                    Counter->LimitTrainingEvents(Limit);
                else
                    Counter->LimitTestingEvents(Limit);
            }
            //  Exclude certain DSIDs from accepting Signal/Bkg events
            else if (IsKeyWordSatisfied(sstr, "ExcludeFromBkg"))
                MVATrainingService::getService()->getCounter(GetIntegerFromStream(sstr))->LockBackground();
            else if (IsKeyWordSatisfied(sstr, "ExcludeFromSignal"))
                MVATrainingService::getService()->getCounter(GetIntegerFromStream(sstr))->LockSignal();
            //  Assign DSIDs to be used for training and dsids to be used for testing
            else if (IsKeyWordSatisfied(sstr, "New_TrainValidSample") && !AssignNewTrainValidSample(inf))
                return false;
            //  XAMPPtmva shall train the classification of objects, like di-tau/gluon-jets etc.
            else if (IsKeyWordSatisfied(sstr, "ObjectTraining")) {
                MVACacheProvider::getProvider()->setTrainingObject(ParReaderStorage::GetInstance()->GetReader(GetWordFromStream(sstr)));
                m_doObjectTraining = true;
            }
            //  MVA training config
            else if (IsKeyWordSatisfied(sstr, "New_MVAVar") && !AddMVAVariable(inf, true))
                return false;
            else if (IsKeyWordSatisfied(sstr, "BookMethod") && !BookMVAMethods(sstr, true))
                return false;
            // If XAMPPtmva is submitted on the cluster we must take care that only one job gets assigned one MVA method in order
            // not to waste too much resources. In principle we could read in all train config files and create copies on the cluster disk
            // with erased BookMethod statemnts. Or we just copy the train config file to the cluster and append two extra lines ;-)
            else if (IsKeyWordSatisfied(sstr, "ClearBookedMethods"))
                MVACacheProvider::getProvider()->clearBookedMethods();
            else if (IsKeyWordSatisfied(sstr, "New_TMVAMethod") && !NewTMVATrainingMethod(inf))
                return false;
            else if (IsKeyWordSatisfied(sstr, "New_SciKitMethod") && !NewSciKitTrainingMethod(inf))
                return false;
            else if (IsKeyWordSatisfied(sstr, "setTrainingLabel"))
                MVATrainingService::getService()->setTopologyLabel(line.substr(line.find(" ") + 1, std::string::npos));
        }
        return (ReadLine(inf, line) != IfDefLineParser::ReadError);
    }
    bool SetupTMVA::NewTMVATrainingMethod(std::ifstream& inf) {
        std::string line;
        std::string name;
        TMVA::Types::EMVA Type;
        std::vector<std::string> Options;
        IfDefLineParser ReadLine;

        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "Name"))
                name = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "Type")) {
                if (IsKeyWordSatisfied(sstr, "BDT"))
                    Type = TMVA::Types::kBDT;
                else if (IsKeyWordSatisfied(sstr, "Fisher"))
                    Type = TMVA::Types::kFisher;
                else if (IsKeyWordSatisfied(sstr, "Cuts"))
                    Type = TMVA::Types::kCuts;
                else if (IsKeyWordSatisfied(sstr, "Likelihood"))
                    Type = TMVA::Types::kLikelihood;
                else if (IsKeyWordSatisfied(sstr, "PDERS"))
                    Type = TMVA::Types::kPDERS;
                else if (IsKeyWordSatisfied(sstr, "PDEFoam"))
                    Type = TMVA::Types::kPDEFoam;
                else if (IsKeyWordSatisfied(sstr, "KNN"))
                    Type = TMVA::Types::kKNN;
                else if (IsKeyWordSatisfied(sstr, "HMatrix"))
                    Type = TMVA::Types::kHMatrix;
                else if (IsKeyWordSatisfied(sstr, "LD"))
                    Type = TMVA::Types::kLD;
                else if (IsKeyWordSatisfied(sstr, "FDA"))
                    Type = TMVA::Types::kFDA;
                else if (IsKeyWordSatisfied(sstr, "MLP"))
                    Type = TMVA::Types::kMLP;
                else if (IsKeyWordSatisfied(sstr, "CFMlpANN"))
                    Type = TMVA::Types::kCFMlpANN;
                else if (IsKeyWordSatisfied(sstr, "TMlpANN"))
                    Type = TMVA::Types::kTMlpANN;
                else if (IsKeyWordSatisfied(sstr, "SVM"))
                    Type = TMVA::Types::kSVM;
                else if (IsKeyWordSatisfied(sstr, "Keras"))
                    Type = TMVA::Types::kPyKeras;
                else if (IsKeyWordSatisfied(sstr, "RuleFit"))
                    Type = TMVA::Types::kRuleFit;
                else {
                    Error("SetupTMVA::CreateNewMethod()", "Unknown type " + GetWordFromStream(sstr));
                    return false;
                }
            } else if (IsKeyWordSatisfied(sstr, "Option"))
                ExtractListFromStream(sstr, Options);
            else if (IsKeyWordSatisfied(sstr, "End_TMVAMethod") || IsKeyWordSatisfied(sstr, "End_MVAMethod")) {
                if (TMVACacheProvider::getProvider()) return TMVACacheProvider::getProvider()->addTrainingMethod(Type, name, Options);
                return true;
            }
        }
        Error("SetupTMVA::CreateNewMethod()", "Missing 'End_TMVAMethod' statement");
        return false;
    }
    bool SetupTMVA::AddMVAVariable(std::ifstream& inf, bool AssignToFactory) {
        std::string line, name;
        ITreeVarReader* R = nullptr;
        bool isTargetVar = false;
        if (!AssignToFactory && !m_CurrentHelper) {
            Error("SetupTMVA::AddMVAVariable()", "There is no MVATrainingReader which can manage the MVA input variables");
            return false;
        }
        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            std::string word = GetWordFromStream(sstr);
            if (word.find("Reader") != std::string::npos) {
                R = word.find("New_") != 0 ? ReaderProvider::GetInstance()->CreateReader(line)
                                           : ReaderProvider::GetInstance()->CreateReader(inf, line);
                if (R == nullptr) {
                    Error("SetupTMVA::AddMVAVariable()", "Failed to create ITreeVarReader from " + line);
                    return false;
                }
                if (!AssignToFactory && !m_CurrentHelper->isEventBased() && word.find("ParReader") != std::string::npos) {
                    bool CreateAbs = (word.find("|") == 0 && word.rfind("|") == word.size() - 1);
                    ParticleReader* particle = ParticleReader::GetReader(GetWordFromStream(sstr));
                    if (particle == nullptr) {
                        Error("SetupTMVA::AddMVAVariable()", "The associated particle to " + R->name() + " is a composite particle.");
                        return false;
                    }
                    std::string particle_var = GetWordFromStream(sstr);
                    Cube Matrix = GetMatrixRangesToRead(particle_var);
                    if (particle_var.find("|") == 0 && particle_var.rfind("|") == particle_var.size() - 1) {
                        CreateAbs = true;
                        particle_var = particle_var.substr(1, particle_var.size() - 1);
                    }
                    ITreeVectorReader* vec_reader = particle->RetrieveBranchAccess(particle_var);
                    if (vec_reader == nullptr) return false;
                    // A certain entry in the container has been fixed via ParReader myPart myVar[0]
                    if (Matrix.first.first != Matrix.first.second || Matrix.first.first != -1) {
                        return false;
                    } else if (Matrix.second.second != Matrix.second.first) {
                        return false;
                    }
                    // The associated variable is a particle vector variable and a certain target of the vector has
                    // been chosen
                    if (Matrix.second.first == Matrix.second.second && Matrix.second.first != -1) {
                        ITreeMatrixReader* mat_reader = dynamic_cast<ITreeMatrixReader*>(vec_reader);
                        if (mat_reader == nullptr) return false;
                        vec_reader = MatrixNthColumnReader::GetReader(mat_reader, Matrix.second.second);
                    }
                    R = !CreateAbs
                            ? vec_reader
                            : MathITreeVarReader::GetReader(vec_reader, vec_reader, MathITreeVarReader::ArithmetricOperators::Absolute);
                }
            } else if (word == "isTargetVariable") {
                isTargetVar = true;
            }

            else if (word == "Name")
                name = GetWordFromStream(sstr);
            else if (word == "End_MVAVar") {
                if (AssignToFactory) {
                    if (isTargetVar) {
                        if (m_classification) {
                            Error("SetupTMVA::AddMVAVariable()", "Target variables are not allowed for classification problems");
                            return false;
                        } else
                            return MVACacheProvider::getProvider()->addTargetVariable(R, name);
                    }
                    return MVACacheProvider::getProvider()->addTrainingVariable(R, name);
                } else if (isTargetVar)
                    return true;
                else {
                    if (!m_CurrentHelper->addVariable(name, R)) return false;
                    m_Readers.push_back(R);
                }
                Info("SetupTMVA::AddVariable()", "Add variable " + name + " using " + R->name());
                return true;
            }
        }
        Error("SetupTMVA::AddMVAVariable()", "Missing 'End_MVAVar' statement.");
        return false;
    }
    bool SetupTMVA::BookMVAMethods(std::stringstream& sstr, bool BookFactory) {
        std::string word = GetWordFromStream(sstr);
        if (word.empty() || word.find("#") == 0) { return true; }
        if (BookFactory && !MVACacheProvider::getProvider()->bookMethod(word))
            return false;
        else if (!BookFactory && !m_CurrentHelper) {
            Error("SetupTMVA::BookMVAMethods()", "The method " + word + " could not be booked for application");
            return false;
        } else if (!BookFactory)
            m_CurrentHelper->BookMethod(word);
        Info("SetupTMVA::BookMVAMethods()", "Book new MVA method " + word + ".");
        m_BookedMethods.push_back(word);
        return BookMVAMethods(sstr, BookFactory);
    }
    bool SetupTMVA::RunTreeMaker() {
        if (m_Analysis) {
            Error("SetupTMVA::RunTreeMaker()", "There is already a tree maker");
            return false;
        }
        FinalizeActRegion();
        MVACacheProvider::getProvider();
        std::shared_ptr<MVATreeMaker> m_Trainer = std::make_shared<MVATreeMaker>(m_AnaName, m_Tree, inputFiles());
        m_Analysis = std::shared_ptr<Analysis>(m_Trainer);
        if (!m_classification) m_Trainer->RegressionProblem();
        m_Trainer->SetReaders(m_Readers);
        std::vector<std::shared_ptr<Condition>> SumCuts;
        for (auto& R : m_RegionCuts) {
            // Only the default Standard region is defined. Lets Finalize it
            if (m_RegionCuts.size() == 1) R.second->Finalize();
            if (R.second->RetrieveCompressedCut()) SumCuts.push_back(R.second->RetrieveCompressedCut());
        }
        m_Trainer->SetCuts(SumCuts);
        m_Trainer->SetOutputLocation(m_outFile);
        MVATrainingService::getService()->SetEventSignalRequirements(m_SignalConditions);
        MVATrainingService::getService()->SetSignals(m_SignalDSIDs);
        if (m_SkipNegWeight) m_Trainer->RequirePosWeights();
        if (m_BatchMode) m_Trainer->DisableProgressBar();
        if (m_doObjectTraining) m_Trainer->ObjectTraining();
        // if (!m_Trainer->SetTrainValidSamples(m_TrainValidSamples)) return false;
        // Process the loop to parse all the events to the factory
        if (!m_Trainer->Process()) return false;
        MVATrainingService::getService()->PromptSampleStats();
        return m_Trainer->CheckTraining();
    }
    bool SetupTMVA::AssignNewTrainValidSample(std::ifstream& inf) {
        std::string line, name;
        std::vector<int> Valid, Train;
        long long unsigned LimitValid(0), LimitTrain(0);

        IfDefLineParser ReadLine;
        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "Name")) {
                name = GetWordFromStream(sstr);
            } else if (IsKeyWordSatisfied(sstr, "ValidDSID")) {
                ExtractListFromStream(sstr, Valid);
            } else if (IsKeyWordSatisfied(sstr, "TrainDSID")) {
                ExtractListFromStream(sstr, Train);
            } else if (IsKeyWordSatisfied(sstr, "LimitEvents")) {
                if (IsKeyWordSatisfied(sstr, "Training"))
                    sstr >> LimitTrain;
                else if (IsKeyWordSatisfied(sstr, "Testing"))
                    sstr >> LimitValid;
                else {
                    Error("SetupTMVA::AssignNewTrainValidSample()",
                          Form("Invalid type of events %s. Allowed types are 'Training', 'Testing'", GetWordFromStream(sstr).c_str()));
                    return false;
                }
            } else if (IsKeyWordSatisfied(sstr, "End_TrainValidSample")) {
                if (name.empty()) {
                    Error("SetupTMVA::AssignNewTrainValidSample()", "No name is given.");
                    return false;
                }
                MVALogicalSmp_Ptr Sample = MVATrainingService::getService()->getLogicalSample(name);
                Sample->LimitTrainingEvents(LimitTrain);
                Sample->LimitTestingEvents(LimitValid);
                if (!Sample->AddTrainSample(Train) || !Sample->AddTestSample(Valid)) { return false; }
                return true;
            }
        }
        Error("SetupTMVA::AssignNewTrainValidSample()", "Missing 'End_TrainValidSample' statement");
        return false;
    }
    bool SetupTMVA::CreateTMVAReader(std::ifstream& inf, bool EvClass) {
        std::string line(""), name(""), xmlPath(""), TrainConf("");
        m_applicationToSetup = MachineFrameWork::TMVA;
        IfDefLineParser ReadLine;

        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "Name"))
                name = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "xmlPath"))
                xmlPath = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "Training"))
                TrainConf = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "End_TMVAEventClass") || IsKeyWordSatisfied(sstr, "End_TMVAObjectClass")) {
                if (xmlPath.empty()) {
                    Error("SetupTMVA::CreateMVAReader()", "No xmlPath was defined.");
                    return false;
                } else if (TrainConf.empty()) {
                    Error("SetupTMVA::CreateMVAReader()", "No Training was defined.");
                    return false;
                }
                if (EvClass)
                    m_CurrentHelper = MVATrainApplicationService::getService()->TMVAEventClassifier(name, xmlPath);
                else
                    m_CurrentHelper = MVATrainApplicationService::getService()->TMVAObjectClassifier(name, xmlPath);

                if (m_CurrentHelper->isEventClassifier() != EvClass) {
                    Error("SetupTMVA::CreateMVAReader()", "The training " + m_CurrentHelper->name() + " is already " +
                                                              std::string(EvClass ? "object" : "event") + " classification.");
                    return false;
                }
                if (!ImportFile(TrainConf, &SetupTMVA::ConfigureMVAReader, true) || !m_CurrentHelper->loadMethods()) return false;
                m_CurrentHelper = nullptr;
                m_applicationToSetup = -1;
                return true;
            }
        }
        return false;
    }
    bool SetupTMVA::ConfigureMVAReader(std::ifstream& inf) {
        std::string line;
        IfDefLineParser ReadLine;

        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "New_MVAVar") && !AddMVAVariable(inf, false)) {
                Error("SetupTMVA::ConfigureMVAReader()", "Failed to add variable");
                return false;
            }
            // Load the python module in case of SciKitLearn...
            else if (m_applicationToSetup == MachineFrameWork::SciKitLearn && IsKeyWordSatisfied(sstr, "Module") &&
                     !SciKiyPythonObjectService::getService()->importTrainingModule(GetWordFromStream(sstr)))
                return false;
            else if (IsKeyWordSatisfied(sstr, "BookMethod") && !BookMVAMethods(sstr, false))
                return false;
            else if (IsKeyWordSatisfied(sstr, "ClearBookedMethods") && m_CurrentHelper)
                m_CurrentHelper->clearBookedMethods();
            else if (IsKeyWordSatisfied(sstr, "Import") && !ImportFile(GetWordFromStream(sstr), &SetupTMVA::ConfigureMVAReader, true))
                return false;
        }
        return true;
    }
    bool SetupTMVA::NewSciKitTrainingMethod(std::ifstream& inf) {
        std::string line;
        std::string name;
        std::string python_script;
        std::vector<std::string> options;
        IfDefLineParser ReadLine;

        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "Name"))
                name = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "Module"))
                python_script = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "Option")) {
                std::string opts;
                std::stringstream opts_sstr(ReplaceExpInString(line, "Option", ""));
                std::string word;
                while (opts_sstr >> word) {
                    if (word.find("#") == 0) break;
                    opts += word + std::string(" ");
                }
                options.push_back(opts);
            } else if (IsKeyWordSatisfied(sstr, "End_SciKitMethod")) {
                if (SciKitCacheProvider::getProvider())
                    return SciKitCacheProvider::getProvider()->addTrainingMethod(python_script, name, options);
                return true;
            }
        }
        Error("SetupTMVA::CreateNewMethod()", "Missing 'End_SciKitMethod' statement");
        return false;
    }
    bool SetupTMVA::CreateSciKitReader(std::ifstream& inf, bool EventBased) {
        std::string line(""), name(""), dir(""), TrainConf("");
        m_applicationToSetup = MachineFrameWork::SciKitLearn;
        IfDefLineParser ReadLine;
        bool isRegressor = false;

        while (ReadLine(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "Name"))
                name = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "Directory"))
                dir = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "Training"))
                TrainConf = GetWordFromStream(sstr);
            else if (IsKeyWordSatisfied(sstr, "Regression"))
                isRegressor = true;
            else if (IsKeyWordSatisfied(sstr, "End_SciKitEventClass") || IsKeyWordSatisfied(sstr, "End_SciKitObjectClass")) {
                if (dir.empty()) {
                    Error("SetupTMVA::CreateSciKitReader()", "No training directory was defined.");
                    return false;
                } else if (TrainConf.empty()) {
                    Error("SetupTMVA::CreateSciKitReader()", "No Training was defined.");
                    return false;
                }
                if (EventBased) {
                    if (isRegressor) {
                        m_CurrentHelper = MVATrainApplicationService::getService()->SciKitEventRegressor(name, dir);
                        if (!m_CurrentHelper->isEventRegression()) {
                            Error("SetupTMVA::CreateMVAReader()",
                                  "The training " + m_CurrentHelper->name() + " is not an event regressor.");
                            return false;
                        }
                    } else {
                        m_CurrentHelper = MVATrainApplicationService::getService()->SciKitEventClassifier(name, dir);
                        if (!m_CurrentHelper->isEventClassifier()) {
                            Error("SetupTMVA::CreateMVAReader()",
                                  "The training " + m_CurrentHelper->name() + " is not an event classifier.");
                            return false;
                        }
                    }

                } else {
                    if (!isRegressor) {
                        m_CurrentHelper = MVATrainApplicationService::getService()->SciKitObjectClassifier(name, dir);
                        if (!m_CurrentHelper->isObjectClassifier()) {
                            Error("SetupTMVA::CreateMVAReader()",
                                  "The training " + m_CurrentHelper->name() + " is not an object classifier.");
                            return false;
                        }
                    } else
                        return false;
                }
                if (!ImportFile(TrainConf, &SetupTMVA::ConfigureMVAReader, true) || !m_CurrentHelper->loadMethods()) return false;
                m_CurrentHelper = nullptr;
                m_applicationToSetup = -1;
                return true;
            }
        }
        return false;
    }
}  // namespace XAMPP
