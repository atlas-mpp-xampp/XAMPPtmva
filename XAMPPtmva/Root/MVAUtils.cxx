#include <RooDataHist.h>
#include <RooGaussian.h>
#include <RooHistPdf.h>
#include <TF1.h>
#include <TGraphAsymmErrors.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <XAMPPtmva/MVAUtils.h>
namespace XAMPP {
    std::pair<double, double> MVAPerformanceHelper::fitResolution(std::shared_ptr<TH1> histo, double x_min, double x_max) {
        return fitResolution(histo.get(), x_min, x_max);
    }
    std::pair<double, double> MVAPerformanceHelper::fitResolution(TH1* histo, double x_min, double x_max) {
        std::pair<double, double> res(FLT_MAX, FLT_MAX);
        if (histo) {
            std::unique_ptr<TF1> fit_func = std::make_unique<TF1>(RandomString(24).c_str(), "gaus(0)", x_min, x_max);
            histo->Fit(fit_func.get(), "EMRN0S", "", x_min, x_max);
            res.first = fit_func->GetParameter(1);
            res.second = fit_func->GetParameter(2);
        }
        return res;
    }
    std::shared_ptr<TH1> MVAPerformanceHelper::createGiniHisto(const std::shared_ptr<TH1>& signal, const std::shared_ptr<TH1>& background) {
        return createGiniHisto(signal.get(), background.get());
    }
    std::shared_ptr<TH1> MVAPerformanceHelper::createGiniHisto(const TH1* signal, const TH1* background) {
        std::shared_ptr<TH1> gini = clone(signal, true);

        if (signal == nullptr || background == nullptr || signal->GetDimension() != background->GetDimension() ||
            signal->GetDimension() != 1)
            return std::shared_ptr<TH1>();

        for (int g = GetNbins(gini); g > 0; --g) {
            double signal_yield = Integrate(signal, 0, g);
            double bkg_yield = Integrate(background, 0, g);
            double p = signal_yield / (signal_yield + bkg_yield);
            gini->SetBinContent(g, p * (1 - p));
        }
        return gini;
    }
    double MVAPerformanceHelper::calculateGiniIndex(const std::shared_ptr<TH1>& signal, const std::shared_ptr<TH1>& background) {
        return GetMaximum(createGiniHisto(signal, background));
    }
    double MVAPerformanceHelper::calculateGiniIndex(const TH1* signal, const TH1* background) {
        return GetMaximum(createGiniHisto(signal, background));
    }

    MVAPerformanceHelper::MVAPerformanceHelper(const std::shared_ptr<TH1>& background_train, const std::shared_ptr<TH1>& background_test,
                                               const std::shared_ptr<TH1>& signal_train, const std::shared_ptr<TH1>& signal_test) :
        m_background_train(background_train),
        m_background_test(background_test),
        m_signal_train(signal_train),
        m_signal_test(signal_test) {}

    MVAPerformanceHelper::MVAPerformanceHelper(const TH1* background_train, const TH1* background_test, const TH1* signal_train,
                                               const TH1* signal_test) :
        MVAPerformanceHelper(clone(background_train), clone(background_test), clone(signal_train), clone(signal_test)) {}
    std::shared_ptr<TGraphAsymmErrors> MVAPerformanceHelper::generate_ROC(const std::shared_ptr<TH1>& bkg, const std::shared_ptr<TH1>& sig,
                                                                          bool is_train, bool inv_bkg_eff) {
        if (is_train || inv_bkg_eff) { /*make compilation warning silent*/
        }
        if (!bkg || !sig) { return std::shared_ptr<TGraphAsymmErrors>(); }
        std::shared_ptr<TGraphAsymmErrors> Graph = std::make_shared<TGraphAsymmErrors>();
        std::shared_ptr<TH1> cul_bkg = createCulmulative(bkg);
        std::shared_ptr<TH1> cul_sig = createCulmulative(sig);
        if (cul_bkg->GetBinContent(1) > 1.) cul_bkg->Scale(1. / cul_bkg->GetBinContent(1));
        if (cul_sig->GetBinContent(1) > 1.) cul_bkg->Scale(1. / cul_sig->GetBinContent(1));

        return Graph;
    }
    std::shared_ptr<TH1> MVAPerformanceHelper::createCulmulative(const std::shared_ptr<TH1>& H) {
        std::shared_ptr<TH1> cul = clone(H, true);
        for (unsigned int i = 1; i < GetNbins(H); ++i) {
            std::pair<double, double> int_err = IntegrateAndError(H, i);
            cul->SetBinContent(i, int_err.first);
            cul->SetBinError(i, int_err.second);
        }
        return cul;
    }

}  // namespace XAMPP
