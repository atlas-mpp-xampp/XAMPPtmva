#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPtmva/FactoryUtils.h>
#include <XAMPPtmva/SciKitLearnFactory.h>

#include <TSystem.h>
#include <cfloat>
namespace XAMPP {
    //########################################################################
    //                      SciKitLearnFactory
    //########################################################################
    SciKitLearnFactory::SciKitLearnFactory() : HDF5ClassFactory() {}
    bool SciKitLearnFactory::initializeFile() {
        // The file has already been initialized
        if (isInitialized()) { return true; }
        if (!HDF5ClassFactory::initializeFile()) { return false; }

        return add_attribute("TopologyLabel", MVATrainingService::getService()->topologyLabel()) &&

               add_attribute("trainingType", data_type());
    }
    unsigned int SciKitLearnFactory::data_type() const { return EventType::Classification; }
    //########################################################################
    //                      SciKitRegressionFactory
    //########################################################################

    SciKitRegressionFactory::SciKitRegressionFactory() : SciKitLearnFactory() {}
    bool SciKitRegressionFactory::initializeFile() {
        if (!SciKitLearnFactory::initializeFile()) return false;
        return HDF5RegressionFactory::initializeFile();
    }
    unsigned int SciKitRegressionFactory::data_type() const { return EventType::Regression; }

}  // namespace XAMPP
