#include <XAMPPtmva/TMVAFactoryUtils.h>

#include <PathResolver/PathResolver.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>

#include <TMVA/Config.h>
#include <TMVA/DataLoader.h>
#include <TMVA/Factory.h>
#include <TMVA/MethodBase.h>

#include <TH1.h>
#include <TH2.h>
#include <TString.h>
#include <TSystem.h>

namespace XAMPP {
    //#########################################################################################
    //                            TMVAFactoryMethod
    //#########################################################################################
    TMVAFactoryMethod::TMVAFactoryMethod(const TMVA::Types::EMVA& Type, const std::string& Name, const std::vector<std::string>& Options) :
        m_Type(Type),
        m_Name(Name),
        m_Options(Options) {}
    std::string TMVAFactoryMethod::name() const { return m_Name; }

    bool TMVAFactoryMethod::BookInFactory(std::shared_ptr<TMVA::Factory> Factory) {
        std::string OptionString;
        for (auto& Opt : m_Options) { OptionString += Opt + ":"; }
        OptionString = OptionString.substr(0, OptionString.size() - 1);
        TMVACacheProvider* Provider = TMVACacheProvider::getProvider();
        if (!Provider) return false;
        Factory->BookMethod(
            Provider->dataLoader().get(), m_Type,
            TString((name() + std::string(MVATrainingService::getService()->InvertedSplitting() ? "_swapped" : "")).c_str()),
            TString(OptionString.c_str()));
        Info("TMVAFactoryMethod()", "Book method in factory " + name());
        return true;
    }

    //#########################################################################################
    //                            TMVACacheProvider
    //#########################################################################################
    TMVACacheProvider* TMVACacheProvider::getProvider() {
        if (!m_Inst) m_Inst = new TMVACacheProvider();
        TMVACacheProvider* Ptr = dynamic_cast<TMVACacheProvider*>(m_Inst);
        if (Ptr == nullptr)
            Warning("TMVACacheProvider()", "Another instance of the MVACacheProvider has been loaded. Segmentation is ahead");
        return Ptr;
    }
    TMVACacheProvider::~TMVACacheProvider() {}
    TMVACacheProvider::TMVACacheProvider() :
        MVACacheProvider(),
        m_DataLoader(std::make_shared<TMVA::DataLoader>("TrainingFiles")),
        m_Factory(),
        m_TrainingMethods(),
        m_outfile(),
        m_outDir() {
        // Define a flag useful in the train configs
        IfDefFlags::Instance()->AddFlag("iSTMVATraining");
        TMVA::gConfig().GetIONames().fWeightFileDir = TString();
    }
    MVACacheHandler_Ptr TMVACacheProvider::createHandler(MVALogicalSmp_Ptr Smp, unsigned int TrainSize, unsigned int ValidSize,
                                                         bool isEvHandler) const {
        if (isEvHandler)
            return std::make_shared<TMVAEventClassification>(Smp, TrainSize, ValidSize);
        else
            return std::make_shared<TMVAObjectClassification>(Smp, TrainSize, ValidSize);
    }
    std::shared_ptr<TMVA::DataLoader> TMVACacheProvider::dataLoader() const { return m_DataLoader; }

    bool TMVACacheProvider::setupFactory() {
        for (auto& feature : getTrainFeatures()) { dataLoader()->AddVariable(TString(feature.name.c_str())); }
        if (getTrainFeatures().empty()) {
            Error("TMVACacheProvider::initFactory()", "No train features given");
            return false;
        }
        return true;
    }
    bool TMVACacheProvider::prepareAndExecuteTraining() {
        if (!m_Factory) {
            Error("TMVACacheProvider()", "No factory has been parsed for training");
            return false;
        }
        bool dir_made = false;
        if (!DoesDirectoryExist(dataLoader()->GetName())) {
            dir_made = true;
            gSystem->mkdir(dataLoader()->GetName(), true);
        }
        TCut mycuts = "";
        Info("TMVACacheProvider()", "Prepare the Training and Test tree for TMVA");
        dataLoader()->PrepareTrainingAndTestTree(mycuts, mycuts, "");
        Info("TMVACacheProvider()", "Execute the TMVA Factory");
        for (auto& Booked : m_TrainingMethods) {
            if (IsElementInList(getBookedMethods(), Booked->name())) Booked->BookInFactory(m_Factory);
        }
        try {
            m_Factory->TrainAllMethods();
            m_Factory->TestAllMethods();
            m_Factory->EvaluateAllMethods();
        } catch (...) { return false; }
        TString topo_label(MVATrainingService::getService()->topologyLabel().c_str());
        m_outfile->WriteObject(&topo_label, "TopologyLabel");

        if (m_outDir != dataLoader()->GetName()) {
            Info("TMVACacheProvider()",
                 Form("TMVA is a genius it puts the dataloader name  %s in front of the *ABSOLUTE* path. Move it by hand now to %s",
                      dataLoader()->GetName(), m_outDir.c_str()));
            for (const auto item : ListDirectory(dataLoader()->GetName(), std::string())) {
                system(Form("mv %s %s/%s", item.c_str(), m_outDir.c_str(), item.substr(item.rfind("/"), std::string::npos).c_str()));
            }
            if (dir_made) {
                Info("TMVACacheProvider()", Form("And I even have to clean everything. Delete %s", dataLoader()->GetName()));
                system(Form("rm -rf %s", dataLoader()->GetName()));
            }
        }
        return true;
    }
    void TMVACacheProvider::setOutFile(std::shared_ptr<TFile> outfile) {
        if (outfile.get() == nullptr) return;
        m_outfile = outfile;
        m_Factory = std::make_shared<TMVA::Factory>(
            "TMVAClassification", outfile.get(), "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification");
    }
    void TMVACacheProvider::setOutDirectory(const std::string& outDir) {
        //  Apparently TMVA came up with the idea to put the DataLoader in name of the output files
        //  which Genius did this? Anyway we can forget about the fWeightFileDir property
        m_outDir = outDir;
    }
    bool TMVACacheProvider::addTrainingMethod(const TMVA::Types::EMVA& Type, const std::string& name,
                                              const std::vector<std::string>& Options) {
        if (name.empty()) {
            Error("TMVACacheProvider()", "No name has been set");
            return false;
        }
        if (IsMethodKnown(name)) {
            Error("TMVACacheProvider()", "There arleady exists a training method " + name + ".");
            return false;
        }
        m_TrainingMethods.push_back(std::make_shared<TMVAFactoryMethod>(Type, name, Options));
        return true;
    }
    bool TMVACacheProvider::IsMethodKnown(const std::string& method) const {
        for (const auto& known : m_TrainingMethods) {
            if (known->name() == method) return true;
        }
        return false;
    }
    //#########################################################################################
    //                            TMVACache
    //#########################################################################################
    TMVACache::TMVACache(unsigned int T, unsigned int Size) :
        MVABuffer(T, Size),
        m_Loader(TMVACacheProvider::getProvider()->dataLoader()) {}
    TMVACache::~TMVACache() {}
    bool TMVACache::FillEvent(const Cache& Event, const double& Weight) {
        if (type() & XAMPP::EventType::Background) {
            if (type() & EventType::Training) {
                m_Loader->AddBackgroundTrainingEvent(Event, Weight);
            } else {
                m_Loader->AddBackgroundTestEvent(Event, Weight);
            }
        } else if (type() & XAMPP::EventType::Signal) {
            if (type() & EventType::Training) {
                m_Loader->AddSignalTrainingEvent(Event, Weight);
            } else {
                m_Loader->AddSignalTestEvent(Event, Weight);
            }
        } else
            return false;
        return true;
    }
    //#########################################################################################
    //                            TMVAEventCache
    //#########################################################################################
    TMVAEventCache::TMVAEventCache(unsigned int T, unsigned int Size) : TMVACache(T, Size) {}
    TMVAEventCache::~TMVAEventCache() {}
    bool TMVAEventCache::PushBack(ITreeVarReader* Reader, Cache& Vec) {
        if (Reader->entries() == 1) {
            if (!IsFinite(Reader->readEntry(0))) {
                Error("TMVAEventCache::PushBack()", "The reader " + Reader->name() + " does not return a number.");
                return false;
            }
            Vec.push_back(Reader->readEntry(0));
        } else if (Reader->entries() > 1) {
            Error("TMVAEventCache::PushBack()", "The variable " + Reader->name() + " is not a scalar. What are you doing?!");
            return false;
        } else {
            Error("TMVAEventCache::PushBack()", "Whaaat?! The variable " + Reader->name() + " is empty");
            return false;
        }
        return true;
    }
    //#########################################################################################
    //                            TMVAEventClassification
    //#########################################################################################
    TMVAEventClassification::~TMVAEventClassification() {}
    TMVAEventClassification::TMVAEventClassification(MVALogicalSmp_Ptr Sample, unsigned int TrainSize, unsigned int ValidSize) :
        MVACacheHandler(Sample, TrainSize, ValidSize) {}
    std::shared_ptr<MVATrainTestSplitting> TMVAEventClassification::CreateCache(unsigned int Type) {
        return std::make_shared<TMVAEventTrainTestHandler>(Type | EventType::Classification, this);
    }
    //#########################################################################################
    //                            TMVAEventTrainTestHandler
    //#########################################################################################
    TMVAEventTrainTestHandler::TMVAEventTrainTestHandler(unsigned int Type, MVACacheHandler* Handler) :
        MVATrainTestSplitting(Type, Handler) {}
    TMVAEventTrainTestHandler::~TMVAEventTrainTestHandler() {}
    std::shared_ptr<MVABuffer> TMVAEventTrainTestHandler::CreateCache(unsigned int T, unsigned int Size) {
        return std::make_shared<TMVAEventCache>(T, Size);
    }

    //#########################################################################################
    //                            TMVAObjectCache
    //#########################################################################################
    TMVAObjectCache::TMVAObjectCache(TMVAObjectTrainTestHandler* Handler, unsigned int T, unsigned int Size) :
        TMVACache(T, Size),
        m_ObjectHandler(Handler) {}
    TMVAObjectCache::~TMVAObjectCache() {}

    bool TMVAObjectCache::PushBack(ITreeVarReader* Reader, Cache& Vec) {
        if (Reader->entries() <= m_ObjectHandler->TargetEntry()) {
            Error("TMVAObjectCache::PushBack()", Reader->name() + " is not large enough");
            return false;
        }
        double val = Reader->readEntry(m_ObjectHandler->TargetEntry());
        if (!IsFinite(val)) {
            Error("TMVAObjectCache::PushBack()", "The reader " + Reader->name() + " does not return a number.");
            return false;
        }
        Vec.push_back(val);
        return true;
    }
    //#########################################################################################
    //                            TMVAObjectTrainTestHandler
    //#########################################################################################
    TMVAObjectTrainTestHandler::TMVAObjectTrainTestHandler(unsigned int Type, TMVAObjectClassification* Handler) :
        MVATrainTestSplitting(Type, Handler),
        m_parent(Handler) {}
    TMVAObjectTrainTestHandler::~TMVAObjectTrainTestHandler() {}
    unsigned int TMVAObjectTrainTestHandler::TargetEntry() const { return m_parent->TargetEntry(); }
    std::shared_ptr<MVABuffer> TMVAObjectTrainTestHandler::CreateCache(unsigned int T, unsigned int Size) {
        return std::make_shared<TMVAObjectCache>(this, T, Size);
    }
    //#########################################################################################
    //                            TMVAObjectClassification
    //#########################################################################################
    TMVAObjectClassification::TMVAObjectClassification(MVALogicalSmp_Ptr Sample, unsigned int TrainSize, unsigned int ValidSize) :
        MVACacheHandler(Sample, TrainSize, ValidSize) {}
    std::shared_ptr<MVATrainTestSplitting> TMVAObjectClassification::CreateCache(unsigned int Type) {
        return std::make_shared<TMVAObjectTrainTestHandler>(Type | EventType::Classification, this);
    }
    CachingStatus TMVAObjectClassification::CacheEvent(const std::vector<ITreeVarReader*>& Readers) {
        if (!isResponsible()) { return CachingStatus::NotResponsible; }
        if (Readers.empty()) {
            Error("TMVAObjectClassification()", "No variables given");
            return CachingStatus::Failure;
        }
        ITreeVarReader* First = Readers.at(0);
        for (size_t i = 0; i < First->entries(); ++i) {
            setTargetEntry(i);
            if (MVACacheHandler::CacheEvent(Readers) == CachingStatus::Failure) return CachingStatus::Failure;
        }
        return CachingStatus::Done;
    }

}  // namespace XAMPP
