#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPtmva/MVAParticleReader.h>
#include <XAMPPtmva/MVAReaderProvider.h>
#include <XAMPPtmva/SciKitMVAvariable.h>
#include <XAMPPtmva/TMVAvariable.h>
#include <XAMPPtmva/TemplateFitReader.h>

namespace XAMPP {
    ReaderProvider* MVAReaderProvider::GetInstance(bool DisableGeVToMeV) {
        if (!m_Inst) m_Inst = new MVAReaderProvider(DisableGeVToMeV);
        return m_Inst;
    }
    MVAReaderProvider::MVAReaderProvider(bool DisableGeVToMeV) : ReaderProvider(DisableGeVToMeV) {}
    MVAReaderProvider::~MVAReaderProvider() {}
    ParticleReader* MVAReaderProvider::GetParticle(XAMPP::TimeComponent t, const std::string& Name, const std::string& Coll) {
        MVAParticleReader* Particle =
            t == TimeComponent::Energy ? MVAParticleReader::GetReader(Name, Coll) : MVAParticleReaderM::GetReader(Name, Coll);
        if (disableGeVtoMeV()) Particle->DisableMeVtoGeV();
        return Particle;
    }
    ITreeVarReader* MVAReaderProvider::CreateReader(std::stringstream& sstr) {
        std::stringstream::pos_type Pos = sstr.tellg();
        std::string word(GetWordFromStream(sstr));
        bool CreateAbs = word.find("|") == 0 && word.rfind("|") == word.size() - 1;
        if (CreateAbs) word = word.substr(1, word.size() - 2);
        if (word == "TMVAEventReader")
            return CreateEventTMVAReader(sstr);
        else if (word == "SciKitEventReader")
            return CreateEventSciKitReader(sstr);
        else if (word == "SciKitEvRegressionReader")
            return CreateSciKitRegressionReader(sstr);
        sstr.clear();
        sstr.seekg(Pos, sstr.beg);
        return ReaderProvider::CreateReader(sstr);
    }
    ITreeVarReader* MVAReaderProvider::CreateReader(std::ifstream& inf, std::stringstream& sstr) {
        sstr.clear();
        sstr.seekg(0, sstr.beg);
        std::string Keyword = GetWordFromStream(sstr);
        if (Keyword == "New_TemplateFitReader") {
            TemplateFitReader* r = TemplateFitReader::GetReader(GetWordFromStream(sstr));
            if (!r || !r->setupTemplates(inf)) return nullptr;
            return r;
        }
        return ReaderProvider::CreateReader(inf, sstr);
    }
    ITreeVarReader* MVAReaderProvider::CreateEventTMVAReader(std::stringstream& sstr) {
        std::string Method = GetWordFromStream(sstr);
        std::string Classifier = GetWordFromStream(sstr);
        return TMVAEventMethodScore::GetReader(Method, Classifier);
    }
    ITreeVarReader* MVAReaderProvider::CreateEventSciKitReader(std::stringstream& sstr) {
        std::string Method = GetWordFromStream(sstr);
        std::string Classifier = GetWordFromStream(sstr);
        return SciKitEventScoreVarReader::GetReader(Method, Classifier);
    }
    ITreeVarReader* MVAReaderProvider::CreateSciKitRegressionReader(std::stringstream& sstr) {
        std::string Method = GetWordFromStream(sstr);
        std::string TrainGroup = GetWordFromStream(sstr);
        std::string Target = GetWordFromStream(sstr);
        return SciKitEventRegressionVarReader::GetReader(Method, TrainGroup, Target);
    }

}  // namespace XAMPP
