#include <XAMPPtmva/SciKitFactoryUtilsRegression.h>

#include <PathResolver/PathResolver.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>
namespace XAMPP {

    SciKitRegressionBuffer::SciKitRegressionBuffer(SciKitRegressionAuxillaryBuffer_Ptr Aux, unsigned int T, unsigned int Size) :
        MVABuffer(T, Size),
        m_auxBuffer(Aux) {}
    SciKitRegressionBuffer::~SciKitRegressionBuffer() {}

    bool SciKitRegressionBuffer::FillEvent(const Cache& ev, const double& w) {
        if ((type() | EventType::Target) != type())
            return m_auxBuffer->BufferTraining(ev, w);
        else if (!m_auxBuffer->BufferTarget(ev))
            return false;
        return m_auxBuffer->ParseToFactory(type());
    }

    SciKitRegressionAuxillaryBuffer::SciKitRegressionAuxillaryBuffer() :
        m_train(),
        m_target(),
        m_weight(),
        m_factory(SciKitRegressionCacheProvider::getProvider()->regressionFactory()) {}
    bool SciKitRegressionAuxillaryBuffer::BufferTraining(const std::vector<double>& train, const double weight) {
        if (!m_train.empty()) {
            Error("SciKitRegressionBuffer()", "Training information from the last event still present");
            return false;
        }
        m_train = train;
        m_weight = weight;
        return true;
    }
    bool SciKitRegressionAuxillaryBuffer::BufferTarget(const std::vector<double>& target) {
        if (!m_target.empty()) {
            Error("SciKitRegressionBuffer()", "Training information from the last event still present");
            return false;
        }
        m_target = target;
        return true;
    }
    bool SciKitRegressionAuxillaryBuffer::ParseToFactory(unsigned int event_type) {
        if (m_target.empty() || m_train.empty()) {
            Error("SciKitRegressionBuffer()", "Not enough information provided");
            return false;
        } else if ((event_type & EventType::Training) == EventType::Training) {
            if (!m_factory->AddTrainingEvent(m_train, m_target, m_weight)) return false;
        } else if ((event_type & EventType::Testing) == EventType::Testing) {
            if (!m_factory->AddTestingEvent(m_train, m_target, m_weight)) return false;
        } else {
            Error("SciKitRegressionBuffer()", "Unknown event");
            return false;
        }
        m_target.clear();
        m_train.clear();
        return true;
    }

    //################################################################
    //              SciKitRegressionEventBuffer
    //################################################################
    SciKitRegressionEventBuffer::SciKitRegressionEventBuffer(SciKitRegressionAuxillaryBuffer_Ptr Aux, unsigned int T, unsigned int Size) :
        SciKitRegressionBuffer(Aux, T, Size) {}
    bool SciKitRegressionEventBuffer::PushBack(ITreeVarReader* Reader, Cache& Vec) {
        if (Reader->entries() == 1) {
            if (!IsFinite(Reader->readEntry(0))) {
                Error("SciKitLearnEventCache::PushBack()", "The reader " + Reader->name() + " does not return a number.");
                return false;
            }
            Vec.push_back(Reader->readEntry(0));
        } else if (Reader->entries() > 1) {
            Error("SciKitLearnEventCache::PushBack()", "The variable " + Reader->name() + " is not a scalar. What are you doing?!");
            return false;
        } else {
            Error("SciKitLearnEventCache::PushBack()", "Whaaat?! The variable " + Reader->name() + " is empty");
            return false;
        }
        return true;
    }
    //#################################################################
    //              SciKitRegressionBufferHandler
    //#################################################################

    SciKitRegressionBufferHandler::SciKitRegressionBufferHandler(unsigned int Type, MVACacheHandler* Handler) :
        MVATrainTestSplitting(Type, Handler),
        m_auxBuffer(std::make_shared<SciKitRegressionAuxillaryBuffer>()),
        m_Handler(Handler) {}
    bool SciKitRegressionBufferHandler::init() {
        m_TrainingCache =
            CreateCache(type(), (type() & EventType::Training) == EventType::Training ? m_Handler->TrainSize() : m_Handler->TestSize());
        m_TestCache = CreateCache(type() | EventType::Target,
                                  (type() & EventType::Testing) == EventType::Testing ? m_Handler->TrainSize() : m_Handler->TestSize());
        return true;
    }
    std::shared_ptr<MVABuffer> SciKitRegressionBufferHandler::CreateCache(unsigned int T, unsigned int Size) {
        return std::shared_ptr<MVABuffer>(new SciKitRegressionEventBuffer(m_auxBuffer, T, Size));
    }

    //#################################################################
    //              ScikitRegressionCacheHandler
    //#################################################################

    ScikitRegressionCacheHandler::ScikitRegressionCacheHandler(MVALogicalSmp_Ptr sample, unsigned int TrainSize, unsigned int ValidSize) :
        MVACacheHandler(sample, TrainSize, ValidSize) {}
    bool ScikitRegressionCacheHandler::init() {
        m_SignalCache = CreateCache(EventType::Training);
        m_BackgroundCache = CreateCache(EventType::Testing);
        return m_SignalCache->init() && m_BackgroundCache->init();
    }

    std::shared_ptr<MVATrainTestSplitting> ScikitRegressionCacheHandler::CreateCache(unsigned int Type) {
        return std::shared_ptr<MVATrainTestSplitting>(new SciKitRegressionBufferHandler(Type | XAMPP::EventType::Regression, this));
    }

    //#################################################################
    //                      SciKitRegressionCacheProvider
    //#################################################################
    SciKitRegressionCacheProvider* SciKitRegressionCacheProvider::getProvider() {
        if (!m_Inst) m_Inst = new SciKitRegressionCacheProvider();
        SciKitRegressionCacheProvider* Ptr = dynamic_cast<SciKitRegressionCacheProvider*>(m_Inst);
        if (Ptr == nullptr)
            Warning("SciKitRegressionCacheProvider()", "Another instance of the MVACacheProvider has been loaded. Segmentation is ahead");
        return Ptr;
    }
    SciKitRegressionCacheProvider::~SciKitRegressionCacheProvider() {}
    SciKitRegressionCacheProvider::SciKitRegressionCacheProvider() : SciKitCacheProvider() {
        m_SciKitFactory = std::make_shared<SciKitRegressionFactory>();
    }
    SciKitRegressionFactory* SciKitRegressionCacheProvider::regressionFactory() const {
        SciKitRegressionFactory* reg = dynamic_cast<SciKitRegressionFactory*>(getFactory().get());
        if (reg == nullptr) Warning("SciKitRegressionCacheProvider()", "Could not find a valid instance of the regression factory");
        return reg;
    }
    MVACacheHandler_Ptr SciKitRegressionCacheProvider::createHandler(MVALogicalSmp_Ptr Smp, unsigned int TrainSize, unsigned int ValidSize,
                                                                     bool) const {
        return MVACacheHandler_Ptr(new ScikitRegressionCacheHandler(Smp, TrainSize, ValidSize));
    }
    bool SciKitRegressionCacheProvider::setupFactory() {
        for (auto& feature : getTrainFeatures()) {
            if (!getFactory()->addVariable(feature.name)) return false;
        }
        for (auto& feature : getTargetFeatures()) {
            if (!regressionFactory()->addTargetVariable(feature.name)) return false;
        }
        if (getTrainFeatures().empty()) {
            Error("SciKitRegressionCacheProvider::setupFactory()", "No train features given");
            return false;
        }
        if (getTargetFeatures().empty()) {
            Error("SciKitRegressionCacheProvider::setupFactory()", "No target features given");
            return false;
        }

        return true;
    }
}  // namespace XAMPP
