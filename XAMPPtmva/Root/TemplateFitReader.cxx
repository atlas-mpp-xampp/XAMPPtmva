#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/ReaderProvider.h>
#include <XAMPPtmva/TemplateFitReader.h>

#include <TCanvas.h>
namespace XAMPP {
    Chi2HistoLikeliHood::Chi2HistoLikeliHood() :
        m_TH1(),
        m_variable(nullptr),
        m_service(EventService::getService()),
        m_slices(),
        m_log_slices(),
        m_current_event(-1),
        m_current_slice(nullptr),
        m_current_log_slice(nullptr),
        m_current_bin(-1) {}

    ITreeVarReader* Chi2HistoLikeliHood::reader() const { return m_variable; }
    bool Chi2HistoLikeliHood::set_reader(ITreeVarReader* r) {
        if (m_variable) {
            Error("Chi2HistoLikeliHood::set_reader()", Form("%s has already been given to the object", m_variable->name().c_str()));
            return false;
        } else if (!r) {
            Error("Chi2HistoLikeliHood::set_reader()", "No reader was given");
            return false;
        }
        m_variable = r;
        return true;
    }

    int Chi2HistoLikeliHood::find_bin(double assumption) const { return m_TH1.axis(1)->FindBin(assumption); }

    void Chi2HistoLikeliHood::pick_slice() {
        if (m_current_event == m_service->currentEvent()) return;
        m_current_event = m_service->currentEvent();
        // We need to roll the dice since a new event is

        int look_for = m_TH1.axis(0)->FindBin(m_variable->readEntry(0));
        if (look_for != m_current_bin) {
            std::map<int, std::shared_ptr<TH1>>::const_iterator itr = m_log_slices.find(look_for);
            if (itr != m_log_slices.end()) {
                m_current_log_slice = itr->second;
                m_current_slice = m_slices[look_for];
            } else {
                Warning("Chi2HistoLikeliHood::pick_slice()", Form("Invalid bin %d ", look_for));
                return;
            }
            m_current_bin = look_for;
        }
    }

    std::shared_ptr<TH1> Chi2HistoLikeliHood::log_likelihood() {
        pick_slice();
        return m_current_log_slice;
    }
    std::shared_ptr<TH1> Chi2HistoLikeliHood::likelihood() {
        pick_slice();
        return m_current_slice;
    }
    bool Chi2HistoLikeliHood::LoadHistogramFromFile(std::stringstream& sstr) {
        if (!m_TH1.LoadHistogramFromFile(sstr)) return false;
        construct_slices();
        return true;
    }
    bool Chi2HistoLikeliHood::LoadHistogramFromFile(const std::string& file, const std::string& histo_path) {
        if (!m_TH1.LoadHistogramFromFile(file, histo_path)) return false;
        construct_slices();
        return true;
    }

    void Chi2HistoLikeliHood::construct_slices() {
        m_slices.clear();
        m_log_slices.clear();
        int axis = 1;
        for (int i = 0; i <= m_TH1.axis(axis)->GetNbins() + 1; ++i) {
            std::shared_ptr<TH1> slice = ProjectInto1D(m_TH1.histo(), axis, i, i + 1, 0, -1);
            std::pair<double, double> int_err = IntegrateAndError(slice);
            if (int_err.first > 0) slice->Scale(1. / int_err.first);
            m_slices.insert(std::pair<int, std::shared_ptr<TH1>>(i, slice));
            /// Clone the slice
            slice = clone(slice);
            // transform them into the log space
            for (int i = 0; i <= slice->GetNbinsX() + 1; ++i) {
                slice->SetBinContent(i, slice->GetBinContent(i) != 0 ? std::log(slice->GetBinContent(i)) : -1.e9);
                slice->SetBinError(i, slice->GetBinContent(i) != 0 ? std::log(slice->GetBinError(i)) : 1);
            }
            m_log_slices.insert(std::pair<int, std::shared_ptr<TH1>>(i, slice));
        }
        // Reset cached histograms and bins
        m_current_event = -1;
        m_current_slice.reset();
        m_current_log_slice.reset();
        m_current_bin = -1;
    }
    bool Chi2HistoLikeliHood::is_valid() const { return reader() != nullptr && !m_slices.empty(); }
    TAxis* Chi2HistoLikeliHood::axis() const { return m_TH1.axis(1); }

    //#################################################################
    //                      TemplateFitReader
    //#################################################################
    TemplateFitReader::TemplateFitReader(const std::string& r_name) :
        IScalarReader(),
        m_name(r_name),
        m_Registered(false),
        m_templates(),
        m_service(EventService::getService()),
        m_last_event(-1),
        m_cache(0),
        m_n_iter(1000),
        m_rnd_generator(std::make_unique<TRandom3>(r_name.size())) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    std::string TemplateFitReader::name() const { return m_name; }

    double TemplateFitReader::read() const {
        if (m_last_event != m_service->currentEvent()) {
            m_last_event = m_service->currentEvent();
            std::shared_ptr<TH1> log_likelihood = clone(m_templates[0]->log_likelihood());
            for (unsigned int i = 1; i < m_templates.size(); ++i) { log_likelihood->Add(m_templates[i]->log_likelihood().get()); }
            double tot_int(0), cul_pdf(0), rnd_num(m_rnd_generator->Uniform(0, 1));

            for (unsigned int i = 0; i <= GetNbins(log_likelihood); ++i) {
                log_likelihood->SetBinContent(i, std::exp(log_likelihood->GetBinContent(i)));
                tot_int += log_likelihood->GetBinContent(i);
            }

            bool has_rnd = false;
            if (tot_int > 0) {
                log_likelihood->Scale(1 / tot_int);
                for (unsigned int i = 0; i <= GetNbins(log_likelihood); ++i) {
                    cul_pdf += log_likelihood->GetBinContent(i);
                    if (cul_pdf >= rnd_num) {
                        m_cache = m_rnd_generator->Uniform(log_likelihood->GetXaxis()->GetBinLowEdge(i),
                                                           log_likelihood->GetXaxis()->GetBinUpEdge(i));
                        has_rnd = true;
                        break;
                    }
                }
            }
            if (!has_rnd)
                m_cache = m_rnd_generator->Uniform(log_likelihood->GetXaxis()->GetBinLowEdge(1),
                                                   log_likelihood->GetXaxis()->GetBinUpEdge(log_likelihood->GetNbinsX()));
        }
        return m_cache;
    }
    bool TemplateFitReader::init(TTree* t) {
        if (!m_Registered) {
            Error("TemplateFitReader::init()", "Something failed with during the creation of " + name());
            return false;
        }
        if (m_templates.empty()) {
            Error("TemplateFitReader::init()", "No templates were defined for " + name());

            return false;
        }
        for (auto& temp : m_templates) {
            if (!temp->reader()->init(t)) return false;
        }
        m_last_event = -1;
        return true;
    }
    TemplateFitReader* TemplateFitReader::GetReader(const std::string& r_name) {
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) return new TemplateFitReader(r_name);
        return dynamic_cast<TemplateFitReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(r_name));
    }
    bool TemplateFitReader::setupTemplates(std::ifstream& inf) {
        std::string line;
        IfDefLineParser parser;
        std::shared_ptr<Chi2HistoLikeliHood> likelihood;
        while (parser(inf, line) == IfDefLineParser::NewProperty) {
            std::stringstream sstr(line);
            if (IsKeyWordSatisfied(sstr, "End_TemplateFitReader")) {
                if (likelihood) {
                    Error("TemplateFitReader::setupTemplates()", "The definition of the previous template has not been finished yet.....");
                    return false;
                }
                return true;
            } else if (IsKeyWordSatisfied(sstr, "New_Template")) {
                if (likelihood) {
                    Error("TemplateFitReader::setupTemplates()", "The definition of the previous template has not been finished yet.....");
                    return false;
                }
                likelihood = std::make_shared<Chi2HistoLikeliHood>();
            } else if (GetWordFromStream(sstr, true).find("Reader") != std::string::npos) {
                if (!likelihood) {
                    Error("TemplateFitReader::setupTemplates()",
                          "No histogram is in prepartion. Please check carefully your config files. Create a new template via "
                          "'New_Template'");
                    return false;
                }
                ITreeVarReader* R = GetWordFromStream(sstr, true).find("New_") == 0 ? ReaderProvider::GetInstance()->CreateReader(inf, line)
                                                                                    : ReaderProvider::GetInstance()->CreateReader(sstr);
                if (!likelihood->set_reader(R)) return false;
            } else if (IsKeyWordSatisfied(sstr, "Histo")) {
                if (!likelihood) {
                    Error("TemplateFitReader::setupTemplates()",
                          "No histogram is in prepartion. Please check carefully your config files. Create a new template via "
                          "'New_Template'");
                    return false;
                }
                if (!likelihood->LoadHistogramFromFile(sstr)) return false;
            } else if (IsKeyWordSatisfied(sstr, "End_Template")) {
                if (!likelihood) {
                    Error("TemplateFitReader::setupTemplates()",
                          "No histogram is in prepartion. Please check carefully your config files. Create a new template via "
                          "'New_Template'");
                    return false;
                }
                if (!likelihood->is_valid()) {
                    Error("TemplateFitReader::setupTemplates()",
                          "The trial to define a chi2 likelihood template hillariously failed. Either no histogram has been loaded or no "
                          "variable is associated.");
                    return false;
                }
                std::vector<std::shared_ptr<Chi2HistoLikeliHood>>::const_iterator itr = std::find_if(
                    m_templates.begin(), m_templates.end(),
                    [likelihood](const std::shared_ptr<Chi2HistoLikeliHood>& l) { return l->reader() == likelihood->reader(); });
                if (itr != m_templates.end()) {
                    Error("TemplateFitReader::setupTemplates()",
                          Form("%s is already used in one template. It's not fair to weight the same variable twice.",
                               likelihood->reader()->name().c_str()));
                    return false;
                }
                m_templates.push_back(likelihood);
                likelihood.reset();
            }
        }
        Error("TemplateFitReader::setupTemplates()", "Missing  'End_TemplateFitReader' statement");
        return false;
    }

}  // namespace XAMPP
