#include <XAMPPtmva/MVATrainApplicationService.h>

#include <TMVA/Reader.h>

namespace XAMPP {
    MVATrainApplicationService* MVATrainApplicationService::m_Inst = nullptr;
    MVATrainApplicationService::~MVATrainApplicationService() {
        delete SciKiyPythonObjectService::getService();
        m_Inst = nullptr;
    }
    MVATrainApplicationService* MVATrainApplicationService::getService() {
        if (m_Inst == nullptr) m_Inst = new MVATrainApplicationService();
        return m_Inst;
    }
    MVATrainApplicationService::MVATrainApplicationService() : m_classifiers() {}
    std::shared_ptr<MVATrainingReader> MVATrainApplicationService::getTraining(const std::string& name, MachineFrameWork M) const {
        for (auto& cl : m_classifiers) {
            if (cl->framework() == M && cl->name() == name) return cl;
        }
        return std::shared_ptr<MVATrainingReader>();
    }
    TMVAReaderHelper* MVATrainApplicationService::getTMVAClassifier(const std::string& name) const {
        return dynamic_cast<TMVAReaderHelper*>(getTraining(name, MachineFrameWork::TMVA).get());
    }
    TMVAReaderHelper* MVATrainApplicationService::TMVAEventClassifier(const std::string& name, const std::string& xml_path) {
        if (!getTMVAClassifier(name))
            m_classifiers.push_back(std::make_shared<TMVAReaderHelper>(name, xml_path, MVATrainingReader::TrainingType::EventClass));
        return getTMVAClassifier(name);
    }
    TMVAReaderHelper* MVATrainApplicationService::TMVAObjectClassifier(const std::string& name, const std::string& xml_path) {
        if (!getTMVAClassifier(name))
            m_classifiers.push_back(std::make_shared<TMVAReaderHelper>(name, xml_path, MVATrainingReader::TrainingType::ObjectClass));

        return getTMVAClassifier(name);
    }

    SciKitMVATrainingReader* MVATrainApplicationService::getSciKitTraining(const std::string& name) const {
        return dynamic_cast<SciKitMVATrainingReader*>(getTraining(name, MachineFrameWork::SciKitLearn).get());
    }
    SciKitMVATrainingReader* MVATrainApplicationService::SciKitEventClassifier(const std::string& name, const std::string& train_path) {
        if (!getSciKitTraining(name)) {
            m_classifiers.push_back(
                std::make_shared<SciKitMVATrainingReader>(name, train_path, MVATrainingReader::TrainingType::EventClass));
        }
        return getSciKitTraining(name);
    }
    SciKitMVATrainingReader* MVATrainApplicationService::SciKitObjectClassifier(const std::string& name, const std::string& train_path) {
        if (!getSciKitTraining(name)) {
            m_classifiers.push_back(
                std::make_shared<SciKitMVATrainingReader>(name, train_path, MVATrainingReader::TrainingType::ObjectClass));
        }
        return getSciKitTraining(name);
    }
    SciKitMVATrainingReader* MVATrainApplicationService::SciKitEventRegressor(const std::string& name, const std::string& train_path) {
        if (!getSciKitTraining(name)) {
            m_classifiers.push_back(
                std::make_shared<SciKitMVATrainingReader>(name, train_path, MVATrainingReader::TrainingType::EventRegression));
        }
        return getSciKitTraining(name);
    }

}  // namespace XAMPP
