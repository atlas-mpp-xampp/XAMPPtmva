#include <XAMPPtmva/SciKitFactoryUtils.h>

#include <PathResolver/PathResolver.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>
namespace XAMPP {
    std::string SciKitLearnMethod::name() const { return m_name; }
    std::string SciKitLearnMethod::script() const { return m_pyscript; }
    void SciKitLearnMethod::write(std::ofstream& stream) {
        writeCmd(stream, false);  // Oridinary trainig and testing
        // Perform one training swapping train
        writeCmd(stream, true);
    }
    void SciKitLearnMethod::writeIOArgs(std::ofstream& stream, bool swapped) {
        stream << " --outDir " << SciKitCacheProvider::getProvider()->getFactory()->outDir() << "/Trainings/";
        stream << " --HDF5File " << SciKitCacheProvider::getProvider()->getFactory()->outDir() << "/"
               << SciKitCacheProvider::getProvider()->getFactory()->outFile();
        stream << " --classifierName " << name();
        if (swapped)
            stream << "_swapped --swapTrainTesting" << std::endl;
        else
            stream << std::endl;
    }
    void SciKitLearnMethod::writeCmd(std::ofstream& stream, bool swapped) {
        stream << "python " << script() << " ";
        for (auto& opt : m_options) stream << " " << opt << " ";
        writeIOArgs(stream, swapped);
    }

    SciKitLearnMethod::SciKitLearnMethod(const std::string& to_execute, const std::string& className,
                                         const std::vector<std::string>& options) :
        m_pyscript(to_execute),
        m_name(className),
        m_options(options) {}
    //########################################################################
    //                      ScikitLearnCache
    //########################################################################
    SciKitLearnCache::SciKitLearnCache(unsigned int T, unsigned int Size) :
        MVABuffer(T, Size),
        m_Factory(SciKitCacheProvider::getProvider()->getFactory()) {}

    SciKitLearnCache::~SciKitLearnCache() {}
    bool SciKitLearnCache::FillEvent(const Cache& Event, const double& Weight) {
        if ((type() & XAMPP::EventType::Background) && (type() & XAMPP::EventType::Training)) {
            return m_Factory->AddBackgroundTrainingEvent(Event, Weight);
        } else if ((type() & XAMPP::EventType::Background) && (type() & XAMPP::EventType::Testing)) {
            return m_Factory->AddBackgroundTestingEvent(Event, Weight);
        } else if ((type() & XAMPP::EventType::Signal) && (type() & XAMPP::EventType::Training)) {
            return m_Factory->AddSignalTrainingEvent(Event, Weight);
        } else if ((type() & XAMPP::EventType::Signal) && (type() & XAMPP::EventType::Testing)) {
            return m_Factory->AddSignalTestingEvent(Event, Weight);
        }
        return false;
    }
    //########################################################################
    //                      SciKitCacheProvider
    //########################################################################
    SciKitCacheProvider* SciKitCacheProvider::getProvider() {
        if (!m_Inst) m_Inst = new SciKitCacheProvider();
        SciKitCacheProvider* Ptr = dynamic_cast<SciKitCacheProvider*>(m_Inst);
        if (Ptr == nullptr)
            Warning("SciKitCacheProvider()", "Another instance of the MVACacheProvider has been loaded. Segmentation is ahead");
        return Ptr;
    }
    SciKitCacheProvider::~SciKitCacheProvider() {}
    SciKitCacheProvider::SciKitCacheProvider() :
        MVACacheProvider(),
        m_SciKitFactory(std::make_shared<SciKitLearnFactory>()),
        m_trainMethods() {
        // Define a flag useful in the train configs
        IfDefFlags::Instance()->AddFlag("isSciKitTraining");
    }
    MVACacheHandler_Ptr SciKitCacheProvider::createHandler(MVALogicalSmp_Ptr Smp, unsigned int TrainSize, unsigned int ValidSize,
                                                           bool isEvHandler) const {
        if (isEvHandler) return std::make_shared<SciKitLearnEventClassification>(Smp, TrainSize, ValidSize);
        return std::make_shared<SciKitLearnObjectClassification>(Smp, TrainSize, ValidSize);
    }
    std::shared_ptr<SciKitLearnFactory> SciKitCacheProvider::getFactory() const { return m_SciKitFactory; }

    bool SciKitCacheProvider::setupFactory() {
        for (auto& feature : getTrainFeatures()) {
            if (!getFactory()->addVariable(feature.name)) return false;
        }
        if (getTrainFeatures().empty()) {
            Error("SciKitCacheProvider::setupFactory()", "No train features given");
            return false;
        }
        return true;
    }
    void SciKitCacheProvider::setOutDirectory(const std::string& outDir) { getFactory()->setDirectory(outDir); }
    void SciKitCacheProvider::setOutFileName(const std::string& filename) {
        getFactory()->setFileName(filename.substr(0, filename.find(".root")) + ".H5");
    }
    bool SciKitCacheProvider::IsMethodKnown(const std::string& method) const {
        for (auto& train : m_trainMethods) {
            if (train->name() == method) return true;
        }
        return false;
    }
    bool SciKitCacheProvider::prepareAndExecuteTraining() {
        if (getBookedMethods().empty()) return true;
        std::ofstream ofs_traincmd;
        std::ofstream ofs_diagnostics;
        std::string cmd_file = Form("%s/%s_trainCmds.txt", getFactory()->outDir().c_str(),  // Training directory
                                    getFactory()->outFile().substr(0, getFactory()->outFile().rfind(".")).c_str());
        ofs_traincmd.open(cmd_file, std::ofstream::out | std::ofstream::trunc);
        std::string diagnostic_file = Form("%s/%s_diagnostic_paths.txt", getFactory()->outDir().c_str(),  // Training directory
                                           getFactory()->outFile().substr(0, getFactory()->outFile().rfind(".")).c_str());
        ofs_diagnostics.open(diagnostic_file, std::ofstream::out | std::ofstream::trunc);

        if (!ofs_traincmd.is_open()) return false;
        unsigned int n = 0;
        for (auto& train : m_trainMethods) {
            if (!IsElementInList(getBookedMethods(), train->name())) continue;
            train->write(ofs_traincmd);
            train->writeIOArgs(ofs_diagnostics, false);
            train->writeIOArgs(ofs_diagnostics, true);
            ++n;
        }
        Info("SciKitCacheProvider::prepareAndExecuteTraining()", Form("Written %d methods in %s", n, cmd_file.c_str()));
        ofs_traincmd.close();
        ofs_diagnostics.close();
        PrintFooter();
        getFactory()->close();
        return true;
    }
    bool SciKitCacheProvider::addTrainingMethod(const std::string& py_script, const std::string& name,
                                                const std::vector<std::string>& options) {
        if (IsMethodKnown(name) || name.empty()) {
            Error("SciKitCacheProvider::addTrainingMethod()", name + " is already known");
            return false;
        }
        if (py_script.empty()) {
            Error("SciKitCacheProvider::addTrainingMethod()", "Please provide a python module to use for " + name);
            return false;
        }
        std::string resolved_script = PathResolverFindCalibDirectory("XAMPPtmva/SciKitLearn/") + py_script +
                                      std::string(py_script.find(".py") == std::string::npos ? ".py" : "");
        if (resolved_script.empty()) {
            Error("SciKitCacheProvider::addTrainingMethod()", "Could not find the python dir 'XAMPPtmva/SciKitLearn'");
            return false;
        }
        m_trainMethods.push_back(std::make_shared<SciKitLearnMethod>(resolved_script, name, options));
        return true;
    }
    //########################################################################
    //                      SciKitLearnEventCache
    //########################################################################
    SciKitLearnEventCache::SciKitLearnEventCache(unsigned int T, unsigned int Size) : SciKitLearnCache(T, Size) {}
    SciKitLearnEventCache::~SciKitLearnEventCache() {}
    bool SciKitLearnEventCache::PushBack(ITreeVarReader* Reader, Cache& Vec) {
        if (Reader->entries() == 1) {
            if (!IsFinite(Reader->readEntry(0))) {
                Error("SciKitLearnEventCache::PushBack()", "The reader " + Reader->name() + " does not return a number.");
                return false;
            }
            Vec.push_back(Reader->readEntry(0));
        } else if (Reader->entries() > 1) {
            Error("SciKitLearnEventCache::PushBack()", "The variable " + Reader->name() + " is not a scalar. What are you doing?!");
            return false;
        } else {
            Error("SciKitLearnEventCache::PushBack()", "Whaaat?! The variable " + Reader->name() + " is empty");
            return false;
        }
        return true;
    }

    //########################################################################
    //                      SciKitLearnEventClassification
    //########################################################################
    SciKitLearnEventClassification::SciKitLearnEventClassification(MVALogicalSmp_Ptr Sample, unsigned int TrainSize,
                                                                   unsigned int ValidSize) :
        MVACacheHandler(Sample, TrainSize, ValidSize) {}
    SciKitLearnEventClassification::~SciKitLearnEventClassification() {}
    std::shared_ptr<MVATrainTestSplitting> SciKitLearnEventClassification::CreateCache(unsigned int Type) {
        return std::make_shared<SciKitLearnEventTrainTestHandler>(Type | EventType::Classification, this);
    }
    //########################################################################
    //                      SciKitLearnEventTrainTestHandler
    //########################################################################
    SciKitLearnEventTrainTestHandler::SciKitLearnEventTrainTestHandler(unsigned int Type, MVACacheHandler* Handler) :
        MVATrainTestSplitting(Type, Handler) {}
    SciKitLearnEventTrainTestHandler::~SciKitLearnEventTrainTestHandler() {}
    std::shared_ptr<MVABuffer> SciKitLearnEventTrainTestHandler::CreateCache(unsigned int T, unsigned int Size) {
        return std::make_shared<SciKitLearnEventCache>(T, Size);
    }
    //#########################################################################################
    //                            SciKitLearnObjectCache
    //#########################################################################################
    SciKitLearnObjectCache::SciKitLearnObjectCache(SciKitLearnObjectTrainTestHandler* Handler, unsigned int T, unsigned int Size) :
        SciKitLearnCache(T, Size),
        m_ObjectHandler(Handler) {}
    SciKitLearnObjectCache::~SciKitLearnObjectCache() {}

    bool SciKitLearnObjectCache::PushBack(ITreeVarReader* Reader, Cache& Vec) {
        if (Reader->entries() <= m_ObjectHandler->TargetEntry()) {
            Error("TMVAObjectCache::PushBack()", Reader->name() + " is not large enough");
            return false;
        }
        double val = Reader->readEntry(m_ObjectHandler->TargetEntry());
        if (!IsFinite(val)) {
            Error("TMVAObjectCache::PushBack()", "The reader " + Reader->name() + " does not return a number.");
            return false;
        }
        Vec.push_back(val);
        return true;
    }
    //#########################################################################################
    //                            SciKitLearnObjectTrainTestHandler
    //#########################################################################################
    SciKitLearnObjectTrainTestHandler::SciKitLearnObjectTrainTestHandler(unsigned int Type, SciKitLearnObjectClassification* Handler) :
        MVATrainTestSplitting(Type, Handler),
        m_parent(Handler) {}
    SciKitLearnObjectTrainTestHandler::~SciKitLearnObjectTrainTestHandler() {}
    unsigned int SciKitLearnObjectTrainTestHandler::TargetEntry() const { return m_parent->TargetEntry(); }
    std::shared_ptr<MVABuffer> SciKitLearnObjectTrainTestHandler::CreateCache(unsigned int T, unsigned int Size) {
        return std::make_shared<SciKitLearnObjectCache>(this, T, Size);
    }
    //#########################################################################################
    //                            SciKitLearnObjectClassification
    //#########################################################################################
    SciKitLearnObjectClassification::SciKitLearnObjectClassification(MVALogicalSmp_Ptr Sample, unsigned int TrainSize,
                                                                     unsigned int ValidSize) :
        MVACacheHandler(Sample, TrainSize, ValidSize) {}
    SciKitLearnObjectClassification::~SciKitLearnObjectClassification() {}
    std::shared_ptr<MVATrainTestSplitting> SciKitLearnObjectClassification::CreateCache(unsigned int Type) {
        return std::make_shared<SciKitLearnObjectTrainTestHandler>(Type | EventType::Classification, this);
    }
    CachingStatus SciKitLearnObjectClassification::CacheEvent(const std::vector<ITreeVarReader*>& Readers) {
        if (!isResponsible()) { return CachingStatus::NotResponsible; }
        if (Readers.empty()) {
            Error("SciKitLearnObjectClassification()", "No variables given");
            return CachingStatus::Failure;
        }
        ITreeVarReader* First = Readers.at(0);
        for (size_t i = 0; i < First->entries(); ++i) {
            setTargetEntry(i);
            if (MVACacheHandler::CacheEvent(Readers) == CachingStatus::Failure) return CachingStatus::Failure;
        }
        return CachingStatus::Done;
    }
}  // namespace XAMPP
