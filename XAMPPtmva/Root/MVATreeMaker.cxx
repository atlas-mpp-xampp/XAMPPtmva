#include "../XAMPPtmva/MVATreeMaker.h"

#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>

#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {

    MVATreeMaker::MVATreeMaker(const std::string& Name, const std::string& TreeName, const std::vector<std::string>& Files) :
        Selector(Name, TreeName, Files),
        m_trainVariables(),
        m_targetVariables(),
        m_DoNegative(true),
        m_doObjTraining(false),
        m_doClassification(true),
        m_SampleCaches(),
        m_GeneralCache(),
        m_trainService(MVATrainingService::getService()) {}
    MVATreeMaker::~MVATreeMaker() {}
    bool MVATreeMaker::AnalyzeEvent() {
        // Reject data input
        if (isData()) {
            Error("MVATreeMaker::AnalyzeEvent()", "You have given data to train TMVA this is not supported");
            return false;
        }
        // Check if negative events should be skipped
        if ((!m_DoNegative && m_weight->GetWeight() < 0.) || m_weight->GetWeight() == 0) { return true; }
        //
        if (!m_trainService->getCounter()->hasCapacity() || !PassCuts(false)) { return true; }

        for (const auto& Sample : m_SampleCaches) {
            CachingStatus Status =
                m_doClassification ? Sample->CacheEvent(m_trainVariables) : Sample->CacheEvent(m_trainVariables, m_targetVariables);
            if (Status == CachingStatus::Done)
                return true;
            else if (Status == CachingStatus::Failure)
                return false;
        }
        return (m_doClassification ? m_GeneralCache->CacheEvent(m_trainVariables)
                                   : m_GeneralCache->CacheEvent(m_trainVariables, m_targetVariables)) == CachingStatus::Done;
    }
    void MVATreeMaker::SetOutputLocation(std::shared_ptr<TFile> File) {
        if (File == m_outFile) {
            Warning("MVATreeMaker::SetOutputLocaltion()", "File " + std::string(m_outFile->GetName()) + " is already known.");
            return;
        } else if (m_outFile) {
            Info("MVATreeMaker::SetOutputLocaltion()", "Close file" + std::string(m_outFile->GetName()) + ".");
            m_outFile->Close();
        }
        m_outFile = File;
    }
    void MVATreeMaker::ObjectTraining() { m_doObjTraining = true; }
    bool MVATreeMaker::initBranches(const std::string& Syst) {
        // Reset the background and Signal event counter
        m_trainService->Reset();
        if (!MVACacheProvider::getProvider()->setupFactory()) return false;
        // The users wants to perform a regression problem.. We need to check if the training variables are disjoint
        // from the true labels
        CopyVector(MVACacheProvider::getProvider()->getTargetVariables(), m_targetVariables);
        CopyVector(MVACacheProvider::getProvider()->getTrainingVariables(), m_trainVariables);

        AppendReader(m_trainVariables);
        AppendReader(m_targetVariables);

        if (!m_doClassification) {
            if (m_targetVariables.empty()) {
                Error("MVATreeMaker()", "Which quantity should I predict if I've nothing to compare against the true value?");
                return false;
            }
            // We need only to check wheather there is a suitable signal definition or not for classification problems
        } else if (!m_trainService->HasSignalDefinition()) {
            Error("MVATreeMaker()", "Nothing has been declared as signal");
            return false;
        }
        // The splitting between training and signal events is invalid. You want to split more events to training than your block size
        if (!m_trainService->TainSamplesConsistent()) {
            Error("MVATreeMaker()", "Found inconsistent training - testing sample configuration");
            return false;
        }
        int GCD = gcd(m_trainService->SplitBlockSize() - m_trainService->Splitting(), m_trainService->Splitting());
        unsigned int TrainSize = (m_trainService->Splitting() / GCD);
        unsigned int ValidSize = ((m_trainService->SplitBlockSize() - m_trainService->Splitting()) / GCD);

        if (!m_doObjTraining) {
            m_GeneralCache = MVACacheProvider::getProvider()->GeneralEventCache(TrainSize, ValidSize);
            m_SampleCaches = MVACacheProvider::getProvider()->createEventCaches(TrainSize, ValidSize);
        } else {
            m_GeneralCache = MVACacheProvider::getProvider()->GeneralObjectCache(TrainSize, ValidSize);
            m_SampleCaches = MVACacheProvider::getProvider()->createObjectCaches(TrainSize, ValidSize);
        }
        if (m_GeneralCache == nullptr || !m_GeneralCache->init()) {
            Error("MVATreeMaker()", "Could not initialize the training caches");
            return false;
        }
        for (auto& smp : m_SampleCaches)
            if (!smp->init()) return false;
        return Selector::initBranches(Syst);
    }

    void MVATreeMaker::RequirePosWeights() { m_DoNegative = false; }
    void MVATreeMaker::FinalizeOutput() {}
    void MVATreeMaker::SetOutputLocation(const std::string&) {}
    bool MVATreeMaker::CheckTraining() const {
        for (auto& Smp : m_SampleCaches) {
            if (!Smp->DrainBuffers() || !Smp->CheckTraining()) { return false; }
        }
        if (!m_GeneralCache->CheckTraining()) { return false; }
        if (m_doClassification) {
            if (!GetNumTrainSig()) {
                Error("MVATreeMaker::CheckTraining()", "No signal training events are given");
                return false;
            }
            if (!GetNumTestSig()) {
                Error("MVATreeMaker::CheckTraining()", "No signal validation events are given");
                return false;
            }
            if (!GetNumTrainBkg()) {
                Error("MVATreeMaker::CheckTraining()", "No background training events are given");
                return false;
            }
            if (!GetNumTestBkg()) {
                Error("MVATreeMaker::CheckTraining()", "No background validation events are given");
                return false;
            }

            Info("MVATreeMaker::CheckTraining()", "Sucessfully made the tree with " + std::to_string(GetNumTrainSig()) + " & " +
                                                      std::to_string(GetNumTestSig()) + " training and testing signal events.");
            Info("MVATreeMaker::CheckTraining()", "For the background " + std::to_string(GetNumTrainBkg()) + " & " +
                                                      std::to_string(GetNumTestBkg()) +
                                                      " events were assigned to be training & testing, respectively.");
        } else {
            long long unsigned int nTrain = GetNumTrainBkg() + GetNumTrainSig();
            long long unsigned int nTest = GetNumTestBkg() + GetNumTestSig();
            if (nTrain == 0) {
                Error("MVATreeMaker::CheckTraining()", "No training events were parsed");
                return false;
            } else if (nTest == 0) {
                Error("MVATreeMaker::CheckTraining()", "No testing events were parsed");
                return false;
            }
            Info("MVATreeMaker::CheckTraining()", "Successfully made the regression tree with " + std::to_string(nTrain) + " and " +
                                                      std::to_string(nTest) + " training and testing events, respectively");
        }
        return true;
    }

    long long unsigned int MVATreeMaker::GetEvents(MVACacheHandler::Events Ev) const {
        long long unsigned int N = (m_GeneralCache.get()->*Ev)();
        for (auto& Smp : m_SampleCaches) { N += (Smp.get()->*Ev)(); }
        return N;
    }
    long long unsigned int MVATreeMaker::GetNumTrainSig() const { return GetEvents(&MVACacheHandler::SigTrainEvents); }
    long long unsigned int MVATreeMaker::GetNumTrainBkg() const { return GetEvents(&MVACacheHandler::BkgTrainEvents); }
    long long unsigned int MVATreeMaker::GetNumTestSig() const { return GetEvents(&MVACacheHandler::SigTestEvents); }
    long long unsigned int MVATreeMaker::GetNumTestBkg() const { return GetEvents(&MVACacheHandler::BkgTestEvents); }
    void MVATreeMaker::RegressionProblem() { m_doClassification = false; }
    void MVATreeMaker::AppendReader(const std::vector<ITreeVarReader*>& Vars) {
        for (const auto& V : Vars) AppendReader(V);
    }
}  // namespace XAMPP
