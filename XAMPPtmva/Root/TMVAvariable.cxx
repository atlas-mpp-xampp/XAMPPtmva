#include <PathResolver/PathResolver.h>
#include <TFile.h>
#include <TMVA/Config.h>
#include <TMVA/IMethod.h>
#include <TMVA/Reader.h>

#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>
#include <XAMPPtmva/MVATrainApplicationService.h>
#include <XAMPPtmva/TMVAvariable.h>

namespace XAMPP {
    //#########################################################################################
    //                              TMVAReaderHelper
    //#########################################################################################
    TMVAReaderHelper::TMVAReaderHelper(const std::string& name, const std::string& XMLPath, MVATrainingReader::TrainingType T) :
        MVATrainingReader(name, XMLPath, MachineFrameWork::TMVA, T),
        m_Reader(std::make_shared<TMVA::Reader>("!Color:!Silent")),
        m_tmva_methods() {}
    std::shared_ptr<TMVA::Reader> TMVAReaderHelper::GetTMVAReader() const { return m_Reader; }
    std::shared_ptr<MVAParserInterface> TMVAReaderHelper::newParserInterface(const std::string& var_name, ITreeVarReader* tree_reader) {
        return std::make_shared<TMVAParserInterface>(GetTMVAReader(), var_name, tree_reader);
    }
    bool TMVAReaderHelper::loadMethods() {
        for (const auto& Method : getBookedMethods()) {
            if (getFilePath().empty()) {
                Error("TMVAReaderHelper::BookMethod()", "No XML path specified");
                return false;
            }
            // Why is this line here?
            TMVA::gConfig().GetIONames().fWeightFileDir = getFilePath().c_str();
            TString WeightFile = Form("%s/TMVAClassification_%s.weights.xml", getFilePath().c_str(), Method.c_str());
            if (DoesFileExist(WeightFile.Data())) {
                m_tmva_methods.push_back(std::shared_ptr<TMVA::IMethod>(GetTMVAReader()->BookMVA(TString(Method.c_str()), WeightFile)));
            } else {
                Error("TMVAReaderHelper()", Form("The Weights file %s does not exist", WeightFile.Data()));
                return false;
            }
        }
        clearBookedMethods();
        return true;
    }
    std::shared_ptr<TMVA::IMethod> TMVAReaderHelper::GetMethod(const std::string& method_name) const {
        for (const auto& method : m_tmva_methods) {
            if (method->GetName() == method_name) return method;
        }
        return std::shared_ptr<TMVA::IMethod>();
    }
    TMVAReaderHelper::~TMVAReaderHelper() { m_tmva_methods.clear(); }
    bool TMVAReaderHelper::parse_toML() { return true; }
    //#########################################################################################
    //                              TMVAParserInterface
    //#########################################################################################
    TMVAParserInterface::TMVAParserInterface(std::shared_ptr<TMVA::Reader> TMVAReader, const std::string& Name,
                                             ITreeVarReader* InputReader) :
        MVAParserInterface(Name, InputReader),
        m_tmva_reader(TMVAReader) {}
    bool TMVAParserInterface::update(size_t target) {
        if (m_tmva_reader) {
            m_tmva_reader->AddVariable(name().c_str(), getCache());
            m_tmva_reader.reset();
        }
        return MVAParserInterface::update(target);
    }
    //#########################################################################################
    //                            TMVAEventMethodScore
    //#########################################################################################
    std::string TMVAEventMethodScore::name() const { return m_helper ? "TMVAEventReader " + m_method_name + " " + m_helper->name() : ""; }
    bool TMVAEventMethodScore::init(TTree*) {
        if (!m_Registered) {
            Error("TMVAEventMethodScore::init()", "Something went wrong creating " + name());
            return false;
        } else if (!m_helper || !m_helper->hasVariables() || !m_helper->isEventClassifier()) {
            Error("TMVAEventMethodScore::init()", "Failed to establish link to TMVA");
            return false;
        } else if (!m_Method) {
            Warning("TMVAEventMethodScore::init()", "The method " + name() + " is not booked");
        }
        m_EventNumber = -1;
        m_Cache = -1.e25;
        return true;
    }
    double TMVAEventMethodScore::read() const {
        if (m_Method && m_EventNumber != Weight::getWeighter()->eventNumber()) {
            m_helper->newEvent();
            m_Cache = m_Method->GetMvaValue();
            m_EventNumber = Weight::getWeighter()->eventNumber();
        }
        return m_Cache;
    }
    bool TMVAEventMethodScore::update() { return m_Method && m_helper->newEvent() && IScalarReader::update(); }
    TMVAEventMethodScore::~TMVAEventMethodScore() {}
    TMVAEventMethodScore::TMVAEventMethodScore(const std::string& Method, const std::string& ClassName) :
        IScalarReader(),
        m_method_name(Method),
        m_helper(MVATrainApplicationService::getService()->getTMVAClassifier(ClassName)),
        m_Method(m_helper ? m_helper->GetMethod(Method).get() : nullptr),
        m_Registered(false),
        m_EventNumber(-1),
        m_Cache(-1.e25) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    TMVAEventMethodScore* TMVAEventMethodScore::GetReader(const std::string& Method, const std::string& ClassName) {
        std::string Name = "TMVAEventReader " + Method + " " + ClassName;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(Name)) { return new TMVAEventMethodScore(Method, ClassName); }
        return dynamic_cast<TMVAEventMethodScore*>(ITreeVarReaderStorage::GetInstance()->GetReader(Name));
    }
    //#########################################################################################
    //                            TMVAObjectMethodScore
    //#########################################################################################
    std::string TMVAObjectMethodScore::name() const { return m_helper ? "TMVAObjectReader " + m_method_name + " " + m_helper->name() : ""; }
    bool TMVAObjectMethodScore::init(TTree*) {
        if (!m_Registered) {
            Error("MVAObjectMethodScore::init()", "Something went wrong creating " + name());
            return false;
        } else if (!m_helper || !m_helper->hasVariables() || m_helper->isEventClassifier()) {
            Error("MVAObjectMethodScore::init()", "Failed to establish link to TMVA");
            return false;
        } else if (!m_Method) {
            Warning("MVAObjectMethodScore::init()", "The method " + name() + " is not booked");
        }
        return true;
    }
    double TMVAObjectMethodScore::readEntry(const size_t I) const {
        if (!m_Method) {
            Warning("MVObjectReader::read()", "No TMVA method has been booked before");
            return -1.e25;
        } else if (!m_helper->newEvent(I)) {
            Warning("MVAObjectReader::readEntry()", "Index " + std::to_string(I) + " is out of range");
            return 1.e25;
        }
        return m_Method->GetMvaValue(0, 0);
    }
    size_t TMVAObjectMethodScore::entries() const { return m_Method ? m_helper->Size() : 0; }
    TMVAObjectMethodScore::TMVAObjectMethodScore(const std::string& Method, const std::string& ClassName) :
        IVectorReader(),
        m_method_name(Method),
        m_helper(MVATrainApplicationService::getService()->getTMVAClassifier(ClassName)),
        m_Method(m_helper ? m_helper->GetMethod(Method).get() : nullptr),
        m_Registered(false) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
    }
    ITreeVectorReader* TMVAObjectMethodScore::GetReader(const std::string& Method, const std::string& ClassName) {
        std::string name = "TMVAObjectReader " + Method + " " + ClassName;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(name)) return new TMVAObjectMethodScore(Method, ClassName);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(name));
    }
}  // namespace XAMPP
