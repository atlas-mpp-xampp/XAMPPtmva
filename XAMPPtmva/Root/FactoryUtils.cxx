#include <XAMPPtmva/FactoryUtils.h>

#include <PathResolver/PathResolver.h>
#include <TFile.h>
#include <TRandom3.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>

namespace XAMPP {
    //#########################################################################################
    //                            MVAEventCounter
    //#########################################################################################
    MVAEventCounter::MVAEventCounter(unsigned int dsid) :
        m_DSID(dsid),
        m_NTrainSignalEvents(0),
        m_NTrainBkgEvents(0),
        m_NTestSignalEvents(0),
        m_NTestBkgEvents(0),
        m_LimitTesting(false),
        m_LimitTraining(false),
        m_LimitTrainEvents(0),
        m_LimitTestEvents(0),
        m_LockSignalEvents(false),
        m_LockBkgEvents(false),
        m_msgLockSignal(false),
        m_msgLockBkg(false),
        m_msgLimitTest(false),
        m_msgLimitTrain(false) {}
    unsigned int MVAEventCounter::DSID() const { return m_DSID; }
    long long unsigned MVAEventCounter::nSignalTrain() const { return m_NTrainSignalEvents; }
    long long unsigned MVAEventCounter::nBackgroundTrain() const { return m_NTrainBkgEvents; }
    long long unsigned MVAEventCounter::nSignalTest() const { return m_NTestSignalEvents; }
    long long unsigned MVAEventCounter::nBackgroundTest() const { return m_NTestBkgEvents; }
    void MVAEventCounter::LockSignal(bool B) { m_LockSignalEvents = B; }
    void MVAEventCounter::LockBackground(bool B) { m_LockBkgEvents = B; }
    bool MVAEventCounter::AddEvent(unsigned int T) {
        if (m_LockSignalEvents && (T | XAMPP::EventType::Signal) == T) {
            if (!m_msgLockSignal) Info("MVAEventCounter()", "The DSID " + std::to_string(DSID()) + " has been locked for signal events");
            m_msgLockSignal = true;
            return false;
        } else if (m_LockBkgEvents && ((T | XAMPP::EventType::Background) == T || (T | XAMPP::EventType::Regression) == T)) {
            if (!m_msgLockBkg) Info("MVAEventCounter()", "The DSID " + std::to_string(DSID()) + " has been locked for background events");
            m_msgLockBkg = true;
            return false;
        } else if (m_LimitTraining && (T | XAMPP::EventType::Training) == T) {
            if (nSignalTrain() + nBackgroundTrain() >= m_LimitTrainEvents) {
                if (!m_msgLimitTrain)
                    Info("MVAEventCounter()", "The limit on " + std::to_string(m_LimitTrainEvents) +
                                                  " training events has been reached for dataset " + std::to_string(DSID()) +
                                                  " reject the event.");
                m_msgLimitTrain = true;
                return false;
            }
        } else if ((T | XAMPP::EventType::Testing) == T && m_LimitTesting) {
            if (nSignalTest() + nBackgroundTest() >= m_LimitTestEvents) {
                if (!m_msgLimitTest)
                    Info("MVAEventCounter()", "The limit on " + std::to_string(m_LimitTestEvents) +
                                                  " testing events has been reached for dataset " + std::to_string(DSID()) +
                                                  " reject the event.");
                m_msgLimitTest = true;
                return false;
            }
        }
        if ((T & XAMPP::EventType::Background) || (T & XAMPP::EventType::Regression)) {
            if (T & XAMPP::EventType::Training)
                ++m_NTrainBkgEvents;
            else if (T & XAMPP::EventType::Testing)
                ++m_NTestBkgEvents;
        } else if (T & XAMPP::EventType::Signal) {
            if (T & XAMPP::EventType::Training)
                ++m_NTrainSignalEvents;
            else if (T & XAMPP::EventType::Testing)
                ++m_NTestSignalEvents;
        }
        return true;
    }
    void MVAEventCounter::LimitTestingEvents(long long unsigned Limit) {
        if (nSignalTrain() > 0 || nBackgroundTrain() > 0 || nSignalTest() > 0 || nBackgroundTest() > 0) return;
        Info("MVAEventCounter()", "Set the testing limit of DSID " + std::to_string(DSID()) + " to " + std::to_string(Limit));
        m_LimitTestEvents = Limit;
        m_LimitTesting = true;
    }
    void MVAEventCounter::LimitTrainingEvents(long long unsigned Limit) {
        if (nSignalTrain() > 0 || nBackgroundTrain() > 0 || nSignalTest() > 0 || nBackgroundTest() > 0) return;
        Info("MVAEventCounter()", "Set the training limit of DSID " + std::to_string(DSID()) + " to " + std::to_string(Limit));
        m_LimitTrainEvents = Limit;
        m_LimitTraining = true;
    }
    void MVAEventCounter::Reset() {
        m_NTrainSignalEvents = m_NTrainBkgEvents = m_NTestSignalEvents = m_NTestBkgEvents = 0;
        m_msgLimitTest = m_msgLimitTrain = false;
    }
    bool MVAEventCounter::hasCapacity() const {
        return !m_LimitTraining || !m_LimitTesting || nSignalTrain() + nBackgroundTrain() < m_LimitTrainEvents ||
               nSignalTest() + nBackgroundTest() < m_LimitTestEvents;
    }
    //#########################################################################################
    //                            MVALogicalTrainTestSample
    //#########################################################################################
    MVALogicalTrainTestSample::MVALogicalTrainTestSample(const std::string& Name) :
        m_Name(Name),
        m_Train(),
        m_Test(),
        m_LimitTesting(false),
        m_LimitTraining(false),
        m_LimitTrainEvents(0),
        m_LimitTestEvents(0),
        m_NTrainEvents(0),
        m_NTestEvents(0) {}
    MVALogicalTrainTestSample::~MVALogicalTrainTestSample() {}
    void MVALogicalTrainTestSample::LockSamples(std::vector<int>& dsids) {
        for (const auto& id : dsids) {
            EventCounterPtr smp = MVATrainingService::getService()->getCounter(id);
            smp->LockBackground(true);
            smp->LockSignal(true);
        }
        dsids.clear();
    }
    bool MVALogicalTrainTestSample::AddTrainSample(const std::vector<int>& Smp) {
        for (auto ID : Smp) {
            if (!AddTrainSample(ID)) { return false; }
        }
        return true;
    }
    bool MVALogicalTrainTestSample::AddTestSample(const std::vector<int>& Smp) {
        for (auto ID : Smp) {
            if (!AddTestSample(ID)) { return false; }
        }
        return true;
    }
    std::string MVALogicalTrainTestSample::name() const { return m_Name; }
    bool MVALogicalTrainTestSample::AddTrainSample(int DSID) {
        if (IsTestDSID(DSID)) return false;
        if (!IsTrainDSID(DSID)) m_Train.push_back(DSID);
        return true;
    }
    bool MVALogicalTrainTestSample::AddTestSample(int DSID) {
        if (IsTrainDSID(DSID)) return false;
        if (!IsTestDSID(DSID)) m_Test.push_back(DSID);
        return true;
    }

    bool MVALogicalTrainTestSample::IsTrainDSID(int DSID) const { return IsElementInList(m_Train, DSID); }
    void MVALogicalTrainTestSample::AddTrainingEvent() {
        if (!m_LimitTraining) return;
        ++m_NTrainEvents;
        if (m_NTrainEvents < m_LimitTrainEvents) return;
        Info("MVALogicalTrainTestSample::AddTrainingEvent()",
             Form("%s has reached its limit of %llu training events. Will now disable all associated dsids", name().c_str(),
                  m_LimitTrainEvents));
        LockSamples(m_Train);

        m_LimitTraining = false;
    }
    void MVALogicalTrainTestSample::AddTestingEvent() {
        if (!m_LimitTesting) return;
        ++m_NTestEvents;
        if (m_NTestEvents < m_LimitTestEvents) return;
        Info("MVALogicalTrainTestSample::AddTestingEvent()",
             Form("%s has reached its limit of %llu validation events. Will now disable all associated dsids", name().c_str(),
                  m_LimitTestEvents));
        LockSamples(m_Test);

        m_LimitTesting = false;
    }

    bool MVALogicalTrainTestSample::IsTestDSID(int DSID) const { return IsElementInList(m_Test, DSID); }
    const std::vector<int>& MVALogicalTrainTestSample::TrainDSIDs() const { return m_Train; }
    const std::vector<int>& MVALogicalTrainTestSample::TestDSIDs() const { return m_Test; }
    bool MVALogicalTrainTestSample::Copy(const MVALogicalTrainTestSample& ToCopy) {
        return AddTrainSample(ToCopy.TrainDSIDs()) && AddTestSample(ToCopy.TestDSIDs());
    }
    void MVALogicalTrainTestSample::LimitTestingEvents(long long unsigned Limit) {
        m_LimitTestEvents = Limit;
        m_LimitTesting = Limit > 0;
    }
    void MVALogicalTrainTestSample::LimitTrainingEvents(long long unsigned Limit) {
        m_LimitTrainEvents = Limit;
        m_LimitTraining = Limit > 0;
    }
    //#########################################################################################
    //                            MVATrainingService
    //#########################################################################################
    MVATrainingService* MVATrainingService::m_Inst = nullptr;
    MVATrainingService::MVATrainingService() :
        // the new TMVA creates the .xml file location out of the DataLoader name and the fWeightFileDir
        m_topology(),
        m_LogicTrainSmp(),
        m_Weighter(Weight::getWeighter()),
        m_InvertSplitting(false),
        m_SplitBase(50),
        m_SplitBlock(100),
        m_NumEvSignal(0),
        m_NumEvBkg(0),
        m_Mode(MVATrainingService::SplitMode::Alternate),
        m_Signals(),
        m_IsSignalEvent(nullptr),
        m_UsedSamples(),
        m_currentSample(),
        m_LastEvent(-1) {}
    MVATrainingService* MVATrainingService::getService() {
        if (!m_Inst) m_Inst = new MVATrainingService();
        return m_Inst;
    }
    bool MVATrainingService::InvertedSplitting() const { return m_InvertSplitting; }
    MVALogicalSmp_Ptr MVATrainingService::getLogicalSample(const std::string& smp_name) {
        for (auto& Smp : m_LogicTrainSmp) {
            if (Smp->name() == smp_name) return Smp;
        }
        MVALogicalSmp_Ptr NewSmp = MVALogicalSmp_Ptr(new MVALogicalTrainTestSample(smp_name));
        m_LogicTrainSmp.push_back(NewSmp);
        return NewSmp;
    }
    std::vector<MVALogicalSmp_Ptr> MVATrainingService::getAllSamples() const { return m_LogicTrainSmp; }
    MVATrainingService::~MVATrainingService() {
        delete MVACacheProvider::getProvider();
        m_Inst = nullptr;
    }
    bool MVATrainingService::isTrainingBkgEvent() { return isTrainingEvent(EventType::Background); }
    bool MVATrainingService::isTrainingSignalEvent() { return isTrainingEvent(EventType::Signal); }
    unsigned int MVATrainingService::GetUsedEvents(unsigned int T) const {
        if (T & EventType::Background)
            return m_NumEvBkg;
        else if (T & EventType::Signal)
            return m_NumEvSignal;
        return m_NumEvBkg + m_NumEvSignal;
    }
    bool MVATrainingService::isTrainingEvent(unsigned int T) {
        // Increment the event counter  for  all  unassigned events
        // To check the last event allow us to call the method multiple times within the same event
        if (m_LastEvent != m_Weighter->eventNumber()) {
            if (T & EventType::Background)
                ++m_NumEvBkg;
            else if (T & EventType::Signal)
                ++m_NumEvSignal;
            else if (T & EventType::Regression)
                ++m_NumEvBkg;
        }
        if (T & EventType::Regression) m_LastEvent = m_Weighter->eventNumber();
        // determine dependent on the SplitMode and used events whats going  on
        if (m_Mode == MVATrainingService::SplitMode::Undefined) {
            Warning("MVASplitter::isTrainingEvent()", "Undefined split mode");
            return false;
        } else if (m_Mode == MVATrainingService::SplitMode::Alternate)
            return (GetUsedEvents(T) % 2 == 1) != m_InvertSplitting;
        else if (m_Mode == SplitMode::Block)
            return (GetUsedEvents(T) % Splitting() < SplitBlockSize()) != m_InvertSplitting;
        else if (m_Mode == SplitMode::Random) {
            TRandom3 Random(GetUsedEvents(EventType::Total));
            return (Random.Rndm() < Splitting()) != m_InvertSplitting;
        } else if (m_Mode == SplitMode::Fraction) {
            return (GetUsedEvents(T) % Splitting() == SplitBlockSize()) != m_InvertSplitting;
        }
        return false;
    }

    bool MVATrainingService::SetSplitMode(const std::string& mode) {
        if (mode.find("Alternate") != std::string::npos)
            m_Mode = SplitMode::Alternate;
        else if (mode.find("Block") != std::string::npos)
            m_Mode = SplitMode::Block;
        else if (mode.find("Random") != std::string::npos)
            m_Mode = SplitMode::Random;
        else
            return false;
        InvertSplitting(mode.find("Inv") != std::string::npos);
        return true;
    }
    bool MVATrainingService::SetSplitMode(MVATrainingService::SplitMode Mode) {
        if (Mode == MVATrainingService::SplitMode::Undefined) return false;
        m_Mode = Mode;
        return true;
    }

    bool MVATrainingService::SetTrainingFraction(unsigned int Split) {
        if (Split > 99 || Split == 0) {
            Error("MVASplitter::SetTrainTestSplitting()", "Please give a number between 1 - 99");
            return false;
        } else {
            m_SplitBase = Split;
        }
        return true;
    }
    void MVATrainingService::InvertSplitting(bool B) { m_InvertSplitting = B; }
    bool MVATrainingService::SetSplitBlockSize(unsigned int Size) {
        if (Size == 0) {
            Error("TMVASplitter::SetSplitBolockSize()", "Zero blocks are meaningless");
            return false;
        }
        if (Splitting() >= Size) {
            Warning("TMVASplitter::SetSplitBlockSize()", "The current splitting of " + std::to_string(Splitting()) +
                                                             " events is bigger than the new block size of " + std::to_string(Size) +
                                                             " events.");
        }
        m_SplitBlock = Size;
        return true;
    }
    unsigned int MVATrainingService::SplitBlockSize() const { return m_SplitBlock; }
    unsigned int MVATrainingService::Splitting() const { return m_SplitBase; }
    void MVATrainingService::Reset() {
        m_NumEvBkg = m_NumEvSignal = 0;
        for (const auto& Smp : m_UsedSamples) Smp->Reset();
    }

    bool MVATrainingService::isSignalEvent() const {
        if (!m_Signals.empty() && isSignalDSID(DSID())) return true;
        if (!m_IsSignalEvent) return true;

        return (isSignalDSID(DSID()) || m_Signals.empty()) && (!m_IsSignalEvent || m_IsSignalEvent->Pass());
    }
    bool MVATrainingService::isSignalDSID(int ds) const { return IsElementInList(m_Signals, ds); }
    void MVATrainingService::SetSignals(const std::vector<int>& DSIDs) { CopyVector(DSIDs, m_Signals); }
    bool MVATrainingService::HasSignalDefinition() const { return !m_Signals.empty() || m_IsSignalEvent; }

    void MVATrainingService::SetEventSignalRequirements(const std::vector<std::shared_ptr<Condition>>& SignalRequirement) {
        for (auto& Cut : SignalRequirement) {
            if (!m_IsSignalEvent) {
                m_IsSignalEvent = Cut;
            } else {
                m_IsSignalEvent = Condition::Combine(m_IsSignalEvent, Cut, Condition::Mode::AND);
                Info("SetEventSignalRequirements()", "Add the following requirement: " + Cut->name());
            }
        }
    }
    EventCounterPtr MVATrainingService::getCounter() const { return getCounter(DSID()); }
    EventCounterPtr MVATrainingService::getCounter(unsigned int dsid) const {
        if (!m_currentSample.get() || m_currentSample->DSID() != dsid) {
            for (const auto& Smp : m_UsedSamples) {
                if (Smp->DSID() == dsid) {
                    m_currentSample = Smp;
                    return m_currentSample;
                }
            }
        } else
            return m_currentSample;
        m_UsedSamples.push_back(EventCounterPtr(new MVAEventCounter(dsid)));
        Info("MVATrainingService", "Found new sample with DSID " + std::to_string(dsid) + ".");
        return getCounter(dsid);
    }
    void MVATrainingService::PromptSampleStats() const {
        std::string header_DSID("DSID");

        std::string header_TrainBkg("Training (Background)");
        std::string header_TrainSig("Training (Signal)");

        std::string header_TestBkg("Testing (Background)");
        std::string header_TestSig("Testing (Signal)");
        std::sort(m_UsedSamples.begin(), m_UsedSamples.end(),
                  [](const EventCounterPtr& a, const EventCounterPtr& b) { return a->DSID() < b->DSID(); });
        unsigned int DSID_Width(header_DSID.size()), TrainBkgWidth(header_TrainBkg.size()), TrainSigWidth(header_TrainSig.size()),
            TestBkgWidth(header_TestBkg.size()), TestSigWidth(header_TestSig.size());
        for (auto Smp : m_UsedSamples) {
            maxLength(Form("%u", Smp->DSID()), DSID_Width);
            maxLength(Form("%llu", Smp->nSignalTrain()), TrainSigWidth);
            maxLength(Form("%llu", Smp->nBackgroundTrain()), TrainBkgWidth);
            maxLength(Form("%llu", Smp->nBackgroundTest()), TestBkgWidth);
            maxLength(Form("%llu", Smp->nSignalTest()), TestSigWidth);
        }

        PrintHeadLine("Input sample statistics");
        std::string line = FormStrIntoTable(header_DSID, DSID_Width) + FormStrIntoTable(header_TrainBkg, TrainBkgWidth) +
                           FormStrIntoTable(header_TestBkg, TestBkgWidth) + FormStrIntoTable(header_TrainSig, TrainSigWidth) +
                           FormStrIntoTable(header_TestSig, TestSigWidth);
        std::cout << line << std::endl;
        for (auto& Smp : m_UsedSamples) {
            if (Smp->nSignalTrain() + Smp->nBackgroundTrain() + Smp->nBackgroundTest() + Smp->nSignalTest() == 0) continue;
            std::cout << FormStrIntoTable(Form("%u", Smp->DSID()), DSID_Width) +
                             FormStrIntoTable(Form("%llu", Smp->nBackgroundTrain()), TrainBkgWidth) +
                             FormStrIntoTable(Form("%llu", Smp->nBackgroundTest()), TestBkgWidth) +
                             FormStrIntoTable(Form("%llu", Smp->nSignalTrain()), TrainSigWidth) +
                             FormStrIntoTable(Form("%llu", Smp->nSignalTest()), TestSigWidth)
                      << std::endl;
        }
        PrintFooter();
    }
    std::string MVATrainingService::FormStrIntoTable(const std::string& str_toprint, unsigned int columnsize) const {
        return GetWhiteSpaces(str_toprint, columnsize, true) + str_toprint + GetWhiteSpaces(str_toprint, columnsize, false);
    }
    void MVATrainingService::maxLength(const std::string& str, unsigned int& Length) const {
        if (str.size() > Length) Length = str.size();
    }

    std::string MVATrainingService::GetWhiteSpaces(const std::string& str_toprint, size_t maxsize, bool Front) const {
        int N = maxsize - str_toprint.size();
        if (N < 0) return WhiteSpaces(0) + (Front ? "" : " | ");
        if (Front) return WhiteSpaces((N - N % 2) / 2);
        return WhiteSpaces((N + N % 2) / 2) + " | ";
    }
    unsigned int MVATrainingService::DSID() const { return m_Weighter->mcChannelNumber(); }
    bool MVATrainingService::TainSamplesConsistent() const {
        for (size_t i = 1; i < getAllSamples().size(); ++i) {
            MVALogicalSmp_Ptr Test = getAllSamples().at(i);
            std::vector<int> DSID_InTest = Test->TrainDSIDs();
            CopyVector(Test->TestDSIDs(), DSID_InTest);
            for (size_t j = 0; j < i; ++j) {
                MVALogicalSmp_Ptr Against = getAllSamples().at(j);
                for (auto ds : DSID_InTest) {
                    if (Against->IsTrainDSID(ds) || Against->IsTestDSID(ds)) {
                        Error("MVATrainingService::TainSamplesConsistent()", "DSID: " + std::to_string(ds) + " is used in " +
                                                                                 Against->name() + " and " + Test->name() +
                                                                                 ". Please disentangle");
                        return false;
                    }
                }
            }
        }
        return true;
    }

    std::string MVATrainingService::topologyLabel() const { return m_topology; }
    void MVATrainingService::setTopologyLabel(const std::string& name) {
        if (!name.empty()) m_topology = name;
    }
    //#########################################################################################
    //                            MVACacheProvider
    //#########################################################################################
    MVACacheProvider* MVACacheProvider::m_Inst = nullptr;
    MVACacheProvider* MVACacheProvider::getProvider() {
        if (!m_Inst) m_Inst = new MVACacheProvider();
        return m_Inst;
    }
    MVACacheProvider::~MVACacheProvider() { m_Inst = nullptr; }
    MVACacheProvider::MVACacheProvider() : m_TrainingVariables(), m_TargetVariables(), m_BookedMethods(), m_trainParticle(nullptr) {}
    void MVACacheProvider::setTrainingObject(IParticleReader* particle) { m_trainParticle = particle; }
    IParticleReader* MVACacheProvider::getTrainingObject() const { return m_trainParticle; }
    bool MVACacheProvider::setupFactory() {
        Error("MVACacheProvider()", "This method must be overwritten");
        return false;
    }
    bool MVACacheProvider::addTrainingVariable(ITreeVarReader* Reader, const std::string& varName, const std::string& label) {
        if (!Reader) {
            Error("MVACacheProvider()", "No ITreeVarReader has been given");
            return false;
        }
        if (varName.empty()) {
            Error("MVACacheProvider()", "Empty variable names are not allowed");
            return false;
        }
        if (IsElementInList(getTrainingVariables(), Reader) || IsElementInList(getTargetVariables(), Reader)) {
            Error("MVACacheProvider()", "The Reader " + Reader->name() + " has already been used for training.");
            return false;
        }
        Info("MVACacheProvider()", "Add new training variable " + varName + " using " + Reader->name() + ".");
        m_TrainingVariables.push_back(MVAFeature(Reader, varName, label));
        std::sort(m_TrainingVariables.begin(), m_TrainingVariables.end(),
                  [](const MVAFeature& a, const MVAFeature& b) { return a.name < b.name; });
        return true;
    }
    bool MVACacheProvider::addTargetVariable(ITreeVarReader* Reader, const std::string& varName, const std::string& label) {
        if (!Reader) {
            Error("MVACacheProvider()", "No ITreeVarReader has  been given");
            return false;
        }
        if (varName.empty()) {
            Error("MVACacheProvider()", "Empty variable names are not allowed");
            return false;
        }
        if (IsElementInList(getTrainingVariables(), Reader) || IsElementInList(getTargetVariables(), Reader)) {
            Error("MVACacheProvider()", "The Reader " + Reader->name() + " has already been used for training.");
            return false;
        }
        Info("MVACacheProvider()", "Add new target variable " + varName + " using " + Reader->name() + ".");

        m_TargetVariables.push_back(MVAFeature(Reader, varName, label));
        std::sort(m_TargetVariables.begin(), m_TargetVariables.end(),
                  [](const MVAFeature& a, const MVAFeature& b) { return a.name < b.name; });

        return true;
    }
    const std::vector<MVAFeature>& MVACacheProvider::getTrainFeatures() const { return m_TrainingVariables; }
    const std::vector<MVAFeature>& MVACacheProvider::getTargetFeatures() const { return m_TargetVariables; }
    std::vector<ITreeVarReader*> MVACacheProvider::getTrainingVariables() const {
        std::vector<ITreeVarReader*> vars;
        vars.reserve(m_TrainingVariables.size());
        for (const auto& feat : m_TrainingVariables) { vars.push_back(feat.reader); }
        return vars;
    }
    std::vector<ITreeVarReader*> MVACacheProvider::getTargetVariables() const {
        std::vector<ITreeVarReader*> vars;
        vars.reserve(m_TargetVariables.size());
        for (const auto& feat : m_TargetVariables) { vars.push_back(feat.reader); }
        return vars;
    }
    MVACacheHandler_Ptr MVACacheProvider::GeneralEventCache(unsigned int TrainSize, unsigned int TestSize) const {
        return createHandler(MVALogicalSmp_Ptr(), TrainSize, TestSize, true);
    }
    MVACacheHandler_Ptr MVACacheProvider::GeneralObjectCache(unsigned int TrainSize, unsigned int TestSize) const {
        return createHandler(MVALogicalSmp_Ptr(), TrainSize, TestSize, false);
    }

    std::vector<MVACacheHandler_Ptr> MVACacheProvider::createEventCaches(unsigned int TrainSize, unsigned int TestSize) const {
        return createCaches(TrainSize, TestSize, true);
    }
    std::vector<MVACacheHandler_Ptr> MVACacheProvider::createObjectCaches(unsigned int TrainSize, unsigned int TestSize) const {
        return createCaches(TrainSize, TestSize, false);
    }
    std::vector<MVACacheHandler_Ptr> MVACacheProvider::createCaches(unsigned int TrainSize, unsigned int TestSize, bool isEvHandler) const {
        std::vector<MVACacheHandler_Ptr> Caches;
        for (const auto& Smp : MVATrainingService::getService()->getAllSamples()) {
            Caches.push_back(createHandler(Smp, TrainSize, TestSize, isEvHandler));
        }
        return Caches;
    }
    MVACacheHandler_Ptr MVACacheProvider::createHandler(MVALogicalSmp_Ptr, unsigned int, unsigned int, bool) const {
        Warning("MVACacheProvider::createHandler()", "This is a dummy instance");
        return MVACacheHandler_Ptr();
    }
    bool MVACacheProvider::prepareAndExecuteTraining() {
        Warning("MVACacheProvider()", "What framework do you want to use for training");
        return true;
    }
    bool MVACacheProvider::bookMethod(const std::string& name) {
        if (!IsMethodKnown(name) || name.empty()) {
            Error("MVACacheProvider()", Form("The method %s is unknown", name.c_str()));
            return false;
        }
        if (!IsElementInList(m_BookedMethods, name)) m_BookedMethods.push_back(name);
        return true;
    }
    bool MVACacheProvider::IsMethodKnown(const std::string&) const {
        Warning("MVACacheProvider()", "What framework do you want to use for training");
        return false;
    }
    bool MVACacheProvider::bookMethods(const std::vector<std::string>& Methods) {
        for (auto& M : Methods) {
            if (!bookMethod(M)) return false;
        }
        return true;
    }
    void MVACacheProvider::setOutFile(std::shared_ptr<TFile>) {
        Warning("MVACacheProvider()", "What framework do you want to use for training");
    }
    void MVACacheProvider::setOutDirectory(const std::string&) {
        Warning("MVACacheProvider()", "What framework do you want to use for training");
    }
    void MVACacheProvider::setOutFileName(const std::string&) {
        Warning("MVACacheProvider()", "What framework do you want to use for training");
    }

    const std::vector<std::string>& MVACacheProvider::getBookedMethods() const { return m_BookedMethods; }
    void MVACacheProvider::clearBookedMethods() {
        if (!m_BookedMethods.empty()) Warning("MVACacheProvider()", Form("I'm unbooking all %lu methods", m_BookedMethods.size()));
        m_BookedMethods.clear();
    }
    //#########################################################################################
    //                            MVABuffer
    //#########################################################################################
    MVABuffer::MVABuffer(unsigned int T, unsigned int Size) :
        m_Type(T),
        m_CacheSize(Size),
        m_weight(Weight::getWeighter()),
        m_CachedEvents(),
        m_CachedWeights(),
        m_NumEvents() {}
    bool MVABuffer::CacheEvent(const std::vector<ITreeVarReader*>& Readers) {
        if (CacheSize() == 0) {
            Error("MVABuffer::CacheEvent()", "Whaaaat?! You want to cache into the nirvana");
            return false;
        }
        if (Readers.empty()) {
            Error("MVABuffer::CacheEvent()", "Whaaat no variables given?!");
            return false;
        }
        // Maximum limit has been reached
        if (!MVATrainingService::getService()->getCounter()->AddEvent(m_Type)) { return true; }
        CacheVector::iterator Event = NewEvent(Readers.size());
        for (auto& R : Readers) {
            if (!PushBack(R, *Event)) { return false; }
        }
        return true;
    }
    unsigned int MVABuffer::CacheSize() const { return m_CacheSize; }
    bool MVABuffer::IsCacheFull() const { return m_CachedEvents.size() >= m_CacheSize; }
    size_t MVABuffer::currently_buffered() const { return m_CachedEvents.size(); }
    bool MVABuffer::FillFactory(unsigned int max_elements) {
        unsigned int FillSize = m_CacheSize <= currently_buffered() ? m_CacheSize : currently_buffered();
        if (FillSize == 0) {
            Warning("MVABuffer::FillFactory()", "Nothing cached yet");
            return true;
        } else if (max_elements > 0 && max_elements < FillSize)
            FillSize = max_elements;
        CacheVector::const_iterator begin = m_CachedEvents.begin();
        Cache::const_iterator w_begin = m_CachedWeights.begin();
        CacheVector::const_iterator Ev = begin;
        bool Failed = false;
        for (Cache::const_iterator W = w_begin; Ev != begin + FillSize && W != w_begin + FillSize; ++Ev, ++W) {
            if (!FillEvent(*Ev, *W)) Failed = true;
            if (Failed) break;
        }
        m_NumEvents += FillSize;
        m_CachedEvents.erase(m_CachedEvents.begin(), m_CachedEvents.begin() + FillSize);
        m_CachedWeights.erase(m_CachedWeights.begin(), m_CachedWeights.begin() + FillSize);
        return !Failed;
    }

    void MVABuffer::Clear() {
        m_CachedEvents.clear();
        m_CachedWeights.clear();
    }
    long long unsigned int MVABuffer::Events() const { return m_NumEvents; }
    MVABuffer::~MVABuffer() {}
    MVABuffer::CacheVector::iterator MVABuffer::NewEvent(unsigned int size) {
        m_CachedEvents.push_back(Cache());
        CacheVector::iterator Event = m_CachedEvents.end() - 1;
        Event->reserve(size);
        m_CachedWeights.push_back(m_weight->GetWeight());
        return Event;
    }
    unsigned int MVABuffer::type() const { return m_Type; }

    //########################################################################
    //              MVACacheHandler
    //########################################################################
    MVACacheHandler::MVACacheHandler(MVALogicalSmp_Ptr Sample, unsigned int TrainSize, unsigned int TestSize) :
        m_Sample(Sample),
        m_TrainSize(TrainSize),
        m_TestSize(TestSize),
        m_Service(MVATrainingService::getService()),
        m_targetEntry(0),
        m_trainParticle(MVACacheProvider::getProvider()->getTrainingObject()),
        m_SignalCache(),
        m_BackgroundCache() {}
    bool MVACacheHandler::DrainBuffers() {
        if (m_SignalCache && !m_SignalCache->DrainBuffers()) return false;
        if (m_BackgroundCache && !m_BackgroundCache->DrainBuffers()) return false;
        return true;
    }
    unsigned int MVACacheHandler::TargetEntry() const { return m_targetEntry; }
    bool MVACacheHandler::setTargetEntry(unsigned int target) {
        m_targetEntry = target;
        if (!m_trainParticle) {
            Error("MVACacheHandler::setTargetEntry()",
                  "Please assign a particle inside your training config when you define 'ObjectTraining <particle_to_train_on>'");
            return false;
        }
        return m_trainParticle->At(target);
    }
    unsigned int MVACacheHandler::TrainSize() const { return m_TrainSize; }
    unsigned int MVACacheHandler::TestSize() const { return m_TestSize; }
    MVACacheHandler::~MVACacheHandler() {}
    bool MVACacheHandler::MVACacheHandler::init() {
        m_SignalCache = CreateCache(EventType::Signal);
        m_BackgroundCache = CreateCache(EventType::Background);
        return m_SignalCache->init() && m_BackgroundCache->init();
    }
    bool MVACacheHandler::isGeneralCache() const { return m_Sample.get() == nullptr; }
    std::string MVACacheHandler::name() const {
        if (!isGeneralCache()) return m_Sample->name();
        return "General Cache";
    }
    bool MVACacheHandler::isResponsible() const {
        return isGeneralCache() || m_Sample->IsTrainDSID(m_Service->DSID()) || m_Sample->IsTestDSID(m_Service->DSID());
    }
    CachingStatus MVACacheHandler::CacheEvent(const std::vector<ITreeVarReader*>& Readers) {
        if (!isResponsible()) { return CachingStatus::NotResponsible; }

        if (m_Service->isSignalEvent()) return m_SignalCache->CacheEvent(Readers);
        return m_BackgroundCache->CacheEvent(Readers);
    }
    CachingStatus MVACacheHandler::CacheEvent(const std::vector<ITreeVarReader*>& train_vars,
                                              const std::vector<ITreeVarReader*>& true_labels) {
        if (!isResponsible()) { return CachingStatus::NotResponsible; }
        CachingStatus state = m_SignalCache->CacheEvent(train_vars, true_labels);
        if (state == CachingStatus::NotResponsible) { return m_BackgroundCache->CacheEvent(train_vars, true_labels); }
        return state;
    }
    long long unsigned int MVACacheHandler::BkgTrainEvents() const { return m_BackgroundCache->TrainingEvents(); }
    long long unsigned int MVACacheHandler::BkgTestEvents() const { return m_BackgroundCache->TestingEvents(); }
    long long unsigned int MVACacheHandler::SigTrainEvents() const { return m_SignalCache->TrainingEvents(); }
    long long unsigned int MVACacheHandler::SigTestEvents() const { return m_SignalCache->TestingEvents(); }
    bool MVACacheHandler::CheckTraining() const {
        if (SigTrainEvents() && !SigTestEvents()) {
            if (isGeneralCache()) {
                Error("MVACacheHandler::CheckTraining()",
                      "Sample " + name() + " contains training signal events but no validation events.");
                return false;
            } else
                Warning("MVACacheHandler::CheckTraining()",
                        "Sample " + name() + " contains training signal events but no validation events.");

        } else if (!SigTrainEvents() && SigTestEvents()) {
            if (isGeneralCache()) {
                Error("MVACacheHandler::CheckTraining()",
                      "Sample " + name() + " contains validation signal events but no training events.");
                return false;
            } else
                Warning("MVACacheHandler::CheckTraining()",
                        "Sample " + name() + " contains validation signal events but no training events.");

        } else if (BkgTrainEvents() && !BkgTestEvents()) {
            if (isGeneralCache()) {
                Error("MVACacheHandler::CheckTraining()",
                      "Sample " + name() + " contains training background events but no validation events.");
                return false;
            } else
                Warning("MVACacheHandler::CheckTraining()",
                        "Sample " + name() + " contains training background events but no validation events.");

        } else if (!BkgTrainEvents() && BkgTestEvents()) {
            if (isGeneralCache()) {
                Error("MVACacheHandler::CheckTraining()",
                      "Sample " + name() + " contains validation background events but no training events.");
                return false;
            } else
                Warning("MVACacheHandler::CheckTraining()",
                        "Sample " + name() + " contains validation background events but no training events.");
        }
        return true;
    }
    MVALogicalSmp_Ptr MVACacheHandler::logicalSample() const { return m_Sample; }

    //########################################################################
    //              MVATrainTestSplitting
    //########################################################################
    MVATrainTestSplitting::MVATrainTestSplitting(unsigned int Type, MVACacheHandler* Handler) :
        m_Type(Type),
        m_Handler(Handler),
        m_Service(MVATrainingService::getService()),
        m_TrainingCache(),
        m_TestCache() {}
    unsigned int MVATrainTestSplitting::type() const { return m_Type; }
    MVATrainTestSplitting::~MVATrainTestSplitting() {}
    bool MVATrainTestSplitting::init() {
        if (m_Type == XAMPP::EventType::Total) {
            Error("MVACacheHandler::init()", "Total is not an option");
            return false;
        }
        m_TrainingCache = CreateCache(m_Type | EventType::Training, m_Handler->TrainSize());
        m_TestCache = CreateCache(m_Type | EventType::Testing, m_Handler->TestSize());
        return true;
    }
    EventType MVATrainTestSplitting::assign_cache() {
        if (!m_Handler->isGeneralCache()) {
            if (m_Handler->logicalSample()->IsTrainDSID(m_Service->DSID()))
                return m_Service->InvertedSplitting() ? EventType::Testing : EventType::Training;
            else
                return m_Service->InvertedSplitting() ? EventType::Training : EventType::Testing;
        } else if (m_Service->isTrainingEvent(m_Type))
            return EventType::Training;
        return EventType::Testing;
    }
    CachingStatus MVATrainTestSplitting::CacheEvent(const std::vector<ITreeVarReader*>& Readers) {
        if ((type() | EventType::Signal) != type() && (type() | EventType::Background) != type()) {
            Error("MVATrainTestSplitting::CacheEvent()",
                  "No signal or background type has been defined. It seems that we're not in a classification problem");
            return CachingStatus::Failure;
        }
        EventType assignment = assign_cache();
        if (assignment == EventType::Training && !m_TrainingCache->CacheEvent(Readers))
            return CachingStatus::Failure;
        else if (assignment == EventType::Testing && !m_TestCache->CacheEvent(Readers))
            return CachingStatus::Failure;
        // Fill the factories
        bool is_general = m_Handler->isGeneralCache();
        if (!is_general || (m_TrainingCache->IsCacheFull() && m_TestCache->IsCacheFull())) {
            if (is_general || assignment == EventType::Training) {
                if (!m_TrainingCache->FillFactory()) return CachingStatus::Failure;
                // if (!is_general) m_Handler->logicalSample()->AddTrainingEvent();
            }
            if (is_general || assignment == EventType::Testing) {
                if (!m_TestCache->FillFactory()) return CachingStatus::Failure;
                //   if (!is_general) m_Handler->logicalSample()->AddTestingEvent();
            }
        }
        return CachingStatus::Done;
    }
    CachingStatus MVATrainTestSplitting::CacheEvent(const std::vector<ITreeVarReader*>& train_vars,
                                                    const std::vector<ITreeVarReader*>& target_vars) {
        // The instance is indeed a classification instance
        if ((type() | EventType::Signal) == type() || (type() | EventType::Background) == type()) {
            Error("MVATrainTestSplitting()", "Classification does not make much sense in this context.");
            return CachingStatus::Failure;
        }

        EventType assignment = assign_cache();

        if (!(type() & assignment)) { return CachingStatus::NotResponsible; }
        if (!m_TrainingCache->CacheEvent(train_vars) || !m_TestCache->CacheEvent(target_vars)) return CachingStatus::Failure;
        bool is_general = m_Handler->isGeneralCache();
        if (!is_general || (m_TrainingCache->IsCacheFull() && m_TestCache->IsCacheFull())) {
            while (m_TrainingCache->currently_buffered() || m_TestCache->currently_buffered()) {
                if (!m_TrainingCache->FillFactory(1) || !m_TestCache->FillFactory(1)) return CachingStatus::Failure;
                if (is_general) continue;
                if (assignment == EventType::Training)
                    m_Handler->logicalSample()->AddTrainingEvent();
                else
                    m_Handler->logicalSample()->AddTestingEvent();
            }
        }
        return CachingStatus::Done;
    }
    bool MVATrainTestSplitting::DrainBuffers() {
        if (m_TrainingCache->currently_buffered() && !m_TrainingCache->FillFactory(m_TrainingCache->currently_buffered())) return false;
        if (m_TestCache->currently_buffered() && !m_TestCache->FillFactory(m_TestCache->currently_buffered())) return false;
        return true;
    }
    long long unsigned int MVATrainTestSplitting::TrainingEvents() const { return m_TrainingCache->Events(); }
    long long unsigned int MVATrainTestSplitting::TestingEvents() const { return m_TestCache->Events(); }

}  // namespace XAMPP
