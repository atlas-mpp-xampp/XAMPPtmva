#include <PathResolver/PathResolver.h>
#include <TF1.h>
#include <TRandom3.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>
#include <XAMPPtmva/MVATrainApplicationService.h>
#include <XAMPPtmva/SciKitMVAvariable.h>
namespace XAMPP {
    SciKiyPythonObjectService* SciKiyPythonObjectService::m_Inst = nullptr;
    SciKiyPythonObjectService* SciKiyPythonObjectService::getService() {
        if (m_Inst == nullptr) m_Inst = new SciKiyPythonObjectService();
        return m_Inst;
    }
    SciKiyPythonObjectService::~SciKiyPythonObjectService() { m_Inst = nullptr; }
    SciKiyPythonObjectService::SciKiyPythonObjectService() :
        m_read_module(nullptr),
        m_scikit_classReader_table(nullptr),
        m_sciKit_class_evaluate(nullptr),
        m_sciKit_class_consistency(nullptr),
        m_scikit_classRegression_table(nullptr),
        m_scikit_regression_evaluate(nullptr),
        m_scikit_regression_consistency(nullptr),
        m_scikit_regression_nTargets(nullptr),
        m_scikit_regression_targetIdx(nullptr),
        m_hasErrors(false) {
        Py_Initialize();
        np::initialize();
        // Load the python module as an shared pointer
        try {
            m_read_module = std::shared_ptr<py_obj>(std::make_shared<py_obj>(boost::python::import("XAMPPtmva.SciKitReader")));
        } catch (...) { setInErrorState(); }
        //  The SciKitReader class in XAMPPtmva/python/python/SciKitReader.py is responsible
        //  to load a specific machine learning training from the disk and to evaluate the
        //  training-classifier based on the signature of the current event. During loading
        //  a consistency check between the variable names in the machine-learning file and the ones
        //  given to the reader from the XAMPP framework is performed
        if (!m_hasErrors) {
            try {
                m_scikit_classReader_table = std::make_shared<py_obj>(m_read_module->attr("SciKitClassReader"));
            } catch (...) { setInErrorState(); }
        }
        if (!m_hasErrors) {
            try {
                m_scikit_classRegression_table = std::make_shared<py_obj>(m_read_module->attr("SciKitRegressionReader"));
            } catch (...) { setInErrorState(); }
        }
        // Get the attributes to evaluate the machine - learning
        if (!m_hasErrors) {
            try {
                m_sciKit_class_evaluate = std::make_shared<py_obj>(m_scikit_classReader_table->attr("evaluateScore"));
            } catch (...) { setInErrorState(); }
        }
        if (!m_hasErrors) {
            try {
                m_scikit_regression_evaluate = std::make_shared<py_obj>(m_scikit_classRegression_table->attr("predict"));
            } catch (...) { setInErrorState(); }
        }
        // Attributes to check the consistency of the training
        if (!m_hasErrors) {
            try {
                m_sciKit_class_consistency = std::make_shared<py_obj>(m_scikit_classReader_table->attr("checkConsistency"));
            } catch (...) { setInErrorState(); }
        }
        if (!m_hasErrors) {
            try {
                m_scikit_regression_consistency = std::make_shared<py_obj>(m_scikit_classRegression_table->attr("checkConsistency"));
            } catch (...) { setInErrorState(); }
        }
        // For regression we need to know how many targets are intended and the index of the target inside the training
        if (!m_hasErrors) {
            try {
                m_scikit_regression_nTargets = std::make_shared<py_obj>(m_scikit_classRegression_table->attr("nTargets"));
            } catch (...) { setInErrorState(); }
        }
        if (!m_hasErrors) {
            try {
                m_scikit_regression_targetIdx = std::make_shared<py_obj>(m_scikit_classRegression_table->attr("targetIdx"));
            } catch (...) { setInErrorState(); }
        }
    }
    std::shared_ptr<py_obj> SciKiyPythonObjectService::initSciKitClassReader(const std::string& training_path,
                                                                             const std::string& train_name) {
        if (!m_hasErrors) {
            try {
                py_obj instance = m_scikit_classReader_table->operator()(boost::python::str(training_path), boost::python::str(train_name));
                return std::make_shared<py_obj>(instance);
            } catch (...) { setInErrorState(); }
        }
        return std::shared_ptr<py_obj>();
    }
    std::shared_ptr<boost::python::list> SciKiyPythonObjectService::newPyList() {
        if (!m_hasErrors) {
            try {
                return std::make_shared<boost::python::list>();
            } catch (...) { setInErrorState(); }
        }
        return std::shared_ptr<boost::python::list>();
    }
    std::shared_ptr<py_obj> SciKiyPythonObjectService::evaluationClassObj() const { return m_sciKit_class_evaluate; }
    std::shared_ptr<py_obj> SciKiyPythonObjectService::consistencyClassObj() const { return m_sciKit_class_consistency; }

    std::shared_ptr<py_obj> SciKiyPythonObjectService::regressionTargetsObj() const { return m_scikit_regression_nTargets; }
    std::shared_ptr<py_obj> SciKiyPythonObjectService::regressionTargetIdxObj() const { return m_scikit_regression_targetIdx; }

    std::shared_ptr<py_obj> SciKiyPythonObjectService::initSciKitRegressionReader(const std::string& training_path,
                                                                                  const std::string& train_name) {
        if (!m_hasErrors) {
            try {
                py_obj instance =
                    m_scikit_classRegression_table->operator()(boost::python::str(training_path), boost::python::str(train_name));
                return std::make_shared<py_obj>(instance);
            } catch (...) { setInErrorState(); }
        }
        return std::shared_ptr<py_obj>();
    }
    std::shared_ptr<py_obj> SciKiyPythonObjectService::evaluationRegressionObj() const { return m_scikit_regression_evaluate; }
    std::shared_ptr<py_obj> SciKiyPythonObjectService::consistencyRegressionObj() const { return m_scikit_regression_consistency; }

    bool SciKiyPythonObjectService::hasErrors() const { return m_hasErrors; }
    void SciKiyPythonObjectService::setInErrorState() {
        PyErr_Print();
        m_hasErrors = true;
    }
    bool SciKiyPythonObjectService::importTrainingModule(const std::string& module) {
        if (hasErrors()) return false;
        // The module has already been imported
        if (m_scikit_class.find(module) != m_scikit_class.end()) { return true; }
        try {
            std::shared_ptr<py_obj> py_module = std::make_shared<py_obj>(boost::python::import(Form("XAMPPtmva.%s", module.c_str())));
            m_scikit_class.insert(std::pair<std::string, std::shared_ptr<py_obj>>(module, py_module));
        } catch (...) { setInErrorState(); }
        return true;
    }
    //##################################################################
    //                  SciKitChunkBufferService
    //##################################################################
    SciKitChunkBufferService* SciKitChunkBufferService::m_Inst = nullptr;
    SciKitChunkBufferService* SciKitChunkBufferService::getService() {
        if (m_Inst == nullptr) m_Inst = new SciKitChunkBufferService();
        return m_Inst;
    }
    SciKitChunkBufferService::SciKitChunkBufferService() :
        m_eventService(EventService::getService()),
        m_treeNumber(-1),
        m_updated_last_time(-1),
        m_buffer_size(50000),
        m_score_buffers() {}
    SciKitChunkBufferService::~SciKitChunkBufferService() { m_Inst = nullptr; }
    bool SciKitChunkBufferService::renew_buffer() const {
        if (m_score_buffers.empty()) return false;
        // A new TTree has loaded in the background
        if (m_treeNumber != m_eventService->n_thTree()) return true;
        // Important for the very first event
        if (m_updated_last_time > m_eventService->currentEvent()) return true;
        // We must renew the buffer since we ran out of validity
        if (m_updated_last_time + m_buffer_size <= m_eventService->currentEvent()) return true;
        return false;
    }
    unsigned int SciKitChunkBufferService::buffer_size() const { return m_buffer_size; }
    Long64_t SciKitChunkBufferService::last_updated() const { return m_updated_last_time; }
    bool SciKitChunkBufferService::fill_buffer() {
        if (!renew_buffer()) return true;

        Long64_t event_in_chain = m_eventService->currentEvent();
        Long64_t max_ev = event_in_chain + m_buffer_size;
        // define the number of events to be filled
        unsigned int n_ev = m_buffer_size;
        if (max_ev > m_eventService->entries()) {
            max_ev = m_eventService->entries();
            n_ev = max_ev - event_in_chain;
        }

        // Tell the buffers to create a new numpy array
        for (auto& B : m_score_buffers) { B->newSizeBuffer(n_ev); }
        // Now find out how large the data-buffer actually needs to be
        // That can be used for object classification as well
        unsigned int n = 0;
        for (Long64_t k = event_in_chain; k < max_ev; ++k, ++n) {
            if (!m_eventService->getEntry(k)) {
                Error("SciKitChunkBufferService()", "Failed to load the event");
                return false;
            }
            for (auto& B : m_score_buffers) { B->getNumObjects(n); }
        }
        // Make use of what we've found out and create the data buffers themselves
        bool has_valid = false;
        for (auto& B : m_score_buffers) {
            B->newDataBuffer();
            has_valid = has_valid || B->needed_buffer_space() > 0;
        }
        // The second loop is only worth if at least one of the buffers
        // is not empty
        if (has_valid) {
            n = 0;
            for (Long64_t k = event_in_chain; k < max_ev; ++k, ++n) {
                // Add the event to the buffer
                if (!m_eventService->getEntry(k)) {
                    Error("SciKitChunkBufferService()", "Failed to load the event");
                    return false;
                }
                for (auto& B : m_score_buffers) {
                    if (!B->addToBuffer(n)) { return false; }
                }
            }
        }
        // Now we need to propagate the update everywhere
        for (auto B : m_score_buffers) {
            if (!B->propagate_update()) {
                Warning("SciKitChunkBufferService()", "Failed to propagate the update");
                return false;
            }
        }
        // Update the information that we've cached the event right now
        m_treeNumber = m_eventService->n_thTree();
        m_updated_last_time = event_in_chain;
        return m_buffer_size == 1 || m_eventService->getEntry(event_in_chain);
    }
    void SciKitChunkBufferService::Register(SciKitDataReadBuffer* B) {
        if (B != nullptr && !IsElementInList(m_score_buffers, B)) m_score_buffers.push_back(B);
    }
    //##################################################################
    //                  SciKitDataReadBuffer
    //##################################################################
    SciKitDataReadBuffer::SciKitDataReadBuffer(const std::vector<std::shared_ptr<SciKitScoreParser>>& T) :
        m_python_service(SciKiyPythonObjectService::getService()),
        m_buffer_service(SciKitChunkBufferService::getService()),
        m_data_buffer(),
        m_size_buffer(),
        m_interfaces(),
        m_trainings(T),
        m_N(0),
        m_Space(0) {
        m_buffer_service->Register(this);
    }
    void SciKitDataReadBuffer::append(MVAParserInterface* I) {
        if (I != nullptr && !IsElementInList(m_interfaces, I)) {
            m_interfaces.push_back(I);
            std::sort(m_interfaces.begin(), m_interfaces.end(),
                      [](const MVAParserInterface* a, const MVAParserInterface* b) { return a->name() < b->name(); });
            ++m_N;
        }
    }
    void SciKitDataReadBuffer::newSizeBuffer(unsigned int n_ev) {
        m_size_buffer = std::make_shared<SciKitNumPyArray<unsigned int>>(n_ev, 2);
        m_Space = 0;
    }
    unsigned int SciKitDataReadBuffer::min_size() const {
        unsigned int min = -1;
        for (const auto& buffer : m_interfaces) {
            if (buffer->Size() < min) min = buffer->Size();
            if (min == 0) return min;
        }
        return min;
    }
    bool SciKitDataReadBuffer::getNumObjects(unsigned int pos) {
        if (!m_size_buffer->set(pos, 0, m_Space)) return false;
        unsigned int N = min_size();
        m_Space += N;
        return m_size_buffer->set(pos, 1, N);
    }
    void SciKitDataReadBuffer::newDataBuffer() {
        if (m_Space > 0) {
            m_data_buffer = std::make_unique<SciKitNumPyArray<float>>(m_Space, m_N);
        } else {
            m_data_buffer.reset();
        }
    }
    bool SciKitDataReadBuffer::addToBuffer(unsigned int pos) {
        // Nothing sensible is added in this round
        if (m_Space == 0) return true;
        // The size buffer is really needed. We need to ensure that the size meta
        // data is present
        if (!m_size_buffer) {
            Error("SciKitDataReadBuffer::addToBuffer()", "No data buffer has been created");
            return false;
        }
        unsigned int begin = m_size_buffer->get(pos, 0);
        unsigned int N = m_size_buffer->get(pos, 1);
        // Nothing needs to be done if the buffer is empty this event
        if (N == 0) return true;
        std::vector<MVAParserInterface*>::const_iterator end_itr = m_interfaces.end();
        for (unsigned int i = 0; i < N; ++i) {
            std::vector<MVAParserInterface*>::const_iterator d = m_interfaces.begin();
            for (unsigned int k = 0; d != end_itr && k < m_N; ++d, ++k) {
                if (!(*d)->update(i) || !m_data_buffer->set(begin + i, k, *(*d)->getCache())) {
                    Error("SciKitDataReadBuffer::addToBuffer()", "Failed to cache event..");
                    return false;
                }
            }
        }
        return true;
    }
    bool SciKitDataReadBuffer::propagate_update() {
        for (auto& T : m_trainings) {
            T->setSizeInformation(m_size_buffer);
            if (m_Space > 0 && !T->updateScores(m_data_buffer->get_data())) return false;
        }
        m_data_buffer.reset();
        return true;
    }
    unsigned int SciKitDataReadBuffer::needed_buffer_space() const { return m_Space; }
    //######################################################
    //              SciKitMVATrainingReader
    //######################################################
    SciKitMVATrainingReader::SciKitMVATrainingReader(const std::string& training, const std::string& file_path,
                                                     MVATrainingReader::TrainingType T) :
        MVATrainingReader(training, file_path, MachineFrameWork::SciKitLearn, T),
        m_trainings(),
        m_data_buffer(std::make_shared<SciKitDataReadBuffer>(m_trainings)) {}
    std::shared_ptr<MVAParserInterface> SciKitMVATrainingReader::newParserInterface(const std::string& var_name,
                                                                                    ITreeVarReader* tree_reader) {
        return std::make_shared<SciKitMVAParserInterface>(var_name, tree_reader, m_data_buffer);
    }
    bool SciKitMVATrainingReader::parse_toML() { return true; }

    std::shared_ptr<SciKitScoreParser> SciKitMVATrainingReader::addScoreParser(const std::string& method) {
        std::shared_ptr<SciKitScoreParser> newReader;
        if (isClassifier())
            newReader = std::make_shared<SciKitScoreClassParser>(getFilePath(), method);
        else
            newReader = std::make_shared<SciKitScoreRegressionParser>(getFilePath(), method);
        if (!newReader->checkConsistency(trainVarNames())) return std::shared_ptr<SciKitScoreParser>();
        return newReader;
    }
    bool SciKitMVATrainingReader::loadMethods() {
        for (const auto& method : getBookedMethods()) {
            if (method.empty()) {
                Error("SAciKitMVATrainingReader()", "Empty method names are not allowed");
                return false;
            }
            for (auto& score : m_trainings) {
                if (score->method() == method) return true;
            }
            std::shared_ptr<SciKitScoreParser> newReader = addScoreParser(method);
            std::shared_ptr<SciKitScoreParser> cross_valid = addScoreParser(method + "_swapped");
            if (!newReader || !cross_valid) {
                Error("SciKitMVATrainingReader::loadMethods()", "Failed to load methods");
                return false;
            }

            m_trainings.push_back(newReader);
            m_trainings.push_back(cross_valid);
        }
        return true;
    }
    std::shared_ptr<SciKitScoreParser> SciKitMVATrainingReader::getMethod(const std::string& method) const {
        for (auto& score : m_trainings) {
            if (score->method() == method) return score;
        }
        Warning("SciKitMVATrainingReader()", "The method " + method + " has not been booked yet");
        return std::shared_ptr<SciKitScoreParser>();
    }
    //######################################################
    //              SciKitMVAParserInterface
    //######################################################
    SciKitMVAParserInterface::SciKitMVAParserInterface(const std::string& variable_name, ITreeVarReader* reader,
                                                       std::shared_ptr<SciKitDataReadBuffer> cache) :
        MVAParserInterface(variable_name, reader) {
        cache->append(this);
    }

    //######################################################
    //              SciKitScoreParser
    //######################################################
    SciKitScoreParser::SciKitScoreParser(const std::string& method) :
        m_python_service(SciKiyPythonObjectService::getService()),
        m_buffer_service(SciKitChunkBufferService::getService()),
        m_event_service(EventService::getService()),
        m_method(method),
        m_score_buffer(nullptr),
        m_size_buffer(nullptr),
        m_cols(0) {}
    std::string SciKitScoreParser::method() const { return m_method; }
    void SciKitScoreParser::setSizeInformation(std::shared_ptr<SciKitNumPyArray<unsigned int>> buffer) { m_size_buffer = buffer; }
    SciKitScoreParser::~SciKitScoreParser() {}
    unsigned int SciKitScoreParser::nColumns() const { return m_cols; }
    void SciKitScoreParser::setColumns(unsigned int c) { m_cols = c; }
    unsigned int SciKitScoreParser::entries() const {
        if (!m_buffer_service->fill_buffer() || !m_size_buffer) {
            Warning("SciKitScoreParser::entries()", "No meta information saved.");
            return 0;
        }
        unsigned int e = m_event_service->currentEvent() - m_buffer_service->last_updated();
        return m_size_buffer->get(e, 1);
    }
    double SciKitScoreParser::evaluate(unsigned int n, unsigned int col) {
        if (m_python_service->hasErrors()) {
            Error("SciKitScoreParser::evaluate()", "Something really strange happened and screwed everything up");
            return 1.e25;
        } else if (!m_buffer_service->fill_buffer()) {
            Warning("SciKitScoreParser::evaluate()", "Failed to fill the buffer");
            return false;
        }
        unsigned int e = m_event_service->currentEvent() - m_buffer_service->last_updated();
        if (!m_score_buffer || !m_size_buffer) {
            Warning("SciKitScoreParser::get_score()", " Could not retrieve the score. Return random number");
            return 1.e21;
        }
        unsigned int N = m_size_buffer->get(e, 1);
        if (n >= N) {
            Warning("ScikitScorParser::get_score()", "Recieved invalid index. Return random number");
            return 1.e25;
        }
        unsigned int begin = m_size_buffer->get(e, 0);
        return m_score_buffer->get(begin + n, col);
    }
    bool SciKitScoreParser::updateScores(std::shared_ptr<np::ndarray> data_buffer) {
        if (data_buffer == nullptr) {
            Error("SciKitScoreParser::evaluate()", "Where is my numpy array? I parsed it and you destroyed it, you little :(");
            return false;
        } else if (m_python_service->hasErrors())
            return false;

        try {
            np::ndarray my_obj = np::from_object((*evaluation_obj())(*train_obj(), (*data_buffer)), np::dtype::get_builtin<double>());
            m_score_buffer = std::make_unique<SciKitNumPyArray<double>>(my_obj);
        } catch (...) {
            m_python_service->setInErrorState();
            return false;
        }
        return true;
    }
    bool SciKitScoreParser::checkConsistency(const std::vector<std::string>& train_varNames) {
        if (m_python_service->hasErrors()) return false;
        std::shared_ptr<boost::python::list> vars_in_python = m_python_service->newPyList();
        if (!vars_in_python) return false;
        for (auto& var_name : train_varNames) {
            try {
                vars_in_python->append(boost::python::str(var_name));
            } catch (...) {
                m_python_service->setInErrorState();
                return false;
            }
        }
        try {
            return boost::python::extract<bool>((*consistency_obj())(*train_obj(), *vars_in_python));
        } catch (...) {
            m_python_service->setInErrorState();
            return false;
        }
        return false;
    }
    //###################################################################
    //                 SciKitScoreClassParser                           #
    //###################################################################
    SciKitScoreClassParser::SciKitScoreClassParser(const std::string& train_path, const std::string& method) :
        SciKitScoreParser(method),
        m_python_service(SciKiyPythonObjectService::getService()),
        m_py_reader(m_python_service->initSciKitClassReader(train_path, method)) {}
    std::shared_ptr<py_obj> SciKitScoreClassParser::train_obj() const { return m_py_reader; }
    std::shared_ptr<py_obj> SciKitScoreClassParser::evaluation_obj() const { return m_python_service->evaluationClassObj(); }
    std::shared_ptr<py_obj> SciKitScoreClassParser::consistency_obj() const { return m_python_service->consistencyClassObj(); }
    //###################################################################
    //                 SciKitScoreRegressionParser                      #
    //###################################################################
    SciKitScoreRegressionParser::SciKitScoreRegressionParser(const std::string& train_path, const std::string& method) :
        SciKitScoreParser(method),
        m_python_service(SciKiyPythonObjectService::getService()),
        m_py_reader(m_python_service->initSciKitRegressionReader(train_path, method)),
        m_diag_file(TFile::Open(Form("%s/%s.root", train_path.c_str(), method.c_str()), "READ")) {
        if (!m_diag_file || !m_diag_file->IsOpen()) {
            Warning("SciKitScoreRegressionParser()",
                    Form("Could not find diagnostic file %s/%s.root. This file is essential to extract the resolutions", train_path.c_str(),
                         method.c_str()));
            m_diag_file.reset();
        }
    }
    std::shared_ptr<py_obj> SciKitScoreRegressionParser::train_obj() const { return m_py_reader; }
    std::shared_ptr<py_obj> SciKitScoreRegressionParser::evaluation_obj() const { return m_python_service->evaluationRegressionObj(); }
    std::shared_ptr<py_obj> SciKitScoreRegressionParser::consistency_obj() const { return m_python_service->consistencyRegressionObj(); }
    bool SciKitScoreRegressionParser::checkConsistency(const std::vector<std::string>& train_varNames) {
        if (!SciKitScoreParser::checkConsistency(train_varNames)) return false;
        try {
            unsigned int targets = boost::python::extract<int>((*m_python_service->regressionTargetsObj())(*train_obj()));
            setColumns(targets);

        } catch (...) {
            m_python_service->setInErrorState();
            return false;
        }
        return true;
    }
    unsigned int SciKitScoreRegressionParser::tagetIndex(const std::string& var_name) const {
        try {
            return boost::python::extract<int>((*m_python_service->regressionTargetIdxObj())(*train_obj(), boost::python::str(var_name)));

        } catch (...) {
            m_python_service->setInErrorState();
            return -1;
        }
        return -1;
    }
    std::shared_ptr<TF1> SciKitScoreRegressionParser::resolution(const std::string& var_name) const {
        TH1* reso_histo = nullptr;
        if (!m_diag_file) {
            Warning("SciKitScoreRegressionParser::resolution()", "diagnostics file corrupted");
            return std::shared_ptr<TF1>();
        }
        m_diag_file->GetObject(Form("Diagnostics/Systematics_Testing/MVA_%s", var_name.c_str()), reso_histo);
        if (!reso_histo) {
            Warning("SciKitScoreRegressionParser::resolution()", Form("No resolution histogram found for variable %s.", var_name.c_str()));
            return std::shared_ptr<TF1>();
        }

        std::shared_ptr<TF1> fit_func = std::make_shared<TF1>(RandomString(53).c_str(), "gaus(0)", -1, 1);
        reso_histo->Fit(fit_func.get(), "EMRN0S", "", -1, 1);
        return fit_func;
    }
    //#########################################################################################
    //                            SciKitEventScoreVarReader
    //#########################################################################################
    std::string SciKitEventScoreVarReader::name() const {
        return m_helper ? "SciKitEventScoreVarReader " + m_Method + " " + m_helper->name() : "";
    }
    bool SciKitEventScoreVarReader::init(TTree*) {
        if (!m_Registered) {
            Error("SciKitEventScoreVarReader::init()", "Something went wrong creating " + name());
            return false;
        } else if (!m_helper || !m_helper->hasVariables() || !m_helper->isEventClassifier()) {
            Error("SciKitEventScoreVarReader::init()", "Failed to establish link to SciKitLearn");
            return false;
        } else if (!m_MVA) {
            Warning("SciKitEventScoreVarReader::init()", "The method " + name() + " is not booked");
            m_Booked = false;
        } else if (SciKiyPythonObjectService::getService()->hasErrors()) {
            Error("SciKitEventScoreVarReader::init()", "Problems with the python interface encountered");
            return false;
        }
        m_EventNumber = -1;
        m_Cache = -1.e25;
        return true;
    }
    double SciKitEventScoreVarReader::read() const {
        if (m_Booked && m_EventNumber != m_eventService->currentEvent()) {
            m_Cache = m_MVA->evaluate(0, 0);
            m_EventNumber = m_eventService->currentEvent();
        }
        return m_Cache;
    }
    bool SciKitEventScoreVarReader::update() { return m_Booked && entries() > 0 && IScalarReader::update(); }
    size_t SciKitEventScoreVarReader::entries() const { return m_MVA->entries() == 1 ? 1 : 0; }

    SciKitEventScoreVarReader::~SciKitEventScoreVarReader() {}
    SciKitEventScoreVarReader::SciKitEventScoreVarReader(const std::string& Method, const std::string& ClassName) :
        IScalarReader(),
        m_Method(Method),
        m_helper(MVATrainApplicationService::getService()->getSciKitTraining(ClassName)),
        m_eventService(EventService::getService()),
        m_MVA(),
        m_Registered(false),
        m_EventNumber(-1),
        m_Cache(-1.e25),
        m_Booked(true) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
        if (m_helper)
            m_MVA = m_helper->getMethod(Method);
        else
            m_Registered = false;
    }
    ITreeVarReader* SciKitEventScoreVarReader::GetReader(const std::string& Method, const std::string& ClassName) {
        std::string Name = "SciKitEventScoreVarReader " + Method + " " + ClassName;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(Name)) { return new SciKitEventScoreVarReader(Method, ClassName); }
        return ITreeVarReaderStorage::GetInstance()->GetReader(Name);
    }
    //#########################################################################################
    //                            SciKitRegressionSystHandler
    //#########################################################################################

    std::string SciKitRegressionSystHandler::syst_prestring() const {
        return Form("SciKitRegression_%s%s", m_helper->name().c_str(), m_MVA->method().c_str());
    }
    SciKitRegressionSystHandler::SciKitRegressionSystHandler(const std::string& method, const std::string& className,
                                                             const std::string& target_var) :
        m_helper(MVATrainApplicationService::getService()->getSciKitTraining(className)),
        m_MVA(nullptr),
        m_target_variable(target_var),
        m_res_func(),
        m_up_syst(std::make_shared<SciKitRegressionSystmatic>(this, true)),
        m_down_syst(std::make_shared<SciKitRegressionSystmatic>(this, false)),
        m_syst(1.) {
        if (m_helper) m_MVA = dynamic_cast<SciKitScoreRegressionParser*>(m_helper->getMethod(method).get());
        if (m_MVA) m_res_func = m_MVA->resolution(target_var);
        if (!EventService::getService()->registerKinematic(m_up_syst)) m_up_syst.reset();
        if (!EventService::getService()->registerKinematic(m_down_syst)) m_down_syst.reset();
    }
    bool SciKitRegressionSystHandler::is_good() const { return true; }
    std::string SciKitRegressionSystHandler::target_variable() const { return m_target_variable; }
    SciKitMVATrainingReader* SciKitRegressionSystHandler::get_helper() const { return m_helper; }

    //#########################################################################################
    //                            SciKitRegressionSystmatic
    //#########################################################################################
    SciKitRegressionSystHandler::SciKitRegressionSystmatic::SciKitRegressionSystmatic(SciKitRegressionSystHandler* instance, bool is_up) :
        m_reference(instance),
        m_is_up(is_up) {}
    std::string SciKitRegressionSystHandler::SciKitRegressionSystmatic::SciKitRegressionSystmatic::syst_name() const {
        return Form("%s__%s", m_reference->syst_prestring().c_str(), m_is_up ? "Up" : "Dn");
    }
    std::string SciKitRegressionSystHandler::SciKitRegressionSystmatic::SciKitRegressionSystmatic::tree_name() const {
        return EventService::getService()->getNominal()->tree_name();
    }
    bool SciKitRegressionSystHandler::SciKitRegressionSystmatic::SciKitRegressionSystmatic::is_nominal() const { return false; }
    bool SciKitRegressionSystHandler::SciKitRegressionSystmatic::SciKitRegressionSystmatic::apply_syst() {
        if (!m_reference->m_res_func) { return false; }
        m_reference->m_syst = 1 + (m_is_up ? m_reference->m_res_func->GetParameter(2) : -m_reference->m_res_func->GetParameter(2));
        return true;
    }
    void SciKitRegressionSystHandler::SciKitRegressionSystmatic::SciKitRegressionSystmatic::reset() { m_reference->m_syst = 1; }

    //#########################################################################################
    //                            SciKitEventRegressionVarReader
    //#########################################################################################
    SciKitEventRegressionVarReader::SciKitEventRegressionVarReader(const std::string& method, const std::string& className,
                                                                   const std::string& target_var) :
        IScalarReader(),
        m_systHandler(new SciKitRegressionSystHandler(method, className, target_var)),
        m_eventService(EventService::getService()),
        m_targetIdx(-1),
        m_Registered(false),
        m_EventNumber(-1),
        m_Cache(1.e25) {
        m_Registered = ITreeVarReaderStorage::GetInstance()->Register(this);
        if (m_systHandler->get_parser())
            m_targetIdx = m_systHandler->get_parser()->tagetIndex(target_var);
        else {
            m_Registered = false;
        }
    }
    SciKitEventRegressionVarReader::~SciKitEventRegressionVarReader() {}
    bool SciKitEventRegressionVarReader::update() { return entries() > 0 && IScalarReader::update(); }
    size_t SciKitEventRegressionVarReader::entries() const { return m_systHandler->get_parser()->entries() == 1 ? 1 : 0; }
    double SciKitEventRegressionVarReader::read() const {
        if (m_EventNumber != m_eventService->currentEvent()) {
            m_Cache = m_systHandler->get_parser()->evaluate(0, m_targetIdx) * m_systHandler->scale_syst();
            m_EventNumber = m_eventService->currentEvent();
        }
        return m_Cache;
    }
    std::string SciKitEventRegressionVarReader::name() const {
        return m_systHandler->get_helper() ? "SciKitRegression " + m_systHandler->get_parser()->method() + "_" +
                                                 m_systHandler->target_variable() + " " + m_systHandler->get_helper()->name()
                                           : "";
    }

    bool SciKitEventRegressionVarReader::init(TTree*) {
        if (!m_systHandler->is_good()) return false;
        if (!m_Registered) {
            Error("SciKitEventRegressionVarReader::init()", "Something went wrong creating " + name());
            return false;
        } else if (m_targetIdx > m_systHandler->get_parser()->nColumns()) {
            Error("SciKitEventRegressionVarReader::init()", m_systHandler->target_variable() + " is not part of the target set");
            return false;
        } else if (SciKiyPythonObjectService::getService()->hasErrors()) {
            Error("SciKitEventRegressionVarReader::init()", "Problems with the python interface encountered");
            return false;
        }
        m_EventNumber = -1;
        m_Cache = -1.e25;
        return true;
    }
    ITreeVarReader* SciKitEventRegressionVarReader::GetReader(const std::string& Method, const std::string& ClassName,
                                                              const std::string& target_var) {
        std::string r_name = "SciKitRegression " + Method + "_" + target_var + " " + ClassName;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(r_name)) new SciKitEventRegressionVarReader(Method, ClassName, target_var);
        return ITreeVarReaderStorage::GetInstance()->GetReader(r_name);
    }

    //#########################################################################################
    //                            SciKitObjectScoreVarReader
    //#########################################################################################
    std::string SciKitObjectScoreVarReader::name() const {
        return m_helper ? "SciKitObjectScoreVarReader " + m_Method + " " + m_helper->name() : "";
    }
    bool SciKitObjectScoreVarReader::init(TTree*) {
        if (!m_Registered) {
            Error("SciKitObjectScoreVarReader::init()", "Something went wrong creating " + name());
            return false;
        } else if (!m_helper || !m_helper->hasVariables() || m_helper->isEventClassifier()) {
            Error("SciKitObjectScoreVarReader::init()", "Failed to establish link to TMVA");
            return false;
        } else if (!m_MVA) {
            Warning("SciKitObjectScoreVarReader::init()", "The method " + name() + " is not booked");
            m_Booked = false;
        } else if (SciKiyPythonObjectService::getService()->hasErrors()) {
            Error("SciKitObjectScoreVarReader::init()", "Problems with the pyhon interface encountered");
            return false;
        }
        return true;
    }
    double SciKitObjectScoreVarReader::readEntry(const size_t I) const {
        if (I >= entries()) {
            Warning("MVObjectReader::read()", "No SciKitLearn method has been booked before method has been booked before");
            return -1.e25;
        }
        return m_MVA->evaluate(I, 0);
    }
    size_t SciKitObjectScoreVarReader::entries() const {
        if (!m_Booked) return 0;
        return m_MVA->entries();
    }
    SciKitObjectScoreVarReader::SciKitObjectScoreVarReader(const std::string& Method, const std::string& ClassName) :
        m_Method(Method),
        m_helper(MVATrainApplicationService::getService()->getSciKitTraining(ClassName)),
        m_MVA(),
        m_Registered(false),
        m_Booked(true) {
        if (m_helper)
            m_MVA = m_helper->getMethod(Method);
        else
            m_Registered = false;
    }
    ITreeVectorReader* SciKitObjectScoreVarReader::GetReader(const std::string& Method, const std::string& ClassName) {
        std::string name = "SciKitObjectScoreVarReader " + Method + " " + ClassName;
        if (!ITreeVarReaderStorage::GetInstance()->GetReader(name)) return new SciKitObjectScoreVarReader(Method, ClassName);
        return dynamic_cast<ITreeVectorReader*>(ITreeVarReaderStorage::GetInstance()->GetReader(name));
    }

}  // namespace XAMPP
