#include <XAMPPtmva/MVAParticleReader.h>
#include <XAMPPtmva/TMVAvariable.h>

namespace XAMPP {
    //##########################################################
    //                  MVAParticleReader
    //##########################################################
    MVAParticleReader* MVAParticleReader::GetReader(const std::string& Name, const std::string& Coll) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) new MVAParticleReader(Name, Coll);
        return dynamic_cast<MVAParticleReader*>(S->GetReader(Name));
    }
    MVAParticleReader::MVAParticleReader(const std::string& Name, const std::string& Coll) : ParticleReader(Name, Coll) {}
    ITreeVectorReader* MVAParticleReader::AddVariable(std::stringstream& sstr) {
        if (IsKeyWordSatisfied(sstr, "TMVA")) {
            std::string method(GetWordFromStream(sstr)), training(GetWordFromStream(sstr));
            ITreeVectorReader* MVA = TMVAObjectMethodScore::GetReader(method, training);
            if (!IsVariableDefined(method) && !InsertVariable(method, MVA)) return nullptr;
            return MVA;
        }
        return ParticleReader::AddVariable(sstr);
    }

    //##########################################################
    //                  MVAParticleReaderM
    //##########################################################
    MVAParticleReaderM* MVAParticleReaderM::GetReader(const std::string& Name, std::string Coll) {
        ParReaderStorage* S = ParReaderStorage::GetInstance();
        if (!S->GetReader(Name)) new MVAParticleReaderM(Name, Coll);
        return dynamic_cast<MVAParticleReaderM*>(S->GetReader(Name));
    }
    MVAParticleReaderM::MVAParticleReaderM(std::string Name, std::string Coll) : MVAParticleReader(Name, Coll) {}
    std::string MVAParticleReaderM::time_component() const { return "m"; }
    const TLorentzVector& MVAParticleReaderM::P4() const {
        m_P4.SetPtEtaPhiM(m_PtReader->read(), m_EtaReader->read(), m_PhiReader->read(), m_TimeComReader->read());
        return m_P4;
    }
    TLorentzVector MVAParticleReaderM::GetP4(size_t E) {
        if (E <= Size()) {
            size_t BE = GetTargetEntry(E);
            m_P4.SetPtEtaPhiM(m_PtReader->readEntry(BE), m_EtaReader->readEntry(BE), m_PhiReader->readEntry(BE),
                              m_TimeComReader->readEntry(BE));
        } else {
            Error("ParticleReaderM::GetFourMomentum()",
                  "The  index " + std::to_string(E) + " given  to " + name() + " is out of range. Return empty TLorentzVector.");
            m_P4.SetPxPyPzE(0, 0, 0, 0);
        }
        return m_P4;
    }

}  // namespace XAMPP
