#include <PathResolver/PathResolver.h>
#include <TH1.h>
#include <TH2.h>
#include <TMVA/Config.h>
#include <TMVA/DataLoader.h>
#include <TMVA/Factory.h>
#include <TMVA/MethodBase.h>
#include <TString.h>
#include <TSystem.h>
#include <XAMPPplotting/Cuts.h>
#include <XAMPPplotting/IfDefHelpers.h>
#include <XAMPPplotting/PlottingUtils.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>
#include <XAMPPtmva/TMVAFactoryUtilsRegression.h>

namespace XAMPP {

    //#########################################################################################
    //                            TMVACacheProvider
    //#########################################################################################
    TMVACacheProvider* TMVARegressionCacheProvider::getProvider() {
        if (!m_Inst) m_Inst = new TMVARegressionCacheProvider();
        TMVACacheProvider* Ptr = dynamic_cast<TMVARegressionCacheProvider*>(m_Inst);
        if (Ptr == nullptr)
            Warning("TMVARegressionCacheProvider()", "Another instance of the MVACacheProvider has been loaded. Segmentation is ahead");
        return Ptr;
    }
    bool TMVARegressionCacheProvider::addTargetVariable(ITreeVarReader* Reader, const std::string& varName) { return false; }
    TMVARegressionCacheProvider::~TMVARegressionCacheProvider() {}
    TMVARegressionCacheProvider::TMVARegressionCacheProvider() : TMVACacheProvider() {}

}  // namespace XAMPP
