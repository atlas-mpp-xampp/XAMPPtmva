#include <PathResolver/PathResolver.h>
#include <TFile.h>
#include <XAMPPplotting/TreeVarReader.h>
#include <XAMPPplotting/UtilityReader.h>
#include <XAMPPplotting/Weight.h>
#include <XAMPPtmva/MVAvariable.h>
#include <XAMPPtmva/SciKitMVAvariable.h>

namespace XAMPP {
    MVAParserInterface::MVAParserInterface(const std::string& variable_name, ITreeVarReader* reader) :
        m_name(variable_name),
        m_reader(reader),
        m_variable(FLT_MAX) {}
    std::string MVAParserInterface::name() const { return m_name; }
    ITreeVarReader* MVAParserInterface::variable() const { return m_reader; }
    bool MVAParserInterface::update(size_t target) {
        if (!m_reader) {
            Error("MVAReaderInterface::update()", "No ITreeVarReader given to " + name() + ".");
            return false;
        }
        if (target >= Size()) return false;
        double cache = m_reader->readEntry(target);
        if (!IsFinite(cache)) {
            Error("MVAReaderInterface::update()", "The reader " + m_reader->name() + " does not return a finite number to " + name() + ".");
            return false;
        }
        m_variable = cache;
        return true;
    }
    const float* MVAParserInterface::getCache() const { return &m_variable; }
    float* MVAParserInterface::getCache() { return &m_variable; }
    size_t MVAParserInterface::Size() const { return m_reader->entries(); }

    //#################################################################
    //                      MVATrainingReader
    //#################################################################
    MVATrainingReader::MVATrainingReader(const std::string& training, const std::string& file_path, MachineFrameWork M,
                                         MVATrainingReader::TrainingType T) :
        m_training(training),
        m_path(file_path),
        m_framework(M),
        m_classType(T),
        m_interfaces(),
        m_methods_to_book(),
        m_event_number(-1),
        m_eventService(EventService::getService()),
        m_target(-1) {
        if (isEventBased()) m_target = 0;
    }
    bool MVATrainingReader::IsBooked(const std::string& method) const { return IsElementInList(m_methods_to_book, method); }

    void MVATrainingReader::BookMethod(const std::string& method) {
        if (!IsBooked(method)) m_methods_to_book.push_back(method);
    }
    void MVATrainingReader::clearBookedMethods() { m_methods_to_book.clear(); }
    const std::vector<std::string>& MVATrainingReader::getBookedMethods() const { return m_methods_to_book; }
    MVATrainingReader::~MVATrainingReader() {}
    std::string MVATrainingReader::name() const { return m_training; }
    std::string MVATrainingReader::getFilePath() const { return m_path; }
    const std::vector<std::shared_ptr<MVAParserInterface>>& MVATrainingReader::getInterfaces() const { return m_interfaces; }

    bool MVATrainingReader::isEventClassifier() const { return train_type() == MVATrainingReader::TrainingType::EventClass; }
    bool MVATrainingReader::isObjectClassifier() const { return train_type() == MVATrainingReader::TrainingType::ObjectClass; }
    bool MVATrainingReader::isEventRegression() const { return train_type() == MVATrainingReader::TrainingType::EventRegression; }
    bool MVATrainingReader::isObjectRegression() const { return train_type() == MVATrainingReader::TrainingType::ObjectRegression; }

    bool MVATrainingReader::isClassifier() const { return isEventClassifier() || isObjectClassifier(); }
    bool MVATrainingReader::isRegressor() const { return isEventRegression() || isObjectRegression(); }
    bool MVATrainingReader::isEventBased() const { return isEventClassifier() || isEventRegression(); }
    bool MVATrainingReader::isObjectBased() const { return isObjectClassifier() || isObjectRegression(); }

    bool MVATrainingReader::hasVariables() const { return !m_interfaces.empty(); }
    MachineFrameWork MVATrainingReader::framework() const { return m_framework; }
    MVATrainingReader::TrainingType MVATrainingReader::train_type() const { return m_classType; }

    bool MVATrainingReader::newEvent(size_t target) {
        if ((!isEventClassifier() && m_target != target) || m_event_number != m_eventService->currentEvent()) {
            for (auto& V : m_interfaces) {
                if (!V->update(target)) {
                    return false;
                } else if (isEventClassifier() && (m_target > 0 || V->Size() != 1)) {
                    Error("MVATrainingReader()", "Event classifier cannot cope with vector variables ");
                    Error("MVATrainingReader()", Form("Faulty variable: %s  size: %lu", V->name().c_str(), V->Size()));

                    return false;
                }
            }
            m_event_number = m_eventService->currentEvent();
            if (!isEventClassifier()) m_target = target;
            if (!parse_toML()) return false;
        }
        return true;
    }
    bool MVATrainingReader::parse_toML() {
        Warning("MVATrainingReader::parse_toML()", "This is a dummy method...");
        return false;
    }
    bool MVATrainingReader::isVariableUsed(const std::string& Name, ITreeVarReader* R) const {
        for (auto& V : m_interfaces) {
            if (V->variable() == R || V->name() == Name) return true;
        }
        return false;
    }
    bool MVATrainingReader::addVariable(const std::vector<StringPair>& variables, ParticleReader* ref_particle) {
        if (!isEventClassifier()) {
            Error("MVATrainingReader::AddVariable()",
                  name() +
                      " is an event classifier. The retieval of variables from a reference particle collection doees not make any sense");
            return false;
        }
        for (auto& var : variables) {
            ITreeVectorReader* var_reader = ref_particle->RetrieveBranchAccess(var.second);
            if (var_reader == nullptr) {
                Error("MVATrainingReader::AddVariables()",
                      "The particle " + ref_particle->name() + " does not know about " + var.second + ".");
                return false;
            } else if (!addVariable(var.first, var_reader))
                return false;
        }
        return true;
    }
    bool MVATrainingReader::addVariable(const std::string& Name, ITreeVarReader* InputReader) {
        if (isVariableUsed(Name, InputReader)) {
            Error("MVATrainingReader::AddVariable()", "The thing " + Name + " is already booked");
            return false;
        }
        m_interfaces.push_back(newParserInterface(Name, InputReader));
        std::sort(m_interfaces.begin(), m_interfaces.end(),
                  [](const std::shared_ptr<MVAParserInterface>& a, const std::shared_ptr<MVAParserInterface>& b) {
                      return a->name() < b->name();
                  });
        Info("MVATrainingReader::AdVariable()", "Append new MVA variable " + Name + " using " + InputReader->name() + ".");
        return true;
    }
    std::vector<std::string> MVATrainingReader::trainVarNames() const {
        std::vector<std::string> names;
        for (auto V : m_interfaces) names.push_back(V->name());
        return names;
    }
    size_t MVATrainingReader::Size() const {
        if (m_interfaces.empty()) {
            Warning("MVATrainingReader::Size()", "No variable has yet been added to the interfaces");
            return 0;
        }
        return m_interfaces[0]->variable()->entries();
    }
}  // namespace XAMPP
