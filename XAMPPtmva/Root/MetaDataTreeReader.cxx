#include <XAMPPplotting/HistFitterWeight.h>
#include <XAMPPtmva/FactoryUtils.h>
#include <XAMPPtmva/MVAReaderProvider.h>
#include <XAMPPtmva/MVATrainApplicationService.h>
#include <XAMPPtmva/MetaDataTreeReader.h>

#include <TStopwatch.h>
namespace XAMPP {

    //##############################################################
    //                        TMVANormDataBase
    //##############################################################
    TMVANormDataBase::TMVANormDataBase() : HistFitterNormDataBase() {
        delete MVAReaderProvider::GetInstance(true);
        MVAReaderProvider::GetInstance(true);
    }
    TMVANormDataBase::~TMVANormDataBase() {
        delete MVATrainApplicationService::getService();
        delete MVATrainingService::getService();
        delete MVAReaderProvider::GetInstance();
    }
    NormalizationDataBase* TMVANormDataBase::getDataBase() {
        if (!m_Inst) m_Inst = new TMVANormDataBase();
        return m_Inst;
    }

    //##############################################################
    //                      TMVAHybridNormDataBase
    //##############################################################
    TMVAHybridNormDataBase::TMVAHybridNormDataBase() : HybridNormDataBase() {
        Info("HybridNormDataBase()", "Use this database instead of the ordinary one");
        delete MVAReaderProvider::GetInstance();
        MVAReaderProvider::GetInstance();
    }
    TMVAHybridNormDataBase::~TMVAHybridNormDataBase() {
        delete MVATrainApplicationService::getService();
        delete MVATrainingService::getService();
        delete MVAReaderProvider::GetInstance();
    }
    NormalizationDataBase* TMVAHybridNormDataBase::getDataBase() {
        if (m_Inst == nullptr) m_Inst = new TMVAHybridNormDataBase();
        return m_Inst;
    }
}  // namespace XAMPP
