#
# File describing how to build externals needed by the analysis.
#

setup_python_package(scipy
                     https://files.pythonhosted.org/packages/a4/32/48b7882953d6dbbeea9e4259a5d42258d6e77d1d6f80f9298d82af444af4/scipy-1.2.2.tar.gz
                     d94de858fba4f24de7d6dd16f1caeb5d
                     #DEPENDS numpy
                    ) 
setup_python_package(scikit-learn 
                    https://files.pythonhosted.org/packages/49/0e/8312ac2d7f38537361b943c8cde4b16dadcc9389760bb855323b67bac091/scikit-learn-0.20.2.tar.gz
                    f1c25c4ed650589c887edb0434af8a1b
                    #DEPENDS scipy
                    )
#### After we have installed scikit-learn let's install keras and theano, too
setup_python_package(theano 
                     https://pypi.python.org/packages/62/da/ab486aae8e538d8ae91fa0e6ab26d3a454d7c5c7a66541f40300e58a3314/Theano-1.0.1.tar.gz
                     a38b36c0fdc3126c574163db0a253e69
                     #DEPENDS numpy
                     ) 
setup_python_package(keras_application
                     https://files.pythonhosted.org/packages/0c/f1/8d3dc4b770d51f0591c2913e55dd69e70c2e217835970ffa0b1acc091d8e/Keras_Applications-1.0.6.tar.gz
                     8576b899c659c8531f7a5c9c7a77e9da
                     DEPENDS theano) 
setup_python_package(keras_preprocessing
                    https://files.pythonhosted.org/packages/34/ef/808258b8b5e39ae4d7c5606d03d3bbaf68bb8a639ce2e64220ea4ca12ff9/Keras_Preprocessing-1.0.5.tar.gz
                     8687b8c1663e3ec3b4980b159a9b4091
                     DEPENDS keras_application)
setup_python_package(keras 
                     https://files.pythonhosted.org/packages/13/5c/11b1d1e709cfb680cf5cc592f8e37d3db19871ee5c5cc0d9ddbae4e911c7/Keras-2.2.4.tar.gz
                     bd92aafa85de6ae7574b080d65bb1d93
                     DEPENDS keras_preprocessing) 

