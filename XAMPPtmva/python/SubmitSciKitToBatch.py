import os, logging, time

from pprint import pprint
from XAMPPplotting.SubmitToBatch import ClusterAnalysisSubmit
from XAMPPtmva.SubmitTMVAToBatch import ExtractBookedMethods
from ClusterSubmission.Utils import setup_engine, setupBatchSubmitArgParser, WriteList, ReadListFromFile, CheckConfigPaths, CreateDirectory, id_generator


class SciKitSubmit(ClusterAnalysisSubmit):
    def __init__(
            self,
            cluster_engine=None,
            input_cfgs=[],
            train_cfgs=[],
            hold_jobs=[],
            # Timing information
            runTime='24:00:00',
            runMem=1400,
            HDF5Time="12:00:00",
            HDF5mem=400,
            COVTime="24:00:00",
            COVmem=500,
            DiagTime="24:00:00",
            Diagmem=600,
            PlotTime="24:00:00",
            Plotmem=600,
            isRegression=False,
            submit_meta_jobs=True):
        ClusterAnalysisSubmit.__init__(self,
                                       cluster_engine=cluster_engine,
                                       input_cfgs=input_cfgs,
                                       tree_cfgs=train_cfgs,
                                       hold_jobs=hold_jobs,
                                       runTime=runTime,
                                       runMem=runMem,
                                       submit_meta_jobs=submit_meta_jobs)

        self.__HDF5Time = HDF5Time
        self.__HDF5mem = HDF5mem

        self.__COVTime = COVTime
        self.__COVmem = COVmem

        self.__DiagTime = DiagTime
        self.__Diagmem = Diagmem

        self.__PlotTime = PlotTime
        self.__Plotmem = Plotmem

        self.__isRegression = isRegression

        train_names = []
        for i in input_cfgs:
            train_names += ["%s-%s" % (i[i.rfind("/") + 1:i.rfind(".")], t[t.rfind("/") + 1:t.rfind(".")]) for t in train_cfgs]
        WriteList(train_names, self.tmp_file_name_cfg())
        self.__submitted_trainings = []

    def get_submitted_hashes(self):
        return self.__submitted_trainings

    def submit(self):
        if self.n_sheduled() == 0:
            logging.error("<submit>: Nothing has been sheduled")
            return False
        if not self.engine().submit_build_job(): return False
        ### Submit the meta data jobs
        if not self.submit_meta_jobs(): return False
        ### Submit the HDF5 making jobs
        if not self.engine().submit_array(
                script="XAMPPtmva/runHDF5MakerOnBatch.sh",
                mem=self.__HDF5mem,
                run_time=self.__HDF5Time,
                sub_job="HDF5",
                hold_jobs=self.hold_jobs() if not self.common_meta_data() else [self.engine().subjob_name("MetaData")],
                env_vars=[
                    ("HDF5Cfg", self.run_job_cfg()),
                    ("InCfg", self.input_job_cfg()),
                    ("OutDir", self.engine().out_dir()),
                    ("TrainCfg", self.tmp_file_name_cfg()),
                ] + ([] if not self.__isRegression else [("extra_args", "--Regression")]),
                array_size=self.n_sheduled()):
            return False
        if not self.engine().submit_array(
                script="XAMPPtmva/runSciKitCovMakerOnBatch.sh",
                sub_job="COVMAT",
                hold_jobs=[(self.engine().subjob_name("HDF5"), -1)],
                env_vars=[
                    ("InDir", self.engine().out_dir()),
                    ("OutDir", self.engine().out_dir()),
                    ("TrainCfg", self.tmp_file_name_cfg()),
                ],
                mem=self.__COVmem,
                run_time=self.__COVTime,
                array_size=self.n_sheduled(),
        ):
            return False
        train_names = ReadListFromFile(self.tmp_file_name_cfg())
        for i, train in enumerate(ReadListFromFile(self.run_job_cfg())):
            sub_job_name = id_generator(10)
            self.__submitted_trainings += [sub_job_name]

            if not self.engine().submit_job(
                    script="XAMPPtmva/runVariableDiagnosticsOnBatch.sh",
                    hold_jobs=[(self.engine().subjob_name("COVMAT"), [i + 1])],
                    env_vars=[
                        ("HDF5Dir", "%s/%s" % (self.engine().out_dir(), train_names[i])),
                    ],
                    mem=self.__Diagmem,
                    run_time=self.__DiagTime,
                    sub_job="variableplots-%s" % (sub_job_name),
            ):
                return False

            booked_methods = ExtractBookedMethods(train, useTMVA=False)
            if len(booked_methods) == 0:
                logging.warning("Nothing has been booked")
                continue
            if not self.engine().submit_array(
                    script="XAMPPtmva/runSciKitOnBatch.sh",
                    hold_jobs=[(self.engine().subjob_name("COVMAT"), [i + 1])],
                    env_vars=[
                        ("HDF5Dir", "%s/%s" % (self.engine().out_dir(), train_names[i])),
                    ],
                    sub_job=sub_job_name,
                    mem=self.memory(),
                    run_time=self.run_time(),
                    array_size=2 * len(booked_methods),
            ):
                return False
            if not self.engine().submit_array(
                    script="XAMPPtmva/runDiagnosticsOnBatch.sh",
                    hold_jobs=[(self.engine().subjob_name(sub_job_name), -1)],
                    env_vars=[
                        ("HDF5Dir", "%s/%s" % (self.engine().out_dir(), train_names[i])),
                    ],
                    sub_job="diagnostics-%s" % (sub_job_name),
                    mem=self.__Diagmem,
                    run_time=self.__DiagTime,
                    array_size=2 * len(booked_methods),
            ):
                return False
            if not self.engine().submit_job(script="XAMPPtmva/makeDiagnosticPlotsOnBatch.sh",
                                            sub_job="plotting-%s" % (sub_job_name),
                                            mem=self.__Plotmem,
                                            env_vars=[
                                                ("HDF5Dir", "%s/%s" % (self.engine().out_dir(), train_names[i])),
                                            ],
                                            hold_jobs=[self.engine().subjob_name("diagnostics-%s" % (sub_job_name))],
                                            run_time=self.__PlotTime):
                return False
        return True


def SetupSubmitParser():
    parser = setupBatchSubmitArgParser()
    parser.set_defaults(BuildTime="03:59:59")
    parser.add_argument('--inputConf', '-I', nargs="+", default=[], help="Input configs to run over", required=True)
    parser.add_argument('--HDF5config', help='Configs to make the HDF5 input files', nargs='+', default=[], required=True)
    parser.add_argument("--IsRegression", help="Submit a regression training to the cluster", action='store_true', default=False)

    parser.add_argument('--HoldJob', default=[], nargs="+", help='Specfiy job names which should be finished before your job is starting. ')

    ### Run time of the training
    parser.add_argument('--RunTime', help='Changes the RunTime of the analysis Jobs', default='01:59:59')
    parser.add_argument('--RunHDF5', help='Run time for HDF5 making', default="08:00:00")
    parser.add_argument('--RunCOV', help='Run time for covariance matrix making', default="08:00:00")
    parser.add_argument('--RunDiag', help='Run time needed for the diagnostics', default="01:59:59")
    parser.add_argument('--RunPlots', help='Run time needed for the first pdfs from the diagnostics', default="01:30:00")

    ### Memory of the training
    parser.add_argument('--vmem', help='Changes the virtual memory needed by each jobs', type=int, default=8000)
    parser.add_argument('--vmemHDF5', help='Memory to assign to the HDF5 job', type=int, default=1600)
    parser.add_argument('--vmemCOV', help='Memory to reserve for the preprocessing job', type=int, default=1200)
    parser.add_argument('--vmemDiag', help='Memory to reserve for the preprocessing job', type=int, default=1200)
    parser.add_argument("--noCommonMetaData",
                        help="Disable that a temprorary meta data tree is written before the analysis jobs start",
                        action='store_false',
                        default=True)
    return parser


if __name__ == '__main__':
    RunOptions = SetupSubmitParser().parse_args()
    cluster_engine = setup_engine(RunOptions)
    train_session = SciKitSubmit(
        cluster_engine=cluster_engine,
        input_cfgs=CheckConfigPaths(RunOptions.inputConf),
        train_cfgs=CheckConfigPaths(RunOptions.HDF5config),
        hold_jobs=RunOptions.HoldJob,
        # Timing information
        runTime=RunOptions.RunTime,
        runMem=RunOptions.vmem,
        HDF5Time=RunOptions.RunHDF5,
        HDF5mem=RunOptions.vmemHDF5,
        COVTime=RunOptions.RunCOV,
        COVmem=RunOptions.vmem,
        DiagTime=RunOptions.RunDiag,
        Diagmem=RunOptions.vmemDiag,
        PlotTime=RunOptions.RunPlots,
        Plotmem=RunOptions.vmemDiag,
        isRegression=RunOptions.IsRegression,
        submit_meta_jobs=RunOptions.noCommonMetaData)

    if not train_session.submit(): exit(1)

    cluster_engine.submit_clean_all(
        hold_jobs=[train_session.engine().subjob_name("plotting-" + h) for h in train_session.get_submitted_hashes()])
    cluster_engine.finish()
