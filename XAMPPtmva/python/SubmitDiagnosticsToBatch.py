import os
import argparse

from XAMPPplotting.FileUtils import RecursiveLS, WriteList, ResolvePath, id_generator


def SetupSubmitParser():
    parser = argparse.ArgumentParser(description='This script reruns the diagnostics on the cluster',
                                     prog='SubmitDiagnosticsToBatch',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-J', '--jobName', help='Specify the JobName', required=True)
    parser.add_argument('-I',
                        '--inDir',
                        help="Specify either a directory where all your configs are stored or the config File itself",
                        nargs="+",
                        default=[],
                        required=True)
    parser.add_argument('--RunTime', help='Changes the RunTime of the analysis Jobs', default='23:59:59')
    return parser


if __name__ == '__main__':
    RunOptions = SetupSubmitParser().parse_args()

    Trainings = []
    for i in RunOptions.inDir:
        Trainings += RecursiveLS(i, ["pkl"])

    print "INFO: Found %d trainings to submit the diagnostics on." % (len(Trainings))
    if len(Trainings) == 0: exit(1)
    List = "/tmp/%s.txt" % (id_generator(24))
    WriteList([
        "python XAMPPtmva/python/SciKitLearn/RunDiagnostics.py --inDir %s --classifierName %s " %
        (T[:T.rfind("/") + 1], T[T.rfind("/") + 1:T.rfind(".")]) for T in Trainings if T[:T.rfind("/")].split("/")[-1] == "Trainings"
    ], List)
    Cmd = "python %s --jobName %s --ListOfCmds %s --RunTime %s" % (ResolvePath("XAMPPplotting/SubmitScript.py"), RunOptions.jobName, List,
                                                                   RunOptions.RunTime)
    os.system(Cmd)
    os.system("rm %s" % (List))
