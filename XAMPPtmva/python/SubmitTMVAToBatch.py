import os
import commands
import math
import sys
import re
import argparse
import time

from pprint import pprint
from ClusterSubmission.Utils import RecursiveLS, WriteList
from XAMPPplotting.SubmitToBatch import *


def ExtractBookedMethods(TrainConf, useTMVA=True):
    import ROOT

    try:
        CacheProvider = ROOT.XAMPP.TMVACacheProvider.getProvider() if useTMVA else ROOT.XAMPP.SciKitCacheProvider.getProvider()
    except:
        if os.path.isfile("$WorkDir_DIR/scripts/load_packages.C"):
            ROOT.gROOT.Macro("$WorkDir_DIR/scripts/load_packages.C")
        elif os.path.isfile("$ROOTCOREDIR/scripts/load_packages.C"):
            ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")
        ROOT.xAOD.Init().ignore()
        CacheProvider = ROOT.XAMPP.TMVACacheProvider.getProvider() if useTMVA else ROOT.XAMPP.SciKitCacheProvider.getProvider()

    SetupInstance = ROOT.XAMPP.SetupTMVA()
    if not SetupInstance.ReadTrainConfig(TrainConf): exit(1)
    Methods = CacheProvider.getBookedMethods()
    return [M for M in Methods]


def WriteMethodToTConfig(InConfig, NewConfig, Method, InvertSplitting=False):
    WriteList(
        ReadListFromFile(InConfig) + ["ClearBookedMethods", "BookMethod %s" % (Method)] +
        ([] if not InvertSplitting else ["InvertSplitting"]), NewConfig)


def SetupSubmitParser():
    parser = setupBatchSubmitArgParser()
    parser.add_argument('-M', '--MVAConf', help='MVAConfigs', nargs='+', default=[])
    parser.add_argument("--SpareWhatsProcessedIn",
                        help="If the cluster breaks down partially and you do not want to redo everything",
                        default="")
    parser.set_defaults(vmem=8000)
    parser.set_defaults(RunTime="71:59:59")
    parser.set_defaults(BuildTime="03:59:59")
    return parser


def SubmitTrainingJob(RunOptions, Input, ConfigName, Trainings):
    Today = commands.getoutput("date --iso")  #   Each day a new set of Folders is created
    MyBatchLogs = "%s/Batch_job_logs/%s/%s/" % (RunOptions.BaseFolder, Today, RunOptions.jobName)
    MyTmpDir = "%s/Batch_tmp/%s/%s/" % (RunOptions.BaseFolder, Today, RunOptions.jobName)
    ReleaseCopy = "%s/BuildJob/%s/%s/" % (RunOptions.BaseFolder, Today, RunOptions.jobName)
    MyOutDir = "%s/Output/%s/%s" % (RunOptions.BaseFolder, Today, RunOptions.jobName)

    EnviromentVariables = "OriginalArea='%s/source/',OriginalProject='%s',OriginalPatch='%s',Name='%s'" % (ReleaseCopy, ATLASPROJECT,
                                                                                                           ATLASVERSION, RunOptions.jobName)

    In = Input.split("/")[-1].replace(".conf", "")
    TrainCfg = "%s/%s_%s_TrainCfg.conf" % (MyTmpDir, ConfigName, In)
    MethodCfg = "%s/%s_%s_Methods.conf" % (MyTmpDir, ConfigName, In)
    CreateDirectory(TrainCfg[:TrainCfg.rfind("/")], False)

    Finished_Trainings = []
    if len(RunOptions.SpareWhatsProcessedIn) > 0 and os.path.isdir("%s/%s/%s/" % (RunOptions.SpareWhatsProcessedIn, In, ConfigName)):
        Finished_Trainings = [
            P[P.rfind("/") + 1:P.rfind(".")] for P in RecursiveLS("%s/%s/%s/" % (RunOptions.SpareWhatsProcessedIn, In, ConfigName), ["xml"])
        ]

    TrainFile = open(TrainCfg, "w")
    MethodFile = open(MethodCfg, "w")

    nJobs = 0
    Files_ToCopy = []
    for Method, Path in Trainings:
        if "TMVAClassification_%s.weights" % (Method) in Finished_Trainings:
            print "INFO: Method %s has already been trained." % (Method)
            continue
        Files_ToCopy += [
            "%s/%s/%s/TMVAClassification_%s.%s" % (RunOptions.SpareWhatsProcessedIn, In, ConfigName, Method, dt)
            for dt in ["weights.xml", ".root"]
        ]
        TrainFile.write("%s\n" % (Path))
        MethodFile.write("%s\n" % (Method))
        nJobs += 1
    TrainFile.close()
    MethodFile.close()

    if nJobs == 0:
        print "The sample " + SampleName + " has nothing to process. Omitt it"
        return False

    JobName = "%s_%s_%s" % (RunOptions.jobName, In, ConfigName.split("/")[-1])
    ArrayStart = 0
    ArrayEnd = min(RunOptions.JobArraySize, nJobs)
    while ArrayEnd > ArrayStart:
        SubCom = "sbatch --output='%s/Training_%s_%%A_%%a.log' %s --mem=%iM %s --job-name='%s' --array=1-%i --mail-user='%s' --mail-type=FAIL --export=%s,InCfg='%s',OutDir='%s',TrainCfg='%s',Methods='%s',Sample='%s/%s',IdOffSet='%i' %s" % (
            MyBatchLogs, JobName, GetHoldString(RunOptions.HoldJob, JobName), RunOptions.vmem, GetSlurmPartition(
                RunOptions.RunTime), JobName, min(nJobs - ArrayStart, RunOptions.JobArraySize), MYEMAIL, EnviromentVariables, Input,
            MyOutDir, TrainCfg, MethodCfg, In, ConfigName, ArrayStart, CopyExecutingScript("XAMPPtmva/scripts/runTMVAOnBatch.sh", MyTmpDir))
        os.system(SubCom)
        ArrayStart = ArrayEnd
        ArrayEnd = min(RunOptions.JobArraySize + ArrayEnd, nJobs)
    return JobName


if __name__ == '__main__':
    RunOptions = SetupSubmitParser().parse_args()
    Today = commands.getoutput("date --iso")  #   Each day a new set of Folders is created
    JobDir = "%s/%s" % (Today, RunOptions.jobName)
    MyBatchLogs = "%s/Batch_job_logs/%s/" % (RunOptions.BaseFolder, JobDir)
    MyTmpDir = "%s/Batch_tmp/%s/" % (RunOptions.BaseFolder, JobDir)
    ReleaseCopy = "%s/BuildJob/%s/" % (RunOptions.BaseFolder, JobDir)
    MyOutDir = "%s/Output/%s" % (RunOptions.BaseFolder, JobDir)  #   dir on the local pc from which your output dir is specified

    Inputs = [LinkToCopyArea(RunOptions, C) for C in CheckConfigPaths(RunOptions.inputConf)]
    MVAs = {}

    #Extract the methods from the MVA configs in order to paralellize them
    for MVA in CheckConfigPaths(RunOptions.MVAConf):

        Methods = ExtractBookedMethods(MVA)
        ConfName = ""
        if len(MVA.rsplit("/")) > 2:
            ConfName = MVA.rsplit("/")[-2] + "/" + MVA.rsplit("/")[-1].replace(".conf", "")
        else:
            ConfName = MVA.rsplit("/")[-1].replace(".conf", "")

        for M in Methods:
            NewConf = "%s/TrainingFiles/%s.conf" % (MyTmpDir, id_generator(76))
            WriteMethodToTConfig(MVA, NewConf, M, False)
            try:
                MVAs[ConfName].append((M, NewConf))
            except:
                MVAs[ConfName] = [(M, NewConf)]

            ### Submit at the same time the cross-validation configurations
            NewConf = "%s/TrainingFiles/%s.conf" % (MyTmpDir, id_generator(75))
            WriteMethodToTConfig(MVA, NewConf, M, True)
            MVAs[ConfName] += [(M + "_swapped", NewConf)]

    if len(MVAs) == 0:
        print "ERROR: No valid MVA config has been found please check"
        exit(1)
    print "#####################################################################################################"
    print "                        XAMPPtmva  SubmitToBatch "
    print "#####################################################################################################"
    prettyPrint("JobName", RunOptions.jobName)
    prettyPrint("LogDir", MyBatchLogs)
    prettyPrint("CopyRCArea", ReleaseCopy)

    prettyPrint("TmpDir", MyTmpDir)
    prettyPrint("OutDir", MyOutDir)
    prettyPrint("RunTime", RunOptions.RunTime)
    prettyPrint("Memory", str(RunOptions.vmem) + " MB")

    if not SubmitBuildJob(RunOptions): exit(1)

    Jobs = 0
    HoldJob = ["Build_" + RunOptions.jobName]

    # Building the jobs
    #The logic is to get the full combinatorics for each TrainingConf & Input a JobArray is submitted

    for Name, Training in MVAs.iteritems():
        for In in Inputs:
            Job = SubmitTrainingJob(RunOptions, In, Name, Training)
            if len(Job) > 0:
                HoldJob.append(Job)
                Jobs += 1

    if Jobs == 0: print "ERROR: No job to submit"

    SubmitCleanJob(RunOptions, HoldJob)
