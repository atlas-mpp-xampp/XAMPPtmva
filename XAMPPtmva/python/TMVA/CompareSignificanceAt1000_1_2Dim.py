#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
#from XAMPPplotting.SignalStudies.PlotSignificances_Grid import *
#from XAMPPplotting.SignalStudies.PlotSignalGrid import *
#from XAMPPplotting.PlotSignalGrid import *
from XAMPPplotting.PlotSignificances_Grid import *

from XAMPPplotting.Defs import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
from array import array
from XAMPPplotting.StatisticalFunctions import *
import os
import re
from pprint import pprint
from XAMPPplotting.PlotLabels import *

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='CompareROCAUC',
        description='This script retrieves ROC curves from TMVA output root files and compares their area-under-curve (AUC)')

    parser = XAMPPplotting.Utils.setupBaseParser(parser)

    #parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    #parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    #parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    # parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='', required=True)
    parser.add_argument('--rangemin',
                        help='Specify the smallest point of the range. It has to be greater or equal 0.',
                        default=0,
                        type=float)
    parser.add_argument('--rangemax',
                        help='Specify the smallest point of the range. It has to be smaller or equal 1.',
                        default=1,
                        type=float)
    #parser.add_argument('--Parameter',help='Do you have a particular parameter, that is plotted?', default='')
    ####################parser = argparse.ArgumentParser(description='This script produces MC only significance histograms from tree files. For more help type \"python SignificancePlots.py -h\"', prog='SignificancePlots', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-d', "--useData", action="store_true", default=False)
    parser.add_argument('--SoverB', help='Draw signal over background in ratio panel', action='store_true', default=False)
    parser.add_argument('--Purity', help='Draw purity of each background in ratio panel', action='store_true', default=False)
    parser.add_argument('--noStack', help='do not stack the different MC samples', action='store_true', default=False)
    parser.add_argument('--ShapeComp', help='draw different MC samples normalized to unit area', action='store_true', default=False)
    parser.add_argument("--RatRelCont",
                        help="Draw the relative contributions of the backgrounds to the total in the Ratio panel",
                        action='store_true',
                        default=False)
    parser.add_argument('--RatioSmp', help='Specify the sample label for the ratio to be drawn', default='')
    parser.add_argument('--xlabel', help='Specify the sample label for the x-axis', default="m_{#tilde{#chi}^{#pm}_{1}} [GeV]")
    parser.add_argument('--ignore', help='These parameters will be ignored', default=[], type=int, nargs='+')
    parser.add_argument('--MinimumGridPoints', help='minimum number of grid points needed to produce a plot', type=int, default=4)
    parser.add_argument('--BackgroundUncertainty',
                        help='specify relative uncertainty on background for significance calculation',
                        type=float,
                        default=0.3)
    parser.add_argument('--NoForward', help='Draw purity of each background in ratio panel', action='store_true', default=False)
    parser.add_argument('--NoBackward', help='Draw purity of each background in ratio panel', action='store_true', default=True)
    parser.add_argument('--CutValueDigits', help='number of digits for the cut value print', type=int, default=0)
    parser.add_argument('--ZaxisMax',
                        help='Set the maximum of the z-axis. If not defined the highest Significance value is used',
                        type=float,
                        default=-1)
    parser.add_argument(
        '--Parameters',
        help=
        'Which parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        required=True)  ##, nargs='+'
    parser.add_argument('--SignalName', help='name of signal to plot', default="InputConfig_TT_directTT_1000_1_a821_r7676_Input")
    parser.add_argument('--Caption', help='how is the file supposed to be called?', default="1000_1")

    PlottingOptions = parser.parse_args()
    if os.path.isdir(PlottingOptions.outputDir) == False:
        os.system("mkdir -p " + PlottingOptions.outputDir)
    Options = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    #     if not os.path.isdir(Options.inputPath):
    #         print "ERROR: Specified path %s does not exist, exiting..."%Options.inputPath
    #         sys.exit(1)
    #
    #     ROOTFiles = {}
    #     for myfile in os.listdir(Options.inputPath):
    #         if ".root" in myfile and not 'weights_foams' in myfile:
    #             ROOTFiles[myfile] = Options.inputPath+'/'+myfile
    #
    #     if len(ROOTFiles) == 0:
    #         print "ERROR: No .root files found at given path %s, exiting..."%Options.inputPath
    #         sys.exit(1)

    pu = PlotUtils(status=Options.label, size=24)
    pu.Prepare1PadCanvas("Significance", 1200, 600)
    ROOT.gStyle.SetPalette(57)
    pu.GetCanvas().SetTopMargin(0.15)
    pu.GetCanvas().SetRightMargin(0.18)
    pu.GetCanvas().SetLeftMargin(0.18)

    ParameterList = Options.Parameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam - 1):
        for j in range(i + 1, nParam):
            parameter1 = ParameterList[i]
            parameter2 = ParameterList[j]
            FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(PlottingOptions)
            SignalDict = {}
            #     ParameterList=Options.Parameters.split(',')
            #     print ParameterList
            #     print ParameterList[1]
            for ana in FileStructure.GetConfigSet().GetAnalyses():
                if ana not in SignalDict.keys(): SignalDict[ana] = {}
                for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
                    if region not in SignalDict[ana].iterkeys():
                        SignalDict[ana][region] = {}
                    #print 'found %s variables'%len(FileStructure.GetConfigSet().GetVariables(ana, region))
                    #n_found = 0
                    for i_var, var in enumerate(FileStructure.GetConfigSet().GetVariables(ana, region)):
                        #if i_var%10 == 0: print 'var %i'%i_var
                        useVar = True
                        if not parameter1 in var: useVar = False
                        if not parameter2 in var: useVar = False
                        if not useVar: continue
                        #n_found += 1
                        #if n_found > 50: continue
                        if var not in SignalDict[ana][region].iterkeys():
                            SignalDict[ana][region][var] = {}
                        HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, ana, region, var, UseData=False)
                        if len(HistoSets) != 1:
                            print 'wtf?'
                            sys.exit(1)
                        SignificanceSupremum = -1
                        CutValueSignificanceSupremum = ''
                        for hset in HistoSets:
                            SummedBackground = hset.GetSummedBackground()
                            Signal = hset.GetSample(Options.SignalName)
                            Siggi = XAMPPplotting.PlotSignificances_Grid.SignalPoint(Signal,
                                                                                     SummedBackground,
                                                                                     BackgroundUncertainty=Options.BackgroundUncertainty,
                                                                                     NoForward=Options.NoForward,
                                                                                     NoBackward=Options.NoBackward)
                            #significance = hset.get_sig(CutValue)

                            #SignificanceValues=Siggi.CalculateSignificanceValues

                            for k in range(Signal.GetNbins()):
                                CutValue = Signal.GetHistogram().GetBinLowEdge(k)
                                Significance = Siggi.get_sig(CutValue)
                                if Significance == 10: continue
                                if Significance > SignificanceSupremum:
                                    SignificanceSupremum = Significance
                                    CutValueSignificanceSupremum = CutValue
                        if CutValueSignificanceSupremum not in SignalDict[ana][region][var].iterkeys():
                            SignalDict[ana][region][var][CutValueSignificanceSupremum] = SignificanceSupremum
                        else:
                            print "Error! This CutValue exists already! Seems to be a difficult error!"
                            sys.exit(1)
            from pprint import pprint
            #     pprint (SignalDict)
            # Parameters=Options.Parameters
            # i=0
            # ParameterDict={}
            # SignificanceDict={}
            # for Parameter in Parameters:
            #     ParameterDict[i]=Parameter
            #     max=i
            #     i=i+1
            # for i in range(max+1):
            #     for j in range(i+1,max+1):
            #         parameter1=ParameterDict[i]
            #         parameter2=ParameterDict[j]

            for ana in SignalDict.iterkeys():
                for region in SignalDict[ana].iterkeys():
                    SignificanceDict = {}
                    for var in SignalDict[ana][region].iterkeys():
                        # if not parameter1 in var: continue
                        # if not parameter2 in var: continue
                        if var.find(parameter1) > var.find(parameter2):  ######richtige Reihenfolge der Parameter
                            c = parameter1
                            parameter1 = parameter2
                            parameter2 = c
                        ListOfParameterNumbers = re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", var)
                        if len(ListOfParameterNumbers) != 2:
                            print "There need to be exactly 2 numbers in each string which fulfills the criteria. Error!"
                            sys.exit(1)
                        parametervalue1 = float(ListOfParameterNumbers[0])
                        parametervalue2 = float(ListOfParameterNumbers[1])
                        roundedparametervalue1 = round(parametervalue1, 5)  #rounded to five digits
                        roundedparametervalue2 = round(parametervalue2, 5)
                        if roundedparametervalue1 in SignificanceDict.iterkeys():
                            if roundedparametervalue2 in SignificanceDict[roundedparametervalue1].iterkeys():
                                print "At least one Parameter pair is found twice. Error!"
                                print roundedparametervalue1
                                print roundedparametervalue2
                                print var
                                print SignificanceDict
                                sys.exit()
                            else:
                                SignificanceDict[roundedparametervalue1][roundedparametervalue2] = SignalDict[ana][region][var].itervalues(
                                ).next()
                        else:
                            SignificanceDict[roundedparametervalue1] = {}
                            if not roundedparametervalue2 in SignificanceDict.iterkeys():
                                SignificanceDict[roundedparametervalue1][roundedparametervalue2] = SignalDict[ana][region][var].itervalues(
                                ).next()
                    if len(SignificanceDict) == 0:
                        print 'ERROR: No Significance could be retrieved from specified path, exiting...'  ###%Options.inputPath
                        sys.exit(1)

                    #pprint (SignificanceDict)

                    zwischenmin1 = min(SignificanceDict.iterkeys())
                    zwischenmin2 = min(SignificanceDict[zwischenmin1].iterkeys())  ##max1: random value
                    zwischenmax1 = max(SignificanceDict.iterkeys())
                    zwischenmax2 = max(SignificanceDict[zwischenmax1].iterkeys())  ##max1: random value
                    max1 = zwischenmax1 + (zwischenmax1 - zwischenmin1) / (2 * len(SignificanceDict))
                    max2 = zwischenmax2 + (zwischenmax2 - zwischenmin2) / (2 * len(SignificanceDict[roundedparametervalue1]))
                    min1 = zwischenmin1 - (zwischenmax1 - zwischenmin1) / (2 * len(SignificanceDict))
                    min2 = zwischenmin2 - (zwischenmax2 - zwischenmin2) / (2 * len(SignificanceDict[roundedparametervalue1]))

                    #             pprint(SignificanceDict)

                    histo = ROOT.TH2F("h_Significance", "h_Significance", len(SignificanceDict), min1, max1,
                                      len(SignificanceDict.itervalues().next()), min2, max2)
                    #histo = ROOT.TH2F("h_Significance","h_Significance",len(SignificanceDict),min(SignificanceDict.iterkeys()),max(SignificanceDict.iterkeys()),len(SignificanceDict.itervalues().next()),min(SignificanceDict.itervalues().next()), max(SignificanceDict.itervalues().next()))
                    if parameter1 == 'Trees':
                        Parameter1Caption = 'Number of Trees'
                    if parameter1 == 'nCuts':
                        Parameter1Caption = 'Number of Cut Values'
                    if parameter1 == 'MinNodeSize':
                        Parameter1Caption = 'Minimal Node Size in %'
                    if parameter1 == 'MaxDepth':
                        Parameter1Caption = 'Maximal Depth of the Trees'
                    if parameter1 == 'FirstHLayer':
                        Parameter1Caption = 'Neurons in the First Hidden Layer'
                    if parameter1 == 'SecondHLayer':
                        Parameter1Caption = 'Neurons in the Second Hidden Layer'
                    if parameter2 == 'Trees':
                        Parameter2Caption = 'Number of Trees'
                    if parameter2 == 'nCuts':
                        Parameter2Caption = 'Number of Cut Values'
                    if parameter2 == 'MinNodeSize':
                        Parameter2Caption = 'Minimal Node Size in %'
                    if parameter2 == 'MaxDepth':
                        Parameter2Caption = 'Maximal Depth of the Trees'
                    if parameter2 == 'FirstHLayer':
                        Parameter2Caption = 'Neurons in the First Hidden Layer'
                    if parameter2 == 'SecondHLayer':
                        Parameter2Caption = 'Neurons in the Second Hidden Layer'
                    histo.GetXaxis().SetTitle(Parameter1Caption)
                    histo.GetYaxis().SetTitle(Parameter2Caption)
                    for x, xval in enumerate(sorted(SignificanceDict.iterkeys())):
                        for y, yval in enumerate(sorted(SignificanceDict[xval].iterkeys())):
                            histo.SetBinContent(x + 1, y + 1, SignificanceDict[xval][yval])
                            # for i,roundedparametervalue1 in enumerate(sorted(SignificanceDict.iterkeys())):
                            #     for j,roundedparametervalue2 in enumerate(sorted(SignificanceDict[roundedparametervalue1].iterkeys())):
                            #         #histo.SetLabelSize(0.03)
                            #         histo.SetBinContent(i+1,j+1,SignificanceDict[roundedparametervalue1][roundedparametervalue2])
                            #         print roundedparametervalue1, roundedparametervalue2, SignificanceDict[roundedparametervalue1][roundedparametervalue2]
                    histo.GetZaxis().SetTitleSize(.035)
                    histo.GetZaxis().SetTitle("Expected Significance of m_{#tilde{t}}=1 TeV and m_{#tilde{#chi_{0}^{1}}}=1 GeV")
                    #histo.GetZaxis().SetTitle("Significance of m_{#tilde{t}}=%s GeV and m_{#tilde{#chi_{0}^{1}}}=%s GeV"%((Options.SignalName.split('directTT_')[1]).split('_')[0],(Options.SignalName.split('directTT_')[1]).split('_')[1]))

                    histo.Draw("colz")
                    # if not Options.noATLAS: pu.DrawAtlas(0.6, 0.3)
                    # pu.DrawRegionLabel("MVA", Options.regionLabel, 0.6, 0.26)
                    extraString = ""
                    # if Options.rangemax!=1:
                    #     pu.DrawTLatex(0.6, 0.4,"Maximum signal efficiency: %.1f"%Options.rangemax)
                    #     extraString="0_1"                             ######### replace does not work "_%.1f"%Options.rangemax
                    #     ##########extraString.replace(".","_")
                    pu.DrawPlotLabels(0.18, 0.95, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                                      PlottingOptions.noATLAS)
                    pu.GetCanvas().SaveAs("Significance{0}{1}{2}_{3}.pdf".format(Options.Caption, extraString, parameter1, parameter2))
