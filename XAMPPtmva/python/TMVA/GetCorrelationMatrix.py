#! /usr/bin/env python
from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser
from XAMPPtmva.SciKitCorrelationMatrix import createColourPalette, drawMatrix
import os, ROOT

if __name__ == "__main__":
    parser = setupSciKitPlotParser(prog='GetCorrelationMatrix', description='This script creates the correlation matrix plots')
    parser.add_argument('--skimVars', help='Specify list of variables not to draw', default=[], nargs='+')
    parser.set_defaults(output="CovMatricesTMVA")
    Options = parser.parse_args()

    InputFile = setupPlottingEnviroment(Options)

    Standard_Dir = InputFile.GetDirectory("TrainingFiles/")
    if not Standard_Dir:
        print "ERROR: XAMPPtmva always creates the TrainingFiles directory inside the ROOT file. Or did TMVA it changed again. Otherwise it's not a TMVA file"
        exit(1)
    createColourPalette()

    DummyCanvas = ROOT.TCanvas("dumm", "dummy", 1200, 600)
    DummyCanvas.SaveAs("%s/AllCorrelationMatrices.pdf[" % (Options.output))
    PlottedOnce = False

    for mat, canvas_name in [('CorrelationMatrixB', "Bkg_Train"), ('CorrelationMatrixS', "Sig_Train")]:
        CorrMatrix = Standard_Dir.Get(mat)
        PlottedOnce = drawMatrix(Options, CorrMatrix, "CorrelationMatrix%s" % (canvas_name)) or PlottedOnce

    DummyCanvas.SaveAs("%s/AllCorrelationMatrices.pdf]" % (Options.output))
    if not PlottedOnce:
        os.system("rm %s/AllCorrelationMatrices.pdf" % (Options.output))
