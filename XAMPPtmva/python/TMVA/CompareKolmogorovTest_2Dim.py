#! /usr/bin/env python

import re
from pprint import pprint

from XAMPPplotting.PlotUtils import *
#from XAMPPplotting.SignalStudies.PlotSignificances_Grid import *
#from XAMPPplotting.SignalStudies.PlotSignalGrid import *
#from XAMPPplotting.PlotSignalGrid import *
#from XAMPPplotting.PlotSignificances_Grid import *

#from XAMPPplotting.Defs import *
import XAMPPplotting.Utils
# import XAMPPplotting.PlottingHistos
# from array import array
# from XAMPPplotting.StatisticalFunctions import *
# import os
# from pprint import pprint
# from XAMPPplotting.PlotLabels import *

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='CompareROCAUC',
        description='This script retrieves ROC curves from TMVA output root files and compares their area-under-curve (AUC)')
    parser = XAMPPplotting.Utils.setupBaseParser(parser)

    #parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    #parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    #parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='', required=True)
    parser.add_argument(
        '--Parameters',
        help=
        'Which parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        required=True)  ##, nargs='+'

    PlottingOptions = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    if not os.path.isdir(PlottingOptions.inputPath):
        print "ERROR: Specified path %s does not exist, exiting..." % PlottingOptions.inputPath
        sys.exit(1)

    ROOTFiles = {}
    for myfile in os.listdir(PlottingOptions.inputPath):
        if ".root" in myfile and not 'weights_foams' in myfile:
            ROOTFiles[myfile.replace('.root', '').replace('TMVAClassification_', '')] = PlottingOptions.inputPath + '/' + myfile

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % PlottingOptions.inputPath
        sys.exit(1)

    pu = PlotUtils(status=PlottingOptions.label, size=24, normalizedToUnity=True)  # do not draw luminosity label for Kolmogrov plot
    pu.Prepare1PadCanvas("KolmogSig", 1200, 600)
    ROOT.gStyle.SetPalette(57)
    pu.GetCanvas().SetTopMargin(0.15)
    pu.GetCanvas().SetRightMargin(0.18)
    pu.GetCanvas().SetLeftMargin(0.18)

    ParameterList = PlottingOptions.Parameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam - 1):
        for j in range(i + 1, nParam):
            parameter1 = ParameterList[i]
            parameter2 = ParameterList[j]
            #FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(PlottingOptions)
            KolmogSigs = {}
            KolmogBkgs = {}
            for method, ifile in ROOTFiles.iteritems():

                InputFile = ROOT.TFile.Open(ifile)

                ListOfKeys = InputFile.GetListOfKeys()
                myiter = ListOfKeys.MakeIterator()
                mykey = ROOT.TKey()

                for item in ListOfKeys:
                    #########if not 'Method_' in item.GetName(): continue
                    if not parameter1 in method: continue
                    if not parameter2 in method: continue
                    if method.find(parameter1) > method.find(parameter2):  ######richtige Reihenfolge der Parameter
                        c = parameter1
                        parameter1 = parameter2
                        parameter2 = c

                    BackgroundTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_B' % (item.GetName(), method, method, method))
                    if not BackgroundTestHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    SignalTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_S' % (item.GetName(), method, method, method))
                    if not SignalTestHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    BackgroundTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_B' % (item.GetName(), method, method, method))
                    if not BackgroundTrainHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    SignalTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_S' % (item.GetName(), method, method, method))
                    if not SignalTrainHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue

                    #KolmogOptionMBkg =BackgroundTrainHisto.KolmogorovTest(BackgroundTestHisto,"M")
                    KolmogBkg = BackgroundTestHisto.KolmogorovTest(BackgroundTrainHisto,
                                                                   "M")  ##### #statt Option M, sollte nun aber eine Zufallsvariable sein
                    #KolmogOptionMSig =SignalTrainHisto.KolmogorovTest(SignalTestHisto,"M")
                    KolmogSig = SignalTestHisto.KolmogorovTest(SignalTrainHisto, "M")  #############

                    ListOfParameterNumbers = re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", method)
                    if len(ListOfParameterNumbers) != 2:
                        print "There need to be exactly 2 numbers in each string which fulfills the criteria. Error!"
                        sys.exit(1)
                    parametervalue1 = float(ListOfParameterNumbers[0])
                    parametervalue2 = float(ListOfParameterNumbers[1])
                    roundedparametervalue1 = round(parametervalue1, 5)  #rounded to five digits
                    roundedparametervalue2 = round(parametervalue2, 5)
                    #NumberBins = ROCHisto.FindBin(rangemax)-ROCHisto.FindBin(rangemin)
                    #for i in range(NumberBins+1):
                    #      ROCAUC= ROCAUC + ((rangemax - rangemin)/NumberBins) *ROCHisto.GetBinContent(ROCHisto.FindBin(rangemin)+i)  #
                    #if ROCAUC > 0:
                    if roundedparametervalue1 in KolmogSigs.iterkeys():
                        if roundedparametervalue2 in KolmogSigs[roundedparametervalue1].iterkeys():
                            print "At least one Parameter pair is found twice. Error!"
                            print roundedparametervalue1
                            print roundedparametervalue2
                            print method
                            print KolmogSigs
                            sys.exit()
                        else:
                            KolmogSigs[roundedparametervalue1][roundedparametervalue2] = KolmogSig
                    else:
                        KolmogSigs[roundedparametervalue1] = {}
                        if not roundedparametervalue2 in KolmogSigs.iterkeys():
                            KolmogSigs[roundedparametervalue1][roundedparametervalue2] = KolmogSig
                    if roundedparametervalue1 in KolmogBkgs.iterkeys():
                        if roundedparametervalue2 in KolmogBkgs[roundedparametervalue1].iterkeys():
                            print "At least one Parameter pair is found twice. Error!"
                            print roundedparametervalue1
                            print roundedparametervalue2
                            print method
                            print KolmogBkgs
                            sys.exit()
                        else:
                            KolmogBkgs[roundedparametervalue1][roundedparametervalue2] = KolmogBkg
                    else:
                        KolmogBkgs[roundedparametervalue1] = {}
                        if not roundedparametervalue2 in KolmogBkgs.iterkeys():
                            KolmogBkgs[roundedparametervalue1][roundedparametervalue2] = KolmogBkg

            if len(KolmogSigs) == 0:
                print 'ERROR: No Kolmogorov-Smirnov-Signal value could be retrieved from specified path, exiting...'  ###%Options.inputPath
                sys.exit(1)
            if len(KolmogBkgs) == 0:
                print 'ERROR: No Kolmogorov-Smirnov-Background value could be retrieved from specified path, exiting...'  ###%Options.inputPath
                sys.exit(1)

            zwischenmin1 = min(KolmogSigs.iterkeys())
            zwischenmin2 = min(KolmogSigs[zwischenmin1].iterkeys())  ##max1: random value
            zwischenmax1 = max(KolmogSigs.iterkeys())
            zwischenmax2 = max(KolmogSigs[zwischenmax1].iterkeys())  ##max1: random value
            max1 = zwischenmax1 + (zwischenmax1 - zwischenmin1) / (2 * len(KolmogSigs))
            max2 = zwischenmax2 + (zwischenmax2 - zwischenmin2) / (2 * len(KolmogSigs[roundedparametervalue1]))
            min1 = zwischenmin1 - (zwischenmax1 - zwischenmin1) / (2 * len(KolmogSigs))
            min2 = zwischenmin2 - (zwischenmax2 - zwischenmin2) / (2 * len(KolmogSigs[roundedparametervalue1]))

            histo = ROOT.TH2F("h_KolmogSig", "h_KolmogSig", len(KolmogSigs), min1, max1, len(KolmogSigs[roundedparametervalue1]), min2,
                              max2)  ###just a random roundedparametervalue1
            if parameter1 == 'Trees': Parameter1Caption = 'Number of Trees'
            if parameter1 == 'nCuts':
                Parameter1Caption = 'Number of Cut Values'
            if parameter1 == 'MinNodeSize':
                Parameter1Caption = 'Minimal Node Size in %'
            if parameter1 == 'MaxDepth':
                Parameter1Caption = 'Maximal Depth of the Trees'
            if parameter1 == 'FirstHLayer':
                Parameter1Caption = 'Neurons in the First Hidden Layer'
            if parameter1 == 'SecondHLayer':
                Parameter1Caption = 'Neurons in the Second Hidden Layer'
            if parameter2 == 'Trees': Parameter2Caption = 'Number of Trees'
            if parameter2 == 'nCuts':
                Parameter2Caption = 'Number of Cut Values'
            if parameter2 == 'MinNodeSize':
                Parameter2Caption = 'Minimal Node Size in %'
            if parameter2 == 'MaxDepth':
                Parameter2Caption = 'Maximal Depth of the Trees'
            if parameter2 == 'FirstHLayer':
                Parameter2Caption = 'Neurons in the First Hidden Layer'
            if parameter2 == 'SecondHLayer':
                Parameter2Caption = 'Neurons in the Second Hidden Layer'
            histo.GetXaxis().SetTitle(Parameter1Caption)
            histo.GetYaxis().SetTitle(Parameter2Caption)
            for k, roundedparametervalue1 in enumerate(sorted(KolmogSigs.iterkeys())):
                for l, roundedparametervalue2 in enumerate(sorted(KolmogSigs[roundedparametervalue1].iterkeys())):
                    #histo.SetLabelSize(0.03)
                    histo.SetBinContent(k + 1, l + 1, KolmogSigs[roundedparametervalue1][roundedparametervalue2])
                    print roundedparametervalue1, roundedparametervalue2, KolmogSigs[roundedparametervalue1][roundedparametervalue2]
            histo.GetZaxis().SetTitleSize(.04)
            histo.GetZaxis().SetTitleOffset(1.3)
            histo.GetZaxis().SetTitle("Kolmogorov-Smirnov-Statistic Signal")
            maxim = -1
            for x in KolmogSigs:
                for y in KolmogSigs[x]:
                    if KolmogSigs[x][y] > maxim: maxim = KolmogSigs[x][y]
            histo.GetZaxis().SetRangeUser(0.0, maxim)

            histo.Draw("colz")
            pu.DrawPlotLabels(0.18, 0.95, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                              PlottingOptions.noATLAS)
            extraString = ""
            ######### replace does not work "_%.1f"%PlottingOptions.rangemax
            ##########extraString.replace(".","_")
            pu.GetCanvas().SaveAs("KolmogSig{0}_{1}.pdf".format(parameter1, parameter2))

            histo = ROOT.TH2F("h_KolmogBkgs", "h_", len(KolmogBkgs), min1, max1, len(KolmogBkgs[roundedparametervalue1]), min2,
                              max2)  ###just a random roundedparametervalue1
            if parameter1 == 'Trees': Parameter1Caption = 'Number of Trees'
            if parameter1 == 'nCuts':
                Parameter1Caption = 'Number of Cut Values'
            if parameter1 == 'MinNodeSize':
                Parameter1Caption = 'Minimal Node Size in %'
            if parameter1 == 'MaxDepth':
                Parameter1Caption = 'Maximal Depth of the Trees'
            if parameter1 == 'FirstHLayer':
                Parameter1Caption = 'Neurons in the First Hidden Layer'
            if parameter1 == 'SecondHLayer':
                Parameter1Caption = 'Neurons in the Second Hidden Layer'
            if parameter2 == 'Trees': Parameter2Caption = 'Number of Trees'
            if parameter2 == 'nCuts':
                Parameter2Caption = 'Number of Cut Values'
            if parameter2 == 'MinNodeSize':
                Parameter2Caption = 'Minimal Node Size in %'
            if parameter2 == 'MaxDepth':
                Parameter2Caption = 'Maximal Depth of the Trees'
            if parameter2 == 'FirstHLayer':
                Parameter2Caption = 'Neurons in the First Hidden Layer'
            if parameter2 == 'SecondHLayer':
                Parameter2Caption = 'Neurons in the Second Hidden Layer'
            histo.GetXaxis().SetTitle(Parameter1Caption)
            histo.GetYaxis().SetTitle(Parameter2Caption)
            for k, roundedparametervalue1 in enumerate(sorted(KolmogBkgs.iterkeys())):
                for l, roundedparametervalue2 in enumerate(sorted(KolmogBkgs[roundedparametervalue1].iterkeys())):
                    #histo.SetLabelSize(0.03)
                    histo.SetBinContent(k + 1, l + 1, KolmogBkgs[roundedparametervalue1][roundedparametervalue2])
                    print roundedparametervalue1, roundedparametervalue2, KolmogBkgs[roundedparametervalue1][roundedparametervalue2]
            histo.GetZaxis().SetTitleSize(.04)
            histo.GetZaxis().SetTitleOffset(1.3)
            histo.GetZaxis().SetTitle("Kolmogorov-Smirnov-Statistic Background")
            maxim = -1
            for x in KolmogBkgs:
                for y in KolmogBkgs[x]:
                    if KolmogBkgs[x][y] > maxim: maxim = KolmogBkgs[x][y]
            histo.GetZaxis().SetRangeUser(0.0, maxim)
            histo.Draw("colz")
            pu.DrawPlotLabels(0.18, 0.95, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                              PlottingOptions.noATLAS)
            extraString = ""
            ######### replace does not work "_%.1f"%PlottingOptions.rangemax
            ##########extraString.replace(".","_")
            pu.GetCanvas().SaveAs("KolmogBkg{0}_{1}.pdf".format(parameter1, parameter2))
