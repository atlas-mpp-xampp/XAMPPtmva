#! /usr/bin/env python
#! /usr/bin/env python

import re
from pprint import pprint

import XAMPPtmva.VariableNames
from XAMPPplotting.PlotUtils import *
#from XAMPPplotting.SignalStudies.PlotSignificances_Grid import *
#from XAMPPplotting.SignalStudies.PlotSignalGrid import *
#from XAMPPplotting.PlotSignalGrid import *
#from XAMPPplotting.PlotSignificances_Grid import *

#from XAMPPplotting.Defs import *
import XAMPPplotting.Utils
# import XAMPPplotting.PlottingHistos
# from array import array
# from XAMPPplotting.StatisticalFunctions import *
# import os
# from pprint import pprint
# from XAMPPplotting.PlotLabels import *

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='CompareROCAUC',
        description='This script retrieves ROC curves from TMVA output root files and compares their area-under-curve (AUC)')
    parser = XAMPPplotting.Utils.setupBaseParser(parser)

    #parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    #parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    #parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='', required=True)
    parser.add_argument(
        '--FloatParameters',
        help=
        'Which float parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        default="")  ##, nargs='+'
    parser.add_argument(
        '--StringParameters',
        help=
        'Which string parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        default="")
    parser.add_argument('--DoNMinus1Plots', help='Do you do N-1 Plots for the analysis?', action='store_true', default=False)

    PlottingOptions = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    if not os.path.isdir(PlottingOptions.inputPath):
        print "ERROR: Specified path %s does not exist, exiting..." % PlottingOptions.inputPath
        sys.exit(1)

    ROOTFiles = {}
    for myfile in os.listdir(PlottingOptions.inputPath):
        if ".root" in myfile and not 'weights_foams' in myfile:
            ROOTFiles[myfile.replace('.root', '').replace('TMVAClassification_', '')] = PlottingOptions.inputPath + '/' + myfile

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % PlottingOptions.inputPath
        sys.exit(1)

    pu = PlotUtils(status=PlottingOptions.label, size=24, normalizedToUnity=True)  # do not draw luminosity label for Kolmogrov plot
    labelX = 0.2
    labelY = 0.86
    if not PlottingOptions.DoNMinus1Plots:
        pu.Prepare1PadCanvas("KolmogSig", 1000, 600)
    else:
        pu.Prepare1PadCanvas("KolmogSig", 1100, 700)
        #pu.SetBottomMargin(0.15)
        labelX = 0.1815
        labelY = 0.96
        #labelX=0.18 special labels
        #labelY=0.89

    ParameterList = PlottingOptions.FloatParameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam):
        if PlottingOptions.FloatParameters == "": continue
        KolmogSigs = {}
        KolmogBkgs = {}
        parameter1 = ParameterList[i]
        for method, ifile in ROOTFiles.iteritems():

            InputFile = ROOT.TFile.Open(ifile)

            ListOfKeys = InputFile.GetListOfKeys()
            myiter = ListOfKeys.MakeIterator()
            mykey = ROOT.TKey()

            for item in ListOfKeys:
                #if not 'Method_' in item.GetName(): continue
                if not parameter1 in method: continue
                ListOfParameterNumbers = re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", method)
                if len(ListOfParameterNumbers) != 1:
                    print "There needs to be exactly one number in each string which fulfills the criteria. Error!"
                    continue
                parametervalue1 = float(ListOfParameterNumbers[0])
                roundedparametervalue1 = round(parametervalue1, 5)  #rounded to five digits

                BackgroundTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_B' % (item.GetName(), method, method, method))
                if not BackgroundTestHisto:
                    print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                    continue
                SignalTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_S' % (item.GetName(), method, method, method))
                if not SignalTestHisto:
                    print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                    continue
                BackgroundTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_B' % (item.GetName(), method, method, method))
                if not BackgroundTrainHisto:
                    print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                    continue
                SignalTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_S' % (item.GetName(), method, method, method))
                if not SignalTrainHisto:
                    print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                    continue

                #KolmogOptionMBkg =BackgroundTrainHisto.KolmogorovTest(BackgroundTestHisto,"M")
                KolmogBkg = BackgroundTestHisto.KolmogorovTest(BackgroundTrainHisto,
                                                               "M")  ##### #statt Option M, sollte nun aber eine Zufallsvariable sein
                #KolmogOptionMSig =SignalTrainHisto.KolmogorovTest(SignalTestHisto,"M")
                KolmogSig = SignalTestHisto.KolmogorovTest(SignalTrainHisto, "M")  #############
                #ListOfParameterNumbers=re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", method)

                #                 parametervalue1= float(ListOfParameterNumbers[0])
                #                 roundedparametervalue1=round(parametervalue1,5)   #rounded to five digits
                #NumberBins = ROCHisto.FindBin(rangemax)-ROCHisto.FindBin(rangemin)
                #for i in range(NumberBins+1):
                #      ROCAUC= ROCAUC + ((rangemax - rangemin)/NumberBins) *ROCHisto.GetBinContent(ROCHisto.FindBin(rangemin)+i)  #
                #if ROCAUC > 0:
                if roundedparametervalue1 in KolmogSigs.iterkeys():
                    print "At least one Parameter pair is found twice. Error!"
                    print roundedparametervalue1
                    print method
                    print KolmogSigs
                    sys.exit()
                else:
                    KolmogSigs[roundedparametervalue1] = KolmogSig
                if roundedparametervalue1 in KolmogBkgs.iterkeys():
                    print "At least one Parameter pair is found twice. Error!"
                    print roundedparametervalue1
                    print roundedparametervalue2
                    print method
                    print KolmogBkgs
                    sys.exit()

                else:
                    KolmogBkgs[roundedparametervalue1] = KolmogBkg

        if len(KolmogSigs) == 0:
            print 'ERROR: No Kolmogorov-Smirnov-Signal value could be retrieved from specified path, exiting...'  ###%Options.inputPath
            sys.exit(1)
        if len(KolmogBkgs) == 0:
            print 'ERROR: No Kolmogorov-Smirnov-Background value could be retrieved from specified path, exiting...'  ###%Options.inputPath
            sys.exit(1)

        zwischenmin1 = min(KolmogSigs.iterkeys())
        zwischenmax1 = max(KolmogSigs.iterkeys())
        max1 = zwischenmax1 + (zwischenmax1 - zwischenmin1) / (2 * len(KolmogSigs))
        min1 = zwischenmin1 - (zwischenmax1 - zwischenmin1) / (2 * len(KolmogSigs))

        histo = ROOT.TH1F("h_KolmogSig", "h_KolmogSig", len(KolmogSigs), min1, max1)  ###just a random roundedparametervalue1
        if parameter1 == "NCycles":
            histo.GetXaxis().SetTitle("Number of Cycles")
        elif parameter1 == "FirstHiddenLayer":
            histo.GetXaxis().SetTitle("Neurons in the First Hidden Layer")

        for k, roundedparametervalue1 in enumerate(sorted(KolmogSigs.iterkeys())):
            #histo.SetLabelSize(0.03)
            histo.SetBinContent(k + 1, KolmogSigs[roundedparametervalue1])
        histo.GetYaxis().SetTitleSize(.04)
        histo.GetYaxis().SetTitleOffset(1.8)

        histo.GetYaxis().SetTitle("Kolmogorov-Smirnov-statistic Signal")
        maxim = -1
        for x in KolmogSigs:
            if KolmogSigs[x] > maxim: maxim = KolmogSigs[x]
        #histo.GetZaxis().SetRangeUser(0.0,maxim)

        histo.Draw("")
        pu.DrawPlotLabels(0.2, 0.96, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                          PlottingOptions.noATLAS)
        extraString = ""
        ######### replace does not work "_%.1f"%PlottingOptions.rangemax
        ##########extraString.replace(".","_")
        pu.GetCanvas().SaveAs("KolmogSig{0}.pdf".format(parameter1))

        zwischenmin1 = min(KolmogBkgs.iterkeys())
        zwischenmax1 = max(KolmogBkgs.iterkeys())
        max1 = zwischenmax1 + (zwischenmax1 - zwischenmin1) / (2 * len(KolmogBkgs))
        min1 = zwischenmin1 - (zwischenmax1 - zwischenmin1) / (2 * len(KolmogBkgs))

        histo = ROOT.TH1F("h_KolmogBkg", "h_KolmogBkg", len(KolmogBkgs), min1, max1)  ###just a random roundedparametervalue1
        if parameter1 == "NCycles":
            histo.GetXaxis().SetTitle("Number of Cycles")
        elif parameter1 == "FirstHiddenLayer":
            histo.GetXaxis().SetTitle("Neurons in the First Hidden Layer")
        for k, roundedparametervalue1 in enumerate(sorted(KolmogBkgs.iterkeys())):
            #histo.SetLabelSize(0.03)
            histo.SetBinContent(k + 1, KolmogBkgs[roundedparametervalue1])
#                 print roundedparametervalue1, KolmogBkgs[roundedparametervalue1]
        histo.GetYaxis().SetTitleSize(.04)
        histo.GetYaxis().SetTitle("Kolmogorov-Smirnov-statistic Background")
        histo.GetYaxis().SetTitleOffset(1.8)
        maxim = -1
        for x in KolmogBkgs:
            if KolmogBkgs[x] > maxim: maxim = KolmogBkgs[x]
        #histo.GetYaxis().SetRangeUser(0.0,maxim)
        histo.Draw("")
        pu.DrawPlotLabels(0.2, 0.96, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                          PlottingOptions.noATLAS)
        extraString = ""
        ######### replace does not work "_%.1f"%PlottingOptions.rangemax
        ##########extraString.replace(".","_")
        pu.GetCanvas().SaveAs("KolmogBkg{0}.pdf".format(parameter1))

    ParameterList = PlottingOptions.StringParameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam):
        if PlottingOptions.StringParameters == "": continue
        KolmogSigs = {}
        KolmogBkgs = {}
        parameter1 = ParameterList[i]
        for method, ifile in ROOTFiles.iteritems():

            InputFile = ROOT.TFile.Open(ifile)

            ListOfKeys = InputFile.GetListOfKeys()
            myiter = ListOfKeys.MakeIterator()
            mykey = ROOT.TKey()

            for item in ListOfKeys:
                #if not 'Method_' in item.GetName(): continue
                if not parameter1 in method: continue

                if PlottingOptions.DoNMinus1Plots == False:
                    BackgroundTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_B' % (item.GetName(), method, method, method))
                    if not BackgroundTestHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    SignalTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_S' % (item.GetName(), method, method, method))
                    if not SignalTestHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    BackgroundTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_B' % (item.GetName(), method, method, method))
                    if not BackgroundTrainHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    SignalTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_S' % (item.GetName(), method, method, method))
                    if not SignalTrainHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                else:
                    BackgroundTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_B' % (item.GetName(), parameter1, parameter1, parameter1))
                    if not BackgroundTestHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    SignalTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_S' % (item.GetName(), parameter1, parameter1, parameter1))
                    if not SignalTestHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    BackgroundTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_B' %
                                                         (item.GetName(), parameter1, parameter1, parameter1))
                    if not BackgroundTrainHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    SignalTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_S' %
                                                     (item.GetName(), parameter1, parameter1, parameter1))
                    if not SignalTrainHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue

                #KolmogOptionMBkg =BackgroundTrainHisto.KolmogorovTest(BackgroundTestHisto,"M")
                KolmogBkg = BackgroundTestHisto.KolmogorovTest(BackgroundTrainHisto, "M")
                #KolmogOptionMSig =SignalTrainHisto.KolmogorovTest(SignalTestHisto,"M")
                KolmogSig = SignalTestHisto.KolmogorovTest(SignalTrainHisto, "M")  #############
                #ListOfParameterNumbers=re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", method)

                if KolmogBkg > 0:
                    KolmogBkgs[method] = KolmogBkg
                if KolmogSig > 0:
                    KolmogSigs[method] = KolmogSig
#                 parametervalue1= float(ListOfParameterNumbers[0])
#                 roundedparametervalue1=round(parametervalue1,5)   #rounded to five digits
#NumberBins = ROCHisto.FindBin(rangemax)-ROCHisto.FindBin(rangemin)
#for i in range(NumberBins+1):
#      ROCAUC= ROCAUC + ((rangemax - rangemin)/NumberBins) *ROCHisto.GetBinContent(ROCHisto.FindBin(rangemin)+i)  #
#if ROCAUC > 0:

        if len(KolmogSigs) == 0:
            print 'ERROR: No Kolmogorov-Smirnov-Signal value could be retrieved from specified path, exiting...'  ###%Options.inputPath
            sys.exit(1)
        if len(KolmogBkgs) == 0:
            print 'ERROR: No Kolmogorov-Smirnov-Background value could be retrieved from specified path, exiting...'  ###%Options.inputPath
            sys.exit(1)

        histo = ROOT.TH1F("h_KolmogSigs", "h_", len(KolmogSigs), 0, len(KolmogSigs))
        histo.GetXaxis().SetLabelSize(.045)
        if PlottingOptions.DoNMinus1Plots:
            histo.GetXaxis().SetTitleOffset(2.5)
            histo.GetXaxis().SetTitleSize(.035)
        else:
            histo.GetXaxis().SetLabelSize(.045)
        #histo.GetXaxis().SetLabelSize(.105)
        histo.GetYaxis().SetTitleSize(.05)
        for i, method in enumerate(sorted(KolmogBkgs.iterkeys())):
            #histo.SetLabelSize(0.03)
            if PlottingOptions.DoNMinus1Plots == True:
                method2 = (method.split("N-1Var")[1]).split("__")[0]
            elif parameter1 == 'SeparationType':
                method2 = method.split("=")[1]  #unneccessary characters in string are deleted
            elif parameter1 == 'BoostType':
                method2 = method.split("BoostType")[1]
            else:
                method2 = method
            histo.SetBinContent(i + 1, KolmogSigs[method])
            print method
            if method2 == "Grad":
                histo.GetXaxis().SetBinLabel(i + 1, "Gradient")
            elif method2 == "CrossEntropy":
                histo.GetXaxis().SetBinLabel(i + 1, "Cross Entropy")
            elif method2 == "GiniIndex":
                histo.GetXaxis().SetBinLabel(i + 1, "Gini Index")
            elif method2 == "GiniIndexWithLaplace":
                histo.GetXaxis().SetBinLabel(i + 1, "Gini Index with Laplace")
            elif method2 == "MisClassificationError":
                histo.GetXaxis().SetBinLabel(i + 1, "Misclassification Error")
            elif method2 == "RealAdaBoost":
                histo.GetXaxis().SetBinLabel(i + 1, "Real AdaBoost")
            elif method2 == "MLPBNNTrainingMethodBFGS":
                histo.GetXaxis().SetBinLabel(i + 1, "BFGS")
            elif method2 == "MLPBNNTrainingMethodBP":
                histo.GetXaxis().SetBinLabel(i + 1, "BP")
            elif method2 == "MLPBNNNeuronTyperadial":
                histo.GetXaxis().SetBinLabel(i + 1, "Radial")
            elif method2 == "MLPBNNNeuronTypesigmoid":
                histo.GetXaxis().SetBinLabel(i + 1, "Sigmoid")
            elif method2 == "MLPBNNNeuronTypetanh":
                histo.GetXaxis().SetBinLabel(i + 1, "Tangens Hyperbolicus")
            else:
                histo.GetXaxis().SetBinLabel(i + 1, XAMPPtmva.VariableNames.GetTexName(method2))
        histo.GetYaxis().SetTitle("Kolmogorov-Smirnov-Statistic Signal")
        if PlottingOptions.DoNMinus1Plots:
            histo.GetXaxis().SetTitle("Variable not being used for Training")
        elif parameter1 == 'SeparationType':
            histo.GetXaxis().SetTitle("Separation Type")
        elif parameter1 == 'BoostType':
            histo.GetXaxis().SetTitle("Boost Type")
        elif parameter1 == 'TrainingMethod':
            histo.GetXaxis().SetTitle("Training Method")
        elif parameter1 == 'NeuronType':
            histo.GetXaxis().SetTitle("Neuron Type")
        histo.LabelsOption("v")
        histo.Draw()
        pu.DrawPlotLabels(labelX, labelY, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                          PlottingOptions.noATLAS)
        extraString = ""
        ######### replace does not work "_%.1f"%Options.rangemax
        ##########extraString.replace(".","_")
        #         pu.GetCanvas().SaveAs("KolmogSig{0}{1}.pdf".format( extraString,parameter1) )

        pu.GetCanvas().SaveAs("KolmogSig{0}{1}.pdf".format(extraString, parameter1))

        histo = ROOT.TH1F("h_KolmogBkgs", "h_", len(KolmogBkgs), 0, len(KolmogBkgs))
        if PlottingOptions.DoNMinus1Plots:
            histo.GetXaxis().SetTitleOffset(2.5)
            histo.GetXaxis().SetTitleSize(.035)
        else:
            histo.GetXaxis().SetLabelSize(.045)
        histo.GetYaxis().SetTitleSize(.05)
        for i, method in enumerate(sorted(KolmogSigs.iterkeys())):
            #histo.SetLabelSize(0.03)
            if PlottingOptions.DoNMinus1Plots == True:
                method2 = (method.split("N-1Var")[1]).split("__")[0]
            elif parameter1 == 'SeparationType':
                method2 = method.split("=")[1]  #unneccessary characters in string are deleted
            elif parameter1 == 'BoostType':
                method2 = method.split("BoostType")[1]
            else:
                method2 = method
            histo.SetBinContent(i + 1, KolmogBkgs[method])
            if method2 == "Grad":
                histo.GetXaxis().SetBinLabel(i + 1, "Gradient")
            elif method2 == "CrossEntropy":
                histo.GetXaxis().SetBinLabel(i + 1, "Cross Entropy")
            elif method2 == "GiniIndex":
                histo.GetXaxis().SetBinLabel(i + 1, "Gini Index")
            elif method2 == "GiniIndexWithLaplace":
                histo.GetXaxis().SetBinLabel(i + 1, "Gini Index with Laplace")
            elif method2 == "MisClassificationError":
                histo.GetXaxis().SetBinLabel(i + 1, "Misclassification Error")
            elif method2 == "RealAdaBoost":
                histo.GetXaxis().SetBinLabel(i + 1, "Real AdaBoost")
            elif method2 == "MLPBNNTrainingMethodBFGS":
                histo.GetXaxis().SetBinLabel(i + 1, "BFGS")
            elif method2 == "MLPBNNTrainingMethodBP":
                histo.GetXaxis().SetBinLabel(i + 1, "BP")
            elif method2 == "MLPBNNNeuronTyperadial":
                histo.GetXaxis().SetBinLabel(i + 1, "Radial")
            elif method2 == "MLPBNNNeuronTypesigmoid":
                histo.GetXaxis().SetBinLabel(i + 1, "Sigmoid")
            elif method2 == "MLPBNNNeuronTypetanh":
                histo.GetXaxis().SetBinLabel(i + 1, "Tangens Hyperbolicus")
            else:
                histo.GetXaxis().SetBinLabel(i + 1, XAMPPtmva.VariableNames.GetTexName(method2))
            histo.GetYaxis().SetTitle("Kolmogorov-Smirnov-Statistic Background")
            if PlottingOptions.DoNMinus1Plots:
                histo.GetXaxis().SetTitle("Variable not being used for Training")
            elif parameter1 == 'SeparationType':
                histo.GetXaxis().SetTitle("Separation Type")
            elif parameter1 == 'BoostType':
                histo.GetXaxis().SetTitle("Boost Type")
            elif parameter1 == 'TrainingMethod':
                histo.GetXaxis().SetTitle("Training Method")
            elif parameter1 == 'NeuronType':
                histo.GetXaxis().SetTitle("Neuron Type")

        histo.LabelsOption("v")
        histo.Draw()
        pu.DrawPlotLabels(labelX, labelY, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                          PlottingOptions.noATLAS)
        extraString = ""
        ######### replace does not work "_%.1f"%Options.rangemax
        ##########extraString.replace(".","_")
        pu.GetCanvas().SaveAs("KolmogBkg{0}{1}.pdf".format(extraString, parameter1))
