#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from ROOT import TCanvas, TGraph

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='GetRocAndOvertrain',
                                     description='This script makes plots of the ROC curves and the Overtrain Plot done by TMVA')
    parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='', required=True)

    Options = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    if not os.path.isdir(Options.inputPath):
        print "ERROR: Specified path %s does not exist, exiting..." % Options.inputPath
        sys.exit(1)

    ROOTFiles = {}
    for myfile in os.listdir(Options.inputPath):
        if ".root" in myfile and not 'weights_foams' in myfile:
            ROOTFiles[myfile.replace('.root', '').replace('TMVAClassification_', '')] = Options.inputPath + '/' + myfile

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % Options.inputPath
        sys.exit(1)

    ErrFractHists = {}
    for method1, ifile in ROOTFiles.iteritems():

        InputFile = ROOT.TFile.Open(ifile)

        ListOfKeys = InputFile.GetListOfKeys()
        myiter = ListOfKeys.MakeIterator()
        mykey = ROOT.TKey()

        for item in ListOfKeys:
            #if not 'Method_' in item.GetName(): continue
            #method = item.GetName().replace('Method_','')
            ErrFractHist = ROOT.TPad
            print '%s/Method_%s/%s/ErrFractHist' % (item.GetName(), method1, method1)
            ErrFractHist = InputFile.Get('%s/Method_%s/%s/ErrFractHist' % (item.GetName(), method1, method1))

            if not ErrFractHist:
                print 'WARNING: Could not retrieve Error Fraction Histogram plot for method %s from file %s, skipping...' % (method, ifile)
                continue
            #ROCGraph = ROOT.TGraph()
            pu = PlotUtils(status=Options.label, size=24)
            pu.Prepare1PadCanvas("ErrFractHist", 800, 600)

            ErrFractHist.GetXaxis().SetTitle("Tree Number")
            ErrFractHist.GetYaxis().SetTitle("Error Fraction")

            #ROCHisto.SetGrid(5,5)

            ErrFractHist.Draw()

            #pu.DrawTLatex(0.6,0.98, "Error fraction vs. Tree Number" ,0.03,2,22)
            #pu.CreateLegend(0.2, 0.15, 0.4, 0.35)
            #pu.AddToLegend(ErrFractHist, Style="L")
            #pu.DrawLegend()

            pu.GetCanvas().SaveAs("%sErrFractHist.pdf" % method1)
