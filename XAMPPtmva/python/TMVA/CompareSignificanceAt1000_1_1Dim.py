#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
import XAMPPtmva.VariableNames
#from XAMPPplotting.SignalStudies.PlotSignificances_Grid import *
#from XAMPPplotting.SignalStudies.PlotSignalGrid import *
#from XAMPPplotting.PlotSignalGrid import *
from XAMPPplotting.PlotSignificances_Grid import *

from XAMPPplotting.Defs import *
import XAMPPplotting.Utils
import XAMPPplotting.PlottingHistos
from array import array
from XAMPPplotting.StatisticalFunctions import *
import os
import re
from pprint import pprint
from XAMPPplotting.PlotLabels import *

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='CompareSignificance',
        description='This script retrieves ROC curves from TMVA output root files and compares their area-under-curve (AUC)')

    parser = XAMPPplotting.Utils.setupBaseParser(parser)

    #parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    #parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    #parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    # parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='', required=True)
    parser.add_argument('--rangemin',
                        help='Specify the smallest point of the range. It has to be greater or equal 0.',
                        default=0,
                        type=float)
    parser.add_argument('--rangemax',
                        help='Specify the smallest point of the range. It has to be smaller or equal 1.',
                        default=1,
                        type=float)
    #parser.add_argument('--Parameter',help='Do you have a particular parameter, that is plotted?', default='')
    ####################parser = argparse.ArgumentParser(description='This script produces MC only significance histograms from tree files. For more help type \"python SignificancePlots.py -h\"', prog='SignificancePlots', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-d', "--useData", action="store_true", default=False)
    parser.add_argument('--SoverB', help='Draw signal over background in ratio panel', action='store_true', default=False)
    parser.add_argument('--DoStatErrors', help='Draw band calculated by stat error of background', action='store_true', default=False)
    parser.add_argument('--Purity', help='Draw purity of each background in ratio panel', action='store_true', default=False)
    parser.add_argument('--noStack', help='do not stack the different MC samples', action='store_true', default=False)
    parser.add_argument('--ShapeComp', help='draw different MC samples normalized to unit area', action='store_true', default=False)
    parser.add_argument("--RatRelCont",
                        help="Draw the relative contributions of the backgrounds to the total in the Ratio panel",
                        action='store_true',
                        default=False)
    parser.add_argument('--RatioSmp', help='Specify the sample label for the ratio to be drawn', default='')
    parser.add_argument('--xlabel', help='Specify the sample label for the x-axis', default="m_{#tilde{#chi}^{#pm}_{1}} [GeV]")
    parser.add_argument('--ignore', help='These parameters will be ignored', default=[], type=int, nargs='+')
    parser.add_argument('--MinimumGridPoints', help='minimum number of grid points needed to produce a plot', type=int, default=4)
    parser.add_argument('--BackgroundUncertainty',
                        help='specify relative uncertainty on background for significance calculation',
                        type=float,
                        default=0.3)
    parser.add_argument('--NoForward', help='Draw purity of each background in ratio panel', action='store_true', default=False)
    parser.add_argument('--NoBackward', help='Draw purity of each background in ratio panel', action='store_true', default=True)
    parser.add_argument('--CutValueDigits', help='number of digits for the cut value print', type=int, default=0)
    parser.add_argument('--ZaxisMax',
                        help='Set the maximum of the z-axis. If not defined the highest Significance value is used',
                        type=float,
                        default=-1)
    parser.add_argument('--SignalName', help='name of signal to plot', default="InputConfig_TT_directTT_1000_1_a821_r7676_Input")
    parser.add_argument('--Caption', help='how is the file supposed to be called?', default="")
    parser.add_argument(
        '--FloatParameters',
        help=
        'Which float parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        default="")  ##, nargs='+'
    parser.add_argument(
        '--StringParameters',
        help=
        'Which string parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        default="")
    parser.add_argument('--DoNMinus1Plots', help='Do you do N-1 Plots for the analysis?', action='store_true', default=False)

    PlottingOptions = parser.parse_args()
    if os.path.isdir(PlottingOptions.outputDir) == False:
        os.system("mkdir -p " + PlottingOptions.outputDir)
    Options = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    #     if not os.path.isdir(Options.inputPath):
    #         print "ERROR: Specified path %s does not exist, exiting..."%Options.inputPath
    #         sys.exit(1)
    #
    #     ROOTFiles = {}
    #     for myfile in os.listdir(Options.inputPath):
    #         if ".root" in myfile and not 'weights_foams' in myfile:
    #             ROOTFiles[myfile] = Options.inputPath+'/'+myfile
    #
    #     if len(ROOTFiles) == 0:
    #         print "ERROR: No .root files found at given path %s, exiting..."%Options.inputPath
    #         sys.exit(1)
    pu = PlotUtils(status=Options.label, size=24, lumi=PlottingOptions.lumi)
    if not PlottingOptions.DoNMinus1Plots:
        pu.Prepare1PadCanvas("Significance", 1100, 700)
        pu.GetCanvas().SetTopMargin(0.15)
        #pu.GetCanvas().SetTopMargin(0.25)
        pu.GetCanvas().SetRightMargin(0.18)
        pu.GetCanvas().SetLeftMargin(0.18)
    else:
        pu.Prepare1PadCanvas("Significance", 1100, 700)

    ParameterList = Options.FloatParameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam):
        if Options.FloatParameters == "": continue
        parameter1 = ParameterList[i]
        FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(PlottingOptions)
        SignalDict = {}
        #     ParameterList=Options.Parameters.split(',')
        #     print ParameterList
        #     print ParameterList[1]
        for ana in FileStructure.GetConfigSet().GetAnalyses():
            if ana not in SignalDict.keys(): SignalDict[ana] = {}
            for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
                if region not in SignalDict[ana].iterkeys():
                    SignalDict[ana][region] = {}
                #print 'found %s variables'%len(FileStructure.GetConfigSet().GetVariables(ana, region))
                #n_found = 0
                for i_var, var in enumerate(FileStructure.GetConfigSet().GetVariables(ana, region)):
                    #if i_var%10 == 0: print 'var %i'%i_var
                    useVar = True
                    if not parameter1 in var: useVar = False
                    if not useVar: continue
                    #n_found += 1
                    #if n_found > 50: continue
                    if var not in SignalDict[ana][region].iterkeys():
                        SignalDict[ana][region][var] = {}
                    HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, ana, region, var, UseData=False)
                    if len(HistoSets) != 1:
                        print 'wtf?'
                        sys.exit(1)
                    SignificanceSupremum = -1
                    CutValueSignificanceSupremum = ''
                    for hset in HistoSets:
                        SummedBackground = hset.GetSummedBackground()
                        Signal = hset.GetSample(Options.SignalName)
                        Siggi = XAMPPplotting.PlotSignificances_Grid.SignalPoint(Signal,
                                                                                 SummedBackground,
                                                                                 BackgroundUncertainty=Options.BackgroundUncertainty,
                                                                                 NoForward=Options.NoForward,
                                                                                 NoBackward=Options.NoBackward)
                        #significance = hset.get_sig(CutValue)

                        #SignificanceValues=Siggi.CalculateSignificanceValues

                        for k in range(Signal.GetNbins()):
                            CutValue = Signal.GetHistogram().GetBinLowEdge(k)
                            Significance = Siggi.get_sig(CutValue)
                            if Significance == 10: continue
                            if Significance > SignificanceSupremum:
                                SignificanceSupremum = Significance
                                CutValueSignificanceSupremum = CutValue
                    if CutValueSignificanceSupremum not in SignalDict[ana][region][var].iterkeys():
                        SignalDict[ana][region][var][CutValueSignificanceSupremum] = SignificanceSupremum
                    else:
                        print "Error! This CutValue exists already! Seems to be a difficult error!"
                        sys.exit(1)
        from pprint import pprint
        #     pprint (SignalDict)
        # Parameters=Options.Parameters
        # i=0
        # ParameterDict={}
        # SignificanceDict={}
        # for Parameter in Parameters:
        #     ParameterDict[i]=Parameter
        #     max=i
        #     i=i+1
        # for i in range(max+1):
        #     for j in range(i+1,max+1):
        #         parameter1=ParameterDict[i]
        #         parameter2=ParameterDict[j]

        for ana in SignalDict.iterkeys():
            for region in SignalDict[ana].iterkeys():
                SignificanceDict = {}
                for var in SignalDict[ana][region].iterkeys():
                    # if not parameter1 in var: continue
                    # if not parameter2 in var: continue

                    ListOfParameterNumbers = re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", var)
                    if len(ListOfParameterNumbers) != 1:
                        print "There needs to be exactly one number in each string which fulfills the criteria. Error!"
                        continue
                    parametervalue1 = float(ListOfParameterNumbers[0])
                    roundedparametervalue1 = round(parametervalue1, 5)  #rounded to five digits
                    if roundedparametervalue1 in SignificanceDict.iterkeys():
                        print "At least one Parameter is found twice. Error!"
                        print roundedparametervalue1
                        print var
                        print SignificanceDict
                        sys.exit()
                    else:
                        SignificanceDict[roundedparametervalue1] = SignalDict[ana][region][var].itervalues().next()
                if len(SignificanceDict) == 0:
                    print 'ERROR: No Significance could be retrieved from specified path, exiting...'  ###%Options.inputPath
                    sys.exit(1)

                #pprint (SignificanceDict)

                zwischenmin1 = min(SignificanceDict.iterkeys())
                zwischenmax1 = max(SignificanceDict.iterkeys())
                max1 = zwischenmax1 + (zwischenmax1 - zwischenmin1) / (2 * len(SignificanceDict))
                min1 = zwischenmin1 - (zwischenmax1 - zwischenmin1) / (2 * len(SignificanceDict))

                #             pprint(SignificanceDict)

                histo = ROOT.TH1F("h_Significance", "h_Significance", len(SignificanceDict), min1, max1)
                #histo = ROOT.TH2F("h_Significance","h_Significance",len(SignificanceDict),min(SignificanceDict.iterkeys()),max(SignificanceDict.iterkeys()),len(SignificanceDict.itervalues().next()),min(SignificanceDict.itervalues().next()), max(SignificanceDict.itervalues().next()))

                if parameter1 == "NCycles":
                    histo.GetXaxis().SetTitle("Number of Cycles")
                elif parameter1 == "FirstHiddenLayer":
                    histo.GetXaxis().SetTitle("Neurons in the First Hidden Layer")
                else:
                    histo.GetXaxis().SetTitle(parameter1)
                for x, xval in enumerate(sorted(SignificanceDict.iterkeys())):
                    histo.SetBinContent(x + 1, SignificanceDict[xval])
                    # for i,roundedparametervalue1 in enumerate(sorted(SignificanceDict.iterkeys())):
                    #     for j,roundedparametervalue2 in enumerate(sorted(SignificanceDict[roundedparametervalue1].iterkeys())):
                    #         #histo.SetLabelSize(0.03)
                    #         histo.SetBinContent(i+1,j+1,SignificanceDict[roundedparametervalue1][roundedparametervalue2])
                    #         print roundedparametervalue1, roundedparametervalue2, SignificanceDict[roundedparametervalue1][roundedparametervalue2]
                #histo.GetYaxis().SetTitle("Significance of %s"%Options.SignalName)
                #histo.GetYaxis().SetTitle("Expected Significance of m_{#tilde{t}}=%s GeV and m_{#tilde{#chi_{0}^{1}}}=%s GeV"%((Options.SignalName.split('directTT_')[1]).split('_')[0],(Options.SignalName.split('directTT_')[1]).split('_')[1]))
                histo.GetYaxis().SetTitleSize(.035)
                histo.GetYaxis().SetTitle("Expected Significance of m_{#tilde{t}}=1 TeV and m_{#tilde{#chi_{0}^{1}}}=1 GeV")
                histo.Draw("")
                # if not Options.noATLAS: pu.DrawAtlas(0.6, 0.3)
                # pu.DrawRegionLabel("MVA", Options.regionLabel, 0.6, 0.26)
                extraString = ""
                # if Options.rangemax!=1:
                #     pu.DrawTLatex(0.6, 0.4,"Maximum signal efficiency: %.1f"%Options.rangemax)
                #     extraString="0_1"                             ######### replace does not work "_%.1f"%Options.rangemax
                #     ##########extraString.replace(".","_")
                pu.DrawPlotLabels(0.25, 0.96, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                                  PlottingOptions.noATLAS)
                pu.GetCanvas().SaveAs("Significance{0}{1}{2}.pdf".format(Options.Caption, extraString, parameter1))

    ParameterList = Options.StringParameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam):
        if Options.StringParameters == "": continue
        parameter1 = ParameterList[i]
        FileStructure = XAMPPplotting.FileStructureHandler.GetStructure(PlottingOptions)
        SignalDict = {}
        #     ParameterList=Options.Parameters.split(',')
        #     print ParameterList
        #     print ParameterList[1]
        for ana in FileStructure.GetConfigSet().GetAnalyses():
            if ana not in SignalDict.keys(): SignalDict[ana] = {}
            for region in FileStructure.GetConfigSet().GetAnalysisRegions(ana):
                if region not in SignalDict[ana].iterkeys():
                    SignalDict[ana][region] = {}
                #print 'found %s variables'%len(FileStructure.GetConfigSet().GetVariables(ana, region))
                #n_found = 0
                for i_var, var in enumerate(FileStructure.GetConfigSet().GetVariables(ana, region)):
                    #if i_var%10 == 0: print 'var %i'%i_var
                    useVar = True
                    if not parameter1 in var: useVar = False
                    if not useVar: continue
                    #n_found += 1
                    #if n_found > 50: continue
                    if var not in SignalDict[ana][region].iterkeys():
                        SignalDict[ana][region][var] = {}
                    HistoSets = XAMPPplotting.PlottingHistos.CreateHistoSets(Options, ana, region, var, UseData=False)
                    if len(HistoSets) != 1:
                        print 'wtf?'
                        sys.exit(1)
                    SignificanceSupremum = -1
                    CutValueSignificanceSupremum = ''
                    statError = None
                    for hset in HistoSets:
                        SummedBackground = hset.GetSummedBackground()
                        Signal = hset.GetSample(Options.SignalName)
                        Siggi = XAMPPplotting.PlotSignificances_Grid.SignalPoint(Signal,
                                                                                 SummedBackground,
                                                                                 BackgroundUncertainty=Options.BackgroundUncertainty,
                                                                                 NoForward=Options.NoForward,
                                                                                 NoBackward=Options.NoBackward,
                                                                                 DoStatErrors=Options.DoStatErrors)
                        #significance = hset.get_sig(CutValue)

                        #SignificanceValues=Siggi.CalculateSignificanceValues

                        for k in range(Signal.GetNbins()):
                            CutValue = Signal.GetHistogram().GetBinLowEdge(k)
                            Significance = Siggi.get_sig(CutValue)
                            if Significance == 10: continue
                            if Significance > SignificanceSupremum:
                                SignificanceSupremum = Significance
                                CutValueSignificanceSupremum = CutValue
                                if Options.DoStatErrors:
                                    if not Options.NoForward:
                                        statError = Siggi.get_sig_staterr(CutValue)
                                    if not Options.NoBackward:
                                        statError = Siggi.get_sig_backward_staterr(CutValue)
                    if CutValueSignificanceSupremum not in SignalDict[ana][region][var].iterkeys():
                        SignalDict[ana][region][var][CutValueSignificanceSupremum] = (SignificanceSupremum, statError)
                    else:
                        print "Error! This CutValue exists already! Seems to be a difficult error!"
                        sys.exit(1)
        from pprint import pprint
        #     pprint (SignalDict)
        # Parameters=Options.Parameters
        # i=0
        # ParameterDict={}
        # SignificanceDict={}
        # for Parameter in Parameters:
        #     ParameterDict[i]=Parameter
        #     max=i
        #     i=i+1
        # for i in range(max+1):
        #     for j in range(i+1,max+1):
        #         parameter1=ParameterDict[i]
        #         parameter2=ParameterDict[j]

        for ana in SignalDict.iterkeys():
            for region in SignalDict[ana].iterkeys():
                SignificanceDict = {}
                for var in SignalDict[ana][region].iterkeys():
                    # if not parameter1 in var: continue
                    # if not parameter2 in var: continue
                    for CutValue in SignalDict[ana][region][var].iterkeys():  ###There should only be exactly one such value
                        SignificanceDict[var] = SignalDict[ana][region][var][CutValue]

                    if len(SignificanceDict) == 0:
                        print 'ERROR: No Significance value could be retrieved from specified path, exiting...'  ###%Options.inputPath
                        sys.exit(1)

                histo = ROOT.TH1F("h_Significance", "h_Significance", len(SignificanceDict), 0, len(SignificanceDict))
                if Options.DoStatErrors:
                    StatError = ROOT.TGraphAsymmErrors()
                    StatError.SetMarkerStyle(1)
                    StatError.SetMarkerSize(0)
                    StatError.SetMarkerStyle(1)
                    StatError.SetMarkerSize(1)
                    StatError.SetFillStyle(3354)

                histo.GetXaxis().SetLabelSize(.055)
                histo.GetYaxis().SetTitleSize(.035)
                if Options.DoNMinus1Plots:
                    histo.GetXaxis().SetTitleOffset(2.5)
                    histo.GetXaxis().SetTitleSize(.035)
                for i, method in enumerate(sorted(SignificanceDict.iterkeys())):
                    histo.SetBinContent(i + 1, SignificanceDict[method][0])
                    if PlottingOptions.DoNMinus1Plots == True:
                        method2 = (method.split("N-1Var")[1]).split("__")[0]
                    elif parameter1 == 'SeparationType':
                        method2 = method.split("=")[1]  #unneccessary characters in string are deleted
                    elif parameter1 == 'BoostType':
                        method2 = method.split("BoostType")[1]
                    else:
                        method2 = method
                    #histo.GetXaxis().SetBinLabel(i+1,XAMPPtmva.VariableNames.GetTexName(method2))

                    #histo.GetYaxis().SetTitle("Expected Significance of m_{#tilde{t}}=%s GeV and m_{#tilde{#chi_{0}^{1}}}=%s GeV"%((Options.SignalName.split('directTT_')[1]).split('_')[0],(Options.SignalName.split('directTT_')[1]).split('_')[1]))
                    if PlottingOptions.DoNMinus1Plots:
                        histo.GetXaxis().SetTitle("Variable not being used for Training")
                    elif parameter1 == 'SeparationType':
                        histo.GetXaxis().SetTitle("Separation Type")
                    elif parameter1 == 'BoostType':
                        histo.GetXaxis().SetTitle("Boost Type")
                    elif parameter1 == 'TrainingMethod':
                        histo.GetXaxis().SetTitle("Training Method")
                    elif parameter1 == 'NeuronType':
                        histo.GetXaxis().SetTitle("Neuron Type")
                    #histo.GetYaxis().SetTitle("Exp. Significance of the reference model")
                    if method2 == "Grad":
                        histo.GetXaxis().SetBinLabel(i + 1, "Gradient")
                    elif method2 == "CrossEntropy":
                        histo.GetXaxis().SetBinLabel(i + 1, "Cross Entropy")
                    elif method2 == "GiniIndex":
                        histo.GetXaxis().SetBinLabel(i + 1, "Gini Index")
                    elif method2 == "GiniIndexWithLaplace":
                        histo.GetXaxis().SetBinLabel(i + 1, "Gini Index with Laplace")
                    elif method2 == "MisClassificationError":
                        histo.GetXaxis().SetBinLabel(i + 1, "Misclassification Error")
                    elif method2 == "RealAdaBoost":
                        histo.GetXaxis().SetBinLabel(i + 1, "Real AdaBoost")
                    elif method2 == "MLPBNNTrainingMethodBFGS":
                        histo.GetXaxis().SetBinLabel(i + 1, "BFGS")
                    elif method2 == "MLPBNNTrainingMethodBP":
                        histo.GetXaxis().SetBinLabel(i + 1, "BP")
                    elif method2 == "MLPBNNNeuronTyperadial":
                        histo.GetXaxis().SetBinLabel(i + 1, "Radial")
                    elif method2 == "MLPBNNNeuronTypesigmoid":
                        histo.GetXaxis().SetBinLabel(i + 1, "Sigmoid")
                    elif method2 == "MLPBNNNeuronTypetanh":
                        histo.GetXaxis().SetBinLabel(i + 1, "Tangens Hyperbolicus")
                    else:
                        histo.GetXaxis().SetBinLabel(i + 1, XAMPPtmva.VariableNames.GetTexName(method2))
                    if Options.DoStatErrors:
                        LowEdge = histo.GetXaxis().GetBinLowEdge(i + 1)
                        Central = histo.GetXaxis().GetBinCenter(i + 1)
                        HighEdge = histo.GetXaxis().GetBinUpEdge(i + 1)
                        StatError.SetPoint(StatError.GetN(), Central, histo.GetBinContent(i + 1))
                        StatError.SetPointEXhigh(StatError.GetN() - 1, HighEdge - Central)
                        StatError.SetPointEXlow(StatError.GetN() - 1, Central - LowEdge)
                        StatError.SetPointEYhigh(StatError.GetN() - 1, SignificanceDict[method][1])
                        StatError.SetPointEYlow(StatError.GetN() - 1, SignificanceDict[method][1])
                        histo.GetYaxis().SetRangeUser(2., 4.)
                histo.GetYaxis().SetTitle("Expected Significance of m_{#tilde{t}}=1 TeV and m_{#tilde{#chi_{0}^{1}}}=1 GeV")

                histo.GetXaxis().SetLabelSize(.035)
                if Options.DoNMinus1Plots: histo.LabelsOption("v")
                histo.Draw()
                if Options.DoStatErrors:
                    StatError.Draw("sameE2")
                pu.DrawPlotLabels(0.2, 0.96, PlottingOptions.regionLabel if len(PlottingOptions.regionLabel) > 0 else "", "",
                                  PlottingOptions.noATLAS)
                pu.GetCanvas().SaveAs("Significance{0}{1}.pdf".format(Options.Caption, parameter1))
