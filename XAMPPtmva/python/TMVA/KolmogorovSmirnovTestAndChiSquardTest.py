#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='CompareROCAUC',
        description='This script retrieves ROC curves from TMVA output root files and compares their area-under-curve (AUC)')

    parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='', required=True)
    parser.add_argument('--rangemin',
                        help='Specify the smallest point of the range. It has to be greater or equal 0.',
                        default=0,
                        type=float)
    parser.add_argument('--rangemax',
                        help='Specify the smallest point of the range. It has to be smaller or equal 1.',
                        default=1,
                        type=float)
    parser.add_argument('--Parameter', help='Do you have a particular parameter, that is plotted?', default='')

    Options = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    if not os.path.isdir(Options.inputPath):
        print "ERROR: Specified path %s does not exist, exiting..." % Options.inputPath
        sys.exit(1)

    ROOTFiles = {}
    #for method,ifile in ROOTFiles.iteritems():
    #InputFile = ROOT.TFile.Open(ifile)

    #ListOfKeys = InputFile.GetListOfKeys()
    #myiter = ListOfKeys.MakeIterator()
    #mykey = ROOT.TKey()
    for myfile in os.listdir(Options.inputPath):
        if ".root" in myfile and not 'weights_foams' in myfile:
            ROOTFiles[myfile.replace('.root', '').replace('TMVAClassification_', '')] = Options.inputPath + '/' + myfile

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % Options.inputPath
        sys.exit(1)
    for method, ifile in ROOTFiles.iteritems():
        InputFile = ROOT.TFile.Open(ifile)

        ListOfKeys = InputFile.GetListOfKeys()
        myiter = ListOfKeys.MakeIterator()
        mykey = ROOT.TKey()

        for item in ListOfKeys:
            print item.GetName(), method
            BackgroundTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_B' % (item.GetName(), method, method, method))
            print '%s/Method_%s/%s/MVA_%s_B' % (item.GetName(), method, method, method)
            if not BackgroundTestHisto:
                print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                continue
            SignalTestHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_S' % (item.GetName(), method, method, method))
            if not SignalTestHisto:
                print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                continue
            BackgroundTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_B' % (item.GetName(), method, method, method))
            if not BackgroundTrainHisto:
                print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                continue
            SignalTrainHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_Train_S' % (item.GetName(), method, method, method))
            if not SignalTrainHisto:
                print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                continue

            KolmogNormalTestBkg = BackgroundTrainHisto.KolmogorovTest(BackgroundTrainHisto)  #the two Histos are exactly the same
            #KolmogNormalBkg =BackgroundTrainHisto.KolmogorovTest(BackgroundTestHisto)
            KolmogNormalRevBkg = BackgroundTestHisto.KolmogorovTest(BackgroundTrainHisto)
            #KolmogNormalSig =SignalTrainHisto.KolmogorovTest(SignalTestHisto)
            KolmogNormalRevSig = SignalTestHisto.KolmogorovTest(SignalTrainHisto)

            KolmogOptionXTestBkg = BackgroundTrainHisto.KolmogorovTest(BackgroundTrainHisto, "X")
            #KolmogOptionXBkg =BackgroundTrainHisto.KolmogorovTest(BackgroundTestHisto,"X")
            KolmogOptionXRevBkg = BackgroundTestHisto.KolmogorovTest(BackgroundTrainHisto, "X")
            #KolmogOptionXSig =SignalTrainHisto.KolmogorovTest(SignalTestHisto,"X")
            KolmogOptionXRevSig = SignalTestHisto.KolmogorovTest(SignalTrainHisto, "X")

            KolmogOptionMTestBkg = BackgroundTrainHisto.KolmogorovTest(BackgroundTrainHisto, "M")
            #KolmogOptionMBkg =BackgroundTrainHisto.KolmogorovTest(BackgroundTestHisto,"M")
            KolmogOptionMRevBkg = BackgroundTestHisto.KolmogorovTest(BackgroundTrainHisto, "M")
            #KolmogOptionMSig =SignalTrainHisto.KolmogorovTest(SignalTestHisto,"M")
            KolmogOptionMRevSig = SignalTestHisto.KolmogorovTest(SignalTrainHisto, "M")

            KolmogOptionXMTestBkg = BackgroundTrainHisto.KolmogorovTest(BackgroundTrainHisto, "X,M")
            #KolmogOptionXMBkg =BackgroundTrainHisto.KolmogorovTest(BackgroundTestHisto,"X,M")
            KolmogOptionXMRevBkg = BackgroundTestHisto.KolmogorovTest(BackgroundTrainHisto, "X,M")
            #KolmogOptionXMSig =SignalTrainHisto.KolmogorovTest(SignalTestHisto,"X,M")
            KolmogOptionXMRevSig = SignalTestHisto.KolmogorovTest(SignalTrainHisto, "X,M")

            #Chi2TestBkg=BackgroundTrainHisto.Chi2Test(BackgroundTrainHisto,"UU")
            #Chi2NormalBkg =BackgroundTrainHisto.Chi2Test(BackgroundTestHisto,"UU")
            #Chi2NormalRevBkg= BackgroundTestHisto.Chi2Test(BackgroundTrainHisto,"UU")
            #Chi2NormalSig =SignalTrainHisto.Chi2Test(SignalTestHisto,"UU")
            #Chi2NormalRevSig= SignalTestHisto.Chi2Test(SignalTrainHisto,"UU")

            Chi2OptionNormTestBkg = BackgroundTrainHisto.Chi2Test(BackgroundTrainHisto, "UU,NORM")  #####former: "NORM"
            #Chi2OptionNormBkg =BackgroundTrainHisto.Chi2Test(BackgroundTestHisto,"UU,NORM")
            Chi2OptionNormRevBkg = BackgroundTestHisto.Chi2Test(BackgroundTrainHisto, "UU,NORM")
            #Chi2OptionNormSig =SignalTrainHisto.Chi2Test(SignalTestHisto,"UU,NORM")
            Chi2OptionNormRevSig = SignalTestHisto.Chi2Test(SignalTrainHisto, "UU,NORM")

            #Chi2NormalChi2ValueTestBkg=BackgroundTrainHisto.Chi2Test(BackgroundTrainHisto,"UU,CHI2")
            #Chi2NormalChi2ValueBkg =BackgroundTrainHisto.Chi2Test(BackgroundTestHisto,"UU,CHI2")
            #Chi2NormalChi2ValueRevBkg= BackgroundTestHisto.Chi2Test(BackgroundTrainHisto,"UU,CHI2")
            #Chi2NormalChi2ValueSig =SignalTrainHisto.Chi2Test(SignalTestHisto,"UU,CHI2")
            #Chi2NormalChi2ValueRevSig= SignalTestHisto.Chi2Test(SignalTrainHisto,"UU,CHI2")

            Chi2OptionNormChi2ValueTestBkg = BackgroundTrainHisto.Chi2Test(BackgroundTrainHisto, "UU,NORM,CHI2")
            #Chi2OptionNormChi2ValueBkg =BackgroundTrainHisto.Chi2Test(BackgroundTestHisto,"UU,NORM,CHI2")
            Chi2OptionNormChi2ValueRevBkg = BackgroundTestHisto.Chi2Test(BackgroundTrainHisto, "UU,NORM,CHI2")
            #Chi2OptionNormChi2ValueSig =SignalTrainHisto.Chi2Test(SignalTestHisto,"UU,NORM,CHI2")
            Chi2OptionNormChi2ValueRevSig = SignalTestHisto.Chi2Test(SignalTrainHisto, "UU,NORM,CHI2")

            print "KolmogNormalTestBkg: %.5f" % KolmogNormalTestBkg
            #print "KolmogNormalBkg: %.5f"%KolmogNormalBkg
            print "KolmogNormalRevBkg: %.5f" % KolmogNormalRevBkg
            #print "KolmogNormalSig: %.5f"%KolmogNormalSig
            print "KolmogNormalRevSig: %.5f" % KolmogNormalRevSig

            print "KolmogOptionXTestBkg: %.5f" % KolmogOptionXTestBkg
            #print "KolmogOptionXBkg: %.5f"%KolmogOptionXBkg
            print "KolmogOptionXRevBkg: %.5f" % KolmogOptionXRevBkg
            #print "KolmogOptionXSig: %.5f"%KolmogOptionXSig
            print "KolmogOptionXRevSig: %.5f" % KolmogOptionXRevSig

            print "KolmogOptionMTestBkg: %.5f" % KolmogOptionMTestBkg
            #print "KolmogOptionMBkg: %.5f"%KolmogOptionMBkg
            print "KolmogOptionMRevBkg: %.5f" % KolmogOptionMRevBkg
            #print "KolmogOptionMSig: %.5f"%KolmogOptionMSig
            print "KolmogOptionMRevSig: %.5f" % KolmogOptionMRevSig

            print "KolmogOptionXMTestBkg: %.5f" % KolmogOptionXMTestBkg
            #print "KolmogOptionXMBkg: %.5f"%KolmogOptionXMBkg
            print "KolmogOptionXMRevBkg: %.5f" % KolmogOptionXMRevBkg
            #print "KolmogOptionXMSig: %.5f"%KolmogOptionXMSig
            print "KolmogOptionXMRevSig: %.5f" % KolmogOptionXMRevSig

            #print "Chi2TestBkg: %.5f"%Chi2TestBkg
            #print "Chi2NormalBkg: %.5f"%Chi2NormalBkg
            #print "Chi2NormalRevBkg: %.5f"%Chi2NormalRevBkg
            #print "Chi2NormalSig: %.5f"%Chi2NormalSig
            #print "Chi2NormalRevSig: %.5f"%Chi2NormalRevSig

            print "Chi2OptionNormTestBkg: %.5f" % Chi2OptionNormTestBkg
            #print "Chi2OptionNormBkg: %.5f"%Chi2OptionNormBkg
            print "Chi2OptionNormRevBkg: %.5f" % Chi2OptionNormRevBkg
            #print "Chi2OptionNormSig: %.5f"%Chi2OptionNormSig
            print "Chi2OptionNormRevSig: %.5f" % Chi2OptionNormRevSig

            #print "Chi2NormalChi2ValueTestBkg: %.5f"%Chi2NormalChi2ValueTestBkg
            #print "Chi2NormalChi2ValueBkg: %.5f"%Chi2NormalChi2ValueBkg
            #print "Chi2NormalChi2ValueRevBkg: %.5f"%Chi2NormalChi2ValueRevBkg
            #print "Chi2NormalChi2ValueSig: %.5f"%Chi2NormalChi2ValueSig
            #print "Chi2NormalChi2ValueRevSig: %.5f"%Chi2NormalChi2ValueRevSig

            print "Chi2OptionNormChi2ValueTestBkg: %.5f" % Chi2OptionNormChi2ValueTestBkg
            #print "Chi2OptionNormChi2ValueBkg: %.5f"%Chi2OptionNormChi2ValueBkg
            print "Chi2OptionNormChi2ValueRevBkg: %.5f" % Chi2OptionNormChi2ValueRevBkg
            #print "Chi2OptionNormChi2ValueSig: %.5f"%Chi2OptionNormChi2ValueSig
            print "Chi2OptionNormChi2ValueRevSig: %.5f" % Chi2OptionNormChi2ValueRevSig
