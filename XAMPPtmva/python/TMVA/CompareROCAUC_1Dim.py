#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
import XAMPPtmva.VariableNames
import re
from pprint import pprint

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='CompareROCAUC',
        description='This script retrieves ROC curves from TMVA output root files and compares their area-under-curve (AUC)')
    parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    parser.add_argument('--DrawTraining', help='Enable the drawing of the training ROC AUC', action='store_true', default=False)
    parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='', required=True)
    parser.add_argument('--rangemin',
                        help='Specify the smallest point of the range. It has to be greater or equal 0.',
                        default=0,
                        type=float)
    parser.add_argument('--rangemax',
                        help='Specify the smallest point of the range. It has to be smaller or equal 1.',
                        default=1,
                        type=float)
    parser.add_argument(
        '--FloatParameters',
        help=
        'Which float parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        default="")  ##, nargs='+'
    parser.add_argument(
        '--StringParameters',
        help=
        'Which string parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        default="")
    parser.add_argument('--DoNMinus1Plots', help='Do you do N-1 Plots for the analysis?', action='store_true', default=False)

    Options = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    if not os.path.isdir(Options.inputPath):
        print "ERROR: Specified path %s does not exist, exiting..." % Options.inputPath
        sys.exit(1)

    ROOTFiles = {}
    for myfile in os.listdir(Options.inputPath):
        if ".root" in myfile and not 'weights_foams' in myfile:
            ROOTFiles[myfile.replace('.root', '').replace('TMVAClassification_', '')] = Options.inputPath + '/' + myfile

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % Options.inputPath
        sys.exit(1)

    pu = PlotUtils(status=Options.label, size=24, normalizedToUnity=True)  # do not draw luminosity label for ROC AUC plot
    if not Options.DoNMinus1Plots:
        pu.Prepare1PadCanvas("ROCAUC", 1600, 800)
        pu.GetCanvas().SetTopMargin(0.15)
        pu.GetCanvas().SetRightMargin(0.18)
        pu.GetCanvas().SetLeftMargin(0.18)
    else:
        pu.Prepare1PadCanvas("ROCAUC", 1000, 800)

    ParameterList = Options.FloatParameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam):
        if Options.FloatParameters == "": continue
        ROCAUCs = {}
        ROCAUCsTrain = {}
        parameter1 = ParameterList[i]
        for method, ifile in ROOTFiles.iteritems():

            InputFile = ROOT.TFile.Open(ifile)

            ListOfKeys = InputFile.GetListOfKeys()
            myiter = ListOfKeys.MakeIterator()
            mykey = ROOT.TKey()

            for item in ListOfKeys:
                #if not 'Method_' in item.GetName(): continue
                if not parameter1 in method: continue

                ROCHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_rejBvsS' % (item.GetName(), method, method, method))
                if Options.DrawTraining:
                    ROCHistoTrain = InputFile.Get('%s/Method_%s/%s/MVA_%s_trainingRejBvsS' % (item.GetName(), method, method, method))

                if not ROCHisto:
                    print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                    continue
                if Options.rangemax < 0 or Options.rangemax > 1:
                    print "Invalid value for Rangemax"
                    sys.exit(1)
                if Options.rangemin < 0 or Options.rangemin > 1:
                    print "Invalid value for Rangemin"
                    sys.exit(1)
                ROCAUC = 0
                ROCAUCTrain = 0
                lowerBin = ROCHisto.FindBin(Options.rangemin)
                upperBin = ROCHisto.FindBin(Options.rangemax)
                ListOfParameterNumbers = re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", method)
                if len(ListOfParameterNumbers) != 1:
                    print "There needs to be exactly one number in each string which fulfills the criteria. Error!"
                    continue
                parametervalue1 = float(ListOfParameterNumbers[0])
                roundedparametervalue1 = round(parametervalue1, 5)  #rounded to five digits
                #NumberBins = ROCHisto.FindBin(rangemax)-ROCHisto.FindBin(rangemin)
                #for i in range(NumberBins+1):
                #      ROCAUC= ROCAUC + ((rangemax - rangemin)/NumberBins) *ROCHisto.GetBinContent(ROCHisto.FindBin(rangemin)+i)  #
                for y in range(lowerBin, upperBin):
                    ROCAUC = ROCAUC + (ROCHisto.GetXaxis().GetBinUpEdge(y) -
                                       ROCHisto.GetXaxis().GetBinLowEdge(y)) * ROCHisto.GetBinContent(y)
                    if Options.DrawTraining:
                        ROCAUCTrain = ROCAUCTrain + (ROCHistoTrain.GetXaxis().GetBinUpEdge(y) -
                                                     ROCHistoTrain.GetXaxis().GetBinLowEdge(y)) * ROCHistoTrain.GetBinContent(y)
                if ROCAUC > 0:
                    if roundedparametervalue1 in ROCAUCs.iterkeys():
                        print "One Parameter pair is found twice. Error!"
                        print roundedparametervalue1
                        print method
                        print ROCAUCs
                        sys.exit()

                    else:
                        ROCAUCs[roundedparametervalue1] = ROCAUC
                        if Options.DrawTraining:
                            ROCAUCsTrain[roundedparametervalue1] = ROCAUCTrain

        if len(ROCAUCs) == 0:
            print 'ERROR: No ROC curve could be retrieved from specified path, exiting...'  ###%Options.inputPath
            sys.exit(1)

        zwischenmin1 = min(ROCAUCs.iterkeys())
        zwischenmax1 = max(ROCAUCs.iterkeys())
        max1 = zwischenmax1 + (zwischenmax1 - zwischenmin1) / (2 * len(ROCAUCs))
        min1 = zwischenmin1 - (zwischenmax1 - zwischenmin1) / (2 * len(ROCAUCs))

        histo = ROOT.TH1F("h_ROCAUC", "Testing", len(ROCAUCs), min1, max1)  ###just a random roundedparametervalue1
        if parameter1 == "NCycles":
            histo.GetXaxis().SetTitle("Number of Cycles")
        elif parameter1 == "FirstHiddenLayer":
            histo.GetXaxis().SetTitle("Neurons in the First Hidden Layer")
        else:
            histo.GetXaxis().SetTitle(parameter1)
        if Options.DrawTraining:
            histoTrain = ROOT.TH1F("h_ROCAUCTrain", "Training", len(ROCAUCsTrain), min1, max1)
            histoTrain.SetLineColor(ROOT.kRed)
            histoTrain.SetLineStyle(ROOT.kDashed)
        for q, roundedparametervalue1 in enumerate(sorted(ROCAUCs.iterkeys())):
            #histo.SetLabelSize(0.03)
            histo.SetBinContent(q + 1, ROCAUCs[roundedparametervalue1])
            if Options.DrawTraining:
                histoTrain.SetBinContent(q + 1, ROCAUCsTrain[roundedparametervalue1])
        histo.GetYaxis().SetTitle("Area under the ROC-Curve")
        maxim = 0
        minim = 1
        for x in ROCAUCs:
            if ROCAUCs[x] > maxim: maxim = ROCAUCs[x]
            if ROCAUCs[x] < minim: minim = ROCAUCs[x]
            if Options.DrawTraining:
                if ROCAUCsTrain[x] > maxim: maxim = ROCAUCsTrain[x]
                if ROCAUCsTrain[x] < minim: minim = ROCAUCsTrain[x]
        histo.GetYaxis().SetRangeUser(minim * 0.995, maxim * 1.005)
        histo.Draw("")
        if Options.DrawTraining:
            histoTrain.Draw("same")
            pu.CreateLegend(0.2, 0.7, 0.4, 0.8)
            pu.AddToLegend(histo, Style="L")
            pu.AddToLegend(histoTrain, Style="L")
            pu.DrawLegend()
        pu.DrawPlotLabels(0.21, 0.86, Options.regionLabel if len(Options.regionLabel) > 0 else "", "", Options.noATLAS)
        extraString = ""
        if Options.rangemax != 1:
            pu.DrawTLatex(0.6, 0.4, "Maximum signal efficiency: %.1f" % Options.rangemax)
            extraString = "0_1"  ######### replace does not work "_%.1f"%Options.rangemax
            ##########extraString.replace(".","_")
        pu.GetCanvas().SaveAs("ROCAUC{0}{1}.pdf".format(extraString, parameter1))

    ParameterList = Options.StringParameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam):
        if Options.StringParameters == "": continue
        ROCAUCs = {}
        ROCAUCsTrain = {}
        parameter = ParameterList[i]
        for method, ifile in ROOTFiles.iteritems():

            InputFile = ROOT.TFile.Open(ifile)

            ListOfKeys = InputFile.GetListOfKeys()
            myiter = ListOfKeys.MakeIterator()
            mykey = ROOT.TKey()

            for item in ListOfKeys:
                #if not 'Method_' in item.GetName(): continue

                if not parameter in method: continue

                if Options.DoNMinus1Plots == False:
                    ROCHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_rejBvsS' % (item.GetName(), method, method, method))
                    if Options.DrawTraining:
                        ROCHistoTrain = InputFile.Get('%s/Method_%s/%s/MVA_%s_trainingRejBvsS' % (item.GetName(), method, method, method))
                else:
                    ROCHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_rejBvsS' % (item.GetName(), parameter, parameter, parameter))
                    if Options.DrawTraining:
                        ROCHistoTrain = InputFile.Get('%s/Method_%s/%s/MVA_%s_trainingRejBvsS' %
                                                      (item.GetName(), parameter, parameter, parameter))
                if not ROCHisto:
                    print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                    continue
                if Options.rangemax < 0 or Options.rangemax > 1:
                    print "Invalid value for Rangemax"
                    sys.exit(1)
                if Options.rangemin < 0 or Options.rangemin > 1:
                    print "Invalid value for Rangemin"
                    sys.exit(1)
                ROCAUC = 0
                ROCAUCTrain = 0
                lowerBin = ROCHisto.FindBin(Options.rangemin)
                upperBin = ROCHisto.FindBin(Options.rangemax)
                #ListOfParameterNumbers=re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", method)

                for z in range(lowerBin, upperBin):
                    ROCAUC = ROCAUC + (ROCHisto.GetXaxis().GetBinUpEdge(z) -
                                       ROCHisto.GetXaxis().GetBinLowEdge(z)) * ROCHisto.GetBinContent(z)
                    if Options.DrawTraining:
                        ROCAUCTrain = ROCAUCTrain + (ROCHistoTrain.GetXaxis().GetBinUpEdge(z) -
                                                     ROCHistoTrain.GetXaxis().GetBinLowEdge(z)) * ROCHistoTrain.GetBinContent(z)

                if ROCAUC > 0:
                    ROCAUCs[method] = ROCAUC
                    if Options.DrawTraining:
                        ROCAUCsTrain[method] = ROCAUCTrain

        if len(ROCAUCs) == 0:
            print 'ERROR: No ROC curve could be retrieved from specified path, exiting...'  ###%Options.inputPath
            sys.exit(1)

        histo = ROOT.TH1F("h_ROCAUC", "Testing", len(ROCAUCs), 0, len(ROCAUCs))
        histo.GetXaxis().SetLabelSize(.045)
        if Options.DoNMinus1Plots:
            histo.GetXaxis().SetTitleOffset(2.9)
            histo.GetXaxis().SetTitleSize(.028)
        #histo.GetXaxis().SetLabelSize(.015)
        maxim = 0
        minim = 1
        for x in ROCAUCs:
            if ROCAUCs[x] > maxim: maxim = ROCAUCs[x]
            if ROCAUCs[x] < minim: minim = ROCAUCs[x]
            if Options.DrawTraining:
                if ROCAUCsTrain[x] > maxim: maxim = ROCAUCsTrain[x]
                if ROCAUCsTrain[x] < minim: minim = ROCAUCsTrain[x]
        histo.GetYaxis().SetRangeUser(minim * 0.995, maxim * 1.005)
        if Options.DrawTraining:
            histoTrain = ROOT.TH1F("h_ROCAUCTrain", "Training", len(ROCAUCsTrain), 0, len(ROCAUCsTrain))
            histoTrain.SetLineColor(ROOT.kRed)
            histoTrain.SetLineStyle(ROOT.kDashed)

        for i, method in enumerate(sorted(ROCAUCs.iterkeys())):
            #histo.SetLabelSize(0.03)
            print method
            if Options.DoNMinus1Plots == True:
                method2 = (method.split("N-1Var")[1]).split("__")[0]
            elif parameter == 'SeparationType':
                method2 = method.split("=")[1]  #unneccessary characters in string are deleted
            elif parameter == 'BoostType':
                method2 = method.split("BoostType")[1]
            else:
                method2 = method
            histo.SetBinContent(i + 1, ROCAUCs[method])
            if method2 == "Grad":
                histo.GetXaxis().SetBinLabel(i + 1, "Gradient")
            elif method2 == "CrossEntropy":
                histo.GetXaxis().SetBinLabel(i + 1, "Cross Entropy")
            elif method2 == "GiniIndex":
                histo.GetXaxis().SetBinLabel(i + 1, "Gini Index")
            elif method2 == "GiniIndexWithLaplace":
                histo.GetXaxis().SetBinLabel(i + 1, "Gini Index with Laplace")
            elif method2 == "MisClassificationError":
                histo.GetXaxis().SetBinLabel(i + 1, "Misclassification Error")
            elif method2 == "RealAdaBoost":
                histo.GetXaxis().SetBinLabel(i + 1, "Real AdaBoost")
            elif method2 == "MLPBNNTrainingMethodBFGS":
                histo.GetXaxis().SetBinLabel(i + 1, "BFGS")
            elif method2 == "MLPBNNTrainingMethodBP":
                histo.GetXaxis().SetBinLabel(i + 1, "BP")
            elif method2 == "MLPBNNNeuronTyperadial":
                histo.GetXaxis().SetBinLabel(i + 1, "Radial")
            elif method2 == "MLPBNNNeuronTypesigmoid":
                histo.GetXaxis().SetBinLabel(i + 1, "Sigmoid")
            elif method2 == "MLPBNNNeuronTypetanh":
                histo.GetXaxis().SetBinLabel(i + 1, "Tangens Hyperbolicus")
            else:
                histo.GetXaxis().SetBinLabel(i + 1, XAMPPtmva.VariableNames.GetTexName(method2))
            if Options.DrawTraining:
                histoTrain.SetBinContent(i + 1, ROCAUCsTrain[method])
        histo.GetYaxis().SetTitle("Area under the ROC-Curve")
        histo.GetYaxis().SetTitleSize(.05)
        histo.GetYaxis().SetTitleOffset(1.6)
        if Options.DoNMinus1Plots:
            histo.GetXaxis().SetTitle("Variable not being used for Training")
        elif parameter == 'SeparationType':
            histo.GetXaxis().SetTitle("Separation Type")
        elif parameter == 'BoostType':
            histo.GetXaxis().SetTitle("Boost Type")
        elif parameter == 'TrainingMethod':
            histo.GetXaxis().SetTitle("Training Method")
        elif parameter == 'NeuronType':
            histo.GetXaxis().SetTitle("Neuron Type")

        if Options.DoNMinus1Plots: histo.LabelsOption("v")

        histo.Draw("")
        if Options.DrawTraining:
            histoTrain.Draw("same")
            if not Options.DoNMinus1Plots:
                pu.CreateLegend(0.2, 0.7, 0.4, 0.8, 30)
            else:
                pu.CreateLegend(0.2, 0.7, 0.4, 0.83, 35)
            pu.AddToLegend(histo, Style="L")
            pu.AddToLegend(histoTrain, Style="L")
            pu.DrawLegend()

        pu.DrawPlotLabels(0.21, 0.86, Options.regionLabel if len(Options.regionLabel) > 0 else "", "", Options.noATLAS)
        extraString = ""
        if Options.rangemax != 1:
            pu.DrawTLatex(0.6, 0.4, "Maximum signal efficiency: %.1f" % Options.rangemax)
            extraString = "0_1"  ######### replace does not work "_%.1f"%Options.rangemax
            ##########extraString.replace(".","_")
        pu.GetCanvas().SaveAs("ROCAUC{0}{1}.pdf".format(extraString, parameter))
