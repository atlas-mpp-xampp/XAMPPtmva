#! /usr/bin/env python
from XAMPPtmva.SciKitOvertrainPlot import KolmogorovSmirnovScore, makeOverTrainPlot
from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser
import os, ROOT, logging
if __name__ == "__main__":
    parser = setupSciKitPlotParser(prog='GetRocAndOvertrain',
                                   description='This script makes plots of the ROC curves and the Overtrain Plot done by TMVA')
    parser.set_defaults(output="OverTrainPlotsTMVA/")
    parser.add_argument('--xlabel', help='specify the xlabel of you graph. Default: Name of the variable', default='')

    Options = parser.parse_args()
    setupPlottingEnviroment(Options).Close()

    ROOTFiles = sorted(["%s/%s" % (Options.inputPath, myfile) for myfile in os.listdir(Options.inputPath) if myfile.endswith(".root")],
                       key=lambda x: x[x.rfind("/") + 1:x.rfind(".") if x.rfind("_swapped") == -1 else x.rfind("_swapped")])

    if len(ROOTFiles) == 0:
        logging.error("No .root files found at given path %s, exiting..." % Options.inputPath)
        sys.exit(1)

    PlottedOnce = False
    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    DummyCanvas.SaveAs("%s/AllOverTrainPlots.pdf[" % (Options.output))

    for ifile in ROOTFiles:
        InputFile = ROOT.TFile.Open(ifile)
        Standard_Dir = InputFile.GetDirectory("TrainingFiles/")
        if not Standard_Dir:
            logging.error(
                "XAMPPtmva always creates the TrainingFiles directory inside the ROOT file. Or did TMVA it changed again. Otherwise it's not a TMVA file: %s"
                % (ifile))
            continue
        Methods = [
            key.GetName()[key.GetName().find("_") + 1:] for key in Standard_Dir.GetListOfKeys()
            if key.ReadObj().InheritsFrom("TDirectory") and key.GetName().find("Method") != -1
        ]
        for method in Methods:
            BackgroundTestHisto = Standard_Dir.Get('Method_%s/%s/MVA_%s_B' % (method, method, method))
            BackgroundTrainHisto = Standard_Dir.Get('Method_%s/%s/MVA_%s_Train_B' % (method, method, method))
            SignalTestHisto = Standard_Dir.Get('Method_%s/%s/MVA_%s_S' % (method, method, method))
            SignalTrainHisto = Standard_Dir.Get('Method_%s/%s/MVA_%s_Train_S' % (method, method, method))
            PlottedOnce = makeOverTrainPlot(Options,
                                            SignalTestHisto,
                                            SignalTrainHisto,
                                            BackgroundTestHisto,
                                            BackgroundTrainHisto,
                                            method,
                                            substr=ifile[ifile.rfind("/") + 1:ifile.rfind(".")]) or PlottedOnce

        InputFile.Close()

    DummyCanvas.SaveAs("%s/AllOverTrainPlots.pdf]" % (Options.output))
    if not PlottedOnce:
        os.system("rm %s/AllOverTrainPlots.pdf" % (Options.output))
