#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
import re
from pprint import pprint

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='CompareROCAUC',
        description='This script retrieves ROC curves from TMVA output root files and compares their area-under-curve (AUC)')
    parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    parser.add_argument('--label', help='Specify the label to be plotted', default="Internal")
    parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='', required=True)
    parser.add_argument('--rangemin',
                        help='Specify the smallest point of the range. It has to be greater or equal 0.',
                        default=0,
                        type=float)
    parser.add_argument('--rangemax',
                        help='Specify the smallest point of the range. It has to be smaller or equal 1.',
                        default=1,
                        type=float)
    parser.add_argument(
        '--Parameters',
        help=
        'Which parameters shall be plotted? Divide strings by a Comma without an empty space (i.e. "nCuts,Trees") At least two are required',
        required=True)  ##, nargs='+'
    #parser.add_argument('--Parameter1',help='Please enter the first Parameter, for which you want to get the ROC-plot', default='', required=True)
    #parser.add_argument('--Parameter2',help='Please enter the second Parameter, for which you want to get the ROC-plot', default='', required=True)
    parser.add_argument('--MinimalROCAUCValue', help='What minimal ROCAUC value is required?', default=-1, type=float)
    Options = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    if not os.path.isdir(Options.inputPath):
        print "ERROR: Specified path %s does not exist, exiting..." % Options.inputPath
        sys.exit(1)

    ROOTFiles = {}
    for myfile in os.listdir(Options.inputPath):
        if ".root" in myfile and not 'weights_foams' in myfile:
            ROOTFiles[myfile.replace('.root', '').replace('TMVAClassification_', '')] = Options.inputPath + '/' + myfile

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % Options.inputPath
        sys.exit(1)

    ParameterList = Options.Parameters.split(',')
    nParam = len(ParameterList)
    for i in range(nParam - 1):
        for j in range(i + 1, nParam):
            ROCAUCs = {}
            parameter1 = ParameterList[i]
            parameter2 = ParameterList[j]
            for method, ifile in ROOTFiles.iteritems():

                InputFile = ROOT.TFile.Open(ifile)

                ListOfKeys = InputFile.GetListOfKeys()
                myiter = ListOfKeys.MakeIterator()
                mykey = ROOT.TKey()

                for item in ListOfKeys:
                    #if not 'Method_' in item.GetName(): continue
                    if not parameter1 in method: continue
                    if not parameter2 in method: continue
                    if method.find(parameter1) > method.find(parameter2):  ######richtige Reihenfolge der Parameter
                        c = parameter1
                        parameter1 = parameter2
                        parameter2 = c
                    ROCHisto = InputFile.Get('%s/Method_%s/%s/MVA_%s_trainingRejBvsS' % (item.GetName(), method, method, method))
                    if not ROCHisto:
                        print 'WARNING: Could not retrieve ROC plot for method %s from file %s, skipping...' % (method, ifile)
                        continue
                    if Options.rangemax < 0 or Options.rangemax > 1:
                        print "Invalid value for Rangemax"
                        sys.exit(1)
                    if Options.rangemin < 0 or Options.rangemin > 1:
                        print "Invalid value for Rangemin"
                        sys.exit(1)
                    ROCAUC = 0
                    lowerBin = ROCHisto.FindBin(Options.rangemin)
                    upperBin = ROCHisto.FindBin(Options.rangemax)
                    ListOfParameterNumbers = re.findall("[-+]?\d+[\.]?\d*[eE]?[-+]?\d*", method)
                    if len(ListOfParameterNumbers) != 2:
                        print "There need to be exactly 2 numbers in each string which fulfills the criteria. Error!"
                        sys.exit(1)
                    parametervalue1 = float(ListOfParameterNumbers[0])
                    parametervalue2 = float(ListOfParameterNumbers[1])
                    roundedparametervalue1 = round(parametervalue1, 5)  #rounded to five digits
                    roundedparametervalue2 = round(parametervalue2, 5)
                    #NumberBins = ROCHisto.FindBin(rangemax)-ROCHisto.FindBin(rangemin)
                    #for i in range(NumberBins+1):
                    #      ROCAUC= ROCAUC + ((rangemax - rangemin)/NumberBins) *ROCHisto.GetBinContent(ROCHisto.FindBin(rangemin)+i)  #
                    for y in range(lowerBin, upperBin):
                        ROCAUC = ROCAUC + (ROCHisto.GetXaxis().GetBinUpEdge(y) -
                                           ROCHisto.GetXaxis().GetBinLowEdge(y)) * ROCHisto.GetBinContent(y)
                    if ROCAUC > 0:
                        if roundedparametervalue1 in ROCAUCs.iterkeys():
                            if roundedparametervalue2 in ROCAUCs[roundedparametervalue1].iterkeys():
                                print "At least one Parameter pair is found twice. Error!"
                                print roundedparametervalue1
                                print roundedparametervalue2
                                print method
                                print ROCAUCs
                                sys.exit()
                            else:
                                ROCAUCs[roundedparametervalue1][roundedparametervalue2] = ROCAUC
                        else:
                            ROCAUCs[roundedparametervalue1] = {}
                            if not roundedparametervalue2 in ROCAUCs.iterkeys():
                                ROCAUCs[roundedparametervalue1][roundedparametervalue2] = ROCAUC

            if len(ROCAUCs) == 0:
                print 'ERROR: No ROC curve could be retrieved from specified path, exiting...'  ###%Options.inputPath
                sys.exit(1)

            zwischenmin1 = min(ROCAUCs.iterkeys())
            zwischenmin2 = min(ROCAUCs[zwischenmin1].iterkeys())  ##max1: random value
            zwischenmax1 = max(ROCAUCs.iterkeys())
            zwischenmax2 = max(ROCAUCs[zwischenmax1].iterkeys())  ##max1: random value
            max1 = zwischenmax1 + (zwischenmax1 - zwischenmin1) / (2 * len(ROCAUCs))
            max2 = zwischenmax2 + (zwischenmax2 - zwischenmin2) / (2 * len(ROCAUCs[roundedparametervalue1]))
            min1 = zwischenmin1 - (zwischenmax1 - zwischenmin1) / (2 * len(ROCAUCs))
            min2 = zwischenmin2 - (zwischenmax2 - zwischenmin2) / (2 * len(ROCAUCs[roundedparametervalue1]))

            pu = PlotUtils(status=Options.label, size=24)
            pu.Prepare1PadCanvas("ROCAUC", 1200, 600)
            histo = ROOT.TH2F("h_ROCAUC", "h_ROCAUC", len(ROCAUCs), min1, max1, len(ROCAUCs[roundedparametervalue1]), min2,
                              max2)  ###just a random roundedparametervalue1
            if parameter1 == 'Trees': Parameter1Caption = 'Number of Trees'
            if parameter1 == 'nCuts':
                Parameter1Caption = 'Number of Cut Values'
            if parameter1 == 'MinNodeSize':
                Parameter1Caption = 'Minimal Node Size in %'
            if parameter1 == 'MaxDepth':
                Parameter1Caption = 'Maximal Depth of the Trees'
            if parameter1 == 'FirstHLayer':
                Parameter1Caption = 'Neurons in the First Hidden Layer'
            if parameter1 == 'SecondHLayer':
                Parameter1Caption = 'Neurons in the Second Hidden Layer'
            if parameter2 == 'Trees': Parameter2Caption = 'Number of Trees'
            if parameter2 == 'nCuts':
                Parameter2Caption = 'Number of Cut Values'
            if parameter2 == 'MinNodeSize':
                Parameter2Caption = 'Minimal Node Size in %'
            if parameter2 == 'MaxDepth':
                Parameter2Caption = 'Maximal Depth of the Trees'
            if parameter2 == 'FirstHLayer':
                Parameter2Caption = 'Neurons in the First Hidden Layer'
            if parameter2 == 'SecondHLayer':
                Parameter2Caption = 'Neurons in the Second Hidden Layer'
            histo.GetXaxis().SetTitle(Parameter1Caption)
            histo.GetYaxis().SetTitle(Parameter2Caption)
            for q, roundedparametervalue1 in enumerate(sorted(ROCAUCs.iterkeys())):
                for r, roundedparametervalue2 in enumerate(sorted(ROCAUCs[roundedparametervalue1].iterkeys())):
                    #histo.SetLabelSize(0.03)
                    histo.SetBinContent(q + 1, r + 1, ROCAUCs[roundedparametervalue1][roundedparametervalue2])
            histo.GetZaxis().SetTitle("Area under the Training ROC-Curve")
            histo.GetZaxis().SetTitleOffset(1.2)
            maxim = -1
            minim = 10
            for x in ROCAUCs:
                for y in ROCAUCs[x]:
                    if ROCAUCs[x][y] > maxim: maxim = ROCAUCs[x][y]
                    if (ROCAUCs[x][y] < minim) and (ROCAUCs[x][y] > Options.MinimalROCAUCValue):
                        minim = ROCAUCs[x][y]

            histo.GetZaxis().SetRangeUser(minim, maxim)
            ROOT.gStyle.SetPalette(57)
            pu.GetCanvas().SetTopMargin(0.15)
            pu.GetCanvas().SetRightMargin(0.18)
            pu.GetCanvas().SetLeftMargin(0.18)
            histo.Draw("colz")
            if not Options.noATLAS: pu.DrawAtlas(0.6, 0.3)
            #pu.DrawRegionLabel("MVA", Options.regionLabel, 0.6, 0.26)
            pu.DrawPlotLabels(0.18, 0.9, Options.regionLabel if len(Options.regionLabel) > 0 else "", "", Options.noATLAS)
            extraString = ""
            if Options.rangemax != 1:
                pu.DrawTLatex(0.6, 0.4, "Maximum signal efficiency: %.1f" % Options.rangemax)
                extraString = "0_1"  ######### replace does not work "_%.1f"%Options.rangemax
                ##########extraString.replace(".","_")
            pu.GetCanvas().SaveAs("ROCAUC{0}{1}_{2}.pdf".format(extraString, parameter1, parameter2))
