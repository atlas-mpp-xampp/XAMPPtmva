#! /usr/bin/env python
from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment
from XAMPPtmva.SciKitROCCurve import drawROCCurve, setupSciKitROCparser
from XAMPPplotting.Utils import CreateCulumativeHisto
import os, ROOT, logging


def castToTGraph(Histo):
    if Histo == None: return None
    MyGraph = ROOT.TGraphAsymmErrors()
    MyGraph.GetXaxis().SetTitle("Signal efficiency")
    MyGraph.GetYaxis().SetTitle("Background rejection")
    for i in range(1, Histo.GetNbinsX() + 1):
        MyGraph.SetPoint(MyGraph.GetN(), Histo.GetXaxis().GetBinCenter(i), Histo.GetBinContent(i))
    return MyGraph


def createInverseEffiGraph(Histo):
    if Histo == None: return None
    MyGraph = ROOT.TGraphAsymmErrors()
    for i in range(1, Histo.GetNbinsX() + 1):
        MyGraph.SetPoint(MyGraph.GetN(), Histo.GetXaxis().GetBinCenter(i), 1. / max([1. - Histo.GetBinContent(i), 1.e-6]))

    MyGraph.GetXaxis().SetTitle("Signal efficiency")
    MyGraph.GetYaxis().SetTitle("Inverse background efficiency")
    return MyGraph


def generateROC(Bkg, Sig, isTrainCurve=True, inverse_eff=False):
    if not Bkg or not Sig: return None
    graph = ROOT.TGraphAsymmErrors()
    graph.SetName("MVA_%s_%s" % ("roc" if not inverse_eff else "inveffAeff", "Train" if isTrainCurve else "Testing"))
    if not inverse_eff:
        graph.GetXaxis().SetTitle("Background rejection")
        graph.GetYaxis().SetTitle("Signal efficiency")
    else:
        graph.GetXaxis().SetTitle("Signal efficiency")
        graph.GetYaxis().SetTitle("Inverse background efficiency")

    CulBkg = CreateCulumativeHisto(Bkg, True)
    CulSig = CreateCulumativeHisto(Sig, True)

    if CulBkg.GetBinContent(1) > 1.: CulBkg.Scale(1. / CulBkg.GetBinContent(1))
    if CulSig.GetBinContent(1) > 1.: CulSig.Scale(1. / CulSig.GetBinContent(1))

    for i in range(1, CulBkg.GetNbinsX()):
        Bkg_Err = CulBkg.GetBinError(i)
        Sig_Err = CulSig.GetBinError(i)
        if not inverse_eff:
            graph.SetPoint(graph.GetN(), 1. - CulBkg.GetBinContent(i), CulSig.GetBinContent(i))
            ### Let's assign some training errors to the ROC curve
            graph.SetPointError(graph.GetN() - 1, Bkg_Err, Bkg_Err, Sig_Err, Sig_Err)
        else:
            graph.SetPoint(graph.GetN(), CulSig.GetBinContent(i), 1. / max([CulBkg.GetBinContent(i), 1.e-6]))
            # 1/X --> deltaX / X^{2}
            inv_bkg_error = Bkg_Err * graph.GetY()[graph.GetN() - 1] * graph.GetY()[graph.GetN() - 1]
            graph.SetPointError(graph.GetN() - 1, Sig_Err, Sig_Err, inv_bkg_error, inv_bkg_error)
    return graph


if __name__ == "__main__":
    parser = setupSciKitROCparser(prog='GetRocAndOvertrain',
                                  description='This script makes plots of the ROC curves and the Overtrain Plot done by TMVA')
    parser.set_defaults(output="ROCCurvesTMVA")
    Options = parser.parse_args()
    setupPlottingEnviroment(Options)

    ROOTFiles = []
    for myfile in os.listdir(Options.inputPath):
        if not myfile.endswith(".root"): continue
        training = myfile[:myfile.rfind(".")]
        if training.endswith("_swapped"): continue

        valid_file = "%s_swapped.root" % (training)
        if os.path.exists("%s/%s" % (Options.inputPath, valid_file)):
            ROOTFiles.append(("%s/%s" % (Options.inputPath, myfile), "%s/%s" % (Options.inputPath, valid_file)))
        else:
            ROOTFiles.append(("%s/%s" % (Options.inputPath, myfile), "%s/%s" % (Options.inputPath, myfile)))

    if len(ROOTFiles) == 0:
        logging.error("No .root files found at given path %s, exiting..." % Options.inputPath)
        sys.exit(1)

    PlottedOnce = False
    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    DummyCanvas.SaveAs("%s/All%sCurves.pdf[" % (Options.output, "ROC" if not Options.invBkgEff else "InvBkgSigEff"))

    for ifile, valid_file in ROOTFiles:
        InputFile = ROOT.TFile.Open(ifile)
        ValidFile = ROOT.TFile.Open(valid_file) if ifile != valid_file else None

        Std_Dir_Default = InputFile.GetDirectory("TrainingFiles/")
        Std_Dir_Valid = ValidFile.GetDirectory("TrainingFiles/") if ValidFile else None
        if not Std_Dir_Default:
            logging.error(
                "XAMPPtmva always creates the TrainingFiles directory inside the ROOT file. Or did TMVA it changed again. Otherwise it's not a TMVA file: %s"
                % (ifile))

        Default_Methods = [
            key.GetName()[key.GetName().find("_") + 1:] for key in Std_Dir_Default.GetListOfKeys()
            if key.ReadObj().InheritsFrom("TDirectory") and key.GetName().find("Method") != -1
        ] if Std_Dir_Default else []
        Valid_Methods = [
            key.GetName()[key.GetName().find("_") + 1:] for key in Std_Dir_Valid.GetListOfKeys()
            if key.ReadObj().InheritsFrom("TDirectory") and key.GetName().find("Method") != -1
        ] if Std_Dir_Valid else []

        for method in Default_Methods:

            ROCHistoTrain_Default = Std_Dir_Default.Get('Method_%s/%s/MVA_%s_trainingRejBvsS' % (method, method, method))
            ROCHistoTest_Default = Std_Dir_Default.Get('Method_%s/%s/MVA_%s_rejBvsS' % (method, method, method))

            ROCHistoTrain_Valid = Std_Dir_Valid.Get('Method_%s_swapped/%s_swapped/MVA_%s_swapped_trainingRejBvsS' %
                                                    (method, method, method)) if "%s_swapped" % (method) in Valid_Methods else None
            ROCHistoTest_Valid = Std_Dir_Valid.Get('Method_%s_swapped/%s_swapped/MVA_%s_swapped_rejBvsS' %
                                                   (method, method, method)) if "%s_swapped" % (method) in Valid_Methods else None

            substr = ifile[ifile.rfind("/") + 1:ifile.rfind(".")].replace("TMVAClassification_", "")
            substr = "" if substr == method else "_" + substr
            if not Options.invBkgEff:
                PlottedOnce = drawROCCurve(Options,
                                           castToTGraph(ROCHistoTrain_Default),
                                           castToTGraph(ROCHistoTest_Default),
                                           castToTGraph(ROCHistoTrain_Valid),
                                           castToTGraph(ROCHistoTest_Valid),
                                           method=method,
                                           substr=substr) or PlottedOnce
            else:
                PlottedOnce = drawROCCurve(Options,
                                           createInverseEffiGraph(ROCHistoTrain_Default),
                                           createInverseEffiGraph(ROCHistoTest_Default),
                                           createInverseEffiGraph(ROCHistoTrain_Valid),
                                           createInverseEffiGraph(ROCHistoTest_Valid),
                                           method=method,
                                           substr=substr) or PlottedOnce

        InputFile.Close()
        if ValidFile: ValidFile.Close()

    DummyCanvas.SaveAs("%s/All%sCurves.pdf]" % (Options.output, "ROC" if not Options.invBkgEff else "InvBkgSigEff"))
    if not PlottedOnce:
        os.system("rm %s/All%sCurves.pdf" % (Options.output, "ROC" if not Options.invBkgEff else "InvBkgSigEff"))
