from ClusterSubmission.Utils import ResolvePath, ExecuteCommands, CreateDirectory, ReadListFromFile
import os, argparse, logging


def setup_parser():
    parser = argparse.ArgumentParser(prog="TrainSciKitLearn", description="Helper script to run all steps of scikit learn")
    parser.add_argument("--trainConf", "-T", "-t", help="What model config in terms of variables and cuts should be used", required=True)
    parser.add_argument("--inputConf", "-I", "-i", help="What is the inputdata", required=True)
    parser.add_argument("--out_dir", "-d", help="Where to store the training", default="SciKitMVA_TrainFiles")
    parser.add_argument("--isRegression", help="The training is meant for regression purpuoses", default=False, action='store_true')

    return parser


if __name__ == '__main__':
    run_options = setup_parser().parse_args()
    if not ResolvePath(run_options.inputConf) or not ResolvePath(run_options.trainConf):
        logging.error("Input config files not found")
        exit(1)

    log_dir = run_options.out_dir + "/LOGS/"
    tmp_dir = run_options.out_dir + "/TMP/"
    out_file = "output_scikit.root"
    ### Delete first the old directory
    CreateDirectory(run_options.out_dir, True)
    CreateDirectory(log_dir, True)

    hdf5_cmd = "WriteHDF5 -i %s -t %s -d %s -o %s %s %s" % (run_options.inputConf, run_options.trainConf, run_options.out_dir, out_file,
                                                            "" if not run_options.isRegression else "--Regression",
                                                            "2>&1 | tee %s/HDF5_Making.log" % (log_dir))
    if os.system(hdf5_cmd):
        logging.error("Failed to make the HDF5 file")
        exit(1)

    #### Run the covariance matrix calculation
    cov_cmd = "python %s --HDF5File %s/%s.H5 --outDir %s/ --nAuxillaryThreads 4 2>&1 | tee %s/Covariance_Making.log" % (
        ResolvePath("XAMPPtmva/SciKitLearn/Utils/CalculateCovariance.py"), run_options.out_dir, out_file[:out_file.rfind(".")],
        run_options.out_dir, log_dir)

    if os.system(cov_cmd):
        logging.error("Something failed with the covariance matrix calculation of the training data")
        exit(1)
    first_diag_cmd = "python %s  --runVariablesOnly --HDF5File %s/%s.H5 --outDir %s/Diagnostic/ --nAuxillaryThreads 1 --dataPreProcessing %s/Covariance_Matrix.pkl  " % (
        ResolvePath("XAMPPtmva/SciKitLearn/Utils/RunDiagnostics.py"), run_options.out_dir, out_file[:out_file.rfind(".")],
        run_options.out_dir, run_options.out_dir)

    plot_cmd = "python %s -i %s/Diagnostic/ --output %s/Plots --nThreads 1" % (ResolvePath("XAMPPtmva/SciKitPlotting/SciKitFastPlots.py"),
                                                                               run_options.out_dir, run_options.out_dir)

    os.system("%s ; %s  2>&1| tee %s/VariableDiagnostics.log" % (first_diag_cmd, plot_cmd, log_dir))
    #os.system(plot_cmd)
    exit(0)
    train_cmds = [
        "%s  --dataPreProcessing %s/Covariance_Matrix.pkl 2>&1 | tee %s/Training_%d.log" % (cmd, run_options.out_dir, log_dir, i)
        for i, cmd in enumerate(ReadListFromFile("%s/%s_trainCmds.txt" % (run_options.out_dir, out_file[0:out_file.rfind(".")])))
    ]

    ExecuteCommands(train_cmds, MaxCurrent=8)
