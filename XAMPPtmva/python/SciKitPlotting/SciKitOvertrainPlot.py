#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from XAMPPplotting.Utils import CreateCulumativeHisto, GetNbins
from XAMPPplotting.FileStructureHandler import GetFileHandler
import math
import numpy as np


def KolmogorovSmirnovScore(histo1, histo2, consider_stats=True):
    if not histo1 or not histo2: return 1.e6, 1.e6
    Cum1 = CreateCulumativeHisto(histo1, True)
    Cum2 = CreateCulumativeHisto(histo2, True)
    if Cum1.GetBinContent(1) > 1: Cum1.Scale(1. / Cum1.GetBinContent(1))
    if Cum2.GetBinContent(1) > 1: Cum2.Scale(1. / Cum2.GetBinContent(1))

    max_bin = sorted([i for i in range(1, GetNbins(Cum1))],
                     key=lambda x: math.fabs(Cum1.GetBinContent(x) - Cum2.GetBinContent(x)),
                     reverse=True)[0]
    N_Train = histo1.GetEntries()
    N_Test = histo2.GetEntries()
    Sample_Faktor = math.sqrt((N_Train * N_Test) / (N_Train + N_Test)) if consider_stats else 1
    kol_score = math.fabs(Cum1.GetBinContent(max_bin) - Cum2.GetBinContent(max_bin))
    score = Sample_Faktor * kol_score

    # f(x,y) = (x*y) / (x + y) --> d/dx f = y^{2} / (x+y)^{2}
    ##-> sqrt(f) --> d/dx sqrt(f) = 1/2sqrt(f) * d/dx f
    delta_stat = kol_score * (math.sqrt(N_Train) * N_Test * N_Test +
                              math.sqrt(N_Test) * N_Train * N_Train) / (2 * Sample_Faktor * (N_Train + N_Test)**2) if consider_stats else 0
    delta_fabs1 = Sample_Faktor * Cum1.GetBinError(max_bin) * np.sign(Cum1.GetBinContent(max_bin) - Cum2.GetBinContent(max_bin))
    delta_fabs2 = -Sample_Faktor * Cum2.GetBinError(max_bin) * np.sign(Cum1.GetBinContent(max_bin) - Cum2.GetBinContent(max_bin))

    #delta_score = math.sqrt( delta_stat*delta_stat +  delta_fabs1 * delta_fabs1 + delta_fabs2 *delta_fabs2)
    delta_score = math.fabs(delta_stat + delta_fabs1 + delta_fabs2)
    return score, delta_score


def Chi2Test(train_hist, test_hist):
    chi2 = 0
    nDof = 0
    for i in range(1, GetNbins(train_hist)):
        #### Okay the Chi2 with normalized distributions is a bit meaningless
        chi2 += math.pow((train_hist.GetEntries() * train_hist.GetBinContent(i) - test_hist.GetEntries() * test_hist.GetBinContent(i)),
                         2) / (test_hist.GetBinError(i) * test_hist.GetEntries() if test_hist.GetBinError(i) > 0 else 1)
        if train_hist.GetBinContent(i) > 0: nDof += 1
    return chi2, nDof


def makeOverTrainPlot(Options,
                      SignalTestHisto,
                      SignalTrainHisto,
                      BackgroundTestHisto,
                      BackgroundTrainHisto,
                      method,
                      substr="",
                      add_info=""):
    if not BackgroundTestHisto or not SignalTestHisto or not BackgroundTrainHisto or not SignalTrainHisto:
        print 'WARNING: Could not retrieve ROC plot for method %s... skipping...' % (method)
        return False

    pu = PlotUtils(status=Options.label, size=24)
    pu.Prepare1PadCanvas(method + substr, 800, 600)

    BackgroundTestHisto.SetLineColor(2)
    BackgroundTestHisto.SetTitle("Background (testing sample)")
    BackgroundTestHisto.SetFillColor(2)
    BackgroundTestHisto.SetFillStyle(3354)
    BackgroundTestHisto.SetMarkerStyle(20)
    BackgroundTestHisto.SetMarkerSize(0.7)
    BackgroundTestHisto.SetLineWidth(1)
    BackgroundTestHisto.SetMarkerColor(2)

    SignalTestHisto.SetTitle("Signal (testing sample)")
    SignalTestHisto.SetLineColor(4)
    SignalTestHisto.SetFillColor(4)
    SignalTestHisto.SetFillStyle(3345)
    SignalTestHisto.SetMarkerStyle(20)
    SignalTestHisto.SetMarkerSize(0.7)
    SignalTestHisto.SetLineWidth(1)
    SignalTestHisto.SetMarkerColor(2)

    BackgroundTrainHisto.SetTitle("Background (training sample)")
    BackgroundTrainHisto.SetLineColor(2)
    BackgroundTrainHisto.SetMarkerStyle(20)
    BackgroundTrainHisto.SetMarkerSize(0.7)
    BackgroundTrainHisto.SetLineWidth(1)
    BackgroundTrainHisto.SetMarkerColor(2)

    SignalTrainHisto.SetTitle("Signal (training sample)")
    SignalTrainHisto.SetLineColor(4)
    SignalTrainHisto.SetMarkerStyle(20)
    SignalTrainHisto.SetMarkerSize(0.7)
    SignalTrainHisto.SetLineWidth(1)
    SignalTrainHisto.SetMarkerColor(4)

    Histos = [BackgroundTestHisto, SignalTestHisto, BackgroundTrainHisto, SignalTrainHisto]
    ymax = max([H.GetMaximum() for H in Histos])
    xmin = min([H.GetXaxis().GetBinLowEdge(1) for H in Histos])
    xmax = max([H.GetXaxis().GetBinUpEdge(H.GetNbinsX()) for H in Histos])

    styling = BackgroundTestHisto.Clone("%s_style" % BackgroundTestHisto.GetName())
    styling.Reset()
    styling.GetXaxis().SetRangeUser(xmin, xmax)
    styling.GetYaxis().SetRangeUser(0., ymax * 1.47)
    styling.GetYaxis().SetTitle("(1/N) dN/dx")

    if len(Options.xlabel) == 0:
        styling.GetXaxis().SetTitle('%s response' % method)
    else:
        styling.GetXaxis().SetTitle(Options.xlabel)

    styling.Draw("AXIS")
    BackgroundTestHisto.Draw("histsame")
    SignalTestHisto.Draw("histsame")
    BackgroundTrainHisto.Draw("same")
    SignalTrainHisto.Draw("same")

    height = 0.895
    if not Options.noATLAS:
        pu.DrawAtlas(0.19, height)
        height -= 0.05
    pu.DrawSqrtS(0.19, height)
    height -= 0.04
    pu.DrawTLatex(0.19, height, "Kolmogorow-Smirnov test:", size=18)
    height -= 0.045
    kol_sig, delta_kol_sig = KolmogorovSmirnovScore(SignalTrainHisto, SignalTestHisto)
    chi2_sig, nDof_sig = Chi2Test(SignalTrainHisto, SignalTestHisto)
    pu.DrawTLatex(0.19, height, " - signal: %.2f #pm %.2f " % (kol_sig, delta_kol_sig), size=18)
    height -= 0.04
    kol_bk, delta_kol_bk = KolmogorovSmirnovScore(BackgroundTrainHisto, BackgroundTestHisto)
    chi2_bk, nDof_bk = Chi2Test(BackgroundTrainHisto, BackgroundTestHisto)

    pu.DrawTLatex(0.19, height, " - background: %.2f #pm %.2f" % (kol_bk, delta_kol_bk), size=18)

    pu.DrawTLatex(0.53,
                  0.715,
                  "#chi^{2}_{bkg/sig}: %.1f(%d) / %.1f(%d)" % (chi2_bk / (nDof_bk - 1) if nDof_bk > 1 else -1., nDof_bk, chi2_sig /
                                                               (nDof_sig - 1) if nDof_sig > 1 else -1., nDof_sig),
                  size=16)
    if len(Options.regionLabel) > 0:
        pu.DrawTLatex(0.53, 0.89, Options.regionLabel, 18)

    pu.CreateLegend(0.53, 0.74, 0.715, 0.875)
    pu.AddToLegend(BackgroundTrainHisto, Style="PL")
    pu.AddToLegend(BackgroundTestHisto, Style="L")
    pu.AddToLegend(SignalTrainHisto, Style="PL")
    pu.AddToLegend(SignalTestHisto, Style="L")
    if len(add_info) > 0: pu.DrawTLatex(0.53, 0.7, add_info, 18)

    pu.DrawLegend()

    if not Options.summaryPlotOnly:
        pu.saveHisto("%s/Overtrain_%s%s" % (Options.output, method, substr), ["pdf"])
    pu.saveHisto("%s/AllOverTrainPlots" % (Options.output), ["pdf"])
    return True


if __name__ == "__main__":
    from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser

    parser = setupSciKitPlotParser(prog='GetRocAndOvertrain',
                                   description='This script makes plots of the ROC curves and the Overtrain Plot done by SciKitLearn')
    parser.set_defaults(output="OverTrainPlots/")
    parser.add_argument('--xlabel', help='specify the xlabel of you graph. Default: Name of the variable', default='')

    Options = parser.parse_args()
    setupPlottingEnviroment(Options)

    ROOTFiles = []
    for myfile in sorted(os.listdir(Options.inputPath)):
        if not myfile.endswith(".root"): continue
        if myfile.rfind("_swapped") != -1: continue

        ROOTFiles += [(myfile[:myfile.rfind(".")], Options.inputPath + '/' + myfile)]
        cross_valid = "%s/%s_swapped.root" % (Options.inputPath, myfile[:myfile.rfind(".")])
        if not os.path.exists(cross_valid): continue
        ROOTFiles += [(myfile[:myfile.rfind(".")] + " cross validation", cross_valid)]

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % Options.inputPath
        sys.exit(1)

    PlottedOnce = False
    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    DummyCanvas.SaveAs("%s/AllOverTrainPlots.pdf[" % (Options.output))

    n_Opened = 0
    for method, ifile in ROOTFiles:
        if n_Opened % 100 == 0: GetFileHandler().closeAll()
        InputFile = GetFileHandler().LoadFile(ifile)
        n_Opened += 1

        Diagnostic_Dir = InputFile.GetDirectory("Diagnostics/")

        if not Diagnostic_Dir:
            print "WARNING: Could not find any training plot in file %s... Skipping" % (ifile),
            continue
        DiagnosticsHistos = [
            key.GetName() for key in Diagnostic_Dir.GetListOfKeys()
            if key.ReadObj().InheritsFrom("TH1") and key.ReadObj().GetDimension() == 1
        ]

        if len([Ele for Ele in ['MVA_SigTesting', 'MVA_SigTraining', 'MVA_BkgTesting', 'MVA_BkgTraining']
                if Ele in DiagnosticsHistos]) == 4:
            SignalTestHisto = Diagnostic_Dir.Get('MVA_SigTesting')
            SignalTrainHisto = Diagnostic_Dir.Get('MVA_SigTraining')
            BackgroundTestHisto = Diagnostic_Dir.Get('MVA_BkgTesting')
            BackgroundTrainHisto = Diagnostic_Dir.Get('MVA_BkgTraining')

            PlottedOnce = makeOverTrainPlot(Options, SignalTestHisto, SignalTrainHisto, BackgroundTestHisto, BackgroundTrainHisto,
                                            method) or PlottedOnce

        if len([
                Ele for Ele in ['MVA_Raw_SigTesting', 'MVA_Raw_SigTraining', 'MVA_Raw_BkgTesting', 'MVA_Raw_BkgTraining']
                if Ele in DiagnosticsHistos
        ]) == 4:
            SignalTestHisto = Diagnostic_Dir.Get('MVA_Raw_SigTesting')
            SignalTrainHisto = Diagnostic_Dir.Get('MVA_Raw_SigTraining')
            BackgroundTestHisto = Diagnostic_Dir.Get('MVA_Raw_BkgTesting')
            BackgroundTrainHisto = Diagnostic_Dir.Get('MVA_Raw_BkgTraining')

            PlottedOnce = makeOverTrainPlot(Options,
                                            SignalTestHisto,
                                            SignalTrainHisto,
                                            BackgroundTestHisto,
                                            BackgroundTrainHisto,
                                            method,
                                            substr="RAW",
                                            add_info="Evaluated before decorrelation") or PlottedOnce

        if len([
                Ele for Ele in ['MVA_Decorr_SigTesting', 'MVA_Decorr_SigTraining', 'MVA_Decorr_BkgTesting', 'MVA_Decorr_BkgTraining']
                if Ele in DiagnosticsHistos
        ]) == 4:
            SignalTestHisto = Diagnostic_Dir.Get('MVA_Decorr_SigTesting')
            SignalTrainHisto = Diagnostic_Dir.Get('MVA_Decorr_SigTraining')
            BackgroundTestHisto = Diagnostic_Dir.Get('MVA_Decorr_BkgTesting')
            BackgroundTrainHisto = Diagnostic_Dir.Get('MVA_Decorr_BkgTraining')

            PlottedOnce = makeOverTrainPlot(Options,
                                            SignalTestHisto,
                                            SignalTrainHisto,
                                            BackgroundTestHisto,
                                            BackgroundTrainHisto,
                                            method,
                                            substr="DECORR",
                                            add_info="Evaluated after decorrelation") or PlottedOnce

    DummyCanvas.SaveAs("%s/AllOverTrainPlots.pdf]" % (Options.output))
    if not PlottedOnce:
        os.system("rm %s/AllOverTrainPlots.pdf" % (Options.output))
