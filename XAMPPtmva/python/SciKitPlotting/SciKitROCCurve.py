#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from scipy.integrate import simps
from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser
from XAMPPplotting.FileStructureHandler import GetFileHandler


def IntegrateTGraph(mygraph):
    if not mygraph: return 0
    xCleaned = [round(mygraph.GetX()[i], 7) for i in range(mygraph.GetN()) if i == 0 or mygraph.GetX()[i - 1] != mygraph.GetX()[i]]
    yCleaned = [round(mygraph.GetY()[i], 7) for i in range(mygraph.GetN()) if i == 0 or mygraph.GetX()[i - 1] != mygraph.GetX()[i]]
    #simpsonIntegral = simps( y=xCleaned, x=xCleaned)
    myIntegral = 0.
    ### Simpson integral not to be stable. Calculate the integral via trapezoidal rule
    for idx, x in enumerate(xCleaned):
        if x == xCleaned[-1]: break
        # 1/2 * (x2-x1) * (y1+y2)
        myIntegral += (xCleaned[idx + 1] - x) * 0.5 * (yCleaned[idx + 1] + yCleaned[idx])
    return myIntegral


def IntegralErrorTGraph(mygraph):
    if not mygraph: return 0
    xCleaned = [round(mygraph.GetX()[i], 7) for i in range(mygraph.GetN()) if i == 0 or mygraph.GetX()[i - 1] != mygraph.GetX()[i]]
    yCleaned = [round(mygraph.GetY()[i], 7) for i in range(mygraph.GetN()) if i == 0 or mygraph.GetX()[i - 1] != mygraph.GetX()[i]]
    xCleaned_Err = [
        round(mygraph.GetErrorXhigh(i), 7) for i in range(mygraph.GetN()) if i == 0 or mygraph.GetX()[i - 1] != mygraph.GetX()[i]
    ]
    yCleaned_Err = [
        round(mygraph.GetErrorYhigh(i), 7) for i in range(mygraph.GetN()) if i == 0 or mygraph.GetX()[i - 1] != mygraph.GetX()[i]
    ]

    myIntegral = 0.
    for idx, x in enumerate(xCleaned):
        if x == xCleaned[-1]: break
        # 1/2 * (x2-x1) * (y1+y2)
        myIntegral += ((xCleaned[idx + 1] - x)**2) * (yCleaned_Err[idx + 1]**2 + yCleaned_Err[idx]**2)
        myIntegral += (xCleaned_Err[idx + 1]**2 + xCleaned_Err[idx]**2) * (yCleaned[idx + 1] + yCleaned[idx])**2

    return 0.5 * math.sqrt(myIntegral)


def drawROCCurve(Options, ROC_Train_Def, ROC_Test_Def, ROC_Train_Val=None, ROC_Test_Val=None, method="", substr=""):
    ### Either the default curves are not given or only one of the validation curves is given
    if not ROC_Train_Def or not ROC_Test_Def or ((ROC_Train_Val or ROC_Test_Val) and (not ROC_Train_Val or not ROC_Test_Val)):
        print "WARNING: Could not find all ROC curves"
        return False

    pu = PlotUtils(status=Options.label, size=24)
    pu.Prepare1PadCanvas(method + substr, 800, 600)

    #### Styling of the ROC curves
    ROC_Train_Def.SetLineColor(ROOT.kRed)
    ROC_Train_Def.SetTitle("normal Training (%.3f)" % (IntegrateTGraph(ROC_Train_Def)))
    if ROC_Train_Val:
        ROC_Train_Val.SetLineColor(ROOT.kRed)
        ROC_Train_Val.SetTitle("valid. Training (%.3f)" % (IntegrateTGraph(ROC_Train_Val)))
        ROC_Train_Val.SetLineStyle(2)  #-----------------

    ROC_Test_Def.SetLineColor(ROOT.kBlue)
    ROC_Test_Def.SetTitle("normal Testing (%.3f)" % (IntegrateTGraph(ROC_Test_Def)))

    if ROC_Test_Val:
        ROC_Test_Val.SetLineColor(ROOT.kBlue)
        ROC_Test_Val.SetTitle("valid. Testing (%.3f)" % (IntegrateTGraph(ROC_Test_Val)))
        ROC_Test_Val.SetLineStyle(2)  #-----------------

    for x, curve in enumerate([ROC_Train_Def, ROC_Test_Def, ROC_Train_Val, ROC_Test_Val]):
        if not curve: continue
        curve.SetLineWidth(2)
        #### Error bars look nasty erase them from the plot
        #for i in range( curve.GetN() if x > 0  else 0): curve.SetPointError(i,0,0,0,0)
        if x == 0: curve.SetFillColorAlpha(ROOT.kYellow, 0.41)
        if not Options.noLineSmoothing:
            curve.Draw("sameC" if x > 0 else "ACE2")
        else:
            curve.Draw("sameL" if x > 0 else "ALE2")

    ### Create the legend
    x_legend = 0.2 if not Options.invBkgEff else 0.53
    y_legend = 0.46 if not Options.invBkgEff else 0.86
    if len(Options.regionLabel) > 0:
        pu.DrawTLatex(x_legend, y_legend + 0.01, Options.regionLabel, 18)
    if not Options.noATLAS: pu.DrawAtlas(x_legend, y_legend - 0.04)
    pu.DrawSqrtS(x_legend, y_legend - 0.09)

    pu.DrawTLatex(x_legend + 0.16, y_legend - 0.09, method if len(Options.methodName) == 0 else Options.methodName)
    pu.CreateLegend(x_legend, y_legend - 0.26, x_legend + 0.3, y_legend - 0.11)
    pu.AddToLegend(ROC_Train_Def, Style="L")
    if ROC_Train_Val: pu.AddToLegend(ROC_Train_Val, Style="L")

    pu.AddToLegend(ROC_Test_Def, Style="L")
    if ROC_Test_Val: pu.AddToLegend(ROC_Test_Val, Style="L")

    pu.DrawLegend()

    if Options.invBkgEff: pu.GetCanvas().SetLogy()
    if not Options.summaryPlotOnly:
        if not Options.invBkgEff:
            pu.saveHisto("%s/ROC_%s%s" % (Options.output, method, substr), ["pdf"])
        else:
            pu.saveHisto("%s/InvBkgSigEff_%s%s" % (Options.output, method, substr), ["pdf"])

    pu.saveHisto("%s/All%sCurves" % (Options.output, "ROC" if not Options.invBkgEff else "InvBkgSigEff"), ["pdf"])
    return True


def setupSciKitROCparser(prog, description):
    parser = setupSciKitPlotParser(prog=prog, description=description)
    parser.set_defaults(output="ROCCurves")
    parser.add_argument('--noLineSmoothing', help='Disable the smooth draw option of ROOT', action='store_true', default=False)
    parser.add_argument('--invBkgEff',
                        help="Use the inverse background efficiency instead of the background rejection",
                        action="store_true",
                        default=False)
    parser.add_argument('--methodName', help='Define a proper name of the training', default='')
    return parser


if __name__ == "__main__":
    parser = setupSciKitROCparser(prog='SciKitROCCurve',
                                  description='This script makes plots of the ROC curves and the Overtrain Plot done by SciKit learn')
    Options = parser.parse_args()
    setupPlottingEnviroment(Options)

    ROOTFiles = []
    for myfile in sorted(os.listdir(Options.inputPath)):
        if not myfile.endswith(".root"): continue
        if myfile.rfind("_swapped") != -1: continue
        cross_valid = "%s/%s_swapped.root" % (Options.inputPath, myfile[:myfile.rfind(".")])
        if not os.path.exists(cross_valid): continue
        ROOTFiles += [(myfile[:myfile.rfind(".")], Options.inputPath + '/' + myfile, cross_valid)]

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % Options.inputPath
        sys.exit(1)

    PlottedOnce = False
    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    DummyCanvas.SaveAs("%s/All%sCurves.pdf[" % (Options.output, "ROC" if not Options.invBkgEff else "InvBkgSigEff"))
    n_opened = 0
    for method, default_file, validation_file in ROOTFiles:
        if n_opened % 100 == 0: GetFileHandler().closeAll()
        n_opened += 1

        InputFile_Default = ROOT.TFile.Open(default_file)
        InputFile_Validation = ROOT.TFile.Open(validation_file)

        Diag_Def = InputFile_Default.GetDirectory("Diagnostics/")
        Diag_Val = InputFile_Validation.GetDirectory("Diagnostics/")
        if not Diag_Def or not Diag_Val:
            print "WARNING: Could not find any training plot in file %s... Skipping" % (default_file)
            continue

        ROC_Train_Def = Diag_Def.Get("MVA_%s_Train" % ("roc" if not Options.invBkgEff else "inveffAeff"))
        ROC_Train_Val = Diag_Val.Get("MVA_%s_Train" % ("roc" if not Options.invBkgEff else "inveffAeff"))

        ROC_Test_Def = Diag_Def.Get("MVA_%s_Testing" % ("roc" if not Options.invBkgEff else "inveffAeff"))
        ROC_Test_Val = Diag_Val.Get("MVA_%s_Testing" % ("roc" if not Options.invBkgEff else "inveffAeff"))
        PlottedOnce = drawROCCurve(Options, ROC_Train_Def, ROC_Test_Def, ROC_Train_Val, ROC_Test_Val, method) or PlottedOnce

    DummyCanvas.SaveAs("%s/All%sCurves.pdf]" % (Options.output, "ROC" if not Options.invBkgEff else "InvBkgSigEff"))
    if not PlottedOnce:
        os.system("rm %s/All%sCurves.pdf" % (Options.output, "ROC" if not Options.invBkgEff else "InvBkgSigEff"))
