#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from ClusterSubmission.Utils import CreateDirectory
import numpy as np
binlabels = {
    'Meff': 'm_{eff}',
    'MtBMin': 'm_{T}^{b,min}',
    'MET': 'E_{T}^{miss}',
    'MET_phi': '#phi(E_{T}^{miss})',
    'Met': 'E_{T}^{miss}',
    'NJets': 'N_{jets}',
    'Jet_pt0': 'p_{T}^{0}',
    'Jet_pt1': 'p_{T}^{1}',
    'Jet_pt2': 'p_{T}^{2}',
    'Jet_pt3': 'p_{T}^{3}',
    'Jet_e0': 'E^{0}',
    'Jet_e1': 'E^{1}',
    'Jet_e2': 'E^{2}',
    'Jet_e3': 'E^{3}',
    'Jet_eta0': '#eta^{0}',
    'Jet_eta1': '#eta^{1}',
    'Jet_eta2': '#eta^{2}',
    'Jet_eta3': '#eta^{3}',
    'Jet_phi0': '#phi^{0}',
    'Jet_phi1': '#phi^{1}',
    'Jet_phi2': '#phi^{2}',
    'Jet_phi3': '#phi^{3}',
    'Jet_isB0': 'is b^{0}',
    'Jet_isB1': 'is b^{1}',
    'Jet_isB2': 'is b^{2}',
    'Jet_isB3': 'is b^{3}',
    'mt2': 'm_{T2}',
    'MtBMax': 'm_{T}^{b,max}',
    'HtSig': 'E_{T}^{miss}/#sqrt{H_{T}}',
    'AntiKt12M0': 'm^{0}_{jet, R=1.2}',
    'AntiKt12M1': 'm^{1}_{jet, R=1.2}',
    'NBJets': 'N_{b-jets}',
    'AntiKt8M0': 'm^{0}_{jet, R=0.8}',
    'DRBB': '#DeltaR(b, b)',
    'MtNonBMax': 'm_{T}^{non-b, max}',
    'MtNonBMin': 'm_{T}^{non-b, min}',

    #ISR variables:
    'BJet_pt0': 'p_{T,b}^{0,S}',
    'MS': 'm_{S}',
    'dphiISRMet': '|#Delta#phi_{ISR, E_{T}^{miss}}|',
    'ptISR': 'p_{T}^{ISR}',
    'RISR': 'R_{ISR}',
    "tau_pt": "p_{T}(#tau)",
    "lepton_pt": "p_{T}(l)",
    "dPhi_TauLepton": "|#Delta#phi(#tau,l)|",
    "dEta_TauLepton": "|#Delta#eta(#tau,l)|",
    "lepton_MT": "m_{T}(l,E^{miss}_{T})",
    "tau_MT": "m_{T}(#tau,E^{miss}_{T})",
    "M_TauLep": "m(#tau,l)",
    "SumMT": "#Sigma m_{T}",
    "MET_CosMinDeltaPhi": "cos(#Delta#phi_{min})",
    "MET_SumCosDeltaPhi": "#Sigma cos(#Delta#phi)",
    "LepTau_VecSumPt": "#vec{p}_{T}(#tau) + #vec{p}_{T}(l)",
    "MCT": "m_{CT}(#tau,l)",
    "dR_LepTau": "#DeltaR(#tau,l)",
    "TauLep_Balance": "#tau #minus#minus#it{l} balance",
    "VisInvis_Balance": "#tau+#it{l}#minus#minus E^{miss}_{T} balance",
    "dPhi_TauJet": "|#Delta#phi(#tau,jet)|",
    "jet_pt": "p_{T}(jet)",
    "n_Jets": "N_{jets}",
    "RJRZ_m": "m_{#tau, l, E_{T}^{miss}}^{RJ Z}",
    "RJRW_m": "m_{#tau, l, E_{T}^{miss}}^{RJ W}",
    "MET_Et": "E_{T}^{miss}",
    "MET_Tau_MT": "m_{T}(#tau, E_{T}^{miss})",
    "MET_Lep_Tau_SumMT": "#sum m_{T}(i, E_{T}^{miss})",
    "MET_significance": "S_{E_{T}^{miss}}",
    "LepTau_Mvis": "m_{#tau,l}",
    "LepTau_dEta": "#Delta#eta(#tau, l)",
    "Tau_BDTJetScoreSigTrans": "#tau-ID",
    "MET_Lep_MT": "m_{T}(l, E_{T}^{miss})",
    "MT2_max": "m_{T2}",
    "MT2": "m_{T2}",
    "MET_LepTau_MT": "m_{T}(E_{T}^{miss}, #tau+l)",
    "MET_LepTau_VecSumPt": "#vec{p}_{T}(#tau) + #vec{p}_{T}(l) + #vec{p}^{miss}_{T}",
    "Fox_wolfram_moment_3": "3^{rd} Fox-Wolfram",
    "Fox_wolfram_moment_2": "2^{nd} Fox-Wolfram",
    "Fox_wolfram_moment_1": "1^{st} Fox-Wolfram",
    "Fox_wolfram_moment_4": "4^{th} Fox-Wolfram",
    "Fox_wolfram_moment_5": "5^{th} Fox-Wolfram",
    "Fox_wolfram_moment_6": "6^{th} Fox-Wolfram",
    "Fox_wolfram_moment_7": "7^{th} Fox-Wolfram",
    "Fox_wolfram_moment_8": "8^{th} Fox-Wolfram",
    "MET_Centrality": "E_{T}^{miss} centrality",
}


def determinant(matrix):
    mat_array = np.zeros((matrix.GetNbinsX(), matrix.GetNbinsY()))
    for x in range(1, matrix.GetNbinsX() + 1):
        for y in range(1, matrix.GetNbinsY() + 1):
            mat_array[x - 1][y - 1] = matrix.GetBinContent(x, y)
    return np.linalg.det(mat_array)


def SkimMatrix(histo, entriesToRemove=[]):

    bins_toskim = [x for x in range(1, histo.GetNbinsX() + 1) if histo.GetXaxis().GetBinLabel(x) in entriesToRemove]
    if len(bins_toskim) == 0: return histo
    newhisto = ROOT.TH2D("%s_skimmed" % histo.GetName(), "%s_skimmed" % histo.GetName(),
                         histo.GetNbinsX() - len(bins_toskim), 0,
                         histo.GetNbinsX() - len(bins_toskim),
                         histo.GetNbinsY() - len(bins_toskim), 0,
                         histo.GetNbinsY() - len(bins_toskim))

    xnew = 0
    for x in range(1, histo.GetNbinsX() + 1):
        if x in bins_toskim: continue
        xnew += 1
        newhisto.GetXaxis().SetBinLabel(xnew, histo.GetXaxis().GetBinLabel(x))
        ynew = 0
        for y in range(1, histo.GetNbinsY() + 1):
            if y in bins_toskim: continue
            ynew += 1
            newhisto.GetYaxis().SetBinLabel(ynew, histo.GetYaxis().GetBinLabel(y))
            newhisto.SetBinContent(xnew, ynew, histo.GetBinContent(x, y))
            newhisto.SetBinError(xnew, ynew, histo.GetBinError(x, y))
    newhisto.SetDirectory(0)
    newhisto.GetZaxis().SetTitle("Correlation [%]")
    newhisto.GetZaxis().SetTitleOffset(newhisto.GetZaxis().GetTitleOffset() * 1.2)
    newhisto.GetXaxis().LabelsOption("v")
    return newhisto


def RenameAxisLabels(histo):
    if not histo.InheritsFrom("TH2"): return
    histo.GetYaxis().SetLabelSize(0.04)
    histo.GetXaxis().SetLabelSize(0.048)
    for i in range(1, histo.GetNbinsX() + 1):
        if histo.GetXaxis().GetBinLabel(i) in binlabels.iterkeys():
            histo.GetXaxis().SetBinLabel(i, binlabels[histo.GetXaxis().GetBinLabel(i)])
    for i in range(1, histo.GetNbinsY() + 1):
        if histo.GetYaxis().GetBinLabel(i) in binlabels.iterkeys():
            histo.GetYaxis().SetBinLabel(i, binlabels[histo.GetYaxis().GetBinLabel(i)])


def createColourPalette():
    Red = array('d', [0., 0., 0., 0.3, 0.9, 0.8, 0.8])
    Green = array('d', [0., 0.6, 0.9, 0.8, 0.9, 0.6, 0.])
    Blue = array('d', [0.8, 0.8, 0.9, 0., 0., 0., 0.])
    Length = array('d', [0., 0.05, 0.25, 0.5, .75, .95, 1.])
    #ROOT.TColor.CreateGradientColorTable(114, Length, Red, Green, Blue, 1500)
    ROOT.gStyle.SetPalette(ROOT.kViridis)
    #ROOT.gStyle.SetPalette(ROOT.kDarkBodyRadiator)


def drawMatrix(Options, CorrMatrix, CanvasName):
    if not CorrMatrix or not CorrMatrix.InheritsFrom("TH2"): return False

    pu = PlotUtils(status=Options.label, size=21)

    pu.Prepare1PadCanvas(CanvasName, 1200, 600)
    pu.GetCanvas().SetRightMargin(0.18)
    pu.GetCanvas().SetLeftMargin(0.15)
    pu.GetCanvas().SetTopMargin(0.15)

    ## styling of the matrix
    CorrMatrix = SkimMatrix(CorrMatrix, Options.skimVars)
    RenameAxisLabels(CorrMatrix)

    CorrMatrix.SetContour(1500)
    CorrMatrix.GetZaxis().SetRangeUser(-100., 100.)
    CorrMatrix.Draw("colz")
    CorrMatrix.GetXaxis().SetLabelOffset(CorrMatrix.GetXaxis().GetLabelOffset() * 1.07)
    #custom content printout
    for i in xrange(1, CorrMatrix.GetNbinsX() + 1):
        for j in xrange(1, CorrMatrix.GetNbinsY() + 1):
            z = CorrMatrix.GetBinContent(i, j)
            x = CorrMatrix.GetXaxis().GetBinLowEdge(i) + CorrMatrix.GetXaxis().GetBinWidth(i) * 0.1
            y = CorrMatrix.GetYaxis().GetBinCenter(j)
            text = "%.1f" % (z)
            color = 2 if math.fabs(z) > 75. and i != j else 0 if math.fabs(z) < 25 else 1
            font = 43  #62 if math.fabs(z) > 0.9 else 43
            pu.DrawTLatex(x=x, y=y, text=text, size=12, font=font, align=12, color=color, ndc=False)

    ### What kind of matrix do we have
    if CanvasName.find("Sig") != -1:
        pu.DrawTLatex(0.76, 0.87, "Signal (%s)" % ("training" if CanvasName.find("Train") != -1 else "testing"), size=20)
    elif CanvasName.find("Bkg") != -1:
        pu.DrawTLatex(0.76, 0.87, "Background (%s)" % ("training" if CanvasName.find("Train") != -1 else "testing"), size=20)
    else:
        pu.DrawTLatex(0.76, 0.87, "All (%s)" % ("training" if CanvasName.find("Train") != -1 else "testing"), size=20)

    if not Options.noATLAS: pu.DrawAtlas(pu.GetCanvas().GetLeftMargin(), 0.92)
    pu.DrawSqrtS(pu.GetCanvas().GetLeftMargin(), 0.87)

    if CanvasName.find("inEigen") != -1:
        pu.DrawTLatex(0.76, 0.92, "Transformed")

    #pu.DrawTLatex(0.4, 0.87, "Determinant %.3E" % (determinant(CorrMatrix)), size=18)
    if len(Options.regionLabel) > 0:
        pu.DrawTLatex(0.4, 0.92, Options.regionLabel, size=20)

    if not Options.summaryPlotOnly:
        pu.saveHisto("%s/%s" % (Options.output, CanvasName), ["pdf"])
    pu.saveHisto("%s/AllCorrelationMatrices" % (Options.output), ["pdf"])
    return True


if __name__ == "__main__":
    from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser
    parser = setupSciKitPlotParser(prog='SciKitCorrelationMatrix', description='This script creates the correlation matrix plots')
    parser.add_argument('--skimVars', help='Specify list of variables not to draw', default=[], nargs='+')
    parser.set_defaults(output="CovMatrices")
    Options = parser.parse_args()

    InputFile = setupPlottingEnviroment(Options)
    createColourPalette()

    Matrices = [
        "MVA_correlation_Training",
        "MVA_correlation_Bkg_Train",
        "MVA_correlation_Sig_Train",
        "MVA_correlation_Training_inEigen",
        "MVA_correlation_Bkg_Train_inEigen",
        "MVA_correlation_Sig_Train_inEigen",
        "MVA_correlation_Testing",
        "MVA_correlation_Testing_inEigen",
        "MVA_correlation_Bkg_Test",
        "MVA_correlation_Sig_Test",
        "MVA_correlation_Bkg_Test_inEigen",
        "MVA_correlation_Sig_Test_inEigen",
    ]
    DummyCanvas = ROOT.TCanvas("dumm", "dummy", 1200, 600)
    DummyCanvas.SaveAs("%s/AllCorrelationMatrices.pdf[" % (Options.output))
    PlottedOnce = False

    for Mat in Matrices:
        CorrMatrix = InputFile.Get('Diagnostics/%s' % (Mat))
        PlottedOnce = drawMatrix(Options, CorrMatrix, "CorrelationMatrix%s" % (Mat[len("MVA_correlation_"):])) or PlottedOnce

    DummyCanvas.SaveAs("%s/AllCorrelationMatrices.pdf]" % (Options.output))
    if not PlottedOnce:
        os.system("rm %s/AllCorrelationMatrices.pdf" % (Options.output))
