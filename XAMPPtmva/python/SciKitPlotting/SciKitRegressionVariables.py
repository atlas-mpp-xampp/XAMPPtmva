#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from ClusterSubmission.Utils import CreateDirectory, id_generator
from XAMPPplotting.Utils import CreateRatioHistos, PrintHistogram
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPtmva.SciKitTrainingVariables import setupSciKitPlotParser, setupPlottingEnviroment
from XAMPPtmva.SciKitCorrelationMatrix import binlabels
from XAMPPtmva.SciKitOvertrainPlot import Chi2Test
import logging


def FitResolution(InFile, variable, isTesting):
    reso_histo = InFile.Get("Diagnostics/Resolution_%s/MVA_%s" % ("Training" if not isTesting else "Testing", variable))
    if not reso_histo:
        logging.warning("No resolution histogram could be found")
        return 0, -1
    fit_func = ROOT.TF1(id_generator(24), "gaus(0)", -1, 1)
    reso_histo.Fit(fit_func, "EMRN0S", "", -1, 1)
    return fit_func.GetParameter(1), fit_func.GetParameter(2)


if __name__ == "__main__":
    parser = setupSciKitPlotParser(prog='SciKitRegressionVariables',
                                   description='This script plots the predicted distributions against their true values')
    parser.add_argument("--plotTesting", help="Plot the testing distributions instead the ings", action='store_true', default=False)
    parser.set_defaults(output="RegressionPerformance/")

    Options = parser.parse_args()
    setupPlottingEnviroment(Options)

    ROOTFiles = []
    for myfile in sorted(os.listdir(Options.inputPath)):
        if not myfile.endswith(".root"): continue
        if myfile.rfind("_swapped") != -1: continue

        ROOTFiles += [(myfile[:myfile.rfind(".")], Options.inputPath + '/' + myfile)]
        cross_valid = "%s/%s_swapped.root" % (Options.inputPath, myfile[:myfile.rfind(".")])
        if not os.path.exists(cross_valid): continue
        ROOTFiles += [(myfile[:myfile.rfind(".")] + " cross validation", cross_valid)]

    if len(ROOTFiles) == 0:
        logging.error("No .root files found at given path %s, exiting..." % Options.inputPath)
        sys.exit(1)

    PlottedOnce = False
    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    DummyCanvas.SaveAs("%s/AllRegressions_%s.pdf[" % (Options.output, "Training" if not Options.plotTesting else "Testing"))

    n_Opened = 0
    for method, ifile in ROOTFiles:
        # if n_Opened % 10 == 0:
        #     GetFileHandler().closeAll()
        InputFile = ROOT.TFile.Open(ifile)  # GetFileHandler().LoadFile(ifile)
        n_Opened += 1
        ### True values
        true_dir = InputFile.Get("TargetDistributions/%s" % ("Training" if not Options.plotTesting else "Testing"))
        ### Predicated values
        pred_dir = InputFile.Get("TargetDistributions/Predicted_%s" % ("Training" if not Options.plotTesting else "Testing"))
        if not true_dir or not pred_dir:
            logging.warning("Not all target distributions could be found in %s" % (ifile))
            continue
        for variable in [Key.GetName() for Key in true_dir.GetListOfKeys() if Key.ReadObj().InheritsFrom("TH1")]:

            CanvasName = "Regression_%s_%s_%s" % (method, "Train" if not Options.plotTesting else "Test", variable)
            pu = PlotUtils(status=Options.label, size=24)
            pu.Prepare2PadCanvas(CanvasName, 800, 600)
            pu.GetTopPad().cd()
            true_histo = true_dir.Get(variable)
            pred_histo = pred_dir.Get(variable)

            ## Set the styling of the true histograms
            true_histo.SetLineColor(ROOT.kRed)
            true_histo.SetFillColor(ROOT.kRed)
            true_histo.SetFillStyle(3005)
            true_histo.SetLineWidth(1)

            pred_histo.SetLineColor(ROOT.kBlue)
            pred_histo.SetFillColor(ROOT.kBlue)
            pred_histo.SetFillStyle(3345)
            pred_histo.SetLineWidth(1)
            pred_histo.SetMarkerStyle(20)
            pred_histo.SetMarkerSize(0.7)
            pred_histo.SetLineWidth(1)
            pred_histo.SetMarkerColor(2)

            styling_histo = true_histo.Clone(id_generator(54))
            styling_histo.GetYaxis().SetTitle("(1/N) dN/dx")

            pu.drawStyling(styling_histo, 0, max([true_histo.GetMaximum(), pred_histo.GetMaximum()]) * 1.3, RemoveLabel=True, TopPad=True)

            true_histo.Draw("histSame")
            pred_histo.Draw("histSame")

            true_histo.SetTitle("target")
            pred_histo.SetTitle("predicted")
            pu.CreateLegend(0.7, 0.6, 0.97, 0.85)
            pu.AddToLegend([true_histo, pred_histo], "FL")
            pu.DrawLegend()

            height = 0.8
            if not Options.noATLAS:
                pu.DrawAtlas(0.19, height)
                height -= 0.07
            pu.DrawSqrtS(0.19, height)
            height -= 0.08
            pu.DrawTLatex(0.19, height, method)
            mean, sigma = FitResolution(InFile=InputFile, variable=variable, isTesting=Options.plotTesting)
            chi2, nDof = Chi2Test(true_histo, pred_histo)
            pu.DrawTLatex(0.65, 0.5, "Event #mu=%.2f, #sigma=%.2f" % (mean, sigma), 18)
            pu.DrawTLatex(0.65, 0.44, "#chi^{2}: %.1f(%d)" % (chi2 / (nDof - 1), nDof), 18)
            ### Draw the ratio between predicted and true values
            pu.GetTopPad().RedrawAxis()
            pu.GetBottomPad().cd()
            pu.GetBottomPad().SetGridy()
            RatioLabel = ""
            ratios = CreateRatioHistos([pred_histo], true_histo)
            ymin = 0.5
            ymax = 1.55
            pu.drawRatioStyling(styling_histo, ymin, ymax, "Predicted / True")
            for r in ratios:
                r.SetFillStyle(0)
                r.Draw("histSAME")
            if not Options.summaryPlotOnly:
                pu.saveHisto("%s/%s" % (Options.output, CanvasName), ["pdf"])
            pu.GetTopPad().cd()
            pu.DrawTLatex(0.5, 0.97, variable, 0.04, 52, 22)
            pu.saveHisto("%s/AllRegressions_%s" % (Options.output, "Training" if not Options.plotTesting else "Testing"), ["pdf"])
            PlottedOnce = True

    DummyCanvas.SaveAs("%s/AllRegressions_%s.pdf]" % (Options.output, "Training" if not Options.plotTesting else "Testing"))
    if not PlottedOnce:
        os.system("rm %s/AllRegressions_%s.pdf" % (Options.output, "Training" if not Options.plotTesting else "Testing"))
