#! /usr/bin/env python
from sklearn.externals import joblib
from ClusterSubmission.Utils import FillWhiteSpaces
from IPython.display import Image
from sklearn import tree
import graphviz, os
import pydotplus
train_path = "/ptmp/mpp/junggjo9/Test_BDTWeights/Trainings/"
train_name = "LepHadBDT"

file_to_load = "%s/%s.pkl" % (train_path, train_name)
classifier = None
if os.path.isfile(file_to_load):
    print "\n\n" + FillWhiteSpaces(100, "*/")
    print "<SciKitReader> INFO: Load machine learning training stored at %s." % (file_to_load)
    print FillWhiteSpaces(100, "*/") + "\n\n"
    classifier = joblib.load(file_to_load)
else:
    raise RuntimeError("<SciKitReader> ERROR: The training %s does not exist please double check" % (file_to_load))

for t in range(classifier.num_trees()):
    clf = classifier.get_tree(t)
    dot_data = tree.export_graphviz(clf,
                                    out_file=None,
                                    feature_names=[classifier.variableName(i) for i in range(classifier.nVars())],
                                    class_names=["Background", "Signal"],
                                    filled=True,
                                    rounded=True,
                                    special_characters=True)
    #     graph = graphviz.Source(dot_data)

    graph = pydotplus.graph_from_dot_data(dot_data)
    Image(graph.create_png())
    #graph.render("iris")
    print t
