#! /usr/bin/env python

from XAMPPtmva.SciKitTrainingVariables import setupSciKitPlotParser
from ClusterSubmission.Utils import ExecuteCommands, ResolvePath
import os
if __name__ == "__main__":
    parser = setupSciKitPlotParser(prog="SciKitFastPlots",
                                   description="This script executed all SciKit plotting scripts creating only the summary pdf")
    parser.set_defaults(output="Plots/")
    parser.add_argument("--nThreads", help="How many threads should be executed simultaenously", default=16, type=int)
    parser.add_argument("--regression", help="Fast plots for regression problems", default=False, action="store_true")
    Options = parser.parse_args()

    Plot_Scripts = [
        ResolvePath("XAMPPtmva/SciKitPlotting/SciKitCorrelationMatrix.py"),
        ResolvePath("XAMPPtmva/SciKitPlotting/SciKitCorrelationPlots.py"),
    ]
    if not Options.regression:
        Plot_Scripts += [
            ResolvePath("XAMPPtmva/SciKitPlotting/SciKitOvertrainPlot.py"),
            ResolvePath("XAMPPtmva/SciKitPlotting/SciKitROCCurve.py"),
            ResolvePath("XAMPPtmva/SciKitPlotting/SciKitTrainingVariables.py"),
            ResolvePath("XAMPPtmva/SciKitPlotting/SciKitVariableRanking.py"),
            "%s --plotTesting" % (ResolvePath("XAMPPtmva/SciKitPlotting/SciKitTrainingVariables.py")),
            "%s --invBkgEff" % (ResolvePath("XAMPPtmva/SciKitPlotting/SciKitROCCurve.py")),
        ]
    else:
        Plot_Scripts += [
            ResolvePath("XAMPPtmva/SciKitPlotting/SciKitRegressionVariables.py"),
            ResolvePath("XAMPPtmva/SciKitPlotting/SciKitRegressionResolution.py"),
            "%s --plotTesting" % (ResolvePath("XAMPPtmva/SciKitPlotting/SciKitRegressionVariables.py")),
            "%s --regression" % (ResolvePath("XAMPPtmva/SciKitPlotting/SciKitTrainingVariables.py")),
        ]
    Cmds = [
        "python %s --summaryPlotOnly --inputPath %s --output %s --label \"%s\"" % (script, Options.inputPath, Options.output, Options.label)
        for script in Plot_Scripts
    ]
    ExecuteCommands(Cmds, MaxCurrent=Options.nThreads)
