#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from ClusterSubmission.Utils import CreateDirectory, id_generator
from XAMPPplotting.Utils import GetNbins, IntegrateTH2
from XAMPPtmva.SciKitCorrelationMatrix import createColourPalette


def PlotCorrelationVariable(InputDir, PlotOptions, group_name, correlation_type, variable):

    histo = InputDir.Get(variable)
    if not histo or not histo.InheritsFrom("TH2") or not histo.GetDimension() == 2:
        print "WARNING: %s is not a correlation plot" % (variable)
        return False

    pu = PlotUtils(status=PlotOptions.label, size=24)

    ### prepare the canvas
    CanvasName = "Correlation_%s-%s" % (group_name, variable)
    pu.Prepare1PadCanvas(CanvasName, 800, 600)
    pu.GetCanvas().SetRightMargin(0.18)
    pu.GetCanvas().SetLeftMargin(0.12)
    pu.GetCanvas().SetTopMargin(0.15)

    ## Histograms are normalized
    histo.GetZaxis().SetRangeUser(0., histo.GetMaximum())
    histo.SetContour(1500)
    histo.GetZaxis().SetTitleOffset(1.2 * histo.GetZaxis().GetTitleOffset())
    histo.GetYaxis().SetTitleOffset(0.8 * histo.GetYaxis().GetTitleOffset())
    if len(histo.GetYaxis().GetTitle()) > 30:
        histo.GetYaxis().SetTitleSize(0.86 * histo.GetYaxis().GetTitleSize())
        histo.GetYaxis().SetTitleOffset(1.15 * histo.GetYaxis().GetTitleOffset())

    ## Draw the histogram
    histo.Draw("colz")

    if not PlotOptions.noATLAS:
        pu.DrawAtlas(pu.GetCanvas().GetLeftMargin(), 0.92)
    pu.DrawSqrtS(pu.GetCanvas().GetLeftMargin(), 0.87)

    if group_name == "Training":
        pu.DrawTLatex(0.6, 0.92, "All training events")
    elif group_name == "Testing":
        pu.DrawTLatex(0.6, 0.92, "All testing events")

    elif group_name == "SigTraining":
        pu.DrawTLatex(0.6, 0.92, "Signal training events")
    elif group_name == "SigTesting":
        pu.DrawTLatex(0.6, 0.92, "Signal testing events")

    elif group_name == "BkgTraining":
        pu.DrawTLatex(0.6, 0.92, "Background training events")
    elif group_name == "BkgTesting":
        pu.DrawTLatex(0.6, 0.92, "Background testing events")

    else:
        pu.DrawTLatex(0.6, 0.92, group_name)
    pu.DrawTLatex(0.6, 0.87, "Correlation rate: %.1f%%" % (histo.GetCorrelationFactor() * 100))
    if len(PlotOptions.regionLabel) > 0:
        pu.DrawTLatex(0.4, 0.87, PlotOptions.regionLabel)
    if not Options.summaryPlotOnly:
        pu.saveHisto("%s/%s" % (PlotOptions.output, CanvasName), ["pdf"])
    pu.saveHisto("%s/AllCorrelationPlots_%s_%s" % (PlotOptions.output, group_name, correlation_type), ["pdf"])

    return True


### The idea is to visualize the differences in the correlation matrices between signal and background
def spotDeviationsPlotCorrelationVariable(DistDir, PlotOptions, signal_grp, background_grp):
    PlottedOnce = False

    DummyCanvas = ROOT.TCanvas("dummy_%s_%s" % (signal_grp, background_grp), "dummy", 2400, 600)
    DummyCanvas.SaveAs("%s/AllDifferencePlots_%s_%s.pdf[" % (Options.output, signal_grp, background_grp))
    for corr_type in [key.GetName() for key in DistDir.Get(signal_grp).GetListOfKeys() if key.ReadObj().InheritsFrom("TDirectory")]:
        sig_dir = DistDir.Get("%s/%s" % (signal_grp, corr_type))
        bkg_dir = DistDir.Get("%s/%s" % (background_grp, corr_type))
        if not sig_dir or not bkg_dir:
            print "How were you able to plot %s or %s before?" % (sginal_grp, background_grp)
            sys.exit(1)
        for var in sorted([key.GetName() for key in sig_dir.GetListOfKeys()]):
            sig_histo = sig_dir.Get(var)
            bkg_histo = bkg_dir.Get(var)

            delta_histo = sig_histo.Clone(id_generator(20))
            if not delta_histo.Add(bkg_histo, -1): return False

            for i in range(1, GetNbins(delta_histo)):
                abs_content = math.fabs(delta_histo.GetBinContent(i))
                ### We cannot make any firm statement about this bin, because we do not have any statistics
                if abs_content < bkg_histo.GetBinError(i) or abs_content < sig_histo.GetBinError(i):
                    abs_content = 0.
                delta_histo.SetBinContent(i, abs_content)

            ### Useless plots there is nothing sensible in it
            if IntegrateTH2(delta_histo) <= 1.e-6: continue
            PlottedOnce = True
            CanvasName = "DifferenceInCorr_%s_%s_%s" % (signal_grp, background_grp, var)

            pu = PlotUtils(status=PlotOptions.label, size=24)
            pu.Prepare1PadCanvas(CanvasName, 2400, 600)
            pu.GetCanvas().SetRightMargin(0.18)
            pu.GetCanvas().SetLeftMargin(0.12)
            pu.GetCanvas().SetTopMargin(0.15)

            Pad1 = ROOT.TPad("p1_" + CanvasName, CanvasName, 0.0, 0.0, 0.33, 1.)
            Pad2 = ROOT.TPad("p2_" + CanvasName, CanvasName, 0.34, 0.0, 0.66, 1.)
            Pad3 = ROOT.TPad("p2_" + CanvasName, CanvasName, 0.67, 0.0, 1., 1.)

            Pad1.Draw()
            Pad2.Draw()
            Pad3.Draw()

            Pad1.cd()
            bkg_histo.Draw("colz")
            bkg_histo.SetContour(1500)

            Pad2.cd()
            sig_histo.Draw("colz")
            sig_histo.SetContour(1500)

            Pad3.cd()
            delta_histo.GetZaxis().SetRangeUser(0., delta_histo.GetMaximum())
            ## Draw the histogram
            delta_histo.Draw("colz")
            delta_histo.SetContour(1500)

            #            if not PlotOptions.noATLAS: pu.DrawAtlas(pu.GetCanvas().GetLeftMargin(), 0.92)
            #            pu.DrawSqrtS(pu.GetCanvas().GetLeftMargin(), 0.87)

            #            if len(PlotOptions.regionLabel) > 0: pu.DrawTLatex(0.4, 0.87, PlotOptions.regionLabel)

            if not Options.summaryPlotOnly:
                pu.saveHisto("%s/%s" % (PlotOptions.output, CanvasName), ["pdf"])
            pu.saveHisto("%s/AllDifferencePlots_%s_%s" % (Options.output, signal_grp, background_grp), ["pdf"])

    DummyCanvas.SaveAs("%s/AllDifferencePlots_%s_%s.pdf]" % (Options.output, signal_grp, background_grp))
    if not PlottedOnce:
        os.system("rm %s/AllDifferencePlots_%s_%s.pdf" % (Options.output, signal_grp, background_grp))


if __name__ == "__main__":
    from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser

    parser = setupSciKitPlotParser(prog='SciKitCorrelationPlots',
                                   description='This script makes plots of the correlation plots made by SciKitDiagnostics')
    parser.set_defaults(output="CorrelationPlots")
    Options = parser.parse_args()

    InputFile = setupPlottingEnviroment(Options)
    createColourPalette()
    Distribution_Dir = InputFile.GetDirectory("Distributions")
    if not Distribution_Dir:
        print "Could not find any distributions in %s" % (InputFile.GetName())
        sys.exit(1)

    Groups = [key.GetName() for key in Distribution_Dir.GetListOfKeys()]
    for grp in Groups:
        for corr in [key.GetName() for key in Distribution_Dir.Get(grp).GetListOfKeys() if key.ReadObj().InheritsFrom("TDirectory")]:
            correlation_dir = Distribution_Dir.GetDirectory("%s/%s/" % (grp, corr))
            if not correlation_dir: continue
            variables = sorted([key.GetName() for key in correlation_dir.GetListOfKeys()])

            DummyCanvas = ROOT.TCanvas("dummy_%s_%s" % (grp, corr), "dummy", 800, 600)
            DummyCanvas.SaveAs("%s/AllCorrelationPlots_%s_%s.pdf[" % (Options.output, grp, corr))
            PlottedOnce = False
            for var in variables:
                PlottedOnce = PlotCorrelationVariable(correlation_dir, Options, grp, corr, var) or PlottedOnce

            DummyCanvas.SaveAs("%s/AllCorrelationPlots_%s_%s.pdf]" % (Options.output, grp, corr))
            if not PlottedOnce:
                os.system("rm %s/AllCorrelationPlots_%s_%s.pdf" % (Options.output, grp, corr))

    spotDeviationsPlotCorrelationVariable(Distribution_Dir, Options, "SigTraining", "BkgTraining")
    spotDeviationsPlotCorrelationVariable(Distribution_Dir, Options, "SigTesting", "BkgTesting")
