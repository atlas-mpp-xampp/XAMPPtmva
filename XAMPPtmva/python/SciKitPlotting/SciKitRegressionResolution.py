#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from ClusterSubmission.Utils import CreateDirectory, id_generator
from XAMPPplotting.Utils import CreateRatioHistos, PrintHistogram
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPtmva.SciKitTrainingVariables import setupSciKitPlotParser, setupPlottingEnviroment
from XAMPPtmva.SciKitCorrelationMatrix import binlabels
from XAMPPtmva.SciKitRegressionVariables import FitResolution
import logging


def FitResolution(InFile, variable, isTesting):
    reso_histo = InFile.Get("Diagnostics/Resolution_%s/MVA_%s" % ("Training" if not isTesting else "Testing", variable))
    if not reso_histo:
        print "WARNING: No resolution histogram could be found"
        return 0, -1
    fit_func = ROOT.TF1(id_generator(24), "gaus(0)", -1, 1)
    reso_histo.Fit(fit_func, "EMRN0S", "", -1, 1)
    return fit_func.GetParameter(1), fit_func.GetParameter(2)


if __name__ == "__main__":
    parser = setupSciKitPlotParser(
        prog='SciKitRegressionResolution',
        description='This script plots foreach variable the resolution of the prediction. Resolution is defined as (pred - true)/ true')
    parser.set_defaults(output="RegressionPerformance/")

    Options = parser.parse_args()
    setupPlottingEnviroment(Options)

    ROOTFiles = []
    for myfile in sorted(os.listdir(Options.inputPath)):
        if not myfile.endswith(".root"): continue
        if myfile.rfind("_swapped") != -1: continue

        ROOTFiles += [(myfile[:myfile.rfind(".")], Options.inputPath + '/' + myfile)]
        cross_valid = "%s/%s_swapped.root" % (Options.inputPath, myfile[:myfile.rfind(".")])
        if not os.path.exists(cross_valid): continue
        ROOTFiles += [(myfile[:myfile.rfind(".")] + " cross validation", cross_valid)]

    if len(ROOTFiles) == 0:
        logging.error("No .root files found at given path %s, exiting..." % Options.inputPath)
        sys.exit(1)

    PlottedOnce = False
    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    DummyCanvas.SaveAs("%s/AllResolutions.pdf[" % (Options.output, ))

    n_Opened = 0
    for method, ifile in ROOTFiles:
        #if n_Opened % 100 == 0: GetFileHandler().closeAll()
        InputFile = ROOT.TFile(ifile)  #GetFileHandler().LoadFile(ifile)
        n_Opened += 1
        ### True values
        train_dir = InputFile.Get("Diagnostics/Resolution_Training")
        test_dir = InputFile.Get("Diagnostics/Resolution_Testing")
        print train_dir, test_dir
        if not train_dir or not test_dir:
            logging.warning("Not all resolutions could be found in %s" % (ifile))
            continue
        for variable in [
                Key.GetName()[Key.GetName().find("_") + 1:] for Key in train_dir.GetListOfKeys() if Key.ReadObj().InheritsFrom("TH1")
        ]:

            CanvasName = "Resolution_%s_%s" % (method, variable)
            pu = PlotUtils(status=Options.label, size=24)
            pu.Prepare1PadCanvas(CanvasName, 800, 600)
            train_reshisto = train_dir.Get("MVA_%s" % (variable))
            test_reshisto = test_dir.Get("MVA_%s" % (variable))

            ## Set the styling of the true histograms
            train_reshisto.SetLineColor(ROOT.kRed)
            train_reshisto.SetFillColor(ROOT.kRed)
            train_reshisto.SetFillStyle(3005)
            train_reshisto.SetLineWidth(1)

            test_reshisto.SetLineColor(ROOT.kBlue)
            test_reshisto.SetFillColor(ROOT.kBlue)
            test_reshisto.SetFillStyle(3345)
            test_reshisto.SetLineWidth(1)
            test_reshisto.SetMarkerStyle(20)
            test_reshisto.SetMarkerSize(0.7)
            test_reshisto.SetLineWidth(1)
            test_reshisto.SetMarkerColor(2)

            styling_histo = train_reshisto.Clone(id_generator(54))
            styling_histo.GetYaxis().SetTitle("(1/N) dN/dx")

            pu.drawStyling(styling_histo,
                           0,
                           max([train_reshisto.GetMaximum(), test_reshisto.GetMaximum()]) * 1.3,
                           RemoveLabel=False,
                           TopPad=False)

            train_reshisto.Draw("histSame")
            test_reshisto.Draw("histSame")

            train_mean, train_sigma = FitResolution(InFile=InputFile, variable=variable, isTesting=False)
            test_mean, test_sigma = FitResolution(InFile=InputFile, variable=variable, isTesting=True)

            train_reshisto.SetTitle("training")
            test_reshisto.SetTitle("testing")
            pu.CreateLegend(0.7, 0.75, 0.97, 0.9)
            pu.AddToLegend([train_reshisto, test_reshisto], "FL")
            pu.DrawLegend()

            pu.DrawTLatex(0.65, 0.65, "training #mu=%.2f, #sigma=%.2f" % (train_mean, train_sigma), 18)
            pu.DrawTLatex(0.65, 0.6, "testing #mu=%.2f, #sigma=%.2f" % (test_mean, test_sigma), 18)

            height = 0.88
            if not Options.noATLAS:
                pu.DrawAtlas(0.19, height)
                height -= 0.04
            pu.DrawSqrtS(0.19, height)
            height -= 0.05
            pu.DrawTLatex(0.19, height, method)

            if not Options.summaryPlotOnly:
                pu.saveHisto("%s/%s" % (Options.output, CanvasName), ["pdf"])
            pu.DrawTLatex(0.5, 0.97, variable, 0.04, 52, 22)

            pu.saveHisto("%s/AllResolutions" % (Options.output, ), ["pdf"])
            PlottedOnce = True

    DummyCanvas.SaveAs("%s/AllResolutions.pdf]" % (Options.output, ))
    if not PlottedOnce:
        os.system("rm %s/AllResolutions.pdf" % (Options.output, ))
