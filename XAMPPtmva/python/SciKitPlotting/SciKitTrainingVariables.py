#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from ClusterSubmission.Utils import CreateDirectory
from XAMPPplotting.Utils import GetNbins, IntegrateTH1, PrintHistogram
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPtmva.SciKitOvertrainPlot import KolmogorovSmirnovScore
from XAMPPtmva.SciKitCorrelationMatrix import binlabels


def setupSciKitPlotParser(prog="SciKitPlotting", description="your favourite description"):
    parser = argparse.ArgumentParser(prog=prog, description=description)
    parser.add_argument('--noATLAS', help='Disable the drawing of the ATLAS Internal label', action='store_true', default=False)
    parser.add_argument('--label', help='Specify the label to be plotted', default="Simulation Internal")
    parser.add_argument('--regionLabel', help='Specify a region label to be drawn', default='')
    parser.add_argument('-i', '--inputPath', help='specify path of root files to compare', default='')
    parser.add_argument("--output", "-o", help="Output directory", default="TrainingVariables")
    parser.add_argument('--summaryPlotOnly',
                        help="Only the summary pdfs containing all plots are created.",
                        default=False,
                        action='store_true')
    return parser


def setupPlottingEnviroment(Options):
    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    if len(Options.inputPath) == 0 or not os.path.isdir(Options.inputPath):
        print "ERROR: Specified path %s does not exist, exiting..." % Options.inputPath
        sys.exit(1)

    ifile = None
    for myfile in os.listdir(Options.inputPath):
        if not myfile.endswith('.root'): continue
        if myfile.find("swapped") != -1: continue
        ifile = "%s/%s" % (Options.inputPath, myfile)

    if not ifile:
        print "ERROR: No .root files found at given path %s, exiting..." % Options.inputPath
        sys.exit(1)

    print "OPEN File: " + ifile
    CreateDirectory(Options.output, False)

    GetFileHandler().switchOffMsg()
    InputFile = GetFileHandler().LoadFile(ifile)
    if not InputFile or not InputFile.IsOpen():
        print "Error could not open file " + ifile
        sys.exit(1)
    if len(Options.regionLabel) == 0:
        LabelStr = InputFile.Get("TopologyLabel")
        if LabelStr: Options.regionLabel = LabelStr.Data()

    return InputFile


if __name__ == "__main__":
    parser = setupSciKitPlotParser(
        prog='SciKitTrainingVariables',
        description='This script plots the background and signal distributions (normalized to unity) against each other.')
    parser.add_argument("--plotTesting", help="Plot the testing distributions instead the trainings", action='store_true', default=False)
    parser.add_argument("--regression", help="The input dataset is meant to solve a regression problem", action='store_true', default=False)

    Options = parser.parse_args()

    InputFile = setupPlottingEnviroment(Options)
    Distribution_Dir = InputFile.GetDirectory("Distributions")
    if not Distribution_Dir:
        print "Could not find any distributions in %s" % (InputFile.GetName())
        sys.exit(1)

    directories = [key.GetName() for key in Distribution_Dir.GetListOfKeys() if key.ReadObj().InheritsFrom("TDirectory")]

    sig_dir_name = "Sig" + ("Testing" if Options.plotTesting else "Training") if not Options.regression else "Training"
    bkg_dir_name = "Bkg" + ("Testing" if Options.plotTesting else "Training") if not Options.regression else "Testing"
    sig_dir = Distribution_Dir.GetDirectory(sig_dir_name)
    bkg_dir = Distribution_Dir.GetDirectory(bkg_dir_name)
    if not sig_dir or not bkg_dir:
        print "ERROR: The ROOT file does not seem to be complete. Are you sure it has been made with SciKitDiagnostics"
        sys.exit(1)

    variables = [
        key.GetName() for key in sig_dir.GetListOfKeys() if key.ReadObj().InheritsFrom("TH1") and key.ReadObj().GetDimension() == 1
    ]

    if len(variables) == 0:
        print "ERROR: No variables are in th e ROOT file"
        sys.exit(1)

    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    DummyCanvas.SaveAs("%s/All%sVars.pdf[" % (Options.output, "Train" if not Options.plotTesting else "Test"))
    for var in variables:
        ### Create the TCanvas
        CanvasName = "%s_%s" % ("Train" if not Options.plotTesting else "Test", var)
        pu = PlotUtils(status=Options.label, size=24)
        pu.Prepare1PadCanvas(CanvasName, 800, 600)

        sig_histo = sig_dir.Get(var)
        bkg_histo = bkg_dir.Get(var)

        sig_histo.SetLineColor(ROOT.kRed)
        bkg_histo.SetLineColor(ROOT.kBlue)

        sig_histo.SetFillColor(ROOT.kRed)
        sig_histo.SetFillStyle(3005)

        bkg_histo.SetFillColor(ROOT.kBlue)
        bkg_histo.SetFillStyle(3345)
        if sig_histo.GetXaxis().GetTitle() in binlabels.iterkeys():
            sig_histo.GetXaxis().SetTitle(binlabels[sig_histo.GetXaxis().GetTitle()])
        pu.drawStyling(sig_histo, 0, max([sig_histo.GetMaximum(), bkg_histo.GetMaximum()]) * 1.3, False)

        bkg_histo.Draw("histSame")
        sig_histo.Draw("histSame")

        sig_histo.SetTitle("signal" if not Options.regression else "training")
        bkg_histo.SetTitle("background" if not Options.regression else "testing")
        pu.CreateLegend(0.2, 0.91, 0.35, 0.81)
        pu.AddToLegend([bkg_histo, sig_histo], "FL")
        pu.DrawLegend()
        if not Options.noATLAS: pu.DrawAtlas(0.5, 0.88)
        pu.DrawSqrtS(0.5, 0.83)
        if not Options.regression:
            power, error = KolmogorovSmirnovScore(sig_histo, bkg_histo, consider_stats=False)
            pu.DrawTLatex(0.2, 0.78, "Separation power: %.3f" % (power), 18)
        if len(Options.regionLabel) > 0:
            pu.DrawTLatex(0.55, 0.78, Options.regionLabel, 18)

        if not Options.summaryPlotOnly:
            pu.saveHisto("%s/%s" % (Options.output, CanvasName), ["pdf"])
        pu.saveHisto("%s/All%sVars" % (Options.output, "Train" if not Options.plotTesting else "Test"), ["pdf"])

    DummyCanvas.SaveAs("%s/All%sVars.pdf]" % (Options.output, "Train" if not Options.plotTesting else "Test"))
