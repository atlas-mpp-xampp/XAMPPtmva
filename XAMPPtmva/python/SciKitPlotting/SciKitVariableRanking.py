## @short Tool to put MVA variables in order of
##  decreasing importance.
##
import ROOT, os, sys
from XAMPPplotting.PlotUtils import PlotUtils
from XAMPPtmva.SciKitTrainingVariables import setupSciKitPlotParser, setupPlottingEnviroment
from ClusterSubmission.Utils import id_generator, IsROOTFile
from XAMPPtmva.SciKitCorrelationMatrix import binlabels
from XAMPPplotting.FileStructureHandler import GetFileHandler


class Ranking(object):
    """
    Class representing a var ranking
    """
    def __init__(self, name="", value=0.):
        self.__name = name if not name in binlabels.iterkeys() else binlabels[name]
        self.__value = value

    @property
    def name(self):
        return self.__name

    @property
    def value(self):
        return self.__value

    def show(self):
        print("%s \t\t %f" % (self.__name, self.__value))


def main(argv):
    parser = setupSciKitPlotParser()
    parser.set_defaults(output="VariableRanking")
    parser.add_argument('--methodName', help='Define a proper name of the training', default='')
    Options = parser.parse_args()
    setupPlottingEnviroment(Options)

    ROOTFiles = []
    ### The ranking of the variables should always be parsed to this histogram
    histoname = "Diagnostics/ClassifierMonitoring/VariableRanking"

    for ifile in sorted(os.listdir(Options.inputPath)):
        if not IsROOTFile(ifile): continue

        rfile = GetFileHandler().LoadFile("%s/%s" % (Options.inputPath, ifile))
        if not rfile:
            print("Error: file %s not accessible..." % (ifile))
            continue

        histo = rfile.Get(histoname)
        if not histo:
            print("Info: histo %s not found / retrieved from file %s. Skipping..." % (histoname, ifile))
            continue
        ROOTFiles += [rfile]
    if len(ROOTFiles) == 0:
        print("WARNING: No valid root file has been found")
        exit(0)

    DummyCanvas = None
    for rfile in ROOTFiles:
        training_name = rfile.GetName()[rfile.GetName().rfind("/") + 1:rfile.GetName().rfind(".")]
        in_histo = rfile.Get(histoname)
        rankings = [Ranking(in_histo.GetXaxis().GetBinLabel(i), in_histo.GetBinContent(i)) for i in range(1, in_histo.GetNbinsX() + 1)]

        #rank by value
        rankings.sort(key=lambda ranking: ranking.value, reverse=True)

        hranking = ROOT.TH1D(id_generator(52), "", len(rankings), 0, len(rankings))

        maxval = 0.
        for j, ranking in enumerate(rankings):
            ibin = j + 1
            ranking.show()
            hranking.GetXaxis().SetBinLabel(ibin, ranking.name)
            hranking.SetBinContent(ibin, ranking.value)
            if ranking.value > maxval:
                maxval = ranking.value

        pu = PlotUtils(status=Options.label, size=24)
        if not DummyCanvas:
            DummyCanvas = ROOT.TCanvas("Dummy", "Dummy", 300 + len(rankings) * 50, 600)
            DummyCanvas.SaveAs("%s/AllVariableRankings.pdf[" % (Options.output))

        pu.Prepare1PadCanvas(id_generator(10), 300 + len(rankings) * 50, 600)
        hranking.SetFillColor(ROOT.kAzure - 2)
        hranking.SetMinimum(0)
        hranking.SetMaximum(maxval * 1.10)
        hranking.GetXaxis().SetLabelOffset(hranking.GetXaxis().GetLabelOffset() * 1.07)
        ROOT.gPad.SetGridy(1)
        ROOT.gPad.SetTicks(1, 1)
        pu.GetCanvas().SetBottomMargin(pu.GetCanvas().GetBottomMargin() * 1.3)
        pu.GetCanvas().SetRightMargin(pu.GetCanvas().GetRightMargin() * 1.5)
        hranking.GetYaxis().SetTitle("Importance")
        hranking.Draw("hist")
        plot_range_x = (hranking.GetXaxis().GetBinUpEdge(hranking.GetNbinsX()) -
                        hranking.GetXaxis().GetBinLowEdge(1)) / (1 - pu.GetCanvas().GetLeftMargin() - pu.GetCanvas().GetRightMargin())
        plot_range_y = (hranking.GetMaximum() - hranking.GetMinimum()) / (1 - pu.GetCanvas().GetTopMargin() -
                                                                          pu.GetCanvas().GetBottomMargin())

        for i in range(1, hranking.GetNbinsX() + 1):
            val = hranking.GetBinContent(i)

            pu.DrawTLatex(pu.GetCanvas().GetLeftMargin() + (hranking.GetBinCenter(i) / plot_range_x),
                          pu.GetCanvas().GetBottomMargin() + (hranking.GetBinContent(i) + 0.005) / plot_range_y,
                          "%.4f" % (val),
                          size=10,
                          align=21)
        if not Options.noATLAS:
            pu.DrawAtlas(0.5, 0.85)
        pu.DrawSqrtS(0.5, 0.80)

        if len(Options.regionLabel) > 0: pu.DrawTLatex(0.5, 0.75, Options.regionLabel)
        pu.DrawTLatex(0.5, 0.7, training_name if len(Options.methodName) == 0 else Options.methodName)

        if not Options.summaryPlotOnly:
            pu.saveHisto("%s/VariableRanking_%s" % (Options.output, training_name), ["pdf"])
        pu.saveHisto("%s/AllVariableRankings" % (Options.output), ["pdf"])

    DummyCanvas.SaveAs("%s/AllVariableRankings.pdf]" % (Options.output))


##
# @brief execute main
##
if __name__ == "__main__":

    main(sys.argv[1:])
