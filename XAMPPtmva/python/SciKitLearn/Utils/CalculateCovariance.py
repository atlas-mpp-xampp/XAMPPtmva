#!/bin/env/python
from XAMPPtmva.LoadHDF5 import setupSciKitTrainingParser
from XAMPPtmva.SciKitCorrelation import SciKitPreProcessing
from ClusterSubmission.Utils import FillWhiteSpaces


def generatePreProcessingFile():
    parser = setupSciKitTrainingParser()
    parser.add_argument("--minEigenAbs", help="What is the minimum absolute of the eigenvalue demanded", default=1.e-8, type=float)
    Options = parser.parse_args()
    Decorrelator = SciKitPreProcessing(infile=Options.HDF5File,
                                       begin=Options.startEvent,
                                       end=Options.lastEvent,
                                       outdir=Options.outDir,
                                       num_threads=Options.nAuxillaryThreads)

    if not Decorrelator.unCorrelatedData(min_eigen=Options.minEigenAbs):
        exit(1)
    if not Decorrelator.writeUncorrelatedData(
            useWeights=True, useRaw=True, writeTesting=True, writeTraining=True, cleanUp=True, num_threads=Options.nAuxillaryThreads):
        exit(1)
    if not Decorrelator.writeMeanNormalizedData(
            useWeights=True, useRaw=True, writeTesting=True, writeTraining=True, cleanUp=True, num_threads=Options.nAuxillaryThreads):
        exit(1)
    Decorrelator.save()


if __name__ == '__main__':
    generatePreProcessingFile()
