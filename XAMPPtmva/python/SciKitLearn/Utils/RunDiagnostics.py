#!/bin/env/python
from XAMPPtmva.LoadHDF5 import setupSciKitTrainingParser, runDiagnostics
from XAMPPtmva.SciKitDiagnostics import SciKitDiagnostics
from XAMPPtmva.DummyClassifier import SciKitDummyClassifier
from ClusterSubmission.Utils import FillWhiteSpaces
from XAMPPtmva.BDTTraining import SciKitBDTClassifier
import os


def setupDiagnosticsParser():
    parser = setupSciKitTrainingParser()
    parser.add_argument("--inDir", help="Where is the training located")
    parser.add_argument("--runVariablesOnly",
                        help="The diagnostics is run on a dummy classifier to obtain the distributions of training and testing variables ",
                        default=False,
                        action='store_true')

    return parser


if __name__ == '__main__':
    Options = setupDiagnosticsParser().parse_args()
    if Options.skipDiagnostics:
        print "<runDiagnostics> ERROR: Reaally? You extra execute this script and then you parse '--skipDiagnostics' what am I meant to do then?!"
        exit(1)
    if not Options.runVariablesOnly:
        file_to_load = "%s/%s.pkl" % (Options.inDir, Options.classifierName)
        if os.path.isfile(file_to_load):
            print "\n\n" + FillWhiteSpaces(100, "/*")
            from sklearn.externals import joblib
            print "<runDiagnostics> INFO: Load machine learning training stored at %s." % (file_to_load)
            print FillWhiteSpaces(100, "*/") + "\n\n"
            classifier = joblib.load(file_to_load)
            if not classifier.load():
                print "<runDiagnostics> ERROR: Could not run diagnostics"
                exit(1)
            runDiagnostics(Options, classifier)
        else:
            print "<runDiagnostics> ERROR: %s is not a training" % (file_to_load)
            exit(1)
    else:
        dummy_classifier = SciKitDummyClassifier(Options)
        runDiagnostics(Options, dummy_classifier)
