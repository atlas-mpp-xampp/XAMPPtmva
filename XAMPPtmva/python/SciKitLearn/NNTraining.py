#!/bin/env/python
from XAMPPtmva.SciKitNNClassifier import SciKitNNClassifier, setupNNTraining

if __name__ == '__main__':
    setupNNTraining()
