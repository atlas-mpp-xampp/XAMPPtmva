#!/bin/env/python
from XAMPPtmva.SciKitDataSet import SciKitDataSet, SciKitDataLoader
from XAMPPtmva.GetROCCurve import generateROC
from ClusterSubmission.Utils import CreateDirectory, ExecuteThreads
from XAMPPplotting.Utils import CreateCulumativeHisto, IntegrateTH1
import numpy as np
import os, math, argparse, random, threading, ROOT


class SciKitDiagnosticHisto(object):
    def __init__(self, name="", axis_title="", bins=-1, x_min=0., x_max=0., bin_width=-1, TH1_dir=None):

        self.__name = name
        self.__xTitle = axis_title

        self.__min = x_min
        self.__min_fixed = False

        self.__max = -1
        self.__max_fixed = False

        self.__width = bin_width
        self.__entries = 0
        self.__content = {}
        self.__error = {}
        self.__TH1 = None
        self.__TDir = TH1_dir
        if bins > 0:
            self.__width = (x_max - x_min) / bins
            self.__TH1 = ROOT.TH1D(name, "Diagnostic histogram", bins, x_min, x_max)
            self.__TH1.Sumw2()
            self.__TH1.SetDirectory(TH1_dir)

    def name(self):
        return self.__name

    def TH1(self):
        return self.__TH1

    def fill(self, value, weight=1.):
        self.__entries += 1
        if self.__TH1: self.__TH1.Fill(value, weight)
        else:
            rng = self.__getInterval(value)
            try:
                self.__content[rng] += weight
            except:
                self.__content[rng] = weight
            try:
                self.__error[rng] += weight**2
            except:
                self.__error[rng] = weight**2

    def __getInterval(self, value):
        i = math.ceil((value - self.__min) / self.__width)
        return (self.__min + self.__width * (i - 1), self.__min + self.__width * i)

    def write(self):
        self.fixHisto()
        if not self.__TH1: return
        H = self.__TH1
        H.GetXaxis().SetTitle(self.__xTitle)
        Integral = H.Integral()

        #### Pull the over flow and underflow
        H.SetBinContent(1, H.GetBinContent(0) + H.GetBinContent(1))
        H.SetBinContent(H.GetNbinsX(), H.GetBinContent(H.GetNbinsX() + 1) + H.GetBinContent(H.GetNbinsX()))
        H.SetBinError(1, math.sqrt(H.GetBinError(0)**2 + H.GetBinError(1)**2))
        H.SetBinError(H.GetNbinsX(), math.sqrt(H.GetBinError(H.GetNbinsX() + 1)**2 + H.GetBinError(H.GetNbinsX())**2))
        ### Reset the overflow and underflow
        H.SetBinContent(0, 0)
        H.SetBinContent(H.GetNbinsX() + 1, 0)
        ### Normalize the histogram to unit area
        H.Scale(1. / (Integral if Integral > 0 else 1.))
        H.GetYaxis().SetTitle("Normalized to unit area (%.2f)" % (self.__width))
        H.SetEntries(self.__entries)
        self.__TDir.WriteObject(self.__TH1, self.__name)

    def max(self):
        if self.__max_fixed: return self.__max
        try:
            return sorted(self.__content.iterkeys(), key=lambda Rng: Rng[1])[-1][1]
        except:
            return float('nan')

    def min(self):
        if self.__min_fixed: return self.__min
        try:
            return sorted(self.__content.iterkeys(), key=lambda Rng: Rng[0])[0][0]
        except:
            return float('nan')

    def setMinimum(self, minimum):
        self.__min = minimum
        self.__min_fixed = True

    def setMaximum(self, maximum):
        self.__max = maximum
        self.__max_fixed = True

    def fixHisto(self):
        if self.__TH1: return
        bins = int((self.max() - self.min()) / self.__width)
        self.__TH1 = ROOT.TH1D(self.name(), "Diagnostic histogram", bins, self.__min, self.__max)
        self.__TH1.SetDirectory(self.__TDir)
        for rng in self.__content.iterkeys():
            bin = self.__TH1.FindBin((rng[1] + rng[0]) / 2.)
            self.__TH1.SetBinContent(bin, self.__content[rng])
            self.__TH1.SetBinError(bin, math.sqrt(self.__error[rng]))
        Integral = IntegrateTH1(self.__TH1)
        self.__TH1.Scale(1. / Integral if Integral > 0 else 1.)


class SciKitDiagnosticHisto2D(object):
    def __init__(self,
                 name="",
                 x_axis_title="",
                 y_axis_title="",
                 x_var=-1,
                 y_var=-1,
                 n_bins_x=-1,
                 n_bins_y=-1,
                 min_x=-1,
                 max_x=-1,
                 min_y=-1,
                 max_y=-1,
                 TH1_dir=None):

        self.__name = name
        self.__entries = 0

        self.__xvariable = x_var
        self.__yvariable = y_var

        self.__TDir = TH1_dir
        self.__TH1 = None

        self.__bin_width_x = (max_x - min_x) / n_bins_x
        self.__bin_width_y = (max_y - min_y) / n_bins_y
        self.__TH1 = ROOT.TH2D(name, "Correlation histogram", n_bins_x, min_x, max_x, n_bins_y, min_y, max_y)
        self.__TH1.Sumw2()
        self.__TH1.SetDirectory(TH1_dir)
        self.__TH1.GetXaxis().SetTitle(x_axis_title)
        self.__TH1.GetYaxis().SetTitle(y_axis_title)
        self.__TH1.GetZaxis().SetTitle("Normalized to unit area (%.2f#times%.2f)" % (self.__bin_width_x, self.__bin_width_y))

    def name(self):
        return self.__name

    def fill(self, event, weight=1.):
        self.__TH1.Fill(event[self.__xvariable], event[self.__yvariable], weight)
        self.__entries += 1

    def fill2D(self, block_a, block_b, weight=1):
        self.__TH1.Fill(block_a[self.__xvariable], block_b[self.__yvariable], weight)
        self.__entries += 1

    def write(self):
        Integral = self.__TH1.Integral()
        self.__TH1.Scale(1. / (Integral if Integral > 0 else 1.))
        self.__TH1.SetEntries(self.__entries)
        self.__TDir.WriteObject(self.__TH1, self.__name)


#### The SciKitDiagnosticPlotGroup
class SciKitDiagnosticPlotGroup(SciKitDataLoader):
    def __init__(self,
                 diagnostics=None,
                 group_name="",
                 histo_prefix="",
                 process_testing=False,
                 process_signal=False,
                 process_background=False,
                 do_transformation=False,
                 end=-1):
        ### Constructor of the threading class
        SciKitDataLoader.__init__(self,
                                  dataset=diagnostics.dataset(),
                                  process_testing=process_testing,
                                  process_signal=process_signal,
                                  process_background=process_background,
                                  useWeights=diagnostics.useWeights(),
                                  progress=True,
                                  chunksize=10000,
                                  end=end)

        ### SciKitDiagnostics object which created the class instance
        self.__diagnostics = diagnostics
        self.__group_name = group_name
        self.__prefix = histo_prefix
        self.__dotrans = do_transformation
        ### Histo content
        self.__isOk = True
        self.__Histos = []
        self.__CorrelationHistos = []
        ###  Plot the target variables in cases of regression problems
        self.__targetHistos = []
        #### Correlation plots between the target variables themselves
        self.__corr_targetHistos = []
        ### Correlation's between the target and input variables
        self.__corr_targetTrainHistos = []

        ### Histograms to evaluate the classifiers themselves
        self.__regHistos = []

        self.__regErrorHistos = []  ### Fill the delta divided by the true value
        self.__regErrorSystHistos = []  ### Fill the delta divided by the predicted value

        self.__classOutput = None

    def group_name(self):
        return self.__group_name

    def prefix(self):
        return self.__prefix

    def fillChunk(self):
        transformed = self.__diagnostics.transform(self.current_dataBlock()) if self.do_transformation() else self.current_dataBlock()
        mva_score = self.__diagnostics.predict(transformed)
        classID = self.current_classIDBlock()
        if self.dataset().isRegressionDS():
            if len(mva_score.shape) == 1: mva_score = mva_score.reshape(mva_score.shape[0], 1)
            if len(classID.shape) == 1: classID = classID.reshape(classID.shape[0], 1)

        for i in xrange(len(transformed)):
            self.fillEvent(transformed[i], self.current_weightBlock()[i], mva_score[i])
            self.fillRegression(classID[i], mva_score[i], self.current_weightBlock()[i])
            for H in self.__corr_targetTrainHistos:
                H.fill2D(classID[i], transformed[i], self.current_weightBlock()[i])
        return True

    def fillEvent(self, to_fill, W, mva_score):
        for i, H in enumerate(self.__Histos):
            H.fill(value=to_fill[i], weight=W)
        for H in self.__CorrelationHistos:
            H.fill(to_fill, W)
        if self.__classOutput:
            self.__classOutput.fill(value=mva_score, weight=W)
        return True

    def fillRegression(self, true_val, pred_val, W):
        if not self.dataset().isRegressionDS(): return
        for H in self.__corr_targetHistos:
            H.fill(to_fill, W)
        for i in range(len(self.__targetHistos)):
            self.__targetHistos[i].fill(value=true_val[i], weight=W)
            self.__regHistos[i].fill(value=pred_val[i], weight=W)
            delta = (true_val[i] - pred_val[i])

            if true_val[i] != 0.: self.__regErrorHistos[i].fill(value=delta / true_val[i], weight=W)
            elif delta == 0: self.__regErrorHistos[i].fill(value=0., weight=W)
            if pred_val[i] != 0.: self.__regErrorSystHistos[i].fill(value=delta / pred_val[i], weight=W)
            elif delta == 0.: self.__regErrorSystHistos[i].fill(value=0., weight=W)

    ### Retrieve the minimum range
    def minMVAscore(self):
        return self.__classOutput.min() if self.__classOutput else float('nan')

    def maxMVAscore(self):
        return self.__classOutput.max() if self.__classOutput else float('nan')

    def setMVAhistoRange(self, minimum, maximum):
        if not self.__classOutput: return
        self.__classOutput.setMinimum(minimum)
        self.__classOutput.setMaximum(maximum)
        self.__classOutput.fixHisto()

    def getMVAHisto(self):
        return self.__classOutput.TH1()

    def do_transformation(self):
        return self.__dotrans

    def __mkdir(self, dir_name):
        if not self.__diagnostics.ROOTFile().GetDirectory(dir_name): self.__diagnostics.ROOTFile().mkdir(dir_name)
        return self.__diagnostics.ROOTFile().GetDirectory(dir_name)

    def init(self):
        ### Save  the distributions each in a seperate directory
        dir_name = "Distributions/%s/" % (self.__group_name)
        Dir = self.__mkdir(dir_name)
        CorrDir = self.__mkdir("%s%s_Correlations/" % (dir_name, "Raw" if not self.do_transformation() else "Transformed"))
        DiagnosticDir = self.__mkdir("Diagnostics/")

        for i in range(self.__diagnostics.nVars()):
            var_name = self.__diagnostics.variableName(i, not self.do_transformation())
            bin_width = (self.__diagnostics.maximum(i, transformed_data=self.do_transformation()) -
                         self.__diagnostics.minimum(i, transformed_data=self.do_transformation())) / self.__diagnostics.nBins()
            self.__Histos += [
                SciKitDiagnosticHisto(name="%s%s" % (self.__prefix + ("_" if len(self.__prefix) > 0 else ""), var_name),
                                      axis_title=var_name,
                                      bins=self.__diagnostics.nBins() + 2,
                                      x_min=self.__diagnostics.minimum(i, transformed_data=self.do_transformation()) - bin_width,
                                      x_max=self.__diagnostics.maximum(i, transformed_data=self.do_transformation()) + bin_width,
                                      TH1_dir=Dir)
            ]
            ### Append the 2D correlation histograms
            for j in range(i):
                correlating_var = self.__diagnostics.variableName(j, not self.do_transformation())
                self.__CorrelationHistos += [
                    SciKitDiagnosticHisto2D(name="%s__%s" % (var_name, correlating_var),
                                            x_axis_title=var_name,
                                            y_axis_title=correlating_var,
                                            x_var=i,
                                            y_var=j,
                                            n_bins_x=self.__diagnostics.nBins(),
                                            n_bins_y=self.__diagnostics.nBins(),
                                            min_x=self.__diagnostics.minimum(i, transformed_data=self.do_transformation()),
                                            max_x=self.__diagnostics.maximum(i, transformed_data=self.do_transformation()),
                                            min_y=self.__diagnostics.minimum(j, transformed_data=self.do_transformation()),
                                            max_y=self.__diagnostics.maximum(j, transformed_data=self.do_transformation()),
                                            TH1_dir=CorrDir)
                ]

        self.__initDiagnosticsRegression()
        if self.dataset().isClassificationDS() and not self.do_transformation():
            self.__classOutput = SciKitDiagnosticHisto(name="MVA_%s%s" % (self.__prefix +
                                                                          ("_" if len(self.__prefix) > 0 else ""), self.__group_name),
                                                       axis_title=self.__diagnostics.name(),
                                                       x_min=0.,
                                                       bin_width=self.__diagnostics.binClassScore(),
                                                       TH1_dir=DiagnosticDir)

    def __initDiagnosticsRegression(self):
        if not self.dataset().isRegressionDS(): return

        TargetDir = self.__mkdir("TargetDistributions/%s/" % (self.__group_name))
        CorrDir = self.__mkdir("TargetDistributions/%s/%s_Correlations/" %
                               (self.__group_name, "Raw" if not self.do_transformation() else "Transformed"))
        PredTargetDir = self.__mkdir("TargetDistributions/Predicted_%s/" % (self.__group_name))
        DiagnosticDir = self.__mkdir("Diagnostics/Resolution_%s/" % (self.__group_name))
        SystDir = self.__mkdir("Diagnostics/Systematics_%s/" % (self.__group_name))

        for i in range(self.__diagnostics.dataset().nTargets()):
            var_name = self.__diagnostics.dataset().targetVariable(i)
            bin_width = (self.__diagnostics.targetMaximum(i, test_ds=self.doTestingEvts()) -
                         self.__diagnostics.targetMinimum(i, test_ds=self.doTestingEvts())) / self.__diagnostics.nBins()

            ### Add the histograms for the target variable itself
            self.__targetHistos += [
                SciKitDiagnosticHisto(name="%s%s" % (self.__prefix + ("_" if len(self.__prefix) > 0 else ""), var_name),
                                      axis_title=var_name,
                                      bins=self.__diagnostics.nBins() + 2,
                                      x_min=self.__diagnostics.targetMinimum(i, test_ds=self.doTestingEvts()) - bin_width,
                                      x_max=self.__diagnostics.targetMaximum(i, test_ds=self.doTestingEvts()) + bin_width,
                                      TH1_dir=TargetDir)
            ] if not self.do_transformation() else []
            ### The outcome of the classifier
            self.__regHistos += [
                SciKitDiagnosticHisto(name="%s%s" % (self.__prefix + ("_" if len(self.__prefix) > 0 else ""), var_name),
                                      axis_title=var_name,
                                      bins=self.__diagnostics.nBins() + 2,
                                      x_min=self.__diagnostics.targetMinimum(i, test_ds=self.doTestingEvts()) - bin_width,
                                      x_max=self.__diagnostics.targetMaximum(i, test_ds=self.doTestingEvts()) + bin_width,
                                      TH1_dir=PredTargetDir)
            ] if not self.do_transformation() else []
            ### Resolution of the classifier per event
            self.__regErrorHistos += [
                SciKitDiagnosticHisto(name="MVA_%s%s" % (self.__prefix + ("_" if len(self.__prefix) > 0 else ""), var_name),
                                      axis_title="Residual %s" % (var_name),
                                      x_min=-1.5,
                                      x_max=1.5,
                                      bins=150,
                                      TH1_dir=DiagnosticDir)
            ] if not self.do_transformation() else []
            self.__regErrorSystHistos += [
                SciKitDiagnosticHisto(name="MVA_%s%s" % (self.__prefix + ("_" if len(self.__prefix) > 0 else ""), var_name),
                                      axis_title="Relative resolution %s" % (var_name),
                                      x_min=-1.5,
                                      x_max=1.5,
                                      bins=150,
                                      TH1_dir=SystDir)
            ] if not self.do_transformation() else []
            ### Plot the correlations of the target variables beteen each others
            for j in range(i):
                correlating_var = self.__diagnostics.dataset().tagetVariable(j)
                self.__corr_targetHistos += [
                    SciKitDiagnosticHisto2D(name="TargetTarget_%s__%s" % (var_name, correlating_var),
                                            x_axis_title=var_name,
                                            y_axis_title=correlating_var,
                                            x_var=i,
                                            y_var=j,
                                            n_bins_x=self.__diagnostics.nBins(),
                                            n_bins_y=self.__diagnostics.nBins(),
                                            min_x=self.__diagnostics.targetMinimum(i, test_ds=self.doTestingEvts()),
                                            max_x=self.__diagnostics.targetMaximum(i, test_ds=self.doTestingEvts()),
                                            min_y=self.__diagnostics.targetMinimum(j, test_ds=self.doTestingEvts()),
                                            max_y=self.__diagnostics.targetMaximum(j, test_ds=self.doTestingEvts()),
                                            TH1_dir=CorrDir)
                ] if not self.do_transformation() else []
            ### Plot the correlations of the target variables to the training variables
            for j in range(self.__diagnostics.nVars()):
                correlating_var = self.__diagnostics.variableName(j, not self.do_transformation())
                self.__corr_targetTrainHistos += [
                    SciKitDiagnosticHisto2D(name="TargetTrain_%s__%s" % (var_name, correlating_var),
                                            x_axis_title=var_name,
                                            y_axis_title=correlating_var,
                                            x_var=i,
                                            y_var=j,
                                            n_bins_x=self.__diagnostics.nBins(),
                                            n_bins_y=self.__diagnostics.nBins(),
                                            min_x=self.__diagnostics.targetMinimum(i, test_ds=self.doTestingEvts()),
                                            max_x=self.__diagnostics.targetMaximum(i, test_ds=self.doTestingEvts()),
                                            min_y=self.__diagnostics.minimum(j, transformed_data=self.do_transformation()),
                                            max_y=self.__diagnostics.maximum(j, transformed_data=self.do_transformation()),
                                            TH1_dir=CorrDir)
                ]

    def write(self):
        if not self.__isOk: return False
        for H in self.__Histos + self.__CorrelationHistos + self.__targetHistos + self.__regHistos + self.__regErrorHistos + self.__corr_targetHistos + self.__regErrorSystHistos + self.__corr_targetTrainHistos:
            H.write()
        if self.__classOutput: self.__classOutput.write()


class SciKitDiagnostics(object):
    def __init__(
        self,
        class_object=None,
        hdf5_file="/ptmp/mpp/jungjo9/SciKiLearn/output_scikit.H5",
        swapTrainTest=False,
        nBins=50,
        score_bin_width=0.05,
        plot_range=6,
    ):

        self.__model = class_object
        self.__preprocessing_obj = self.__model.preProcessing()
        self.__plotRange = plot_range  ### How many sigma should the plot range be shown
        self.__dataset = SciKitDataSet(infile=hdf5_file, SwapTrainTest=swapTrainTest)
        CreateDirectory(self.outDir(), CleanUpOld=False)
        self.__ROOTFile = ROOT.TFile.Open("%s/%s.root" % (self.outDir(), self.name()), "RECREATE")

        self.__nBins = nBins
        self.__scoreBining = score_bin_width

        self.__Groups = []

        self.__transformedMax = np.zeros(self.nVars())
        self.__transformedMin = np.zeros(self.nVars())

    def ROOTFile(self):
        return self.__ROOTFile

    def name(self):
        return self.__model.name()

    def outDir(self):
        return self.__model.outDir()

    def dataset(self):
        return self.__dataset

    def preprocessing_obj(self):
        return self.__preprocessing_obj

    #-----------------------------------------------------------------
    ###     Methods to obtain meta information about the input data
    #-----------------------------------------------------------------
    def variableName(self, n, get_original=True):
        return self.__model.variableName(n, get_original)

    def nVars(self):
        return self.__model.nVars()

    def nTrainEvents(self):
        return self.__dataset.nTrainingEvents()

    def nTestEvents(self):
        return self.__dataset.nTestingEvents()

    def maximum(self, n, transformed_data=False):
        if transformed_data: return self.__transformedMax[n]
        return max([
            self.__dataset.getMaxTrainDeviation(n, n_sigma=self.__plotRange, use_weights=self.useWeights()),
            self.__dataset.getMaxTestDeviation(n, n_sigma=self.__plotRange, use_weights=self.useWeights()),
        ])

    def minimum(self, n, transformed_data=False):
        if transformed_data: return self.__transformedMin[n]
        return max([
            self.__dataset.getMinTrainDeviation(n, n_sigma=self.__plotRange, use_weights=self.useWeights()),
            self.__dataset.getMinTestDeviation(n, n_sigma=self.__plotRange, use_weights=self.useWeights()),
        ])

    def targetMaximum(self, n, test_ds=False):
        if not test_ds:
            return self.__dataset.getMaxTargetTrainDeviation(n, n_sigma=self.__plotRange, use_weights=self.useWeights())
        return self.__dataset.getMaxTargetTestDeviation(n, n_sigma=self.__plotRange, use_weights=self.useWeights())

    def targetMinimum(self, n, test_ds=False):
        if not test_ds:
            return self.__dataset.getMinTargetTrainDeviation(n, n_sigma=self.__plotRange, use_weights=self.useWeights())
        return self.__dataset.getMinTargetTestDeviation(n, n_sigma=self.__plotRange, use_weights=self.useWeights())

    #-----------------------------------------------------------------
    ###    Methods to obtain information about the training itself
    #-----------------------------------------------------------------
    def useWeights(self):
        return self.__model.useWeights()

    def do_transformation(self):
        return self.__model.do_transformation()

    #-----------------------------------------------------------------
    ###    Methods for prediction of data and preprocessing
    #-----------------------------------------------------------------
    def model(self):
        return self.__model

    def transform(self, data):
        return self.__model.transform(data)

    def predict(self, data):
        return self.__model.predict(data)

    ### Information needed to create the plots
    def binClassScore(self):
        return self.__scoreBining

    def nBins(self):
        return self.__nBins

    def init(self):
        if len(self.__Groups) > 0: return
        ### Save diagnostics for all training and testing data
        self.__createGroupPair("Training", False, True, True)
        self.__createGroupPair("Testing", True, True, True)

        ######################################################################################
        #### For classification problems the diagnostics are further split into              #
        #### signal and background. For regression problems it does not make much sense      #
        #### since the classID is multi-dimensional in general.                              #
        ######################################################################################
        if self.__model.isClassificationProblem():
            self.__createGroupPair("SigTraining", False, True, False)
            self.__createGroupPair("SigTesting", True, True, False)
            self.__createGroupPair("BkgTraining", False, False, True)
            self.__createGroupPair("BkgTesting", True, False, True)

        self.ROOTFile().mkdir("Diagnostics/")
        self.ROOTFile().mkdir("Distributions/")
        if self.__model.isRegressionProblem(): self.ROOTFile().mkdir("TargetDistributions/")
        self.__Groups += self.__model.runMVAdiagnosticsOnDS(self)
        for G in self.__Groups:
            G.init()

    def __createGroupPair(self, group_name, testing, doSig, doBkg):
        self.__Groups += [
            SciKitDiagnosticPlotGroup(diagnostics=self,
                                      group_name=group_name,
                                      histo_prefix="" if not self.do_transformation() else "Raw",
                                      process_testing=testing,
                                      process_signal=doSig,
                                      process_background=doBkg,
                                      do_transformation=False)
        ]
        if not self.do_transformation(): return
        self.__Groups += [
            SciKitDiagnosticPlotGroup(diagnostics=self,
                                      group_name=group_name,
                                      histo_prefix="Transformed",
                                      process_testing=testing,
                                      process_signal=doSig,
                                      process_background=doBkg,
                                      do_transformation=True)
        ]

    def __filterSubSample(self, doBkg=True, doTrain=True, do_transformation=False):
        for G in self.__Groups:
            #if G.do_transformation() != do_transformation: continue
            if G.doBackground() != doBkg: continue
            if G.doSignal() == doBkg: continue
            if G.doTestingEvts() == doTrain: continue
            return G
        print "Warning in <SciKitDiagnostics>: Failed to find a subsample for %s %s-data using %s transformation" % (
            "background" if doBkg else "signal", "training" if doTrain else "testing", "si" if do_transformation else "no")
        return None

    def startDiagnostics(self, nThreads=8):

        nThreads = min(nThreads, self.__model.num_threads())

        self.__transformedMax = self.transform(np.array([self.maximum(n, transformed_data=False) for n in range(self.nVars())]))
        self.__transformedMin = self.transform(np.array([self.minimum(n, transformed_data=False) for n in range(self.nVars())]))

        self.init()

        print "<SciKitDiagnostics> INFO: Write classifier specific diagnostics"
        if not self.ROOTFile().GetDirectory("Diagnostics/ClassifierMonitoring"):
            self.ROOTFile().mkdir("Diagnostics/ClassifierMonitoring")

        if not self.__model.saveSpecificDiagnostics(self.ROOTFile().GetDirectory("Diagnostics/ClassifierMonitoring")):
            print "<SciKitDiagnostics> INFO: The training seems to be inconsistent or does not make much sense. It's not worth the effort to run the diagnostics on it"
            self.ROOTFile().Close()
            os.system("rm %s/%s.root" % (self.outDir(), self.name()))
            return True

        print "<ScitKitDiagnostics> INFO: Start to run over the HDF5 file."
        ExecuteThreads(Threads=self.__Groups, MaxCurrent=nThreads, verbose=False)
        for G in self.__Groups:
            if not G.isGoodState():
                print "<SciKitDiagnostics> ERROR: Loop aborted"
                exit(1)
        print "<ScitKitDiagnostics> INFO: Training and Testing data has been read-in"

        ToSave = self.__makeClassifierDiagnostics()

        #### Calculate the correlation matrix
        ToSave += [
            self.__fillCorrelationMatrix(
                matrix=self.__preprocessing_obj.getWeightCovTraining() if self.useWeights() else self.__preprocessing_obj.getCovTraining(),
                add_str="Training",
                useOrigVarNames=True)
        ]

        #### Calculate the correlation matrix
        ToSave += [
            self.__fillCorrelationMatrix(
                matrix=self.__preprocessing_obj.getWeightCovTesting() if self.useWeights() else self.__preprocessing_obj.getCovTesting(),
                add_str="Testing",
                useOrigVarNames=True)
        ]

        ### Covariance eigen values
        ToSave += self.__fillCovarianceEigenValues(
            eigen_basis=self.__preprocessing_obj.getCovBasisTrainW() if self.useWeights() else self.__preprocessing_obj.getCovBasisTrain(),
            eigen_values=self.__preprocessing_obj.getCovEigenValTrainW()
            if self.useWeights() else self.__preprocessing_obj.getCovEigenValTrain(),
            add_str="Training")
        ToSave += self.__fillCovarianceEigenValues(
            eigen_basis=self.__preprocessing_obj.getCovBasisTestW() if self.useWeights() else self.__preprocessing_obj.getCovBasisTest(),
            eigen_values=self.__preprocessing_obj.getCovEigenValTestW()
            if self.useWeights() else self.__preprocessing_obj.getCovEigenValTest(),
            add_str="Testing")

        if self.__model.isClassificationProblem():
            ToSave += [
                self.__fillCorrelationMatrix(matrix=self.__preprocessing_obj.getBkgWeightCovTraining()
                                             if self.useWeights() else self.__preprocessing_obj.getBkgCovTraining(),
                                             add_str="Bkg_Train",
                                             useOrigVarNames=True)
            ]
            ToSave += [
                self.__fillCorrelationMatrix(matrix=self.__preprocessing_obj.getBkgWeightCovTesting()
                                             if self.useWeights() else self.__preprocessing_obj.getBkgCovTesting(),
                                             add_str="Bkg_Test",
                                             useOrigVarNames=True)
            ]
            ToSave += [
                self.__fillCorrelationMatrix(matrix=self.__preprocessing_obj.getSigWeightCovTraining()
                                             if self.useWeights() else self.__preprocessing_obj.getSigCovTraining(),
                                             add_str="Sig_Train",
                                             useOrigVarNames=True)
            ]
            ToSave += [
                self.__fillCorrelationMatrix(matrix=self.__preprocessing_obj.getSigWeightCovTesting()
                                             if self.useWeights() else self.__preprocessing_obj.getSigCovTesting(),
                                             add_str="Sig_Test",
                                             useOrigVarNames=True)
            ]

        ### For do_transformation data write the correlation matrix as well
        if self.do_transformation():
            basis = self.__preprocessing_obj.getCovBasisTrainW() if self.useWeights() else self.__preprocessing_obj.getCovBasisTrain()
            decorr_train = self.__preprocessing_obj.getWeightCovTraining() if self.useWeights(
            ) else self.__preprocessing_obj.getCovTraining()
            ToSave += [
                self.__fillCorrelationMatrix(matrix=np.dot(basis.T, np.dot(decorr_train, basis)),
                                             add_str="Training_inEigen",
                                             useOrigVarNames=False)
            ]
            decorr_test = self.__preprocessing_obj.getWeightCovTesting() if self.useWeights() else self.__preprocessing_obj.getCovTesting()
            ToSave += [
                self.__fillCorrelationMatrix(matrix=np.dot(basis.T, np.dot(decorr_test, basis)),
                                             add_str="Testing_inEigen",
                                             useOrigVarNames=False)
            ]
            if self.__model.isClassificationProblem():
                decorr_bkg_train = self.__preprocessing_obj.getBkgWeightCovTraining() if self.useWeights(
                ) else self.__preprocessing_obj.getBkgCovTraining()
                ToSave += [
                    self.__fillCorrelationMatrix(matrix=np.dot(basis.T, np.dot(decorr_bkg_train, basis)),
                                                 add_str="Bkg_Train_inEigen",
                                                 useOrigVarNames=False)
                ]
                decorr_bkg_test = self.__preprocessing_obj.getBkgWeightCovTesting() if self.useWeights(
                ) else self.__preprocessing_obj.getBkgCovTesting()
                ToSave += [
                    self.__fillCorrelationMatrix(matrix=np.dot(basis.T, np.dot(decorr_bkg_test, basis)),
                                                 add_str="Bkg_Test_inEigen",
                                                 useOrigVarNames=False)
                ]

                decorr_sig_train = self.__preprocessing_obj.getSigWeightCovTraining() if self.useWeights(
                ) else self.__preprocessing_obj.getSigCovTraining()
                ToSave += [
                    self.__fillCorrelationMatrix(matrix=np.dot(basis.T, np.dot(decorr_sig_train, basis)),
                                                 add_str="Sig_Train_inEigen",
                                                 useOrigVarNames=False)
                ]
                decorr_sig_test = self.__preprocessing_obj.getSigWeightCovTesting() if self.useWeights(
                ) else self.__preprocessing_obj.getSigCovTesting()
                ToSave += [
                    self.__fillCorrelationMatrix(matrix=np.dot(basis.T, np.dot(decorr_sig_test, basis)),
                                                 add_str="Sig_Test_inEigen",
                                                 useOrigVarNames=False)
                ]

        Dir = self.ROOTFile().GetDirectory("Diagnostics")
        for obj in sorted(ToSave, key=lambda H: H.GetName()):
            Dir.WriteObject(obj, obj.GetName())
        ### write finally everything to a file
        for G in self.__Groups:
            G.write()
        ### The user can parse a topologyName
        RootSring = ROOT.TString(self.__model.topology_label())
        self.ROOTFile().WriteObject(RootSring, "TopologyLabel")
        self.ROOTFile().Close()

    def __generateSmirnovScore(self, Train, Testing, isBkg=True):
        print "<SciKitDiagnostics> INFO: Generate the Kolmogorov smirnov score of the %s" % ("background" if isBkg else "signal")
        CulTrain = CreateCulumativeHisto(Train, True)
        CulTesting = CreateCulumativeHisto(Testing, True)
        SmirnovTH1 = Train.Clone("MVA_kolmogorov_%s" % ("Bkg" if isBkg else "Sig"))
        SmirnovTH1.Reset()
        SmirnovTH1.GetXaxis().SetTitle("Smirnov score")
        for i in range(1, Train.GetNbinsX()):
            SmirnovTH1.SetBinContent(i, math.fabs(CulTrain.GetBinContent(i) - CulTesting.GetBinContent(i)))
        return SmirnovTH1

    def __fillCorrelationMatrix(self, matrix, add_str="", useOrigVarNames=True):
        dim = len(matrix)

        Histo = ROOT.TH2D("MVA_correlation%s" % (("_" if len(add_str) > 0 else "") + add_str), "Correlation matrix", dim, 0, dim, dim, 0,
                          dim)
        for i in range(dim):
            Histo.GetXaxis().SetBinLabel(i + 1, self.variableName(i, useOrigVarNames))
            Histo.GetYaxis().SetBinLabel(i + 1, self.variableName(i, useOrigVarNames))
            for j in range(0, i + 1):
                bin_ij = Histo.GetBin(i + 1, j + 1)
                bin_ji = Histo.GetBin(j + 1, i + 1)
                norm = math.sqrt(math.fabs(matrix[i][i] * matrix[j][j]))
                entry_ij = 100. * matrix[i][j] / norm
                entry_ji = 100. * matrix[j][i] / norm
                Histo.SetBinContent(bin_ij, entry_ij if math.fabs(entry_ij) > 1.e-6 else 0.)
                Histo.SetBinContent(bin_ji, entry_ji if math.fabs(entry_ji) > 1.e-6 else 0.)
        return Histo

    def __fillCovarianceEigenValues(self, eigen_values, eigen_basis, add_str=""):
        dim = len(eigen_values)
        value_histo = ROOT.TH1D("MVA_cov_eigenValues%s" % (("_" if len(add_str) > 0 else "") + add_str),
                                "Eigen values of the covariance matrix", dim, 0, dim)
        value_histo.GetXaxis().SetTitle("covariance eigen values")
        for i, eigen in enumerate(eigen_values):
            value_histo.SetBinContent(i + 1, eigen)

        basis_histo = ROOT.TH2D("MVA_cov_eigenBasis%s" % (("_" if len(add_str) > 0 else "") + add_str), "Eigen basis transformation", dim,
                                0, dim, dim, 0, dim)
        for i in range(dim):
            basis_histo.GetXaxis().SetBinLabel(i + 1, self.variableName(i, True))
            basis_histo.GetYaxis().SetBinLabel(i + 1, self.variableName(i, False))
            for j in range(dim):
                basis_histo.SetBinContent(basis_histo.GetBin(i + 1, j + 1), eigen_basis[i][j])
        return [basis_histo, value_histo]

    def __makeClassifierDiagnostics(self):
        if not self.__model.isClassificationProblem(): return []
        try:
            min_mva = min([G.minMVAscore() for G in self.__Groups if not math.isnan(G.minMVAscore())])
            max_mva = max([G.maxMVAscore() for G in self.__Groups if not math.isnan(G.maxMVAscore())])
        except:
            print "<SciKitDiagnositcs> WARNING: Could not obtain a reasonable max/min for the MVA score"
            min_mva = -1
            max_mva = 1

        min_mva = round(min_mva, 1) - 0.1
        max_mva = round(max_mva, 1) + 0.1
        for G in self.__Groups:
            G.setMVAhistoRange(minimum=min_mva, maximum=max_mva)
        ### retrieve the MVA histograms
        TH1BkgTrainScore = self.__filterSubSample(doBkg=True, doTrain=True, do_transformation=self.do_transformation()).getMVAHisto()
        TH1SigTrainScore = self.__filterSubSample(doBkg=False, doTrain=True, do_transformation=self.do_transformation()).getMVAHisto()

        TH1BkgTestingScore = self.__filterSubSample(doBkg=True, doTrain=False, do_transformation=self.do_transformation()).getMVAHisto()
        TH1SigTestingScore = self.__filterSubSample(doBkg=False, doTrain=False, do_transformation=self.do_transformation()).getMVAHisto()
        ### use the filled histograms to create the ROC curves
        TH1TrainROC = generateROC(Bkg=TH1BkgTrainScore, Sig=TH1SigTrainScore, isTrainCurve=True, inverse_eff=False)
        TH1TestingROC = generateROC(Bkg=TH1BkgTestingScore, Sig=TH1SigTestingScore, isTrainCurve=False, inverse_eff=False)

        ToSave = [TH1TrainROC, TH1TestingROC]
        ToSave += [
            generateROC(Bkg=TH1BkgTrainScore, Sig=TH1SigTrainScore, isTrainCurve=True, inverse_eff=True),
            generateROC(Bkg=TH1BkgTestingScore, Sig=TH1SigTestingScore, isTrainCurve=False, inverse_eff=True)
        ]
        #### Generate the Kolmogorov Smirnov score
        ToSave += [
            self.__generateSmirnovScore(Train=TH1BkgTrainScore, Testing=TH1BkgTestingScore, isBkg=True),
            self.__generateSmirnovScore(Train=TH1SigTrainScore, Testing=TH1SigTestingScore, isBkg=False)
        ]

        return ToSave
