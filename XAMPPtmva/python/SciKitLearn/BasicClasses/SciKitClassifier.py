#!/bin/env/python
from XAMPPtmva.SciKitDataSet import SciKitDataSet
from XAMPPtmva.SciKitCorrelation import SciKitPreProcessing
from ClusterSubmission.Utils import CreateDirectory
from sklearn.externals import joblib
import numpy as np
import math, time, sys, os, json

### Ensure that always the same seed is used
###
np.random.seed(123456)


class SciKitModelBuilder(object):
    def __init__(
        self,
        ### The extenal model to build it can be either a sci-kit model
        ### or a keras/thenao model
        model=None,
        HDF5File="",  ### Path to the HDF5 in-file
        begin=0,  ### First event
        end=-1,  ### Last event
        outdir="",  ### directory to save the model eventually
        name="MVA_model",  ### Name of the model
        useWeights=False,  ### use Weights to build the model
        SwapTrainTest=False,  ### swap training and testing dataset (i.e. cross-validation)
        isClassification=True,  ### Distinguishes the two big classes of MVA models
        ###    Classifcation vs. Regression

        ### Basic diagnostics to be performed on the training data
        preprocess_obj="",  ### The correlation object is usally of the type SciKitCorrelation. It can be
        transform_data="",

        ### loaded externally from a json/pkl file. Otherwise it's loaded on the fly
        num_threads=8,
        min_spectrum=1.e-8,
        min_cov_eigenValue=1.e-8,
    ):

        self.__model = model

        ### The training is not even started
        ### if for a given feature fabs(max - min) < min_spectrum
        self.__min_spectrum = min_spectrum
        ### The covariance matrix encodes information about the correlations
        ### between the training variables. The eigen-values are a measure of the
        ### relative ranking of the features. If the eigen-value is below, then the
        ### training_data has two fully correlated variables which is harmful for the
        ### MVA training session
        self.__min_covEigenValue = min_cov_eigenValue

        self.__dataset = SciKitDataSet(infile=HDF5File, SwapTrainTest=SwapTrainTest, begin=begin, end=end)
        self.__outdir = outdir
        self.__name = name

        self.__useWeights = useWeights
        self.__swapTrainTest = SwapTrainTest
        self.__isClassification = isClassification

        self.__trained = False
        self.__training_time = 0
        self.__saved = False

        self.__num_threads = num_threads

        self.__preprocess_obj = None
        self.__loaded_preprocessing = False
        self.__imported_preprocessing = False
        ### The correlation object is loaded externally. So we need to make sure
        ### that the swapping is propagated properly. If the object made by the
        ### object itself then the assignment is done properly by default
        if preprocess_obj and len(preprocess_obj) > 0 and os.path.exists(preprocess_obj):
            self.__preprocess_obj = joblib.load(preprocess_obj)
            self.__preprocess_obj.swapTrainAndTest(self.__swapTrainTest)
            self.__imported_preprocessing = True
        else:
            self.__preprocess_obj = SciKitPreProcessing(outdir=self.outDir(),
                                                        dataset=self.dataset(),
                                                        min_spectrum=-1,
                                                        num_threads=num_threads)

        ### Point to a transformation method of the SciKitPreProcessing class:
        ###    -- transform_decorrelated -- affine mapping. The data is shifted by the mean
        ###          and then represented in the eigen-basis of the covariance matrix
        ###    -- transform_mean_normed -- the data is shifted by the mean and then
        ###          divided by the standard-deviation
        self.__evaluation_transform = None

        ### Point to train-dataset transformation method of SciKitPreProcessing class
        ### The entire training dataset is loaded before hand and then piped to a temporary
        ### file to save valuable memory of the machine:
        ###     -- writeUncorrelatedData -->    transform_decorrelated
        ###     -- getMeanNormalizedData -->  transform_mean_normed
        self.__data_transform = None

        self.__data_transform_opt = transform_data
        self.__load_preprocessing()

    def __load_preprocessing(self):
        if not self.__data_transform_opt or self.__data_transform_opt == "None":
            print "<SciKitModelBuilder> INFO: No data preprocessing is defined. Data will be piped to the MVA as it comes in"
        elif self.__data_transform_opt == "decorrelation":
            self.__evaluation_transform = SciKitPreProcessing.transform_decorrelated
            self.__data_transform = SciKitPreProcessing.getDecorrelatedData
        elif self.__data_transform_opt == "normMean":
            self.__evaluation_transform = SciKitPreProcessing.transform_mean_normed
            self.__data_transform = SciKitPreProcessing.getMeanNormalizedData
        else:
            print "<SciKitModelBuilder> ERROR: Invalid data-transformation %s" % (self.__data_transform_opt)
            exit(1)

    def model(self):
        return self.__model

    def num_threads(self):
        return self.__num_threads

    def preProcessing(self):
        if not self.__loaded_preprocessing:
            self.__loaded_preprocessing = True
            self.__preprocess_obj.swapTrainAndTest(self.__swapTrainTest)
            self.__preprocess_obj.load()
        return self.__preprocess_obj

    def isClassificationProblem(self):
        return self.__isClassification

    def isRegressionProblem(self):
        return not self.isClassificationProblem()

    def isTrained(self):
        return self.__trained

    def trainTestSwapped(self):
        return self.__swapTrainTest

    def useWeights(self):
        return self.__useWeights

    def dataset(self):
        return self.__dataset

    ### Basic check whether the data is consistent
    def goodTrainData(self):
        ### check if the training data is valid
        if not self.dataset(): return False
        if self.isClassificationProblem() and not self.dataset().isClassificationDS():
            print "<SciKitModelBuilder> ERROR: The training data is not meant for classification allthough we're running a classification problem"
            return False
        if self.isRegressionProblem() and not self.dataset().isRegressionDS():
            print "<SciKitModelBuilder> ERROR: The training data is not meant for regression allthough we're running a regression problem"
            return False

        for i in range(self.__dataset.nVars()):
            if math.fabs(self.dataset().getTrainingMax(i) - self.dataset().getTrainingMin(i)) < self.__min_spectrum:
                print "<SciKitModelBuilder> ERROR: variable %s ranges between an interval less than %E" % (dataset.variableName(i),
                                                                                                           self.__min_spectrum)
                return False
            if math.fabs(self.dataset().getTestingMax(i) - self.dataset().getTestingMin(i)) < self.__min_spectrum:
                print "<SciKitModelBuilder> ERROR: variable %s ranges between an interval less than %E" % (dataset.variableName(i),
                                                                                                           self.__min_spectrum)
                return False

        if not self.preProcessing().unCorrelatedData(self.__min_covEigenValue): return False
        return True

    ### name of the out file
    def modelFile(self):
        return "%s/%s_trained_model.pkl" % (self.outDir(), self.name())

    def outDir(self):
        return self.__outdir if len(self.__outdir) > 0 else os.getcwd()

    def name(self):
        return self.__name

    ### Returns the name of the n-th training variable
    def variableName(self, n, get_original=True):
        if get_original:
            if n < self.nVars(): return self.__dataset.variableName(n)
            else: return "Invalid variable"
        else: return "%d_variable" % (n)

    ### Returns the number of training variables
    def nVars(self):
        return self.__dataset.nVars()

    ### Returns the name of the n-th target variable.
    ### Only for regression problems
    def targetVariable(self, n):
        if n >= self.nTargets(): return "Invalid variable"
        return self.__dataset.targetVariable(n)

    def nTargets(self):
        return self.__dataset.nTargets() if self.isRegressionProblem() else 0

    def fit(self):
        ### Avoid double fitting or training
        if self.__trained:
            print "<SciKitModelBuilder> ERROR: Training is already performed"
            return False
        if not self.goodTrainData():
            print "<SciKitModelBuilder> ERROR: Training data is invalid"
            return False

        if self.do_transformation():
            in_data = self.__data_transform(self.preProcessing(),
                                            training=not self.trainTestSwapped() or not self.__imported_preprocessing,
                                            useWeights=self.useWeights())
        else:
            in_data = self.dataset().getTrainingData()
        ### dataset takes care of exchanging testing as training data if needed
        ### Call a seperate fitting function which needs to be overwritten in case
        ### people are using keras or theano...
        time_start = time.clock()
        print "<SciKitModelBuilder> INFO: Start the training of %s on %d %s events." % (self.name(), self.dataset().nTrainingEvents(),
                                                                                        "weighted" if self.useWeights() else "raw")
        if not self._fit_mva(x=in_data,
                             y=self.dataset().getTrainingClassifier(),
                             weights=self.dataset().getTrainingWeights() if self.useWeights() else None):
            return False

        self.__trained = True
        self.__training_time = time.clock() - time_start

        print "<SciKitModelBuilder> INFO: Training finished of %s. Needed time: %s" % (
            self.name(), time.strftime("%H:%M:%S", time.gmtime(self.__training_time)))
        return True

    def _dump_model(self):
        return

    def _extra_meta_data(self):
        return {}

    def _fit_mva(self, x, y, weights):
        print "<SciKitModelBuilder> WARNING: This is a dummy function. Please consider to overwrite _fit_mva(self)"
        return False

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        print "<SciKitModelBuilder> WARNING: Please overload the 'saveSpecificDiagnostics(self, Diag_Dir = None)' method in the implementation of your model builder"
        print "<SciKitModelBuilder> WARNING: The method is  intended to sve monitoring histograms to your diagnostics file."
        return False

    ### Optional diagnostics on the dataset it self. E.g. how does
    ### the distribution change by applying the gradient boost weights
    def runMVAdiagnosticsOnDS(self, diagnostic_instance=None):
        print "<SciKitModelBuilder> INFO: No extra MVA diagnostics will be run on ds. If you'd like to change this please overload runMVAdiagnosticsOnDS(self, diagnostic_instance=None)'"
        return []

    def topology_label(self):
        return self.dataset().topology_label()

    def save(self):
        if not self.__trained:
            print "<SciKitModelBuilder> WARNING: The model %s has not even been trained. It does not make sense to save it " % (self.name())
            return
        if self.__saved:
            print "<SciKitModelBuilder> WARNING: The model has already been saved. Do not clobber with old instances..."
            return
        self.__saved = True
        CreateDirectory(self.outDir(), False)
        json_path = "%s/%s.json" % (self.outDir(), self.name())
        json_dict = {
            "Name": self.name(),
            "ModelFile": self.modelFile(),
            "IsClassification": self.isClassificationProblem(),

            ### Training data meta-data
            "HDF5File": self.dataset().file_name(),
            "TrainTestingSwapped": self.trainTestSwapped(),
            "Topology": self.topology_label(),
            "DataTransformation": self.__data_transform_opt,
            "TrainingVariables": [self.variableName(n) for n in range(self.nVars())],
            "TargetVariables": [self.targetVariable(n) for n in range(self.nTargets())],
            ### Training maxima - minima
            "MaxInTrain": [self.dataset().getTrainingMax(n) for n in range(self.nVars())],
            "MaxInTest": [self.dataset().getTestingMax(n) for n in range(self.nVars())],
            "MinInTrain": [self.dataset().getTrainingMin(n) for n in range(self.nVars())],
            "MinInTest": [self.dataset().getTestingMin(n) for n in range(self.nVars())],
            "TrainingEvents": self.dataset().nTrainingEvents(),
            "TestingEvents": self.dataset().nTestingEvents(),
            "FirstEvent": self.dataset().begin(),
            "LastEvent": self.dataset().end(),
            ###
            "Created": time.strftime("%Y-%m-%d - %H:%M:%S"),
            "TrainingTime": self.__training_time
        }
        extra_info = self._extra_meta_data()
        for key, value in extra_info.iteritems():
            if key in json_dict.iterkeys():
                print "<SciKitModelBuilder> WARNING: The key %s is already occupied with %s. The information %s will not be dumped. " % (
                    key,
                    str(json_dict[key]),
                    value,
                )
                continue
            json_dict[key] = value
        ### Dump the json information
        for key, value in json_dict.iteritems():
            if isinstance(value, str): continue
            if isinstance(value, list): json_dict[key] = [str(x) for x in value]
            json_dict[key] = str(value)
        with open(json_path, "w") as dump_file:
            json.dump(json_dict, dump_file)

        ### Pickle everything
        self.dataset().closeFile()
        ### Save the covariance matrices of the
        ### decorrelation object
        self.preProcessing().save()
        self.preProcessing().clear()

        self._dump_model()
        print "<SciKitModelBuilder> INFO: The model has been saved to %s" % (self.modelFile())
        ###
        cache = self.__model
        self.__model = None
        ### reset the state of having a class object
        self.__loaded_preprocessing = False
        ### The function pointers also need to be dismantled
        self.__evaluation_transform = None
        self.__data_transform = None

        out_file = "%s/%s.pkl" % (self.outDir(), self.name())
        print "<SciKitModelBuilder> INFO: Finally save myself to %s" % (out_file)
        joblib.dump(self, out_file)
        ### Now everything is pickled and we can go back to buisness
        self.__model = cache
        self.__load_preprocessing()

    def load(self):
        self.__load_preprocessing()
        if not self.__model: self.__model = self._load_model()
        return True

    def do_transformation(self):
        return self.__evaluation_transform != None

    def transform(self, data):
        if self.do_transformation():
            return self.__evaluation_transform(self.preProcessing(), data)
        return data

    def _call_mva(self, data):
        print "SciKitModelBuilder> WARNING: This function is a dummy method. Please overwrite _call_mva(self,data) in your mdoel builder"
        return [0]

    def predict(self, data):
        if not self.isTrained():
            print "<SciKitClassifier> ERROR: The algorithm %s must be trained before it can predict anything" % (self.name())
            return [0]
        return self._call_mva(self.transform(data))


class SciKitClassifier(SciKitModelBuilder):
    def __init__(self,
                 model=None,
                 HDF5File="",
                 begin=0,
                 end=-1,
                 outdir="",
                 preprocessing=None,
                 name="classifier",
                 useWeights=False,
                 SwapTrainTest=False,
                 transform_data="",
                 min_spectrum=1.e-8,
                 min_covEigen=1.e-8,
                 isClassification=True,
                 num_threads=2):

        SciKitModelBuilder.__init__(self,
                                    model=model,
                                    HDF5File=HDF5File,
                                    begin=0,
                                    end=-1,
                                    outdir=outdir,
                                    name=name,
                                    useWeights=useWeights,
                                    SwapTrainTest=SwapTrainTest,
                                    isClassification=isClassification,
                                    preprocess_obj=preprocessing,
                                    num_threads=num_threads,
                                    min_spectrum=min_spectrum,
                                    transform_data=transform_data,
                                    min_cov_eigenValue=min_covEigen)

    def _call_mva(self, data):
        return self.model().decision_function(data)

    def _fit_mva(self, x, y, weights):
        if not self.useWeights():
            print self.model().fit(x, y)
        else:
            print self.model().fit(x, y, weights)
        return True

    def _dump_model(self):
        joblib.dump(self.model(), self.modelFile())

    def _load_model(self):
        return joblib.load(self.modelFile())


class KerasModelBuilder(SciKitModelBuilder):
    def __init__(
        self,
        HDF5File="",
        begin=0,
        end=-1,
        outdir="",
        preprocessing=None,
        name="classifier",
        useWeights=False,
        SwapTrainTest=False,
        min_spectrum=1.e-8,
        min_cov_eigenValue=1.e-8,
        isClassification=True,
        ### Keras specific attributes
        loss_function="",
        optimizer="",
        epochs=25,
        batch_size=1000,
        transform_data="",
    ):
        ### make sure to call this function setting the backend to theano
        ### before importing anything else from keras otherwise the script
        ### crashes because tensor flow is not installed and you do not want
        ### to install tensorflow on atlas machines....
        from XAMPPtmva.LoadHDF5 import set_keras_backend
        set_keras_backend()
        from keras.models import Sequential
        SciKitModelBuilder.__init__(
            self,
            model=Sequential(),
            HDF5File=HDF5File,
            begin=begin,
            end=end,
            outdir=outdir,
            name=name,
            useWeights=useWeights,
            SwapTrainTest=SwapTrainTest,
            min_spectrum=min_spectrum,
            min_cov_eigenValue=min_cov_eigenValue,
            isClassification=isClassification,
            num_threads=1,
            transform_data=transform_data,
        )

        self.__loss_function = loss_function
        self.__epochs = epochs
        self.__optimizer = optimizer
        self.__batch_size = max(batch_size - batch_size % self.dataset().chunkSize(), self.dataset().chunkSize())

    def modelFile(self):
        return "%s/%s_keras_model.json" % (self.outDir(), self.name())

    def weightFile(self):
        return "%s/%s_keras_weights.h5" % (self.outDir(), self.name())

    def _dump_model(self):
        with open(self.modelFile(), "w") as json_file:
            json_file.write(self.model().to_json())
        self.model().save_weights(self.weightFile())
        return

    def _extra_meta_data(self):
        return {"WeightFile": self.weightFile()}

    def _fit_mva(self, x, y, weights):
        from keras.layers.embeddings import Embedding
        self.model().add(
            Embedding(input_dim=self.dataset().nTrainingEvents(),
                      output_dim=self.__batch_size,
                      embeddings_initializer='uniform',
                      embeddings_regularizer=None,
                      activity_regularizer=None,
                      embeddings_constraint=None,
                      mask_zero=False,
                      input_length=self.nVars()))

        if not self._build_model(): return False
        self.model().compile(loss=self.__loss_function, optimizer=self.__optimizer, metrics=['accuracy'])
        self.model().fit(x=x, y=y, sample_weight=weights, shuffle='batch', epochs=self.__epochs, batch_size=self.__batch_size)
        self.model().summary()
        return True

    def _build_model(self):
        print "<KerasModelBuilder>: Please overload the _build_model(self) method"
        return False

    def _call_mva(self, data):
        return self.model().predict(x=data, batch_size=self.__batch_size)

    def _load_model(self):
        from XAMPPtmva.LoadHDF5 import set_keras_backend
        set_keras_backend()
        from keras.models import model_from_json
        # Model reconstruction from JSON file
        model = None
        with open(self.modelFile(), 'r') as f:
            model = model_from_json(f.read())
        # Load weights into the new model
        model.load_weights(self.weightFile())
        return model
