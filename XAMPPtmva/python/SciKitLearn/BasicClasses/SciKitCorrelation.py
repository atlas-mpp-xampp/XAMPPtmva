#!/bin/env/python
from XAMPPtmva.SciKitDataSet import SciKitDataSet, SciKitDataLoader
from ClusterSubmission.Utils import CreateDirectory, ExecuteThreads, FillWhiteSpaces
import os, time, math
import numpy as np
import h5py


class SciKitCovMatCalculator(SciKitDataLoader):
    def __init__(self, dataset=None, process_testing=False, process_signal=False, process_background=False, useWeights=True):
        ### Constructor of the threading class
        SciKitDataLoader.__init__(self,
                                  process_testing=process_testing,
                                  process_background=process_background,
                                  process_signal=process_signal,
                                  useWeights=useWeights,
                                  dataset=dataset,
                                  progress=process_background and process_signal)
        self.__dim = self.dataset().nVars() if self.dataset() else 1
        ##### The mean value of each variable
        self.__X = np.zeros(self.__dim)
        ##### The variance matrix as XX[i][j] = X_{i}*X_{j}
        self.__XX = np.zeros((self.__dim, self.__dim))
        self.__SumW = np.float32(0.)
        ### Eigen-vectors and eigen-values
        self.__EigenVal = None
        self.__EigenBasis = None

    ### Define what data is actually needed to store it into the file

    def dimension(self):
        return self.__dim

    def fillChunk(self):
        data = self.current_dataBlock()
        weights = self.current_weightBlock()
        self.__SumW += np.sum(weights)
        np.add(self.__X, np.dot(data.T, weights), self.__X)
        weighted_data = np.copy(data)
        weighted_data = np.multiply(weights.reshape(-1, 1), weighted_data)
        np.add(self.__XX, np.dot(data.T, weighted_data), self.__XX)
        return True

    def finish(self):
        if not self.isGoodState(): return
        ### Var = <X - <X> >**2 =  <X**2> - <X>**2
        #### CovMat = <(X - <X>)(Y - <Y>)> = <XY> - <Y><X>
        #####       X' = X - <X> , Y' = Y - <Y>
        ######  ->  CovMat =  <X'Y'> - <Y'><X'> =  <(X - <X>)(Y - <Y>)> - <Y -<Y>><X -<X>> = CovMat (X,Y)

        ### Normalize the mean value
        for i in xrange(self.dimension()):
            self.__X[i] /= self.__SumW
        ### Determine the X^{2}
        for i in xrange(self.dimension()):
            for j in xrange(i + 1):
                Mij = self.__XX[i][j] / self.__SumW - self.__X[i] * self.__X[j]
                self.__XX[i][j] = Mij
                self.__XX[j][i] = Mij

        from numpy import linalg as LA
        print "<SciKitCovMatCalculator> INFO: Calculate the eigen-values and eigenspace of the %s %s covariance matrix" % (
            "weighted" if self.useWeights() else "raw", "testing" if self.doTestingEvts() else "training")
        try:
            self.__EigenVal, self.__EigenBasis = LA.eig(self.__XX)
        except Exception as e:
            print "<SciKitCovMatCalculator> ERROR: The calculation of the eigen basis and matrix failed..."
            print "<SciKitCovMatCalculator> ERROR: %s" % (str(e))
            self.__EigenVal = np.zeros(self.__dim)
            self.__EigenBasis = np.zeros((self.__dim, self.__dim))

        self.__isFinished = True

    def mean(self):
        return self.__X

    def cov(self):
        return self.__XX

    def eigenBasis(self):
        return self.__EigenBasis

    def eigenValues(self):
        return self.__EigenVal

    def nonZeroDeterminant(self, min_eigen=1.e-9):
        nonZero = True
        for i, x in enumerate(self.eigenValues()):
            if math.fabs(np.abs(x)) < min_eigen:
                print "<SciKitCovMatCalculator> WARNING: The %d-th eigen value %s is less than %E (i.e. ~zero)" % (i, str(x), min_eigen)
                nonZero = False
            if np.iscomplexobj(x):
                print "<SciKitCovMatCalculator> ERROR: The %d-th eigen value %s is complex" % (i, str(x))
                nonZero = False
            else:
                print "<SciKitCovMatCalculator> INFO: %d th eigen-value: %E" % (i + 1, x)
        return nonZero

    def write(self, file_name):
        np.savez_compressed(file=file_name,
                            covariance=self.__XX,
                            mean=self.__X,
                            eigen_values=self.__EigenVal,
                            eigen_basis=self.__EigenBasis)

    def load(self, mat_file):
        if not os.path.isfile(mat_file):
            print "<SciKitCovMatCalculator> ERROR: %s is not a file" % (mat_file)
            exit(1)
        loaded = np.load(mat_file)
        self.__X = loaded['mean']
        self.__XX = loaded['covariance']
        self.__EigenVal = loaded['eigen_values']
        self.__EigenBasis = loaded['eigen_basis']


class SciKitDecorrelatedDSdump(SciKitDataLoader):
    def __init__(self, preprocessing=None, dataset=None, process_testing=False, useWeights=True, out_file=""):
        SciKitDataLoader.__init__(self,
                                  dataset=dataset,
                                  process_testing=process_testing,
                                  process_signal=True,
                                  process_background=True,
                                  useWeights=useWeights,
                                  progress=True,
                                  chunksize=dataset.chunkSize())
        self.__preprocessing = preprocessing
        self.__H5File = h5py.File(out_file, 'w')
        self.__Group = self.__H5File.create_group("Testing" if process_testing else "Training")
        self.__DS = self.__Group.create_dataset("Data",
                                                shape=(self.chunkSize(), dataset.nVars()),
                                                maxshape=(None, dataset.nVars()),
                                                chunks=(self.chunkSize(), dataset.nVars()))
        self.__Max = np.ones(dataset.nVars())
        self.__Min = np.ones(dataset.nVars())
        np.dot(self.__Max, -1.e15)
        np.dot(self.__Min, 1.e15)

        self.__start = 0

    def fillChunk(self):
        data = self.current_dataBlock()
        weights = self.current_weightBlock()
        transformed = self.__preprocessing.transform_decorrelated(data, useTraining=not self.doTestingEvts(), useWeights=self.useWeights())
        extension = self.__start + transformed.shape[0]
        self.__DS.resize(extension, axis=0)
        self.__DS[self.__start:] = transformed
        self.__start = extension
        self.__Max = np.maximum(self.__Max, np.amax(transformed, axis=0))
        self.__Min = np.minimum(self.__Min, np.amin(transformed, axis=0))

        return True

    def finish(self):
        if not self.isGoodState(): return
        self.__DS.attrs["Maximum"] = self.__Max
        self.__DS.attrs["Minimum"] = self.__Min
        self.__DS.attrs["varNames"] = ["variable_%d" % (d) for d in range(self.dataset().nVars())]
        self.__DS.attrs["nEvents"] = self.getNumEvents()


class SciKitMeanNormedDSdump(SciKitDataLoader):
    def __init__(self, preprocessing=None, dataset=None, process_testing=False, useWeights=True, out_file=""):
        SciKitDataLoader.__init__(self,
                                  dataset=dataset,
                                  process_testing=process_testing,
                                  process_signal=True,
                                  process_background=True,
                                  useWeights=useWeights,
                                  progress=True,
                                  chunksize=dataset.chunkSize())
        self.__preprocessing = preprocessing
        self.__H5File = h5py.File(out_file, 'w')
        self.__Group = self.__H5File.create_group("Testing" if process_testing else "Training")
        self.__DS = self.__Group.create_dataset("Data",
                                                shape=(self.chunkSize(), dataset.nVars()),
                                                maxshape=(None, dataset.nVars()),
                                                chunks=(self.chunkSize(), dataset.nVars()))
        self.__Max = np.ones(dataset.nVars())
        self.__Min = np.ones(dataset.nVars())
        np.dot(self.__Max, -1.e15)
        np.dot(self.__Min, 1.e15)

        self.__start = 0

    def fillChunk(self):
        data = self.current_dataBlock()
        weights = self.current_weightBlock()
        transformed = self.__preprocessing.transform_mean_normed(data, useTraining=not self.doTestingEvts(), useWeights=self.useWeights())
        extension = self.__start + self.chunkSize()
        self.__DS.resize(extension, axis=0)
        self.__DS[self.__start:self.__start + len(transformed)] = transformed
        self.__start = self.__start + transformed.shape[0]
        self.__Max = np.maximum(self.__Max, np.amax(transformed, axis=0))
        self.__Min = np.minimum(self.__Min, np.amin(transformed, axis=0))

        return True

    def finish(self):
        if not self.isGoodState(): return
        self.__DS.attrs["Maximum"] = self.__Max
        self.__DS.attrs["Minimum"] = self.__Min
        self.__DS.attrs["varNames"] = ["variable_%d" % (d) for d in range(self.dataset().nVars())]
        self.__DS.attrs["nEvents"] = self.getNumEvents()


class SciKitPreProcessing(object):
    def __init__(self, infile="", begin=0, end=-1, outdir="./", dataset=None, min_spectrum=1.e-8, num_threads=6):
        self.__dataset = SciKitDataSet(infile=infile, begin=begin, end=end) if not dataset else dataset
        self.__isClassDS = self.__dataset.isClassificationDS()
        for i in range(self.__dataset.nVars()):
            if math.fabs(self.__dataset.getTrainingMax(i) - self.__dataset.getTrainingMin(i)) < min_spectrum:
                print "<SciKitPreProcessing> ERROR: variable %s ranges between an interval less than %f" % (dataset.variableName(i),
                                                                                                            min_spectrum)
                exit(1)
            if math.fabs(self.__dataset.getTestingMax(i) - self.__dataset.getTestingMin(i)) < min_spectrum:
                print "<SciKitPreProcessing> ERROR: variable %s ranges between an interval less than %f" % (dataset.variableName(i),
                                                                                                            min_spectrum)
                exit(1)
        self.__outdir = outdir
        self.__SwapTrainTest = False

        self.__TrainCovMatW = None
        self.__TrainCovMat = None
        self.__SigTrainCovMatW = None
        self.__SigTrainCovMat = None
        self.__BkgTrainCovMatW = None
        self.__BkgTrainCovMat = None

        self.__TestCovMatW = None
        self.__TestCovMat = None

        self.__SigTestCovMatW = None
        self.__SigTestCovMat = None
        self.__BkgTestCovMatW = None
        self.__BkgTestCovMat = None
        self.__setupMatCalc()
        print "<SciKitPreProcessing> INFO: Load correlation matrices of the input data"

        ExecuteThreads(Threads=self.__matrices, MaxCurrent=num_threads, verbose=False)
        self.__saveable = True

    def trainTestSwapped(self):
        return self.__SwapTrainTest

    def __setupMatCalc(self):
        self.__TrainCovMatW = SciKitCovMatCalculator(dataset=self.__dataset,
                                                     process_testing=False,
                                                     process_signal=True,
                                                     process_background=True,
                                                     useWeights=True)
        self.__TrainCovMat = SciKitCovMatCalculator(dataset=self.__dataset,
                                                    process_testing=False,
                                                    process_signal=True,
                                                    process_background=True,
                                                    useWeights=False)

        #### Signal & Background covariance matrices. Make only sense
        #### for classification problems
        if self.__isClassDS:
            self.__SigTrainCovMatW = SciKitCovMatCalculator(dataset=self.__dataset,
                                                            process_testing=False,
                                                            process_signal=True,
                                                            process_background=False,
                                                            useWeights=True)
            self.__SigTrainCovMat = SciKitCovMatCalculator(dataset=self.__dataset,
                                                           process_testing=False,
                                                           process_signal=True,
                                                           process_background=False,
                                                           useWeights=False)

            self.__BkgTrainCovMatW = SciKitCovMatCalculator(dataset=self.__dataset,
                                                            process_testing=False,
                                                            process_signal=False,
                                                            process_background=True,
                                                            useWeights=True)
            self.__BkgTrainCovMat = SciKitCovMatCalculator(dataset=self.__dataset,
                                                           process_testing=False,
                                                           process_signal=False,
                                                           process_background=True,
                                                           useWeights=False)

        self.__TestCovMatW = SciKitCovMatCalculator(dataset=self.__dataset,
                                                    process_testing=True,
                                                    process_signal=True,
                                                    process_background=True,
                                                    useWeights=True)
        self.__TestCovMat = SciKitCovMatCalculator(dataset=self.__dataset,
                                                   process_testing=True,
                                                   process_signal=True,
                                                   process_background=True,
                                                   useWeights=False)
        #### Signal & Background covariance matrices. Make only sense
        #### for classification problems
        if self.__isClassDS:
            self.__SigTestCovMatW = SciKitCovMatCalculator(dataset=self.__dataset,
                                                           process_testing=True,
                                                           process_signal=True,
                                                           process_background=False,
                                                           useWeights=True)
            self.__SigTestCovMat = SciKitCovMatCalculator(dataset=self.__dataset,
                                                          process_testing=True,
                                                          process_signal=True,
                                                          process_background=False,
                                                          useWeights=False)
            self.__BkgTestCovMatW = SciKitCovMatCalculator(dataset=self.__dataset,
                                                           process_testing=True,
                                                           process_signal=False,
                                                           process_background=True,
                                                           useWeights=True)
            self.__BkgTestCovMat = SciKitCovMatCalculator(dataset=self.__dataset,
                                                          process_testing=True,
                                                          process_signal=False,
                                                          process_background=True,
                                                          useWeights=False)

        self.__matrices = [
            self.__TrainCovMatW,
            self.__TrainCovMat,
            self.__TestCovMatW,
            self.__TestCovMat,
        ]
        if self.__isClassDS:
            self.__matrices += [
                self.__SigTrainCovMatW, self.__SigTrainCovMat, self.__BkgTrainCovMatW, self.__BkgTrainCovMat, self.__SigTestCovMatW,
                self.__SigTestCovMat, self.__BkgTestCovMatW, self.__BkgTestCovMat
            ]

    ### Data preprocessing to be applied on each event
    def transform_decorrelated(self, data, useTraining=True, useWeights=True):
        if useTraining:
            if useWeights:
                return np.dot(np.subtract(data, self.getWeightMeanTraining()), self.getCovBasisTrainW())
            return np.dot(np.subtract(data, self.getMeanTraining()), self.getCovBasisTrain())
        if useWeights:
            return np.dot(np.subtract(data, self.getWeightMeanTesting()), self.getCovBasisTestW())
        return np.dot(np.subtract(data, self.getMeanTesting()), self.getCovBasisTest())

    def transform_mean_normed(self, data, useTraining=True, useWeights=True):
        mean = (self.getWeightMeanTraining() if useWeights else self.getMeanTraining()) if useTraining else (
            self.getWeightMeanTesting() if useWeights else self.getMeanTesting())
        sigma = (self.getWeightCovTraining() if useWeights else self.getCovTraining()) if useTraining else (
            self.getWeightCovTesting() if useWeights else self.getCovTesting())
        return np.divide(np.subtract(data, mean), np.sqrt(np.diag(sigma)))

    def outdir(self):
        return self.__outdir

    ### Check if the data is suitable for training

    def printMatrix(self, matrix, corr_thresh=0.9):
        ### Maximum width of the column
        maxima = np.zeros(matrix.shape[0] + 1)
        to_print = [[self.__dataset.variableName(x) for x in range(self.__dataset.nVars())]]
        maxima[0] = max([len(c) for c in to_print[0]])
        for x in range(matrix.shape[0]):
            column = ["%.2f" % (100. * matrix[x, y] / math.sqrt(matrix[x, x] * matrix[y, y])) for y in range(matrix.shape[1])]
            maxima[x + 1] = max([len(c) for c in column])
            to_print += [column]

        for y in range(matrix.shape[1]):
            line = " %s%s | " % (to_print[0][y], FillWhiteSpaces(int(maxima[0] - len(to_print[0][y]))))
            for x in range(matrix.shape[0]):
                white_spaces = int(maxima[x + 1] - len(to_print[x + 1][y]))
                corel = (x != y and math.fabs(matrix[x, y] / math.sqrt(matrix[x, x] * matrix[y, y])) > corr_thresh)
                if corel:
                    print "<SciKitPreProcessing> WARNING: variables %s and %s are corellated" % (self.__dataset.variableName(x),
                                                                                                 self.__dataset.variableName(y))
                line += "%s%s%s%s%s%s%s | " % (FillWhiteSpaces(
                    (white_spaces - white_spaces % 2) / 2), "\033[91m*" if corel else "", to_print[x + 1][y], "*\033[0m" if corel else "",
                                               "\033[92m" if corel else "", FillWhiteSpaces(
                                                   (white_spaces + white_spaces % 2) / 2), "\033[0m")
            print line

    def unCorrelatedData(self, min_eigen=0):
        print "\n\n\n"
        for M in [self.__TrainCovMatW, self.__TrainCovMat, self.__TestCovMatW, self.__TestCovMat]:
            FillWhiteSpaces(100, "*/")
            print "<ScitKitCorrelation> INFO: Check the eigenvalues of the %s %s covariance matrix" % (
                "weighted" if M.useWeights() else "raw", "testing" if M.doTestingEvts() else "training")
            if not M.nonZeroDeterminant(min_eigen):
                print "<SciKitPreProcessing> ERROR: At least two of the MVA variables are too much correlated. Please reconsider your training"
                self.printMatrix(M.cov())
                return False
            FillWhiteSpaces(100, "/*")
        print "\n\n\n"
        return True

    def load(self):
        if self.__saveable: return
        self.__setupMatCalc()
        for M in self.__matrices:
            f_name = self.__getMatrixFileName(M)
            M.load("%s.npz" % (f_name))

    def __getMatrixFileName(self, M):
        if not M: return ""
        m_type = ""
        if M.doBackground() and M.doSignal(): m_type = "inclusive"
        elif M.doBackground(): m_type = "background"
        elif M.doSignal(): m_type = "signal"
        if len(m_type) == 0: return ""
        f_name = "%s/correlation_data/covariance_data_%s_%s_%s" % (self.outdir(), "weighted" if M.useWeights() else "raw",
                                                                   "training" if not M.doTestingEvts() else "testing", m_type)
        return f_name

    def writeUncorrelatedData(self, useWeights=True, useRaw=True, writeTesting=True, writeTraining=True, cleanUp=True, num_threads=4):
        tmp_dir = "%s/uncorrelated_datasets/" % (self.outdir())
        CreateDirectory(tmp_dir, CleanUpOld=cleanUp)
        writers = []
        swap = self.__SwapTrainTest
        self.__SwapTrainTest = False
        if useWeights and writeTesting and not os.path.exists("%s/weighted_testing.H5" % (tmp_dir)):
            writers += [
                SciKitDecorrelatedDSdump(preprocessing=self,
                                         dataset=self.__dataset,
                                         process_testing=True,
                                         useWeights=True,
                                         out_file="%s/weighted_testing.H5" % (tmp_dir)),
            ]
        if useWeights and writeTraining and not os.path.exists("%s/weighted_training.H5" % (tmp_dir)):
            writers += [
                SciKitDecorrelatedDSdump(preprocessing=self,
                                         dataset=self.__dataset,
                                         process_testing=False,
                                         useWeights=True,
                                         out_file="%s/weighted_training.H5" % (tmp_dir))
            ]

        if useRaw and writeTesting and not os.path.exists("%s/raw_testing.H5" % (tmp_dir)):
            writers += [
                SciKitDecorrelatedDSdump(preprocessing=self,
                                         dataset=self.__dataset,
                                         process_testing=True,
                                         useWeights=False,
                                         out_file="%s/raw_testing.H5" % (tmp_dir))
            ]
        if useRaw and writeTraining and not os.path.exists("%s/raw_training.H5" % (tmp_dir)):
            writers += [
                SciKitDecorrelatedDSdump(preprocessing=self,
                                         dataset=self.__dataset,
                                         process_testing=False,
                                         useWeights=False,
                                         out_file="%s/raw_training.H5" % (tmp_dir))
            ]

        if len(writers) > 0:
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
            print "<SciKitPreProcessing> INFO: Write new %d HDF5 files containing the uncorrelated data to %s" % (len(writers), tmp_dir)
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"

        ExecuteThreads(writers, MaxCurrent=num_threads, verbose=False)
        self.__SwapTrainTest = swap
        for W in writers:
            if not W.isGoodState(): return False
        return True

    def writeMeanNormalizedData(self, useWeights=True, useRaw=True, writeTesting=True, writeTraining=True, cleanUp=True, num_threads=4):
        tmp_dir = "%s/mean_normalized_datasets/" % (self.outdir())
        CreateDirectory(tmp_dir, CleanUpOld=cleanUp)
        writers = []
        swap = self.__SwapTrainTest
        self.__SwapTrainTest = False
        if useWeights and writeTesting and not os.path.exists("%s/weighted_testing.H5" % (tmp_dir)):
            writers += [
                SciKitMeanNormedDSdump(preprocessing=self,
                                       dataset=self.__dataset,
                                       process_testing=True,
                                       useWeights=True,
                                       out_file="%s/weighted_testing.H5" % (tmp_dir)),
            ]
        if useWeights and writeTraining and not os.path.exists("%s/weighted_training.H5" % (tmp_dir)):
            writers += [
                SciKitMeanNormedDSdump(preprocessing=self,
                                       dataset=self.__dataset,
                                       process_testing=False,
                                       useWeights=True,
                                       out_file="%s/weighted_training.H5" % (tmp_dir))
            ]

        if useRaw and writeTesting and not os.path.exists("%s/raw_testing.H5" % (tmp_dir)):
            writers += [
                SciKitMeanNormedDSdump(preprocessing=self,
                                       dataset=self.__dataset,
                                       process_testing=True,
                                       useWeights=False,
                                       out_file="%s/raw_testing.H5" % (tmp_dir))
            ]
        if useRaw and writeTraining and not os.path.exists("%s/raw_training.H5" % (tmp_dir)):
            writers += [
                SciKitMeanNormedDSdump(preprocessing=self,
                                       dataset=self.__dataset,
                                       process_testing=False,
                                       useWeights=False,
                                       out_file="%s/raw_training.H5" % (tmp_dir))
            ]

        if len(writers) > 0:
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
            print "<SciKitPreProcessing> INFO: Write new %d HDF5 files containing the uncorrelated data to %s" % (len(writers), tmp_dir)
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
            print "+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"

        ExecuteThreads(writers, MaxCurrent=num_threads, verbose=False)
        self.__SwapTrainTest = swap
        for W in writers:
            if not W.isGoodState(): return False
        return True

    #### Method called by the SciKitClassifier to get the uncorrelated data
    def getDecorrelatedData(self, training=True, useWeights=True):
        if not self.writeUncorrelatedData(
                useWeights=useWeights, useRaw=not useWeights, writeTesting=not training, writeTraining=training, cleanUp=False):
            CreateDirectory("%s/uncorrelated_datasets/" % (self.outdir()), CleanUpOld=True)
            exit(1)
        self.__dataset = SciKitDataSet(infile="%s/uncorrelated_datasets/%s_%s.H5" %
                                       (self.outdir(), "weighted" if useWeights else "raw", "training" if training else "testing"),
                                       SwapTrainTest=not training)
        return self.__dataset.getTrainingData()

    def getMeanNormalizedData(self, training=True, useWeights=True):
        if not self.writeMeanNormalizedData(
                useWeights=useWeights, useRaw=not useWeights, writeTesting=not training, writeTraining=training, cleanUp=False):
            CreateDirectory("%s/mean_normalized_datasets/" % (self.outdir()), CleanUpOld=True)
            exit(1)
        self.__dataset = SciKitDataSet(infile="%s/mean_normalized_datasets/%s_%s.H5" %
                                       (self.outdir(), "weighted" if useWeights else "raw", "training" if training else "testing"),
                                       SwapTrainTest=not training)
        return self.__dataset.getTrainingData()

    ### Option to exchange training and testing
    def swapTrainAndTest(self, B=True):
        print "<SciKitPreProcessing> INFO: %s the exchange of training and testing" % ("Enable" if B else "Disable")
        self.__SwapTrainTest = B

    def save(self):
        if not self.__saveable: return
        self.__saveable = False
        CreateDirectory(self.outdir(), CleanUpOld=False)
        CreateDirectory("%s/correlation_data/" % (self.outdir()), CleanUpOld=True)

        for M in self.__matrices:
            f_name = self.__getMatrixFileName(M)
            if len(f_name) > 0:
                print "<SciKitPreProcessing> INFO: Save matrix data to %s" % (f_name)
                M.write(f_name)
        self.clear()
        print "<SciKitPreProcessing> INFO: Save instance to %s/Covariance_Matrix.pkl" % (self.outdir())
        from sklearn.externals import joblib
        joblib.dump(self, "%s/Covariance_Matrix.pkl" % (self.outdir()))

    def clear(self):
        self.__TrainCovMatW = self.__TrainCovMat = self.__SigTrainCovMatW = self.__SigTrainCovMat = None
        self.__BkgTrainCovMatW = self.__BkgTrainCovMat = self.__TestCovMatW = self.__TestCovMat = None
        self.__SigTestCovMatW = self.__SigTestCovMat = self.__BkgTestCovMatW = self.__BkgTestCovMat = None
        self.__matrices = []
        ### delete all links to the dataset
        self.__dataset = None

    #----------------------------------------------------------------------------
    #### Getter methods for covariance matrices, mean values and eigen basis    -
    #----------------------------------------------------------------------------
    def getCovTraining(self):
        return self.__TrainCovMat.cov() if not self.__SwapTrainTest else self.__TestCovMat.cov()

    def getCovTesting(self):
        return self.__TrainCovMat.cov() if self.__SwapTrainTest else self.__TestCovMat.cov()

    def getWeightCovTraining(self):
        return self.__TrainCovMatW.cov() if not self.__SwapTrainTest else self.__TestCovMatW.cov()

    def getWeightCovTesting(self):
        return self.__TrainCovMatW.cov() if self.__SwapTrainTest else self.__TestCovMatW.cov()

    def getMeanTraining(self):
        return self.__TrainCovMat.mean() if not self.__SwapTrainTest else self.__TestCovMat.mean()

    def getMeanTesting(self):
        return self.__TrainCovMat.mean() if self.__SwapTrainTest else self.__TestCovMat.mean()

    def getWeightMeanTraining(self):
        return self.__TrainCovMatW.mean() if not self.__SwapTrainTest else self.__TestCovMatW.mean()

    def getWeightMeanTesting(self):
        return self.__TrainCovMatW.mean() if self.__SwapTrainTest else self.__TestCovMatW.mean()

    def getCovBasisTrain(self):
        return self.__TrainCovMat.eigenBasis() if not self.__SwapTrainTest else self.__TestCovMat.eigenBasis()

    def getCovBasisTest(self):
        return self.__TrainCovMat.eigenBasis() if self.__SwapTrainTest else self.__TestCovMat.eigenBasis()

    def getCovBasisTrainW(self):
        return self.__TrainCovMatW.eigenBasis() if not self.__SwapTrainTest else self.__TestCovMatW.eigenBasis()

    def getCovBasisTestW(self):
        return self.__TrainCovMatW.eigenBasis() if self.__SwapTrainTest else self.__TestCovMatW.eigenBasis()

    def getCovEigenValTrain(self):
        return self.__TrainCovMat.eigenValues() if not self.__SwapTrainTest else self.__TestCovMat.eigenValues()

    def getCovEigenValTest(self):
        return self.__TrainCovMat.eigenValues() if self.__SwapTrainTest else self.__TestCovMat.eigenValues()

    def getCovEigenValTrainW(self):
        return self.__TrainCovMatW.eigenValues() if not self.__SwapTrainTest else self.__TestCovMatW.eigenValues()

    def getCovEigenValTestW(self):
        return self.__TrainCovMatW.eigenValues() if self.__SwapTrainTest else self.__TestCovMatW.eigenValues()

    #----------------------------------------------------------------------------
    #### Getter methods for covariance matrices, mean values and eigen basis    -
    #### Background only.                                                       -
    #----------------------------------------------------------------------------
    def getBkgCovTraining(self):
        return self.__BkgTrainCovMat.cov() if not self.__SwapTrainTest else self.__BkgTestCovMat.cov()

    def getBkgCovTesting(self):
        return self.__BkgTrainCovMat.cov() if self.__SwapTrainTest else self.__BkgTestCovMat.cov()

    def getBkgWeightCovTraining(self):
        return self.__BkgTrainCovMatW.cov() if not self.__SwapTrainTest else self.__BkgTestCovMatW.cov()

    def getBkgWeightCovTesting(self):
        return self.__BkgTrainCovMatW.cov() if self.__SwapTrainTest else self.__BkgTestCovMatW.cov()

    def getBkgMeanTraining(self):
        return self.__BkgTrainCovMat.mean() if not self.__SwapTrainTest else self.__BkgTestCovMat.mean()

    def getBkgMeanTesting(self):
        return self.__BkgTrainCovMat.mean() if self.__SwapTrainTest else self.__BkgTestCovMat.mean()

    def getBkgWeightMeanTraining(self):
        return self.__BkgTrainCovMatW.mean() if not self.__SwapTrainTest else self.__BkgTestCovMatW.mean()

    def getBkgWeightMeanTesting(self):
        return self.__BkgTrainCovMatW.mean() if self.__SwapTrainTest else self.__BkgTestCovMatW.mean()

    def getBkgCovBasisTrain(self):
        return self.__BkgTrainCovMat.eigenBasis() if not self.__SwapTrainTest else self.__BkgTestCovMat.eigenBasis()

    def getBkgCovBasisTest(self):
        return self.__BkgTrainCovMat.eigenBasis() if self.__SwapTrainTest else self.__BkgTestCovMat.eigenBasis()

    def getBkgCovBasisTrainW(self):
        return self.__BkgTrainCovMatW.eigenBasis() if not self.__SwapTrainTest else self.__BkgTestCovMatW.eigenBasis()

    def getBkgCovBasisTestW(self):
        return self.__BkgTrainCovMatW.eigenBasis() if self.__SwapTrainTest else self.__BkgTestCovMatW.eigenBasis()

    def getBkgCovEigenValTrain(self):
        return self.__BkgTrainCovMat.eigenValues() if not self.__SwapTrainTest else self.__BkgTestCovMat.eigenValues()

    def getBkgCovEigenValTest(self):
        return self.__BkgTrainCovMat.eigenValues() if self.__SwapTrainTest else self.__BkgTestCovMat.eigenValues()

    def getBkgCovEigenValTrainW(self):
        return self.__BkgTrainCovMatW.eigenValues() if not self.__SwapTrainTest else self.__BkgTestCovMatW.eigenValues()

    def getBkgCovEigenValTestW(self):
        return self.__BkgTrainCovMatW.eigenValues() if self.__SwapTrainTest else self.__BkgTestCovMatW.eigenValues()

    #----------------------------------------------------------------------------
    #### Getter methods for covariance matrices, mean values and eigen basis    -
    #### Signal only.                                                           -
    #----------------------------------------------------------------------------
    def getSigCovTraining(self):
        return self.__SigTrainCovMat.cov() if not self.__SwapTrainTest else self.__SigTestCovMat.cov()

    def getSigCovTesting(self):
        return self.__SigTrainCovMat.cov() if self.__SwapTrainTest else self.__SigTestCovMat.cov()

    def getSigWeightCovTraining(self):
        return self.__SigTrainCovMatW.cov() if not self.__SwapTrainTest else self.__SigTestCovMatW.cov()

    def getSigWeightCovTesting(self):
        return self.__SigTrainCovMatW.cov() if self.__SwapTrainTest else self.__SigTestCovMatW.cov()

    def getSigMeanTraining(self):
        return self.__SigTrainCovMat.mean() if not self.__SwapTrainTest else self.__SigTestCovMat.mean()

    def getSigMeanTesting(self):
        return self.__SigTrainCovMat.mean() if self.__SwapTrainTest else self.__SigTestCovMat.mean()

    def getSigWeightMeanTraining(self):
        return self.__SigTrainCovMatW.mean() if not self.__SwapTrainTest else self.__SigTestCovMatW.mean()

    def getSigWeightMeanTesting(self):
        return self.__SigTrainCovMatW.mean() if self.__SwapTrainTest else self.__SigTestCovMatW.mean()

    def getSigCovBasisTrain(self):
        return self.__SigTrainCovMat.eigenBasis() if not self.__SwapTrainTest else self.__SigTestCovMat.eigenBasis()

    def getSigCovBasisTest(self):
        return self.__SigTrainCovMat.eigenBasis() if self.__SwapTrainTest else self.__SigTestCovMat.eigenBasis()

    def getSigCovBasisTrainW(self):
        return self.__SigTrainCovMatW.eigenBasis() if not self.__SwapTrainTest else self.__SigTestCovMatW.eigenBasis()

    def getSigCovBasisTestW(self):
        return self.__SigTrainCovMatW.eigenBasis() if self.__SwapTrainTest else self.__SigTestCovMatW.eigenBasis()

    def getSigCovEigenValTrain(self):
        return self.__SigTrainCovMat.eigenValues() if not self.__SwapTrainTest else self.__SigTestCovMat.eigenValues()

    def getSigCovEigenValTest(self):
        return self.__SigTrainCovMat.eigenValues() if self.__SwapTrainTest else self.__SigTestCovMat.eigenValues()

    def getSigCovEigenValTrainW(self):
        return self.__SigTrainCovMatW.eigenValues() if not self.__SwapTrainTest else self.__SigTestCovMatW.eigenValues()

    def getSigCovEigenValTestW(self):
        return self.__SigTrainCovMatW.eigenValues() if self.__SwapTrainTest else self.__SigTestCovMatW.eigenValues()
