#!/bin/env/python
from ClusterSubmission.Utils import FillWhiteSpaces
from sklearn.externals import joblib

from pprint import pprint
import numpy as np
import os


class SciKitReader(object):
    def __init__(self, train_path, train_name):
        self.__classifier = None
        file_to_load = "%s/%s.pkl" % (train_path, train_name)
        if os.path.isfile(file_to_load):
            print "\n\n" + FillWhiteSpaces(100, "*/")
            print "<SciKitReader> INFO: Load machine learning training stored at %s." % (file_to_load)
            print FillWhiteSpaces(100, "*/") + "\n\n"
            self.__classifier = joblib.load(file_to_load)
            self.__classifier.load()
        else:
            raise RuntimeError("<SciKitReader> ERROR: The training %s does not exist please double check" % (file_to_load))

    def train_obj(self):
        return self.__classifier

    def checkTrainVarNames(self, var_names):
        consistent = True
        print FillWhiteSpaces(100, "+-")
        print FillWhiteSpaces(100, "+-")
        print "<SciKitReader> INFO: Check consistency between the given variables names for evaluation and the ones used for training"
        print FillWhiteSpaces(100, "+-")
        print FillWhiteSpaces(100, "+-")
        if len(var_names) != self.__classifier.nVars():
            print "<SciKitReader> ERROR: The number of given training variables %d does not match the number used for machine learning training %d." % (
                len(var_names), self.__classifier.nVars())
            consistent = False
        for i, var in enumerate(var_names):
            if var != self.__classifier.variableName(i, get_original=True):
                print "<SciKitReader> ERROR: The %s-th given variable is %s while %s is expected" % (
                    i + 1, var, self.__classifier.variableName(i, get_original=True))
                consistent = False
        if not consistent: print FillWhiteSpaces(100, "+-")
        return consistent


class SciKitClassReader(SciKitReader):
    def __init__(self, train_path, train_name):
        SciKitReader.__init__(self, train_path, train_name)

    def checkConsistency(self, var_names):
        if not self.checkTrainVarNames(var_names): return False
        if not self.train_obj().isClassificationProblem():
            print "<SciKitClassReader> ERROR: The training %s is done for regression not for classification." % (self.name())
            return False
        return True

    def evaluateScore(self, in_vars):
        score = self.train_obj().predict(in_vars)
        return score


class SciKitRegressionReader(SciKitReader):
    def __init__(self, train_path, train_name):
        SciKitReader.__init__(self, train_path, train_name)

    def checkConsistency(self, var_names):
        if self.train_obj().isClassificationProblem():
            print "<SciKitRegressionReader> ERROR: The training %s has been made for classification instead of regression." % (
                self.train_obj().name())
            return False
        ### Check if the training names match with each other
        if not self.checkTrainVarNames(var_names): return False
        if self.train_obj().nTargets() == 0:
            print "<SciKitRegressionReader> ERROR: No target variables in the regression training %s" % (self.train_obj().name())
            return False
        return True

    def predict(self, in_vars):
        regression = self.train_obj().predict(in_vars)
        return regression

    def nTargets(self):
        return self.train_obj().nTargets()

    def targetVariables(self):
        return [self.train_obj().targetVariable(i) for i in range(self.nTargets())]

    def targetIdx(self, var):
        if var not in self.targetVariables():
            print "<SciKitRegressionReader> ERROR: The variable %s is not predicted in %s" % (var, self.train_obj().name())
            return -1
        return self.targetVariables().index(var)


if __name__ == '__main__':
    MyReader = SciKitReader("/afs/ipp-garching.mpg.de/home/n/niko/MachineLearning/source/SciKitLearnFiles/Trainings/", "BDT")
