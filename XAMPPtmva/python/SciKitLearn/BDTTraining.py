#!/bin/env/python
from XAMPPtmva.SciKitBDTClassifier import SciKitBDTClassifier, setupBDTTraining

if __name__ == '__main__':
    setupBDTTraining()
