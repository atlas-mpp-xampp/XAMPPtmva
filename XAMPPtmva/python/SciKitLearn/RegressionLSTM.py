#!/bin/env/python
from XAMPPtmva.KerasLSTMRegression import setupLSTMRegression

if __name__ == '__main__':
    setupLSTMRegression()
