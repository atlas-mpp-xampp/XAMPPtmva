#!/bin/env/python
from XAMPPtmva.SciKitBDTClassifier import setupGradientBDT

if __name__ == '__main__':
    setupGradientBDT()
