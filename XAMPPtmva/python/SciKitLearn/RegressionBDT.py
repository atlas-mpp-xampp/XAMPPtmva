#!/bin/env/python
from XAMPPtmva.BDTRegression import setupRegressionTree

if __name__ == '__main__':
    setupRegressionTree()
