#!/bin/env/python
from XAMPPtmva.SciKitClassifier import SciKitClassifier
from XAMPPtmva.SciKitDataSet import SciKitDataSet, SciKitDataLoader
from XAMPPtmva.SciKitDiagnostics import SciKitDiagnosticHisto, SciKitDiagnosticPlotGroup
from XAMPPtmva.LoadHDF5 import setupSciKitTrainingParser, runTraining
from ClusterSubmission.Utils import FillWhiteSpaces
from array import array
import numpy as np
from scipy.special import expit


#### Plot the distributions seen by each
#### Boosted decision tree to illustrate their
#### tweaking
class ScitKitBDTDiagnostics(SciKitDiagnosticPlotGroup):
    def __init__(self,
                 diagnostics=None,
                 group_name="",
                 histo_prefix="",
                 process_testing=False,
                 process_signal=False,
                 process_background=False,
                 do_transformation=False,
                 end=-1):
        ### Constructor of the threading class
        SciKitDiagnosticPlotGroup.__init__(self,
                                           diagnostics=diagnostics,
                                           group_name=group_name,
                                           histo_prefix=histo_prefix,
                                           process_testing=process_testing,
                                           process_signal=process_signal,
                                           process_background=process_background,
                                           do_transformation=do_transformation)

        self.__diagnostics = diagnostics
        self.__group_name = group_name
        self.__prefix = histo_prefix
        self.__histos = []

    def init(self):
        Top_Dir = self.__diagnostics.ROOTFile().GetDirectory("Diagnostics/ClassifierMonitoring")
        if not Top_Dir:
            self.__diagnostics.ROOTFile().mkdir("Diagnostics/ClassifierMonitoring")
            Top_Dir = self.__diagnostics.ROOTFile().GetDirectory("Diagnostics/ClassifierMonitoring")
        if not Top_Dir.GetDirectory(self.__group_name): Top_Dir.mkdir(self.__group_name)
        decorrelate = self.__diagnostics.model().do_transformation()
        for t in range(self.__diagnostics.model().num_trees()):
            Top_Dir.mkdir("%s/%d_th_tree" % (self.__group_name, t))
            Dir_ToParse = Top_Dir.GetDirectory("%s/%d_th_tree" % (self.__group_name, t))
            histo_set = []
            for i in range(self.__diagnostics.nVars()):
                var_name = self.__diagnostics.variableName(i, not decorrelate)
                bin_width = (self.__diagnostics.maximum(i, transformed_data=decorrelate) -
                             self.__diagnostics.minimum(i, transformed_data=decorrelate)) / self.__diagnostics.nBins()
                histo_set += [
                    SciKitDiagnosticHisto(name="%s%s" % (self.__prefix + ("_" if len(self.__prefix) > 0 else ""), var_name),
                                          axis_title=var_name,
                                          bins=self.__diagnostics.nBins() + 2,
                                          x_min=self.__diagnostics.minimum(i, transformed_data=decorrelate) - bin_width,
                                          x_max=self.__diagnostics.maximum(i, transformed_data=decorrelate) + bin_width,
                                          TH1_dir=Dir_ToParse)
                ]
            self.__histos += [histo_set]

    def fillChunk(self):
        weights = np.copy(self.current_weightBlock())
        data = self.__diagnostics.transform(self.current_dataBlock())
        #self.current_classIDBlock()
        tree_classID = np.zeros((self.__diagnostics.model().num_trees(), len(data)))
        ### Get the prediction of each tree
        for t in range(self.__diagnostics.model().num_trees()):
            tree_classID[t] = self.__diagnostics.model().get_tree(t).predict(data)

        for e in range(len(data)):
            for t in range(self.__diagnostics.model().num_trees()):
                for v in range(self.__diagnostics.nVars()):
                    self.__histos[t][v].fill(value=data[e][v], weight=weights[e])
                if tree_classID[t][e] != self.current_classIDBlock()[e] and self.__diagnostics.model().tree_weight(t) > 0:
                    weights[e] *= self.__diagnostics.model().tree_weight(t)

        return True

    def write(self):
        for grp in self.__histos:
            for h in grp:
                h.write()


###########################################################################################
#   Use derived classes from the SciKitClassifier for each training type.                 #
#   Through this additional training specific diagnostic plots can be written to the      #
#   diagnostics file                                                                      #
###########################################################################################
class SciKitBDTClassifier(SciKitClassifier):
    def __init__(self, Options):
        from sklearn.ensemble import AdaBoostClassifier
        from sklearn.tree import DecisionTreeClassifier
        #   For explanation of the parameters c.f.:
        #       http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.AdaBoostClassifier.html
        #       http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html
        #  Each binary tree classifies the data with -1 or 1 depending on whether it's background or not
        bdt = AdaBoostClassifier(base_estimator=DecisionTreeClassifier(
            max_depth=Options.maxDepth,
            min_weight_fraction_leaf=Options.minNodeSize,
            min_impurity_decrease=Options.minImpurityDecrease,
            criterion="gini" if not Options.useEntropySplit else "entropy",
            splitter="best" if not Options.useRandomSplit else "random",
            min_samples_split=Options.minSamplesSplit,
            min_samples_leaf=Options.minSamplesLeaf,
            max_features=Options.maxFeatures if Options.maxFeatures > 0 else None),
                                 algorithm="SAMME" if not Options.useRealBoostingAlg else "SAMME.R",
                                 learning_rate=Options.learningRate,
                                 n_estimators=Options.nEstimators)

        SciKitClassifier.__init__(self,
                                  model=bdt,
                                  HDF5File=Options.HDF5File,
                                  begin=Options.startEvent,
                                  end=Options.lastEvent,
                                  outdir=Options.outDir,
                                  preprocessing=Options.dataPreProcessing,
                                  name=Options.classifierName,
                                  useWeights=not Options.noWeights,
                                  SwapTrainTest=Options.swapTrainTesting,
                                  transform_data=Options.transform_data)

    def num_trees(self):
        return len(self.model().estimator_weights_)

    def get_tree(self, n):
        return self.model().estimators_[n]

    def tree_weight(self, n):
        return self.model().estimator_weights_[n]

    def runMVAdiagnosticsOnDS(self, diagnostic_instance=None):
        return []
        return [
            ScitKitBDTDiagnostics(diagnostics=diagnostic_instance,
                                  group_name="SigTraining",
                                  process_testing=False,
                                  process_signal=True,
                                  process_background=False),
            ScitKitBDTDiagnostics(diagnostics=diagnostic_instance,
                                  group_name="SigTesting",
                                  process_testing=True,
                                  process_signal=True,
                                  process_background=False),
            ScitKitBDTDiagnostics(diagnostics=diagnostic_instance,
                                  group_name="BkgTraining",
                                  process_testing=False,
                                  process_signal=False,
                                  process_background=True),
            ScitKitBDTDiagnostics(diagnostics=diagnostic_instance,
                                  group_name="BkgTesting",
                                  process_testing=True,
                                  process_signal=False,
                                  process_background=True),
        ]

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        import ROOT
        ### Always good to know the importance of the features for the training
        Importance_Histo = ROOT.TH1D("feature_importance", "Importance of the features evaluated", self.nVars(), 0, self.nVars())
        print FillWhiteSpaces(3, "\n") + FillWhiteSpaces(100, "#-")
        print "<SciKitBDTClassifier> INFO: Importance of the  training variables:"
        Var_Ranking = []
        m_len = 0
        if len(self.model().feature_importances_) == 0 or np.max(self.model().feature_importances_) == np.min(
                self.model().feature_importances_) or len([x for x in self.model().feature_importances_ if np.isnan(x) == True]) > 0:

            print "<SciKitBDTClassifier> WARNING: No sensible training could be obtained. Abort the loop"
            return False

        for v in range(self.nVars()):
            rank = self.model().feature_importances_[v]
            var_name = self.variableName(v, not self.do_transformation())
            if m_len < len(var_name): m_len = len(var_name)
            Var_Ranking += [(rank, var_name)]

            Importance_Histo.GetXaxis().SetBinLabel(v + 1, var_name)
            Importance_Histo.SetBinContent(v + 1, rank)

        m_len += 5
        i = 1
        for rank, var in sorted(Var_Ranking, key=lambda x: x[0], reverse=True):
            print "%2d)       -=-  %s:%s%.6f" % (i, var, FillWhiteSpaces(m_len - len(var)), rank)
            i += 1
        print FillWhiteSpaces(100, "-#") + FillWhiteSpaces(3, "\n")

        #### The BDT weights and the errors
        BDTTree = ROOT.TTree("BDTInformation", "BDT weights and its errors")
        ErrorHisto = ROOT.TH1D("ErrorFraction", "Error fraction against weight number", self.num_trees(), 0, self.num_trees())
        BDTHisto = ROOT.TH1D("BDTWeights", "Error fraction against weight number", self.num_trees(), 0, self.num_trees())

        b_BDTWeight = array("d", [0])
        b_ErrorFrac = array("d", [0])
        b_NthTree = array("i", [0])
        b_nNodes = array("i", [0])
        b_Feature_Ranking = array("d", [0 for i in range(self.nVars())])

        b_children_left = ROOT.std.vector(int)()
        b_children_right = ROOT.std.vector(int)()

        b_NodeFeatures = ROOT.std.vector(int)()
        b_Thresholds = ROOT.std.vector(float)()

        b_Depth = ROOT.std.vector(int)()
        b_IsLeave = ROOT.std.vector(bool)()

        BDTTree.Branch("TreeNumber", b_NthTree, "TreeNumber/i")
        BDTTree.Branch("BDTWeight", b_BDTWeight, "BDTWeight/D")
        BDTTree.Branch("ErrorFraction", b_ErrorFrac, "ErrorFraction/D")
        BDTTree.Branch("VariableRanking", b_Feature_Ranking, "VariableRanking[%d]/D" % (self.nVars()))

        BDTTree.Branch("UsedFeatures", b_NodeFeatures)
        BDTTree.Branch("Children_Left", b_children_left)
        BDTTree.Branch("Children_Right", b_children_right)
        BDTTree.Branch("Cuts", b_Thresholds)
        BDTTree.Branch("Depth", b_Depth)
        BDTTree.Branch("IsLeave", b_IsLeave)

        for i in range(self.num_trees()):
            b_NthTree[0] = i + 1
            b_BDTWeight[0] = self.model().estimator_weights_[i]
            b_ErrorFrac[0] = self.model().estimator_errors_[i]
            BDTHisto.SetBinContent(i + 1, b_BDTWeight[0])
            ErrorHisto.SetBinContent(i + 1, b_ErrorFrac[0])

            for v in range(self.nVars()):
                try:
                    b_Feature_Ranking[v] = self.model().estimators_[i].feature_importances_[v]
                except:
                    return False

            ### Extract the i-th estimator to save diagnostics about the tree structure
            estimator = self.model().estimators_[i]
            n_nodes = estimator.tree_.node_count

            children_left = estimator.tree_.children_left
            children_right = estimator.tree_.children_right
            feature = estimator.tree_.feature
            threshold = estimator.tree_.threshold

            b_nNodes[0] = n_nodes
            ### Clear the old vectors
            for B in [b_NodeFeatures, b_children_left, b_children_right, b_Thresholds, b_Depth, b_IsLeave]:
                B.clear()

            for c in children_left:
                b_children_left.push_back(c)
            for c in children_right:
                b_children_right.push_back(c)
            for f in feature:
                b_NodeFeatures.push_back(f)
            for t in threshold:
                b_Thresholds.push_back(t)

            # The tree structure can be traversed to compute various properties such
            # as the depth of each node and whether or not it is a leaf.
            ### This code snippet has been taken from
            ### http://scikit-learn.org/stable/auto_examples/tree/plot_unveil_tree_structure.html#sphx-glr-auto-examples-tree-plot-unveil-tree-structure-py
            node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
            is_leaves = np.zeros(shape=n_nodes, dtype=bool)
            stack = [(0, -1)]  # seed is the root node id and its parent depth
            while len(stack) > 0:
                node_id, parent_depth = stack.pop()
                node_depth[node_id] = parent_depth + 1
                if (children_left[node_id] != children_right[node_id]):
                    stack.append((children_left[node_id], parent_depth + 1))
                    stack.append((children_right[node_id], parent_depth + 1))
                else:
                    is_leaves[node_id] = True

            for d in node_depth:
                b_Depth.push_back(d)
            for b in is_leaves:
                b_IsLeave.push_back(b)
            print "Decision tree number %d:" % (i)
            for j in range(n_nodes):
                if is_leaves[j]:
                    print "%snode=%d leaf node." % (FillWhiteSpaces((node_depth[j] + 1) * 4), j)
                else:
                    print "%snode=%d test node: go to node %s if X[:, %s] <= %.2f else to node %d." % (FillWhiteSpaces(
                        (node_depth[j] + 1) * 4), j, children_left[j], self.variableName(
                            feature[j], not self.do_transformation()), threshold[j], children_right[j])
            if len([x for x in is_leaves if x == True]) == n_nodes:
                print "WARNING: This tree only contains leaves"
                return False
            print "\n\n"
            BDTTree.Fill()

        Diag_Dir.WriteObject(BDTTree, "BDTInformation")
        Diag_Dir.WriteObject(BDTHisto, "BoostWeights")
        Diag_Dir.WriteObject(ErrorHisto, "ErrorFraction")
        Diag_Dir.WriteObject(Importance_Histo, "VariableRanking")
        return True


#
#
#
#
class SciKitGradientBDTClassifier(SciKitClassifier):
    def __init__(self, Options):
        from sklearn.ensemble import GradientBoostingClassifier
        bdt = GradientBoostingClassifier(
            # loss='deviance', <-- logistic loss, exponential recovers AdaBoost
            learning_rate=Options.learningRate,
            n_estimators=Options.nEstimators,
            subsample=Options.subsamples,
            criterion="friedman_%s" % (Options.qualityMeasure),
            #min_samples_split=2,
            min_samples_leaf=Options.minSamplesLeaf,
            min_weight_fraction_leaf=Options.minWeightFraction,
            max_depth=Options.maxDepth,
            #               min_impurity_decrease=0.0,
            #               min_impurity_split=None,
            #               init=None,
            #               random_state=None,
            #               max_features=None,
            #               verbose=0,
            #               max_leaf_nodes=None,
            warm_start=False,
            #               presort='auto',
            validation_fraction=Options.validationFraction,
            tol=Options.maxTolerance)
        SciKitClassifier.__init__(self,
                                  model=bdt,
                                  HDF5File=Options.HDF5File,
                                  begin=Options.startEvent,
                                  end=Options.lastEvent,
                                  outdir=Options.outDir,
                                  preprocessing=Options.dataPreProcessing,
                                  name=Options.classifierName,
                                  useWeights=not Options.noWeights,
                                  SwapTrainTest=Options.swapTrainTesting,
                                  transform_data=Options.transform_data,
                                  isClassification=True)

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        import ROOT
        ### Always good to know the importance of the features for the training
        Importance_Histo = ROOT.TH1D("feature_importance", "Importance of the features evaluated", self.nVars(), 0, self.nVars())
        print FillWhiteSpaces(3, "\n") + FillWhiteSpaces(100, "#-")
        print "<SciKitGradientBDTClassifier> INFO: Importance of the  training variables:"
        Var_Ranking = []

        m_len = 0
        ### Check if the features are - or fully equal or something else
        if len(self.model().feature_importances_) == 0 or np.max(self.model().feature_importances_) == np.min(
                self.model().feature_importances_) or len([x for x in self.model().feature_importances_ if np.isnan(x) == True]) > 0:
            print "<SciKitGradientBDTClassifier> WARNING: No sensible training could be obtained. Abort the loop"
            return False
        #### Save the importance in the histogram
        for v in range(self.nVars()):
            rank = self.model().feature_importances_[v]
            var_name = self.variableName(v, not self.do_transformation())
            if m_len < len(var_name): m_len = len(var_name)
            Var_Ranking += [(rank, var_name)]

            Importance_Histo.GetXaxis().SetBinLabel(v + 1, var_name)
            Importance_Histo.SetBinContent(v + 1, rank)

        #### Print the features sorted to the screen
        m_len += 5
        i = 1
        for rank, var in sorted(Var_Ranking, key=lambda x: x[0], reverse=True):
            print "%2d)       -=-  %s:%s%.6f" % (i, var, FillWhiteSpaces(m_len - len(var)), rank)
            i += 1
        print FillWhiteSpaces(100, "-#") + FillWhiteSpaces(3, "\n")

        Diag_Dir.WriteObject(Importance_Histo, "VariableRanking")
        return True

    def num_trees(self):
        return len(self.model().estimators_)

    def _call_mva(self, data):
        ##### The expit function is defined as f(x) = 1 / (1 + exp(-x))
        ### return expit( self.model().decision_function(data))
        return np.arctan(self.model().decision_function(data))


#    def runMVAdiagnosticsOnDS(self, diagnostic_instance=None):
#        return []


def setupBDTParser():
    parser = setupSciKitTrainingParser()
    parser.set_defaults(classifierName="BDT")
    ### The training parameters are not optimized yet. They are randomly choosen
    ### Each user must find his best configuration of parameters. The SciKitDiagnositcs class
    ### Is a good starting point to find them out
    parser.add_argument("--nEstimators", help="Maximum number of estimators at which boosting is terminated", type=int, default=200)
    parser.add_argument("--minSamplesSplit",
                        help="The minimum number of samples required to split an internal node",
                        type=float,
                        default=0.02)
    parser.add_argument("--minSamplesLeaf", help="The minimum number of samples required to be at a leaf node", type=float, default=0.01)
    parser.add_argument("--maxFeatures", help="The number of features to consider when looking for the best split", type=int, default=-1)
    parser.add_argument("--maxDepth", help="How many nodes should be created at each tree", type=int, default=3)
    parser.add_argument("--minNodeSize", help="What is the minimal amount of events needed to build a node", type=float, default=0.03)
    parser.add_argument("--minImpurityDecrease",
                        help="A node will be split if this split induces a decrease of the impurity greater than or equal to this value.",
                        type=float,
                        default=0.0)
    parser.add_argument("--learningRate", help="Learning rate of the AdaBoost classifier", type=float, default=1.0)
    parser.add_argument("--useEntropySplit",
                        help="Use the 'entropy' algorithm as criterion of the best split instead of gini in the TreeClassisfier",
                        action='store_true',
                        default=False)
    parser.add_argument("--useRandomSplit",
                        help="Use a random feature to find the best criterion on the classifier in the TreeClassisfier",
                        action='store_true',
                        default=False)
    parser.add_argument("--useRealBoostingAlg",
                        help="Use the 'SAMME' algorithm in the AdaBoostClassfier else 'SAMME.R'",
                        action='store_true',
                        default=False)
    parser.add_argument("--noWeights", help="Disable event weights during classification", default=False, action="store_true")
    return parser


def setupGradientBoostParser():
    parser = setupSciKitTrainingParser()
    parser.set_defaults(classifierName="GradientBDT")
    parser.add_argument("--nEstimators", help="Maximum number of estimators at which boosting is terminated", type=int, default=250)

    parser.add_argument("--learningRate", help="Learning rate in each boosting step", type=float, default=0.75)
    parser.add_argument("--subsamples",
                        help="Fraction of the datasets used in each tree. WARNING: Might lead to overtraining",
                        type=float,
                        default=1.)

    parser.add_argument("--qualityMeasure",
                        help="Measure the quality of the split. Either mse (mean-suared-error) or mae (mean-absolute-error) are available",
                        default="mse",
                        choices=["mse", "mae"])
    parser.add_argument("--minSamplesLeaf", help="The minimum number of samples required to be at a leaf node", type=int, default=100)
    parser.add_argument("--minWeightFraction", help="Minimum fraction of weights in each leaf", type=float, default=0.0105)

    parser.add_argument("--maxDepth", help="How many nodes should be created at each tree", type=int, default=3)
    parser.add_argument("--validationFraction", help="Fraction of the dataset to be used for validation", type=float, default=0.1)
    parser.add_argument("--noWeights", help="Disable event weights during classification", default=False, action="store_true")
    parser.add_argument("--maxTolerance",
                        help="Loss must improve at least by this threshold. Otherwise the growing of the forest is stoped",
                        type=float,
                        default=0.0001)
    return parser


def setupBDTTraining():
    Options = setupBDTParser().parse_args()
    runTraining(Options=Options, classifier=SciKitBDTClassifier(Options))


def setupGradientBDT():
    Options = setupGradientBoostParser().parse_args()
    runTraining(Options=Options, classifier=SciKitGradientBDTClassifier(Options))
