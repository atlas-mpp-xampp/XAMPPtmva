#!/bin/env/python
from XAMPPtmva.SciKitDataSet import SciKitDataSet
from XAMPPtmva.SciKitClassifier import SciKitClassifier
from XAMPPtmva.LoadHDF5 import setupSciKitTrainingParser, runTraining

from ClusterSubmission.Utils import FillWhiteSpaces
from array import array
import numpy as np

###########################################################################################
#   Use derived classes from the SciKitClassifier for each training type.                 #
#   Through this additional training specific diagnostic plots can be written to the      #
#   diagnostics file                                                                      #
###########################################################################################
# http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html


class SciKitNNClassifier(SciKitClassifier):
    def __init__(self, Options):
        from sklearn.neural_network import MLPClassifier
        class_object = MLPClassifier(hidden_layer_sizes=tuple(Options.hiddenLayerSizes),
                                     activation=Options.activation,
                                     solver=Options.solver,
                                     alpha=Options.alpha,
                                     batch_size=("auto" if Options.batchSize == -1 else Options.batchSize),
                                     learning_rate=Options.learningRate,
                                     learning_rate_init=Options.learningRateInit,
                                     power_t=Options.powerT,
                                     max_iter=Options.maxIter,
                                     shuffle=(not Options.noShuffle),
                                     tol=Options.tol,
                                     warm_start=Options.warmStart,
                                     momentum=Options.momentum,
                                     nesterovs_momentum=(not Options.noNesterovsMomentum),
                                     early_stopping=Options.earlyStopping,
                                     validation_fraction=Options.validationFraction,
                                     beta_1=Options.beta1,
                                     beta_2=Options.beta2,
                                     epsilon=Options.epsilon,
                                     verbose=True)

        SciKitClassifier.__init__(self,
                                  model=class_object,
                                  HDF5File=Options.HDF5File,
                                  begin=Options.startEvent,
                                  end=Options.lastEvent,
                                  outdir=Options.outDir,
                                  preprocessing=Options.dataPreProcessing,
                                  name=Options.classifierName,
                                  useWeights=False,
                                  SwapTrainTest=Options.swapTrainTesting,
                                  transform_data=Options.transform_data,
                                  isClassification=True)

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        import ROOT
        ### Always good to know the importance of the features for the training
        Importance_Histo = ROOT.TH1D("feature_importance", "Importance of the features evaluated", self.nVars(), 0, self.nVars())
        print FillWhiteSpaces(3, "\n") + FillWhiteSpaces(100, "#-")
        print "<SciKitNNClassifier> INFO: Importance of the training variables evaluated as the sum of weights^{2} from each input node"
        Var_Ranking = []
        m_len = 0

        for v in range(self.nVars()):
            rank = np.sum(
                np.square(self.model().coefs_[0][v])
            )  #* np.square( (self.decorrelator().getWeightCovTraining() if self.useWeights() else self.decorrelator().getCovTraining())[v][v])
            var_name = self.variableName(v, not self.do_transformation())
            if m_len < len(var_name): m_len = len(var_name)
            Var_Ranking += [(rank, var_name)]
            Importance_Histo.GetXaxis().SetBinLabel(v + 1, var_name)
            Importance_Histo.SetBinContent(v + 1, rank)

        m_len += 5
        i = 1
        for rank, var in sorted(Var_Ranking, key=lambda x: x[0], reverse=True):
            print "%2d)       -=-  %s:%s%.3f" % (i, var, FillWhiteSpaces(m_len - len(var)), rank)
            i += 1
        print FillWhiteSpaces(100, "-#") + FillWhiteSpaces(3, "\n")

        print "<SciKitNNClassifier> INFO: Fill the weight matrix of each Layer\n\n"
        WeightTree = ROOT.TTree("NeuronalNet", "Weights of the MLP matrix")
        b_Layer = array("i", [0])
        b_From = array("i", [0])
        b_To = array("i", [0])
        b_Weight = array("d", [0])

        WeightTree.Branch("Layer", b_Layer, "Layer/i")
        WeightTree.Branch("NodeFrom", b_From, "NodeFrom/I")
        WeightTree.Branch("NodeTo", b_To, "NodeTo/I")

        WeightTree.Branch("Weight", b_Weight, "Weight/D")
        #### Weights from the input
        for l in range(len(self.model().coefs_)):
            b_Layer[0] = l
            for f in range(len(self.model().coefs_[l])):
                b_From[0] = f
                for t in range(len(self.model().coefs_[l][f])):
                    b_To[0] = t
                    b_Weight[0] = self.model().coefs_[l][f][t]
                    WeightTree.Fill()
        ### Weights from the bias nodes
        for l in range(len(self.model().intercepts_)):
            b_Layer[0] = l + 1
            b_From[0] = -1
            for t in range(len(self.model().intercepts_[l])):
                b_To[0] = t
                b_Weight[0] = self.model().intercepts_[l][t]
                WeightTree.Fill()
        ### The loss curve can be monitored if the statstical minimization algorthims are used
        ### it shows
        if hasattr(self.model(), "loss_curve_"):
            print "<SciKitNNClassifier> INFO: Fill the loss curve"
            Loss_Histo = ROOT.TH1D("LossCurve", "Loss curve for each iteration", len(self.model().loss_curve_), 0,
                                   len(self.model().loss_curve_))
            Loss_Histo.GetXaxis().SetTitle("# iteration")
            Loss_Histo.GetYaxis().SetTitle("Loss value")

            for i, loss in enumerate(self.model().loss_curve_):
                Loss_Histo.SetBinContent(i + 1, loss)
            Diag_Dir.WriteObject(Loss_Histo, "ELossCurve")

        print "<SciKiNNClassifier> INFO: Save the training settings"
        Diag_Dir.WriteObject(WeightTree, "NeuronalNet")
        Diag_Dir.WriteObject(Importance_Histo, "VariableRanking")
        return True

    def _call_mva(self, data):
        return self.model().predict_proba(data)[:, 1]

    def _extra_meta_data(self):
        meta_dict = {}
        for param in sorted(self.model().get_params().iterkeys()):
            meta_dict[param] = self.model().get_params()[param]

        meta_dict["iterations"] = self.model().n_iter_
        meta_dict["final_loss"] = self.model().loss_
        return meta_dict


def setupNNParser():
    parser = setupSciKitTrainingParser()
    parser.set_defaults(classifierName="NN-MLP")
    parser.set_defaults(transform_data="normMean")
    ### The training parameters are not optimized yet. They are randomly choosen
    ### Each user must find his best configuration of parameters. The SciKitDiagnositcs class
    ### Is a good starting point to find them out
    parser.add_argument('--hiddenLayerSizes',
                        help='Hidden layer sizes, The ith element represents the number of neurons in the ith hidden layer',
                        default=[100],
                        nargs='+',
                        type=int)
    parser.add_argument('--activation',
                        help='Activation function for the hidden layer',
                        type=str,
                        choices=["identity", "logistic", "tanh", "relu"],
                        default="relu")
    parser.add_argument('--solver', help='The solver for weight optimization', type=str, choices=['lbfgs', 'sgd', 'adam'], default='adam')
    parser.add_argument('--alpha', help='L2 penalty (regularization term) parameter', default=0.0001, type=float)
    parser.add_argument(
        "--batchSize",
        help=
        'Size of minibatches for stochastic optimizers. If the solver is "lbfgs", the classifier will not use minibatch. When set to "auto" (-1), batch_size=min(200, n_samples)',
        type=int,
        default=-1)
    parser.add_argument('--learningRate',
                        help='Learning rate schedule for weight updates',
                        type=str,
                        choices=["constant", "invscaling", "adaptive"],
                        default='constant')
    parser.add_argument(
        '--learningRateInit',
        help='The initial learning rate used. It controls the step-size in updating the weights. Only used when solver="sgd" or "adam"',
        default=0.001,
        type=float)
    parser.add_argument(
        '--powerT',
        help=
        'The exponent for inverse scaling learning rate. It is used in updating effective learning rate when the learning_rate is set to "invscaling". Only used when solver="sgd".',
        default=0.5,
        type=float)
    parser.add_argument(
        "--maxIter",
        help=
        'Maximum number of iterations. The solver iterates until convergence (determined by "tol") or this number of iterations. For stochastic solvers ("sgd", "adam"), note that this determines the number of epochs (how many times each data point will be used), not the number of gradient steps.',
        type=int,
        default=200)
    parser.add_argument("--noShuffle",
                        help='Whether to shuffle samples in each iteration. Only used when solver="sgd" or "adam"',
                        default=False,
                        action="store_true")
    parser.add_argument(
        '--tol',
        help=
        'Tolerance for the optimization. When the loss or score is not improving by at least tol for two consecutive iterations, unless learning_rate is set to "adaptive", convergence is considered to be reached and training stops.',
        default=0.0001,
        type=float)
    parser.add_argument(
        "--warmStart",
        help=
        'When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution.',
        default=False,
        action="store_true")
    parser.add_argument('--momentum',
                        help='Momentum for gradient descent update. Should be between 0 and 1. Only used when solver="sgd".',
                        default=0.9,
                        type=float)
    parser.add_argument('--noNesterovsMomentum',
                        help='Whether to use Nesterovs momentum. Only used when solver="sgd" and momentum > 0',
                        default=False,
                        action="store_true")
    parser.add_argument(
        '--earlyStopping',
        help=
        'Whether to use early stopping to terminate training when validation score is not improving. If set to true, it will automatically set aside 10%% of training data as validation and terminate training when validation score is not improving by at least tol for two consecutive epochs. Only effective when solver="sgd" or "adam"',
        default=False,
        action="store_false")
    parser.add_argument(
        '--validationFraction',
        help=
        'The proportion of training data to set aside as validation set for early stopping. Must be between 0 and 1. Only used if early_stopping is True',
        default=0.1,
        type=float)
    parser.add_argument(
        '--beta1',
        help='Exponential decay rate for estimates of first moment vector in adam, should be in [0, 1). Only used when solver="adam"',
        default=0.9,
        type=float)
    parser.add_argument(
        '--beta2',
        help='Exponential decay rate for estimates of second moment vector in adam, should be in [0, 1). Only used when solver="adam"',
        default=0.999,
        type=float)
    parser.add_argument('--epsilon', help='Value for numerical stability in adam. Only used when solver="adam"', default=1e-8, type=float)
    return parser


def setupNNTraining():
    Options = setupNNParser().parse_args()
    runTraining(Options=Options, classifier=SciKitNNClassifier(Options))
