#!/bin/env/python
from XAMPPtmva.SciKitClassifier import SciKitClassifier
from XAMPPtmva.SciKitDataSet import SciKitDataSet, SciKitDataLoader
from XAMPPtmva.SciKitDiagnostics import SciKitDiagnosticHisto, SciKitDiagnosticPlotGroup
from XAMPPtmva.LoadHDF5 import setupSciKitTrainingParser, runTraining
from XAMPPtmva.SciKitBDTClassifier import ScitKitBDTDiagnostics
from ClusterSubmission.Utils import FillWhiteSpaces
from array import array
import numpy as np
from scipy.special import expit

###########################################################################################
#   Use derived classes from the SciKitClassifier for each training type.                 #
#   Through this additional training specific diagnostic plots can be written to the      #
#   diagnostics file                                                                      #
###########################################################################################


class SciKitRandomForestClassifier(SciKitClassifier):
    def __init__(self, Options):
        from sklearn.ensemble import RandomForestClassifier
        #   For explanation of the parameters c.f.:
        #       https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
        #       http://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html
        #  Each binary tree classifies the data with -1 or 1 depending on whether it's background or not
        random_forest = RandomForestClassifier(
            n_estimators=Options.numTrees,
            criterion='gini' if not Options.useEntropySplit else 'entropy',
            min_samples_split=2,
            min_weight_fraction_leaf=Options.minNodeSize,
            max_features='auto',
            max_leaf_nodes=Options.maxLeafNodes,
            min_impurity_decrease=Options.minImpurityDecrease,
            bootstrap=Options.bootstrap,
            oob_score=Options.oobScore,
            ### This flag might need some fine tuning
            n_jobs=-1,
            verbose=1)

        SciKitClassifier.__init__(self,
                                  model=random_forest,
                                  HDF5File=Options.HDF5File,
                                  begin=Options.startEvent,
                                  end=Options.lastEvent,
                                  outdir=Options.outDir,
                                  preprocessing=Options.dataPreProcessing,
                                  name=Options.classifierName,
                                  useWeights=not Options.noWeights,
                                  SwapTrainTest=Options.swapTrainTesting,
                                  transform_data=Options.transform_data)

    def _call_mva(self, data):
        return self.model().predict_log_proba(data)[:, 1]

    def num_trees(self):
        return len(self.model().estimators_)

    def get_tree(self, n):
        return self.model().estimators_[n]

    def tree_weight(self, n):
        return 1.

    def runMVAdiagnosticsOnDS(self, diagnostic_instance=None):
        return []
        return [
            ScitKitBDTDiagnostics(diagnostics=diagnostic_instance,
                                  group_name="SigTraining",
                                  process_testing=False,
                                  process_signal=True,
                                  process_background=False),
            ScitKitBDTDiagnostics(diagnostics=diagnostic_instance,
                                  group_name="SigTesting",
                                  process_testing=True,
                                  process_signal=True,
                                  process_background=False),
            ScitKitBDTDiagnostics(diagnostics=diagnostic_instance,
                                  group_name="BkgTraining",
                                  process_testing=False,
                                  process_signal=False,
                                  process_background=True),
            ScitKitBDTDiagnostics(diagnostics=diagnostic_instance,
                                  group_name="BkgTesting",
                                  process_testing=True,
                                  process_signal=False,
                                  process_background=True),
        ]

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        import ROOT
        #for d in dir(self.model()):
        #    print d
        #exit(1)
        ### Always good to know the importance of the features for the training
        Importance_Histo = ROOT.TH1D("feature_importance", "Importance of the features evaluated", self.nVars(), 0, self.nVars())
        print FillWhiteSpaces(3, "\n") + FillWhiteSpaces(100, "#-")
        print "<ScitKitRandomForestClassifier> INFO: Importance of the  training variables:"
        Var_Ranking = []
        m_len = 0
        if len(self.model().feature_importances_) == 0 or np.max(self.model().feature_importances_) == np.min(
                self.model().feature_importances_) or len([x for x in self.model().feature_importances_ if np.isnan(x) == True]) > 0:

            print "<ScitKitRandomForestClassifier> WARNING: No sensible training could be obtained. Abort the loop"
            return False

        for v in range(self.nVars()):
            rank = self.model().feature_importances_[v]
            var_name = self.variableName(v, not self.do_transformation())
            if m_len < len(var_name): m_len = len(var_name)
            Var_Ranking += [(rank, var_name)]

            Importance_Histo.GetXaxis().SetBinLabel(v + 1, var_name)
            Importance_Histo.SetBinContent(v + 1, rank)

        m_len += 5
        i = 1
        for rank, var in sorted(Var_Ranking, key=lambda x: x[0], reverse=True):
            print "%2d)       -=-  %s:%s%.6f" % (i, var, FillWhiteSpaces(m_len - len(var)), rank)
            i += 1
        print FillWhiteSpaces(100, "-#") + FillWhiteSpaces(3, "\n")

        #### The BDT weights and the errors
        BDTTree = ROOT.TTree("BDTInformation", "BDT weights and its errors")
        ErrorHisto = ROOT.TH1D("ErrorFraction", "Error fraction against weight number", self.num_trees(), 0, self.num_trees())
        BDTHisto = ROOT.TH1D("BDTWeights", "Error fraction against weight number", self.num_trees(), 0, self.num_trees())

        b_BDTWeight = array("d", [0])
        b_ErrorFrac = array("d", [0])
        b_NthTree = array("i", [0])
        b_nNodes = array("i", [0])
        b_Feature_Ranking = array("d", [0 for i in range(self.nVars())])

        b_children_left = ROOT.std.vector(int)()
        b_children_right = ROOT.std.vector(int)()

        b_NodeFeatures = ROOT.std.vector(int)()
        b_Thresholds = ROOT.std.vector(float)()

        b_Depth = ROOT.std.vector(int)()
        b_IsLeave = ROOT.std.vector(bool)()

        BDTTree.Branch("TreeNumber", b_NthTree, "TreeNumber/i")
        BDTTree.Branch("BDTWeight", b_BDTWeight, "BDTWeight/D")
        BDTTree.Branch("ErrorFraction", b_ErrorFrac, "ErrorFraction/D")
        BDTTree.Branch("VariableRanking", b_Feature_Ranking, "VariableRanking[%d]/D" % (self.nVars()))

        BDTTree.Branch("UsedFeatures", b_NodeFeatures)
        BDTTree.Branch("Children_Left", b_children_left)
        BDTTree.Branch("Children_Right", b_children_right)
        BDTTree.Branch("Cuts", b_Thresholds)
        BDTTree.Branch("Depth", b_Depth)
        BDTTree.Branch("IsLeave", b_IsLeave)

        for i in range(self.num_trees()):
            b_NthTree[0] = i + 1
            #b_BDTWeight[0] = self.model().estimator_weights_[i]
            #b_ErrorFrac[0] = self.model().estimator_errors_[i]
            #BDTHisto.SetBinContent(i + 1, b_BDTWeight[0])
            #ErrorHisto.SetBinContent(i + 1, b_ErrorFrac[0])

            for v in range(self.nVars()):
                try:
                    b_Feature_Ranking[v] = self.model().estimators_[i].feature_importances_[v]
                except:
                    return False

            ### Extract the i-th estimator to save diagnostics about the tree structure
            estimator = self.model().estimators_[i]
            n_nodes = estimator.tree_.node_count

            children_left = estimator.tree_.children_left
            children_right = estimator.tree_.children_right
            feature = estimator.tree_.feature
            threshold = estimator.tree_.threshold

            b_nNodes[0] = n_nodes
            ### Clear the old vectors
            for B in [b_NodeFeatures, b_children_left, b_children_right, b_Thresholds, b_Depth, b_IsLeave]:
                B.clear()

            for c in children_left:
                b_children_left.push_back(c)
            for c in children_right:
                b_children_right.push_back(c)
            for f in feature:
                b_NodeFeatures.push_back(f)
            for t in threshold:
                b_Thresholds.push_back(t)

            # The tree structure can be traversed to compute various properties such
            # as the depth of each node and whether or not it is a leaf.
            ### This code snippet has been taken from
            ### http://scikit-learn.org/stable/auto_examples/tree/plot_unveil_tree_structure.html#sphx-glr-auto-examples-tree-plot-unveil-tree-structure-py
            node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
            is_leaves = np.zeros(shape=n_nodes, dtype=bool)
            stack = [(0, -1)]  # seed is the root node id and its parent depth
            while len(stack) > 0:
                node_id, parent_depth = stack.pop()
                node_depth[node_id] = parent_depth + 1
                if (children_left[node_id] != children_right[node_id]):
                    stack.append((children_left[node_id], parent_depth + 1))
                    stack.append((children_right[node_id], parent_depth + 1))
                else:
                    is_leaves[node_id] = True

            for d in node_depth:
                b_Depth.push_back(d)
            for b in is_leaves:
                b_IsLeave.push_back(b)
            print "Decision tree number %d:" % (i)
            for j in range(n_nodes):
                if is_leaves[j]:
                    print "%snode=%d leaf node." % (FillWhiteSpaces((node_depth[j] + 1) * 4), j)
                else:
                    print "%snode=%d test node: go to node %s if X[:, %s] <= %.2f else to node %d." % (FillWhiteSpaces(
                        (node_depth[j] + 1) * 4), j, children_left[j], self.variableName(
                            feature[j], not self.do_transformation()), threshold[j], children_right[j])
            if len([x for x in is_leaves if x == True]) == n_nodes:
                print "WARNING: This tree only contains leaves"
                return False
            print "\n\n"
            BDTTree.Fill()

        Diag_Dir.WriteObject(BDTTree, "BDTInformation")
        Diag_Dir.WriteObject(BDTHisto, "BoostWeights")
        Diag_Dir.WriteObject(ErrorHisto, "ErrorFraction")
        Diag_Dir.WriteObject(Importance_Histo, "VariableRanking")
        return True


#
#
#
#
def setupForestParser():
    parser = setupSciKitTrainingParser()
    parser.set_defaults(classifierName="RandomForest")
    ### The training parameters are not optimized yet. They are randomly choosen
    ### Each user must find his best configuration of parameters. The SciKitDiagnositcs class
    ### Is a good starting point to find them out
    parser.add_argument("--oobScore",
                        help="Whether to use out-of-bag samples to estimate the generalization accuracy",
                        default=False,
                        action='store_true')
    parser.add_argument("--bootstrap", help="Whether bootstrap samples are used when building trees", default=False, action='store_true')
    parser.add_argument("--minImpurityDecrease",
                        help="A node will be split if this split induces a decrease of the impurity greater than or equal to this value",
                        type=float,
                        default=0.)
    parser.add_argument("--numTrees", help="The number of trees in the forest.", type=int, default=1000)
    parser.add_argument("--maxLeafNodes", help="How many nodes should be created at each tree", type=int, default=3)
    parser.add_argument(
        "--minNodeSize",
        help="The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node",
        type=float,
        default=0.03)
    parser.add_argument("--useEntropySplit",
                        help="Use the 'entropy' algorithm as criterion of the best split instead of gini in the TreeClassisfier",
                        action='store_true',
                        default=False)
    parser.add_argument("--noWeights", help="Disable event weights during classification", default=False, action="store_true")
    return parser


def setupForestTraining():
    Options = setupForestParser().parse_args()
    runTraining(Options=Options, classifier=SciKitRandomForestClassifier(Options))
