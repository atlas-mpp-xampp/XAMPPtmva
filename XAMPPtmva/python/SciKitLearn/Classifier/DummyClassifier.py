#!/bin/env/python
from XAMPPtmva.SciKitClassifier import SciKitClassifier
from XAMPPtmva.SciKitDataSet import SciKitDataSet, SciKitDataLoader
from XAMPPtmva.SciKitDiagnostics import SciKitDiagnosticHisto, SciKitDiagnosticPlotGroup
from XAMPPtmva.LoadHDF5 import setupSciKitTrainingParser, runTraining
from ClusterSubmission.Utils import FillWhiteSpaces
from array import array
import numpy as np
from scipy.special import expit


###########################################################################################
#                                                                                         #
#   The SciKitDummyClassifier serves only the purpose to run the SciKit diagnostic plots  #
#   on the training data to evalute quickly the separation power of each variable         #
#                                                                                         #
###########################################################################################
class SciKitDummyClassifier(SciKitClassifier):
    def __init__(self, Options):
        SciKitClassifier.__init__(
            self,
            model=None,
            HDF5File=Options.HDF5File,
            begin=Options.startEvent,
            end=Options.lastEvent,
            outdir=Options.outDir,
            preprocessing=Options.dataPreProcessing,
            name=Options.classifierName,
            useWeights=True,  #not Options.noWeights,
            SwapTrainTest=Options.swapTrainTesting,
            transform_data=Options.transform_data)

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        import ROOT
        return True

    ### Make sure that the model is always trained
    def isTrained(self):
        return True

    ### Also nothing must be saved
    def save(self):
        return

    def _call_mva(self, data):
        return np.random.random_sample(data.shape[0])
