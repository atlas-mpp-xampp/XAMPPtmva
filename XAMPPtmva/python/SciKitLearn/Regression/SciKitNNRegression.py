#!/bin/env/python
from XAMPPtmva.SciKitClassifier import SciKitClassifier
from XAMPPtmva.LoadHDF5 import runTraining
from XAMPPtmva.SciKitNNClassifier import setupNNParser

###########################################################################################
#   Use derived classes from the SciKitClassifier for each training type.                 #
#   Through this additional training specific diagnostic plots can be written to the      #
#   diagnostics file                                                                      #
###########################################################################################
# http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
from sklearn.neural_network import MLPRegressor
from random import random


class SciKitNNRegression(SciKitClassifier):
    def __init__(self, Options):
        regression_obj = MLPRegressor(hidden_layer_sizes=tuple(Options.hiddenLayerSizes),
                                      activation=Options.activation,
                                      solver=Options.solver,
                                      alpha=Options.alpha,
                                      batch_size=("auto" if Options.batchSize == -1 else Options.batchSize),
                                      learning_rate=Options.learningRate,
                                      learning_rate_init=Options.learningRateInit,
                                      power_t=Options.powerT,
                                      max_iter=Options.maxIter,
                                      shuffle=(not Options.noShuffle),
                                      tol=Options.tol,
                                      warm_start=Options.warmStart,
                                      momentum=Options.momentum,
                                      nesterovs_momentum=(not Options.noNesterovsMomentum),
                                      early_stopping=Options.earlyStopping,
                                      validation_fraction=Options.validationFraction,
                                      beta_1=Options.beta1,
                                      beta_2=Options.beta2,
                                      epsilon=Options.epsilon,
                                      verbose=True)

        SciKitClassifier.__init__(self,
                                  model=regression_obj,
                                  HDF5File=Options.HDF5File,
                                  begin=Options.startEvent,
                                  end=Options.lastEvent,
                                  outdir=Options.outDir,
                                  preprocessing=Options.dataPreProcessing,
                                  name=Options.classifierName,
                                  useWeights=False,
                                  SwapTrainTest=Options.swapTrainTesting,
                                  transform_data=Options.transform_data,
                                  isClassification=False)

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        return True

    def _call_mva(self, data):
        return self.model().predict(data)


def setupNNRegression():
    argparser = setupNNParser()
    argparser.set_defaults(activation="relu")
    argparser.set_defaults(classifierName="MLP_diTau")
    argparser.set_defaults(hiddenLayerSizes=[14 for i in range(5)])
    argparser.set_defaults(tol=1.e-7)
    Options = argparser.parse_args()
    runTraining(Options=Options, classifier=SciKitNNRegression(Options))
