#!/bin/env/python
from XAMPPtmva.SciKitClassifier import KerasModelBuilder
from XAMPPtmva.LoadHDF5 import setupSciKitTrainingParser, runTraining

###########################################################################################
#   Use derived classes from the SciKitClassifier for each training type.                 #
#   Through this additional training specific diagnostic plots can be written to the      #
#   diagnostics file                                                                      #
###########################################################################################


class KerasLSTMRegression(KerasModelBuilder):
    def __init__(self, Options):
        KerasModelBuilder.__init__(self,
                                   HDF5File=Options.HDF5File,
                                   outdir=Options.outDir,
                                   name=Options.classifierName,
                                   SwapTrainTest=Options.swapTrainTesting,
                                   preprocessing=Options.dataPreProcessing,
                                   transform_data=Options.transform_data,
                                   useWeights=False,
                                   begin=Options.startEvent,
                                   end=Options.lastEvent,
                                   isClassification=False,
                                   loss_function="mse",
                                   optimizer="RMSprop",
                                   epochs=150)

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        return True

    def _call_mva(self, data):
        return self.model().predict(data)

    def _build_model(self):
        from keras.layers import Dense
        from keras.layers import LSTM
        self.model().add(LSTM(10))
        self.model().add(Dense(1, activation='linear'))
        return True


def setupLSTMRegression():
    argparser = setupSciKitTrainingParser()
    argparser.set_defaults(classifierName="LSTMkeras")
    argparser.set_defaults(HDF5File="/ptmp/mpp/junggjo9/DiMass_Test_recoil//output_scikit.H5")
    argparser.set_defaults(outDir="/ptmp/mpp/junggjo9/DiMass_Test_recoil//Trainings/")
    argparser.set_defaults(transform_data="normMean")
    Options = argparser.parse_args()
    runTraining(Options=Options, classifier=KerasLSTMRegression(Options))
