#!/bin/env/python
from XAMPPtmva.SciKitClassifier import SciKitClassifier
from XAMPPtmva.LoadHDF5 import setupSciKitTrainingParser, runTraining

###########################################################################################
#   Use derived classes from the SciKitClassifier for each training type.                 #
#   Through this additional training specific diagnostic plots can be written to the      #
#   diagnostics file                                                                      #
###########################################################################################
# http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
from sklearn.neural_network import MLPRegressor
from sklearn.tree import DecisionTreeRegressor
from random import random


class SciKitTreeRegression(SciKitClassifier):
    def __init__(self, Options):
        regression_obj = DecisionTreeRegressor(
            criterion=Options.criterion,
            splitter=Options.splitter,
            max_depth=Options.max_depth,
            min_weight_fraction_leaf=Options.min_weight_fraction_leaf,
            #max_features=None,
            max_leaf_nodes=Options.max_leaf_nodes,
            min_impurity_decrease=Options.min_impurity_decrease,
            presort=False)

        SciKitClassifier.__init__(self,
                                  model=regression_obj,
                                  HDF5File=Options.HDF5File,
                                  begin=Options.startEvent,
                                  end=Options.lastEvent,
                                  outdir=Options.outDir,
                                  preprocessing=Options.dataPreProcessing,
                                  name=Options.classifierName,
                                  useWeights=False,
                                  SwapTrainTest=Options.swapTrainTesting,
                                  transform_data=Options.transform_data,
                                  isClassification=False)

    def saveSpecificDiagnostics(self, Diag_Dir=None):
        return True

    def _call_mva(self, data):
        return self.model().predict(data)


def setupRegressionTree():
    parser = setupSciKitTrainingParser()
    parser.set_defaults(classifierName="RegressionBDT")
    parser.add_argument("--criterion",
                        help="The function to measure the quality of a split.",
                        default="mse",
                        choices=["mse", "friedman_mse", "mae"])
    parser.add_argument("--splitter",
                        help="The strategy used to choose the split at each node.",
                        default="best",
                        choices=["best", "random"])
    parser.add_argument("--max_depth", help="The maximum depth of the tree.", default=8, type=int)
    parser.add_argument(
        "--min_weight_fraction_leaf",
        help="The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node.",
        default=0.01,
        type=int)
    parser.add_argument(
        "--max_leaf_nodes",
        help="Grow a tree with max_leaf_nodes in best-first fashion. Best nodes are defined as relative reduction in impurity.",
        default=15,
        type=int)
    parser.add_argument("--min_impurity_decrease",
                        help="A node will be split if this split induces a decrease of the impurity greater than or equal to this value.",
                        default=0,
                        type=float)
    Options = parser.parse_args()
    runTraining(Options=Options, classifier=SciKitTreeRegression(Options))
