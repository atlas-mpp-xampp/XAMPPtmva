from XAMPPtmva.SciKitROCCurve import IntegrateTGraph, IntegralErrorTGraph
from XAMPPtmva.GetROCCurve import generateROC
from XAMPPtmva.SciKitOvertrainPlot import KolmogorovSmirnovScore
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPplotting.SignificancePlots import CreateSiggiHistos
from ClusterSubmission.Utils import ResolvePath, WriteList, CreateDirectory, ExecuteCommands, RecursiveLS
from XAMPPplotting.StatisticalFunctions import sigmaZn
from XAMPPplotting.Utils import ExtractMasses
import numpy as np
import math, ROOT, os, threading, logging


class BasicMVAPerformanceInfo(object):
    def __init__(self, Bkg_Train=None, Bkg_Test=None, Sig_Train=None, Sig_Test=None, over_training_tolerance=0.01):
        #### ROC curves as the figure of merit
        self.__ROC_Train = generateROC(Bkg_Train, Sig_Train, isTrainCurve=True, inverse_eff=False)
        self.__ROC_Test = generateROC(Bkg_Test, Sig_Test, isTrainCurve=False, inverse_eff=False)

        self.__InvBkgSigEff_Train = generateROC(Bkg_Train, Sig_Train, isTrainCurve=True, inverse_eff=True)
        self.__InvBkgSigEff_Test = generateROC(Bkg_Test, Sig_Test, isTrainCurve=False, inverse_eff=True)

        self.__ROCAUC_Train = IntegrateTGraph(self.train_ROC())
        self.__ROCAUC_Test = IntegrateTGraph(self.test_ROC())

        self.__ROCAUC_Train_err = IntegralErrorTGraph(self.train_ROC())
        self.__ROCAUC_Test_err = IntegralErrorTGraph(self.test_ROC())

        self.__tolerance_level = math.sqrt(-0.5 * math.log(over_training_tolerance / 2))
        ### Kolmogorov scoes
        self.__kol_bkg = 0.
        self.__kol_sig = 0.
        self.__kol_bkg_err = 0.
        self.__kol_sig_err = 0.

        self.__kol_bkg, self.__kol_bkg_err = KolmogorovSmirnovScore(Bkg_Train, Bkg_Test)
        self.__kol_sig, self.__kol_sig_err = KolmogorovSmirnovScore(Sig_Train, Sig_Test)

        self.__n_train_evts_bkg = Bkg_Train.GetEntries() if Bkg_Train else 0
        self.__n_train_evts_sig = Sig_Train.GetEntries() if Sig_Train else 0

        self.__n_test_evts_bkg = Bkg_Test.GetEntries() if Bkg_Test else 0
        self.__n_test_evts_sig = Sig_Test.GetEntries() if Sig_Test else 0

    def train_background_events(self):
        return self.__n_train_evts_bkg

    def test_background_events(self):
        return self.__n_test_evts_bkg

    def train_signal_events(self):
        return self.__n_train_evts_sig

    def test_signal_events(self):
        return self.__n_test_evts_sig

    def train_events(self):
        return self.train_background_events() + self.train_signal_events()

    def test_events(self):
        return self.test_background_events() + self.test_signal_events()

    def train_ROC(self):
        return self.__ROC_Train

    def test_ROC(self):
        return self.__ROC_Test

    def train_invBkg_SigEff(self):
        return self.__InvBkgSigEff_Train

    def test_invBkg_SigEff(self):
        return self.__InvBkgSigEff_Test

    def train_ROCAUC(self):
        return self.__ROCAUC_Train

    def test_ROCAUC(self):
        return self.__ROCAUC_Test

    def delta_ROCAUC(self):
        return math.fabs(self.train_ROCAUC() - self.test_ROCAUC())

    def Kolmogorov_signal(self):
        return self.__kol_sig

    def Kolmogorov_background(self):
        return self.__kol_bkg

    def Kolmogorov(self):
        return max(self.Kolmogorov_signal(), self.Kolmogorov_background())

    def error_Kolmogorov_signal(self):
        return self.__kol_sig_err

    def error_Kolmogorov_background(self):
        return self.__kol_bkg_err

    def error_kolmogorov(self):
        return self.error_Kolmogorov_signal(
        ) if self.Kolmogorov_signal() > self.Kolmogorov_background() else self.error_Kolmogorov_background()

    def error_train_ROCAUC(self):
        return self.__ROCAUC_Train_err

    def error_test_ROCAUC(self):
        return self.__ROCAUC_Test_err

    def error_delta_ROCAUC(self):
        #return math.fabs(self.error_train_ROCAUC() - self.error_test_ROCAUC())
        return math.sqrt(self.error_train_ROCAUC()**2 + self.error_test_ROCAUC()**2)

    ### Define the quality of the training as
    ###   ROC_AUC * exp( - max([kolmog_bkg, kolmog_sig]) - 2*|ROC_AUC^{train} - ROC_AUC^{test}| - |ROC_AUC^{train} - ROC_AUC^{test}|^{2} )
    ###   We want to achieve the highest ROC curve and account for the possible overtraining and the same time
    def training_quality(self):
        return self.test_ROCAUC() * math.exp(-self.delta_ROCAUC()) * self.__tolerance_level / max(self.Kolmogorov(), self.__tolerance_level)

    def error_train_quality(self):
        err_ROC = self.error_test_ROCAUC() * math.exp(-self.delta_ROCAUC()) - self.test_ROCAUC() * math.exp(-self.delta_ROCAUC()) * (
            self.error_test_ROCAUC() - self.error_train_ROCAUC())
        err_Kolm = 0. if self.Kolmogorov() < self.__tolerance_level else -self.__tolerance_level * self.error_kolmogorov() / (
            self.Kolmogorov())**2
        return math.sqrt(err_Kolm**2 + err_ROC**2)


class TrainingPerformanceMVA(object):
    def __init__(self,
                 MVA_Test_Sig_def=None,
                 MVA_Train_Sig_def=None,
                 MVA_Test_Bkg_def=None,
                 MVA_Train_Bkg_def=None,
                 MVA_Test_Sig_val=None,
                 MVA_Train_Sig_val=None,
                 MVA_Test_Bkg_val=None,
                 MVA_Train_Bkg_val=None,
                 over_training_tolerance=0.01):

        self.__default_perf = BasicMVAPerformanceInfo(Bkg_Train=MVA_Train_Bkg_def,
                                                      Bkg_Test=MVA_Test_Bkg_def,
                                                      Sig_Train=MVA_Train_Sig_def,
                                                      Sig_Test=MVA_Test_Sig_def,
                                                      over_training_tolerance=over_training_tolerance)
        self.__crossvalid_perf = BasicMVAPerformanceInfo(Bkg_Train=MVA_Train_Bkg_val,
                                                         Bkg_Test=MVA_Test_Bkg_val,
                                                         Sig_Train=MVA_Train_Sig_val,
                                                         Sig_Test=MVA_Test_Sig_val,
                                                         over_training_tolerance=over_training_tolerance)

    def default_perf_object(self):
        return self.__default_perf

    def valid_perf_object(self):
        return self.__crossvalid_perf

    def training_quality(self):
        return self.__default_perf.training_quality() + self.__crossvalid_perf.training_quality()

    def training_performance(self):
        return self.__default_perf.test_ROCAUC() + self.__crossvalid_perf.test_ROCAUC()

    def error_train_quality(self):
        return math.sqrt(self.__default_perf.error_train_quality()**2 + self.__crossvalid_perf.error_train_quality()**2)

    def error_train_performance(self):
        return math.sqrt(self.default_perf_object().error_test_ROCAUC()**2 + self.valid_perf_object().error_test_ROCAUC()**2)

    def train_Bkg_events(self):
        return self.default_perf_object().train_background_events()

    def test_Bkg_events(self):
        return self.default_perf_object().test_background_events()

    def train_Sig_events(self):
        return self.default_perf_object().train_signal_events()

    def test_Sig_events(self):
        return self.default_perf_object().test_signal_events()

    def train_events(self):
        self.default_perf_object().train_events()

    def test_events(self):
        self.default_perf_object().test_events()


class SciKitMVAPerformance(TrainingPerformanceMVA):
    def __init__(self, default_file_path="", cross_valid_file_path="", over_training_tolerance=0.01):

        diagnostic_default = GetFileHandler().LoadFile(default_file_path).GetDirectory("Diagnostics/") if len(default_file_path) else None
        diagnostic_cross_valid = GetFileHandler().LoadFile(cross_valid_file_path).GetDirectory("Diagnostics/") if len(
            cross_valid_file_path) else None

        ### If the use has switched on decorrelation
        decorr_selection = "" if not diagnostic_default or not "MVA_Decorr_BkgTraining" in [
            key.GetName() for key in diagnostic_default.GetListOfKeys() if key.ReadObj().InheritsFrom("TH1")
        ] else "Decorr_"

        TrainingPerformanceMVA.__init__(
            self,
            ### Now we need the MVA scores
            MVA_Test_Sig_def=diagnostic_default.Get('MVA_%sSigTesting' % (decorr_selection)) if diagnostic_default else None,
            MVA_Train_Sig_def=diagnostic_default.Get('MVA_%sSigTraining' % (decorr_selection)) if diagnostic_default else None,
            MVA_Test_Bkg_def=diagnostic_default.Get('MVA_%sBkgTesting' % (decorr_selection)) if diagnostic_default else None,
            MVA_Train_Bkg_def=diagnostic_default.Get('MVA_%sBkgTraining' % (decorr_selection)) if diagnostic_default else None,

            ### Now we need the MVA scores
            MVA_Test_Sig_val=diagnostic_cross_valid.Get('MVA_%sSigTesting' % (decorr_selection)) if diagnostic_cross_valid else None,
            MVA_Train_Sig_val=diagnostic_cross_valid.Get('MVA_%sSigTraining' % (decorr_selection)) if diagnostic_cross_valid else None,
            MVA_Test_Bkg_val=diagnostic_cross_valid.Get('MVA_%sBkgTesting' % (decorr_selection)) if diagnostic_cross_valid else None,
            MVA_Train_Bkg_val=diagnostic_cross_valid.Get('MVA_%sBkgTraining' % (decorr_selection)) if diagnostic_cross_valid else None,
            over_training_tolerance=over_training_tolerance)


class TMVAPerformance(TrainingPerformanceMVA):
    def __init__(self, nominal_file="", cross_valid_file="", method="", over_training_tolerance=0.01):
        if len(cross_valid_file) == 0 or not os.path.exists(cross_valid_file):
            cross_valid_file = nominal_file
        nominal_dir = GetFileHandler().LoadFile(nominal_file).GetDirectory("TrainingFiles/Method_%s/%s/" % (method, method))
        cross_valid_dir = GetFileHandler().LoadFile(cross_valid_file).GetDirectory("TrainingFiles/Method_%s_swapped/%s_swapped/" %
                                                                                   (method, method))
        if not nominal_dir and cross_valid_dir: nominal_dir = cross_valid_dir
        TrainingPerformanceMVA.__init__(
            self,
            MVA_Test_Bkg_def=nominal_dir.Get('MVA_%s_B' % (nominal_dir.GetName())),
            MVA_Train_Bkg_def=nominal_dir.Get('MVA_%s_Train_B' % (nominal_dir.GetName())),
            MVA_Test_Sig_def=nominal_dir.Get('MVA_%s_S' % (nominal_dir.GetName())),
            MVA_Train_Sig_def=nominal_dir.Get('MVA_%s_Train_S' % (nominal_dir.GetName())),
            MVA_Test_Sig_val=cross_valid_dir.Get('MVA_%s_B' % (cross_valid_dir.GetName())) if cross_valid_dir else None,
            MVA_Train_Sig_val=cross_valid_dir.Get('MVA_%s_Train_B' % (cross_valid_dir.GetName())) if cross_valid_dir else None,
            MVA_Test_Bkg_val=cross_valid_dir.Get('MVA_%s_S' % (cross_valid_dir.GetName())) if cross_valid_dir else None,
            MVA_Train_Bkg_val=cross_valid_dir.Get('MVA_%s_Train_S' % (cross_valid_dir.GetName())) if cross_valid_dir else None,
            over_training_tolerance=over_training_tolerance)


class SciKitVariableScanner(threading.Thread):
    def __init__(
        self,
        in_cfg="",
        base_dir="",
        RunCfg="XAMPPtmva/RunConf/LepHadStau/MVA_Regions/blub",
        topology="",
        MVA="",  ### MVA-method to apply
        MVA_TrainCfg="XAMPPtmva/TrainingConf/LepHadStau/TrainConf.conf",  ### What's the MVA training Cfg to use
        set_of_vars=[],  ### List of available variables
        established_vars=[],  ### List of indeces in set_of_vars                    
        test_var=0,  ### which variable is actually tested
        ### For the later application of the training
        bkg_model_to_test="",
        sig_model_to_test="",
        bkg_app_out_file="",
        sig_app_out_file="",
        lumi=1.,
        bkg_uncert=0.3,

        ### For local processing
        cleanOldData=False,
    ):
        threading.Thread.__init__(self)

        self.__inCfg = in_cfg
        self.__baseDir = base_dir
        self.__topology = topology
        self.__established_vars = sorted(established_vars)
        self.__var_to_test = test_var
        self.__clean_old = cleanOldData

        self.__bkg_app_file = bkg_app_out_file
        self.__sig_app_file = sig_app_out_file

        ### Write first the train_cfg
        WriteList(
            ["define isTrainingSession", "RunConfig %s" % (RunCfg)] +
            [self.__AddMVAvariable(set_of_vars[i][0], set_of_vars[i][1]) for i in range(len(set_of_vars)) if i in self.all_variables()] + [
                ### We must add the variables before we book any method. Otherwise this leads to clashes with the application
                "Import %s" % (MVA_TrainCfg),
                "ClearBookedMethods",
                "BookMethod %s\n\n" % (MVA),
            ],
            self.train_cfg())

        self.__importance_test = 0.
        self.__rank_test = len(self.all_variables())

        self.__train_perf = None
        #### We want to evaluate  the physical write first the run config
        WriteList([
            "Import %s" % (RunCfg),
            "noSyst",
            "New_SciKitEventClass",
            "    Name AppliedMVA",
            "    Directory %s/Trainings/" % (self.train_dir()),
            "    Training %s" % (self.train_cfg()),
            "End_SciKitEventClass",
        ], self.run_cfg())
        ### Next item is the  histogram  config
        WriteList([
            "1DHisto MVA_Classifier 25 -1  1.", "NewVar", "    Template MVA_Classifier",
            "    SciKitEventReader  %s AppliedMVA" % (MVA), "    Name MVA", "    Type 1D",
            "    xLabel %s score" % (MVA), "EndVar", "NewVar", "    Template MVA_Classifier",
            "    SciKitEventReader  %s AppliedMVA" % (MVA), "    Name Cum_MVA", "    Type Cul1D",
            "    xLabel Culumative %s" % (MVA), "EndVar", "NewVar", "    Template MVA_Classifier",
            "    SciKitEventReader  %s_swapped AppliedMVA" % (MVA), "    Name MVA_swapped", "    Type 1D",
            "    xLabel %s score (cross-validation)" % (MVA), "EndVar", "NewVar", "    Template MVA_Classifier",
            "    SciKitEventReader  %s_swapped AppliedMVA" % (MVA), "    Name Cum_MVA_swapped", "    Type Cul1D",
            "    xLabel Culumative %s" % (MVA), "EndVar"
        ], self.histo_cfg())

        ### input config files to be used in order to evaluate  the training
        self.__valid_bkg = ResolvePath(bkg_model_to_test) if len(bkg_model_to_test) > 0 else None
        self.__valid_sig = ResolvePath(sig_model_to_test) if len(sig_model_to_test) > 0 else None
        self.__lumi = lumi
        self.__bkg_uncert = bkg_uncert

        self.__def_sigma = 0.
        self.__val_sigma = 0.

        self.__def_score_cut = -2.
        self.__val_score_cut = -2.

    def __AddMVAvariable(self, Name, Reader):
        return """
    New_MVAVar
        Name %s
        %s
    End_MVAVar
""" % (Name, Reader)

    def all_variables(self):
        return sorted(self.established_variables() + [self.__var_to_test])

    def established_variables(self):
        return self.__established_vars

    def var_to_str(self):
        return "".join(["%02d" % (d) for d in self.all_variables()])

    def nVars(self):
        return len(self.all_variables())

    def train_cfg(self):
        return "%s/TrainCfg_%s.conf" % (self.application_dir(), self.var_to_str())

    def run_cfg(self):
        return "%s/RunConfig.conf" % (self.application_dir())

    def histo_cfg(self):
        return "%s/HistoConfig.conf" % (self.application_dir())

    def train_dir(self):
        return "%s/Scans/%s-%s/%s/" % (self.__baseDir, self.train_model(), self.event_selection(), self.var_to_str())

    def application_dir(self):
        return "%s/AppliedTrain/%s-%s/%s/" % (self.__baseDir, self.train_model(), self.event_selection(), self.var_to_str())

    def log_dir(self):
        return "%s/Log_Files/%s-%s/" % (self.__baseDir, self.train_model(), self.event_selection())

    def log_file(self):
        return "%s/Train%s.log" % (self.log_dir(), self.var_to_str())

    def max_signficance_default(self):
        return self.__def_sigma

    def MVA_score_cut_default(self):
        return self.__def_score_cut

    def max_signficance_valid(self):
        return self.__val_sigma

    def MVA_score_cut_valid(self):
        return self.__val_score_cut

    def Bkg_app_file(self):
        if len(self.__bkg_app_file) > 0 and os.path.isfile(self.__bkg_app_file):
            return self.__bkg_app_file
        return "%s/Background_%s.root" % (self.application_dir(), self.var_to_str())

    def Sig_app_file(self):
        if len(self.__sig_app_file) > 0 and os.path.isfile(self.__sig_app_file):
            return self.__sig_app_file
        return "%s/Signal_%s.root" % (self.application_dir(), self.var_to_str())

    def event_selection(self):
        return self.__topology

    def train_model(self):
        return self.__inCfg[self.__inCfg.rfind("/") + 1:self.__inCfg.rfind(".")]

    def test_variable(self):
        return self.__var_to_test

    def importance(self):
        return self.__importance_test

    def ranking(self):
        return self.__rank_test

    def run(self):
        ### Clean up the old trainings
        CreateDirectory(self.train_dir(), self.__clean_old)
        CreateDirectory(self.application_dir(), self.__clean_old)
        CreateDirectory(self.log_dir(), False)
        if len(RecursiveLS("%s/Trainings/" % (self.train_dir()), ["pkl", "root"])) < 4:
            Cmd = "TrainSciKitLearn -i %s -d %s -t %s --noPlots > %s" % (self.__inCfg, self.train_dir(), self.train_cfg(), self.log_file())
            logging.info("Execute training %s" % (Cmd))
            if os.system(Cmd):
                logging.error("Something went deeply wrong in writing the HDF5 in %s" % (self.train_dir()))
                os.system("rm -rf %s" % (self.train_dir()))
                return False

        ### That's everything from the machine learning case. Let's move on to
        ### physics where we need obtain the significanes
        if not self.__valid_bkg or not self.__valid_sig:
            logging.warning("No valid testing configs were given")
            return self.evaluate_scan()

        Eval_Cmd_Bkg = "WriteHistFitterHistos -i %s -r %s/RunConfig.conf -h %s/HistoConfig.conf -o %s > %s/Bkg.log" % (
            self.__valid_bkg, self.application_dir(), self.application_dir(), self.Bkg_app_file(), self.application_dir())
        Eval_Cmd_Sig = "WriteHistFitterHistos -i %s -r %s/RunConfig.conf -h %s/HistoConfig.conf -o %s > %s/Sig.log" % (
            self.__valid_sig, self.application_dir(), self.application_dir(), self.Sig_app_file(), self.application_dir())
        if not os.path.exists(self.Bkg_app_file()) or not os.path.exists(self.Sig_app_file()):
            logging.info("Prepare histograms for significance test")
            ExecuteCommands([Eval_Cmd_Sig, Eval_Cmd_Bkg], MaxCurrent=2)
        return self.evaluate_scan()

    def __find_all_histos(self, TFile, Path="", pattern=""):
        Histos = []
        Dir = TFile if len(Path) == 0 else TFile.GetDirectory(Path)
        for key in Dir.GetListOfKeys():
            if key.ReadObj().InheritsFrom("TDirectory"):
                Histos += self.__find_all_histos(TFile, Path + key.GetName() + "/", pattern)
            elif key.ReadObj().InheritsFrom("TH1") and len(pattern) == 0 or key.GetName().find(pattern) != -1:
                Histos.append(Path + key.GetName())
        return Histos

    def evaluate_scan(self):
        ### Now we need to find the training_cfgs
        ROOT_Files = RecursiveLS(self.train_dir(), ["root"])
        if len(ROOT_Files) < 2:
            logging.error("Apparently the trainings was so baad that the diagnostics decided to skip it")
            os.system("rm -rf %s" % (self.train_dir()))
            return False

        ### We've them
        logging.info("Found the following two validation files")
        for R in ROOT_Files:
            logging.info("    --- %s" % (R))
            GetFileHandler().LoadFile(R)
        ### Now the fun begins
        def_file_path = ROOT_Files[0] if ROOT_Files[0].find("_swapped") == -1 else ROOT_Files[1]
        val_file_path = ROOT_Files[0] if ROOT_Files[0].find("_swapped") != -1 else ROOT_Files[1]
        default_File = GetFileHandler().LoadFile(def_file_path)
        cross_valid_file = GetFileHandler().LoadFile(val_file_path)
        if not default_File or not cross_valid_file:
            logging.error("Could not load the traiining files")
            os.system("rm -rf %s" % (self.train_dir()))
            return False

        diagnostic_default = default_File.GetDirectory("Diagnostics/")
        Variable_Ranking = diagnostic_default.Get("ClassifierMonitoring/VariableRanking")
        if not Variable_Ranking:
            logging.error("Could not find the variable ranking")
            os.system("rm -rf %s" % (self.train_dir()))
            return False

        self.__train_perf = SciKitMVAPerformance(default_file_path=def_file_path, cross_valid_file_path=val_file_path)

        ### Rank it
        self.__importance_test = Variable_Ranking.GetBinContent(self.all_variables().index(self.test_variable()) + 1)
        self.__rank_test = sorted([Variable_Ranking.GetBinContent(i + 1) for i in range(self.nVars())],
                                  reverse=True).index(self.__importance_test)

        if not os.path.exists(self.Bkg_app_file()) or not os.path.exists(self.Sig_app_file()):
            logging.warning("Could not locate valid file where the training has been applied")
            logging.warning("   --- %s" % (self.Bkg_app_file()))
            logging.warning("   --- %s" % (self.Sig_app_file()))

            return True
        ### We've evaluated the MVA on the test point
        Bkg_File = GetFileHandler().LoadFile(self.Bkg_app_file())
        Sig_File = GetFileHandler().LoadFile(self.Sig_app_file())
        if not Bkg_File or not Sig_File:
            logging.error("Where is my testing file")
            os.system("rm -rf %s" % (self.application_dir()))
            return False
        logging.info("Evaluate the statistical significance for %.1f fb^{-1}" % (self.__lumi))
        logging.info(" --- %s" % (self.Bkg_app_file()))
        logging.info(" --- %s" % (self.Sig_app_file()))

        try:
            MVA_Score_Bkg_def = Bkg_File.Get(self.__find_all_histos(TFile=Bkg_File, pattern="Cum_MVA")[0])
            MVA_Score_Sig_def = Sig_File.Get(self.__find_all_histos(TFile=Sig_File, pattern="Cum_MVA")[0])

            MVA_Score_Bkg_val = Bkg_File.Get(self.__find_all_histos(TFile=Bkg_File, pattern="Cum_MVA_swapped")[0])
            MVA_Score_Sig_val = Sig_File.Get(self.__find_all_histos(TFile=Sig_File, pattern="Cum_MVA_swapped")[0])
        except:
            logging.error("Something went wrong in the evaluation of the testing file")
            os.system("rm -rf %s" % (self.application_dir()))
            return False
        for i in range(1, MVA_Score_Bkg_def.GetNbinsX() + 1):
            S_def = MVA_Score_Sig_def.GetBinContent(i) * self.__lumi
            B_def = MVA_Score_Bkg_def.GetBinContent(i) * self.__lumi

            S_val = MVA_Score_Sig_val.GetBinContent(i) * self.__lumi
            B_val = MVA_Score_Bkg_val.GetBinContent(i) * self.__lumi

            sigma_def = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(S_def, B_def, self.__bkg_uncert)
            sigma_val = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(S_val, B_val, self.__bkg_uncert)

            #            sigma_def = sigmaZn(S_def, B_def, self.__bkg_uncert * B_def)
            #            sigma_val = sigmaZn(S_val, B_val, self.__bkg_uncert * B_def)

            if not np.isnan(sigma_def) and not np.isinf(sigma_def) and sigma_def > self.__def_sigma:
                self.__def_sigma = sigma_def
                self.__def_score_cut = MVA_Score_Sig_def.GetXaxis().GetBinLowEdge(i)
            if not np.isnan(sigma_val) and not np.isinf(sigma_val) and sigma_val > self.__val_sigma:
                self.__val_sigma = sigma_val
                self.__val_score_cut = MVA_Score_Sig_val.GetXaxis().GetBinLowEdge(i)

        return True

    def training_performance(self):
        return self.__train_perf

    def ROCAUC_train_default(self):
        return self.__train_perf.default_perf_object().train_ROCAUC() if self.__train_perf else 0

    def ROCAUC_test_default(self):
        return self.__train_perf.default_perf_object().test_ROCAUC() if self.__train_perf else 0

    def delta_ROCAUC_default(self):
        return self.__train_perf.default_perf_object().delta_ROCAUC() if self.__train_perf else 0

    def ROCAUC_train_valid(self):
        return self.__train_perf.valid_perf_object().train_ROCAUC() if self.__train_perf else 0

    def ROCAUC_test_valid(self):
        return self.__train_perf.valid_perf_object().test_ROCAUC() if self.__train_perf else 0

    def delta_ROCAUC_valid(self):
        return self.__train_perf.valid_perf_object().delta_ROCAUC() if self.__train_perf else 0

    def kolmogorov_Bkg_default(self):
        return self.__train_perf.default_perf_object().Kolmogorov_background() if self.__train_perf else 0.

    def kolmogorov_Sig_default(self):
        return self.__train_perf.default_perf_object().Kolmogorov_signal() if self.__train_perf else 0.

    def kolmogorov_default(self):
        return self.__train_perf.default_perf_object().Kolmogorov() if self.__train_perf else 0

    def kolmogorov_Bkg_valid(self):
        return self.__train_perf.valid_perf_object().Kolmogorov_background() if self.__train_perf else 0.

    def kolmogorov_Sig_valid(self):
        return self.__train_perf.valid_perf_object().Kolmogorov_signal() if self.__train_perf else 0.

    def kolmogorov_valid(self):
        return self.__train_perf.valid_perf_object().Kolmogorov() if self.__train_perf else 0

    def training_quality(self):
        return self.__train_perf.training_quality() if self.__train_perf else -1

    def training_performance(self):
        return self.__train_perf.training_performance() if self.__train_perf else -1

    def train_Bkg_events(self):
        return self.__train_perf.train_Bkg_events() if self.__train_perf else 0.

    def test_Bkg_events(self):
        return self.__train_perf.test_Bkg_events() if self.__train_perf else 0.

    def train_Sig_events(self):
        return self.__train_perf.train_Sig_events() if self.__train_perf else 0.

    def test_Sig_events(self):
        return self.__train_perf.test_Sig_events() if self.__train_perf else 0.


#################################################################
#################################################################
###     The MVA score shape fit class performs                ###
###     the evaluation of the significance on                 ###
###     each bin of the classifier score and                  ###
###     adds the particular significances in quadrature       ###
#################################################################
#################################################################
class MVAShapeSignificance(object):
    #### summed_bkg_histo and signal_histo are
    #### XAMPP::SampleHistos providing some nice features
    #### to calculate the significance more easily
    def __init__(
        self,
        summed_bkg_histo=None,
        signal_histo=None,
        rel_bkg_uncert=0.3,
        use_lebeve_buttinger=False,
    ):
        self.__bkg = summed_bkg_histo
        self.__sig = signal_histo

        self.__sig_histo = CreateSiggiHistos(Signals=[self.__sig],
                                             Background=self.__bkg,
                                             RelUnc=rel_bkg_uncert,
                                             ScaleSig=1.,
                                             DoIntegral=False,
                                             UseSUSYSignificance=use_lebeve_buttinger)[0]
        self.__tot_sigma = math.sqrt(sum([self.__sig_histo.GetBinContent(i)**2 for i in range(self.__sig_histo.GetNbinsX() + 1)]))

        self.__nlsp_mass, self.__lsp_mass = ExtractMasses(self.model_point())

    def significance(self):
        return self.__tot_sigma

    def model_point(self):
        return self.__sig.GetName()

    def variable(self):
        return self.__sig.GetVariableName()

    def region(self):
        return self.__sig.GetRegion()

    def stau_mass(self):
        return self.__nlsp_mass

    def nino_mass(self):
        return self.__lsp_mass
