# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Boost as part of the offline / analysis release.
#

# Tell the user what's happening:
message( STATUS "Building openBlas as part of this project" )

# The source code of OpenBlas:
set( _openBlasSource "https://github.com/xianyi/OpenBLAS/archive/v0.3.5.tar.gz" )
set( _openBlasMd5 "579bda57f68ea6e9074bf5780e8620bb" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/OpenBlasBuild )


set ( OPENBLAS_LIBARY_DIR ${_buildDir}/lib)
set ( OPENBLAS_INCLUDE_DIR ${_buildDir})

set( OPENBLAS_LIBARIES )
foreach( lib openblas cblas )
   list( APPEND OPENBLAS_LIBARIES
      $<BUILD_INTERFACE:${_buildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}${lib}${CMAKE_SHARED_LIBRARY_SUFFIX}>
      $<INSTALL_INTERFACE:lib/${CMAKE_SHARED_LIBRARY_PREFIX}${lib}${CMAKE_SHARED_LIBRARY_SUFFIX}> )
endforeach()

# Build Boost:
ExternalProject_Add( OpenBlas
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   BINARY_DIR ${_buildDir}
   URL ${_openBlasSource}
   URL_MD5 ${_openBlasMd5}
   BUILD_IN_SOURCE 0
   CMAKE_CACHE_ARGS -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir} 
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/
   <INSTALL_DIR> )


   ExternalProject_Add_Step( OpenBlas linkshared
   COMMAND ln -f -s ${_buildDir}/lib/libopenblas.a  ${_buildDir}/lib/libcblas.a 
   COMMENT "Create shared symlink"
   DEPENDEES install )

# Install oenBLAS:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

unset ( _buildDir)
unset (_openBlasSource)
unset (_openBlasMd5)

