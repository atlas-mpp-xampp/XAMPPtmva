import os, ROOT, math, argparse, threading, random
from ClusterSubmission.Utils import FillWhiteSpaces, ClearFromDuplicates, ResolvePath, WriteList, ReadListFromFile, CreateDirectory, id_generator, setup_engine
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPtmva.MVAOptimizationHelpers import SciKitVariableScanner
from XAMPPtmva.SubmitSciKitToBatch import SetupSubmitParser, SciKitSubmit
import numpy as np
import logging


def standardVariables():
    Variable_Choices = [
        ### Lepton momenta
        ("tau_pt", "ParReader SignalTaus pt[0]"),
        ("lepton_pt", "ParReader Leptons pt[0]"),
        ### Missing transverse momentum itself
        ("MET", "EvReader floatGeV MET"),

        #### Angular correlations
        ("dPhi_TauLepton", "|dPhiReader| Leptons[0] SignalTaus[0]"),
        ("dEta_TauLepton", "|dEtaReader| Leptons[0] SignalTaus[0]"),
        ("dPhi_MET_Tau", "|dPhiToMetReader| SignalTaus[0] MET"),
        ("dPhi_MET_Leptons", "|dPhiToMetReader| Leptons[0] MET"),
        ("n_Jets", "EvReader int n_SignalJets"),
        ("dPhi_MET_TauLep", "EvReader float MET_LepTau_MT"),

        ## Recursive JigSaw variables
        ("RJR_Z_m", "ParReader RJRZ m[0]"),
        ("RJR_W_m", "ParReader RJRW m[0]"),
        ("RJR_W_pt", "ParReader RJRW pt[0]"),
        ("RJR_Z_pt", "ParReader RJRZ pt[0]"),

        #### Tau lepton momentum imbalance
        ("Imbal_TauLepton", "\n".join([
            "New_MathReader / ",
            "%sNew_MathReader -" % (FillWhiteSpaces(4, " ")),
            "%sParReader SignalTaus pt[0]" % (FillWhiteSpaces(8, " ")),
            "%sParReader Leptons pt[0]" % (FillWhiteSpaces(8, " ")),
            "%sEnd_MathReader" % (FillWhiteSpaces(4, " ")),
            "%sNew_MathReader +" % (FillWhiteSpaces(4, " ")),
            "%sParReader SignalTaus pt[0]" % (FillWhiteSpaces(8, " ")),
            "%sParReader Leptons pt[0]" % (FillWhiteSpaces(8, " ")),
            "%sEnd_MathReader" % (FillWhiteSpaces(4, " ")), "End_MathReader"
        ])),
        ###        ("MVA_Mass", "SciKitEvRegressionReader MLP_diTau DiMass Mtautau"),
        #### Transverse masses
        ("tau_MT", "ParReader SignalTaus MT[0]"),
        ("lepton_MT", "ParReader Leptons MT[0]"),

        ##### Summed tranverse mass
        ("SumMT", "\n".join([
            "%sNew_MathReader +" % (FillWhiteSpaces(4, " ")),
            "%sParReader SignalTaus MT[0]" % (FillWhiteSpaces(8, " ")),
            "%sParReader Leptons MT[0]" % (FillWhiteSpaces(8, " ")),
            "%sEnd_MathReader" % (FillWhiteSpaces(4, " ")),
        ])),

        #### Centrality and SumCos Delta Phi
        ("MET_SumCosDPhi", "EvReader float MET_SumCosDeltaPhi"),
        ("MET_Centrality", "EvReader float MET_Centrality"),
        ("MET_CosMinDeltaPhi", "EvReader float MET_CosMinDeltaPhi"),
        ### Take met significance out because it's only properly defined
        ### at reco-level
        #("MET_significance", "EvReader float MET_significance"),
        #("MET_LepTau_VecSumPt", "EvReader floatGeV MET_LepTau_VecSumPt"),

        ### Effective mass
        ("Meff", "EvReader floatGeV Meff"),
        ("InvSumPt", "EvReader floatGeV LepTau_VecSumPt"),

        ### Number of jets
        ("Ht_Jet", "EvReader floatGeV Ht_Jet"),
        ### Event shape variables
        ("Aplanarity", "EvReader float Aplanarity"),
        ("Circularity", "EvReader float Circularity"),
        ### How are the variables defined?
        ("CosChi1", "EvReader float CosChi1"),
        ("CosChi2", "EvReader float CosChi2"),

        #### PTt( a, b) = p_{T}(a+b) * sin( P4(a+b).deltaPhi(a-b))
        ("PTt", "EvReader floatGeV PTt"),
        ("Thrust", "EvReader float Thrust"),
        ("Sphericity", "EvReader float  Sphericity"),
        ("Spherocity", "EvReader float Spherocity"),
        ("Planarity", "EvReader float Planarity"),
        ("MT2", "EvReader floatGeV MT2_max"),
        ("Oblateness", "EvReader float Oblateness"),
        ("RJW_CosThetaStarW", "EvReader float RJW_CosThetaStarW"),
        ("RJW_GammaBetaW", "EvReader float RJW_GammaBetaW"),
        ("RJW_dPhiDecayPlaneW", "EvReader float RJW_dPhiDecayPlaneW"),
        ("RJZ_H01_Z", "EvReader floatGeV RJZ_H01_Z"),
        ("RJZ_H11_Z", "EvReader floatGeV RJZ_H11_Z"),
        ("RJZ_H21_Z", "EvReader floatGeV RJZ_H21_Z"),
        ("RJZ_Ht01_Z", "EvReader floatGeV RJZ_Ht01_Z"),
        ("RJZ_Ht11_Z", "EvReader floatGeV RJZ_Ht11_Z"),
        ("RJZ_Ht21_Z", "EvReader floatGeV RJZ_Ht21_Z"),
        ("RJZ_BalMetObj_Z", "EvReader float RJZ_BalMetObj_Z"),
        ("RJZ_R_BoostZ", "EvReader float RJZ_R_BoostZ"),
        ("RJZ_cosTheta_Z", "EvReader float RJZ_cosTheta_Z"),
        ("RJZ_cosTheta_Tau1", "EvReader float RJZ_cosTheta_Tau1"),
        ("RJZ_cosTheta_Tau2", "EvReader float RJZ_cosTheta_Tau2"),
        ("RJZ_dPhiDecayPlane_Z", "EvReader float RJZ_dPhiDecayPlane_Z"),
        ("RJZ_dPhiDecayPlane_Tau1", "EvReader float RJZ_dPhiDecayPlane_Tau1"),
        ("RJZ_dPhiDecayPlane_Tau2", "EvReader float RJZ_dPhiDecayPlane_Tau2"),
    ]
    ### I'm not sure about the WolframMoments but okay  let's  keep  them  for now
    #Variable_Choices += [("WolframMoment_%d" % (d), "EvReader float UnNormed_WolframMoment_%d" % (d)) for d in range(0, 9)]
    return Variable_Choices


##
##
##
def trainQualitySorter(x, y):
    ### sorting according to the achieved significance of the training sample
    delta = x.max_signficance_default() - y.max_signficance_default() if x.max_signficance_default() != y.max_signficance_default(
    ) else x.training_quality() - y.training_quality()
    if delta > 0: return 1
    elif delta < 0: return -1
    return 0


class SciKitVariableOptimizer(object):
    def __init__(
        self,
        cluster_engine=None,
        selections=[],
        variables=standardVariables(),
        max_iterations=35,
        max_vars_to_use=-1,
        seeds=[0, 1],
        vars_to_test=[0],
        iteration=0,
        trainModel="",
        run_conf="",
        mva_method="",
        mva_config="XAMPPtmva/TrainingConf/LepHadStau/TrainConf.conf",
        lumi=150,
        bkg_uncert=0.3,
        reference_signal="",
        reference_bkg="",
    ):

        self.__cluster_engine = cluster_engine
        ### real variable names
        self.__vars = variables
        ### Fixed stock. List of indices mapping to the established and
        ### most perfomant variables
        self.__fixed_stock = ClearFromDuplicates(seeds)
        ### That's the rest we'd like to test
        self.__vars_to_test = sorted(ClearFromDuplicates([v for v in vars_to_test if v not in self.__fixed_stock]))

        ### paramaters for the iterations of the scan
        self.__itr = iteration
        self.__max_itr = max_iterations
        self.__max_vars_to_use = len(self.__vars) if max_vars_to_use < 1 or len(self.__vars) < max_vars_to_use else max_vars_to_use
        self.__trials_at_same_lvl = 3

        ### MVA training model
        self.__train_model = trainModel
        ### MVA technique to use
        self.__mva_method = mva_method
        self.__mva_basement_cfg = mva_config
        ### On which event topologies
        self.__event_select = selections

        self.__train_scanners = []

        ### options to test the training on a reference sample
        self.__lumi = lumi
        self.__bkgUncert = bkg_uncert
        self.__ref_signal = ResolvePath(reference_signal)
        self.__ref_bkg = ResolvePath(reference_bkg)

    def engine(self):
        return self.__cluster_engine

    def lumi(self):
        return self.__lumi

    def combinations_to_test(self):
        return self.__vars_to_test

    def fixed_stock(self):
        return self.__fixed_stock

    def train_variables(self):
        return self.__vars

    def n_vars(self):
        return len(self.train_variables())

    def train_model(self):
        return self.__train_model

    def mva_method(self):
        return self.__mva_method

    def mva_cfg_file(self):
        return self.__mva_basement_cfg

    def event_selections(self):
        return self.__event_select

    def iteration(self):
        return self.__itr

    def max_iterations(self):
        return self.__max_itr

    def setup_scanners(self, fixed_stock=[]):
        trainings = []
        for R in self.event_selections():
            selection = R[R.rfind("/") + 1:R.rfind(".")]
            for i, v in enumerate(self.combinations_to_test()):
                mva_scanner = SciKitVariableScanner(
                    in_cfg=train_model,
                    base_dir=self.engine().base_dir(),
                    RunCfg=self.engine().link_to_copy_area(R),
                    topology=selection,
                    MVA=self.mva_method(),
                    bkg_model_to_test=self.__ref_bkg,
                    sig_model_to_test=self.__ref_signal,
                    lumi=self.lumi(),
                    MVA_TrainCfg=self.mva_cfg_file(),  ### What's the MVA training Cfg to use
                    set_of_vars=self.train_variables(),
                    established_vars=fixed_stock,
                    test_var=v,
                    bkg_uncert=self.__bkgUncert)

                trainings += [mva_scanner]

        return trainings

    def prepare_config_files(self):
        if self.__itr != 0: self.evaluate_train()
        if len(self.fixed_stock()) == 0:
            logging.info("Nothing has been left for training. Will exit the training session now")
            exit(0)

        self.__train_scanners = sorted(self.setup_scanners(self.fixed_stock()), key=lambda x: x.train_cfg())
        if len(self.__train_scanners) == 0:
            logging.error("No thing to submit")
            return False
        return True

    def scanners(self):
        return self.__train_scanners

    def HDF5_configs(self):
        return [t.train_cfg() for t in self.scanners()]

    def run_cfg_eval(self):
        return [t.run_cfg() for t in self.scanners()]

    def histo_cfg_eval(self):
        return [t.histo_cfg() for t in self.scanners()]

    def signal_eval(self):
        return self.__ref_signal

    def background_eval(self):
        return self.__ref_bkg

    def evaluate_train(self):
        ### Do fast evaluation of the scanners
        train_to_eval = sorted(
            [
                t for t in self.setup_scanners(self.fixed_stock())
                if t.evaluate_scan() and t.ROCAUC_train_default() > 0.5 and t.ROCAUC_train_valid() > 0.5
            ],
            key=lambda x: x.training_quality(),
            #cmp=trainQualitySorter,
            reverse=True)

        best_training = train_to_eval[0]
        WriteList(
            ReadListFromFile("%s/Best_Combinations.txt" % (best_training.log_dir())) +
            ["%s  %d" % (" ".join("%2d" % (i) for i in best_training.established_variables()), best_training.test_variable())],
            "%s/Best_Combinations.txt" % (best_training.log_dir()))

        ### Remember that we've written the training summary already to the file
        ### which corresponds to the -1 st entry
        ancestors = ClearFromDuplicates(ReadListFromFile("%s/Best_Combinations.txt" % (best_training.log_dir())))
        anc_vars = ancestors[-2].split() if len(ancestors) > 1 else []

        ### We need to compare this one with the previous round
        best_ancestor = sorted(
            [
                t for t in self.setup_scanners([int(anc_vars[d]) for d in range(len(anc_vars) - 1)])
                if t.evaluate_scan() and t.ROCAUC_train_default() > 0.5 and t.ROCAUC_train_valid() > 0.5
            ],
            #key=lambda x: x.training_performance(),
            key=lambda x: x.training_quality(),
            reverse=True)[0] if len(ancestors) > 1 else None

        #### End of the optimization procedure
        if self.__max_itr < 1 or self.__max_vars_to_use <= len(best_training.all_variables()):
            logging.info("Limit on training has been exceeded. Quit the loop now")
            self.__fixed_stock = []

        ### The ROC-AUC could be enlarged. Let's continue adding variables
        elif best_ancestor == None or best_training.training_performance() >= best_ancestor.training_performance():
            logging.info("Adding a new variable lead to a better performance:")
            self.__fixed_stock = best_training.all_variables()
        ### At least the  quality of the training was improved by the last variable
        elif best_training.training_quality() >= best_ancestor.training_quality():
            logging.info("Although we did not manage to increase the ROC-Curve we improved the quality a bit")
            self.__fixed_stock = best_training.all_variables()
        else:
            logging.info("We did not had any improvement at all. Let's try something different")
            #### Continue with the assumption that the two last variables actually brought an improvement
            #### kick one of the established variables and continue the training
            if len(best_training.all_variables()) > len(best_ancestor.all_variables()):
                shuf_vars = [i for i in best_ancestor.established_variables()]
                random.shuffle(shuf_vars)
                shuf_vars.pop()
                self.__fixed_stock = sorted(shuf_vars + [best_ancestor.test_variable(), best_training.test_variable()])

            else:
                ### Obtain the trainings which are at the same level than ours
                ancestors_samelevel = []
                for d in range(len(ancestors) - 1):
                    vars_in_itr = [int(v) for v in ancestors[-d].split()]
                    if len(vars_in_itr) == len(best_training.all_variables()): ancestors_samelevel += vars_in_itr
                    else: break

                if len(ancestors_samelevel) < self.__trials_at_same_lvl:
                    #### Obtain any performant variable configuration
                    fat_set = []
                    for a in ancestors_samelevel:
                        fat_set += a
                    ClearFromDuplicates(fat_set)
                    while len(fat_set) > best_training.all_variables():
                        random.shuffle(fat_set)
                        fat_set.pop()
                    self.__fixed_stock = sorted(fat_set)
                else:
                    logging.info("needs to be implemented")
        self.__vars_to_test = sorted(ClearFromDuplicates([v for v in self.__vars_to_test if v not in self.__fixed_stock]))
        if best_ancestor: printSummary(best_training, best_ancestor, self.train_variables())
        self.fillDocumentation(train_to_eval)

    def fillDocumentation(self, trainings):
        if len(trainings) == 0:
            logging.error("No trainings for documentation there")
            exit(1)

        Documentation = [
            FillWhiteSpaces(50, "/-*+"),
            "Event topology:                  %s" % (trainings[0].event_selection()),
            "Model:                           %s" % (trainings[0].train_model()),
            "Iteration:                       %s" % (self.iteration() - 1),
            "Established variables:           %s" % (" ".join(["%d" % (i) for i in trainings[0].established_variables()]))
        ] + ["    --- %s" % (self.train_variables()[i][0]) for i in trainings[0].established_variables()] + [
            "Variables tried:                 %d" %
            (len([i for i in range(len(self.train_variables())) if i not in trainings[0].established_variables()])),
            "Trainings succeeded:             %d" % (len(trainings)),
            "Variables in next iteration:     %s" % (" ".join(["%d" % (i) for i in self.fixed_stock()])),
        ] + ["    +++ %s" % (self.train_variables()[i][0]) for i in self.fixed_stock()] + [
            FillWhiteSpaces(50, "/-*+"),
        ]
        for t in trainings:
            Documentation += [
                "\n" + FillWhiteSpaces(40, "@"),
                "    Tested variable:                      %s(%d)" % (vars_to_use[t.test_variable()][0], t.test_variable()),
                "    Importance (Ranking):                 %f(%d)" % (t.importance(), t.ranking()),
                "    Training config:                      %s" % (t.train_cfg()),
                "    Training location:                    %s" % (t.train_dir()),
                "    Training events (background/signal):  %d/%d" % (t.train_Bkg_events(), t.train_Sig_events()),
                "    Testing events (background/signal):   %d/%d" % (t.test_Bkg_events(), t.test_Sig_events()),
                "    Default sample:",
                "     --- ROC-AUC (training/testing):      %f/%f" % (t.ROCAUC_train_default(), t.ROCAUC_test_default()),
                "     --- Kolmogorov background/signal:    %f/%f" % (t.kolmogorov_Bkg_default(), t.kolmogorov_Sig_default()),
                "     --- Maximum significance:            %f (MVA>= %f)" % (t.max_signficance_default(), t.MVA_score_cut_default()),
                "    Cross validation sample:",
                "     --- ROC-AUC (training/testing):      %f/%f" % (t.ROCAUC_train_valid(), t.ROCAUC_test_valid()),
                "     --- Kolmogorov background/signal:    %f/%f" % (t.kolmogorov_Bkg_valid(), t.kolmogorov_Sig_valid()),
                "     --- Maximum significance:            %f (MVA>= %f)\n" % (t.max_signficance_valid(), t.MVA_score_cut_valid()),
                "    ---> Final training quality:          %f" % (t.training_quality()),
                FillWhiteSpaces(40, "@"),
            ]
        WriteList(Documentation, "%s/Iteration_%d.txt" % (trainings[0].log_dir(), self.iteration() - 1))


def printSummary(best_train, train_to_compare, vars_to_use):
    logging.info(FillWhiteSpaces(50, "+-*/"))
    logging.info("Next iteration done to find the best training variables for %s" % (best_train.event_selection()))
    logging.info("These variables have been already established ")
    for i in best_train.established_variables():
        logging.info("    --- %s" % (vars_to_use[i][0]))
    logging.info("This candidate performs best under the remaining variables %s" % (vars_to_use[best_train.test_variable()][0]))
    logging.info("  -=-    ROC-AUC: %.4f" % (best_train.ROCAUC_train_default()))
    logging.info("  -=-   deltaROC: %.4f" % (best_train.delta_ROCAUC_default()))
    logging.info("  -=- Kolmogorov: %.4f" % (best_train.kolmogorov_default()))
    logging.info(" --> Final score: %.6f" % (best_train.training_quality()))

    if train_to_compare:
        logging.info("\n For comparison these are the results from the last round where %s joined the club" %
                     (vars_to_use[train_to_compare.test_variable()][0]))
        logging.info("  -=-    ROC-AUC: %.4f" % (train_to_compare.ROCAUC_train_default()))
        logging.info("  -=-   deltaROC: %.4f" % (train_to_compare.delta_ROCAUC_default()))
        logging.info("  -=- Kolmogorov: %.4f" % (train_to_compare.kolmogorov_default()))
        logging.info(" --> Final score: %.6f" % (train_to_compare.training_quality()))
    logging.info(FillWhiteSpaces(50, "/-*+"))


def setupParser():
    Event_Selections = "XAMPPplotting/RunConf/LepHadStau/MVA/Regions/"

    Region_Dir = ResolvePath(Event_Selections)
    parser = SetupSubmitParser()
    parser.set_defaults(BaseFolder="/ptmp/mpp/%s/LepHadStau_VariableScan/" % (os.getenv("USER")))
    parser.add_argument("--iteration", help="Specify the iteration. Only an internatl variable", default=0, type=int)
    parser.add_argument("--OptimizationTime", help="How much time has this script here time", default="01:59:59")
    parser.add_argument("--OptimizationVMEM", help="What's the virtual memory", default=2000, type=int)
    parser.add_argument("--variableSeed",
                        help="Which three out of the existing variables should be choosen to start with",
                        type=int,
                        nargs='+',
                        default=[i for i in range(4)])
    parser.add_argument("--maxVariables",
                        help="How many variables should  be used in the end",
                        type=int,
                        default=len(standardVariables()) - 3)
    parser.add_argument("--EventSelections",
                        help="Which event selections should be tested",
                        nargs="+",
                        default=[Event_Selections + s for s in os.listdir(Region_Dir) if os.path.isfile(Region_Dir + s)])

    parser.add_argument("--minImportance",
                        help="What is the minimal importance of the variable to test in order to continue",
                        type=float,
                        default=0.05)
    parser.add_argument("--MVAtoUse", help="Which setting of hyper parameters should be used in the end", default="BDT")
    parser.add_argument("--maxIterations", help="How many iterations do we need at maximum", type=int, default=30)

    parser.add_argument("--HistoExec", help="Executable for significance testing", default="WriteHistFitterHistos")
    parser.add_argument("--TestOnBackground",
                        help="What background model should be used to evalaute the significance test",
                        default="XAMPPtmva/InputConf/LepHadStau/Backgrounds/DD_Fakes_PowHeg.conf")
    parser.add_argument("--ReferenceSignal",
                        help="What signal model should be used to evalaute the significance test",
                        default=["XAMPPplotting/InputConf/LepHadStau/MVA/DD_Fakes/StauStau_160_1.conf"],
                        nargs="+")
    parser.add_argument("--lumi", help="Luminosity to use for the significane test", default=140., type=float)
    parser.add_argument("--relBkgUncertainty", help="Relative background uncertainty to apply", default=0.3, type=float)
    parser.add_argument("--sustainAtSameLevel",
                        help="How many iterations shall be tried to shuffle the variables if the quality is stuck",
                        default=3,
                        type=int)

    ### Runtime of the training itself
    parser.set_defaults(BuildTime="06:59:59")
    parser.set_defaults(RunTime="01:59:59")
    parser.set_defaults(RunHDF5="01:59:59")
    parser.set_defaults(RunCOV="01:59:59")
    parser.set_defaults(RunDiag="01:59:59")
    parser.set_defaults(vmem=4500)
    parser.set_defaults(noCommonMetaData=False)
    return parser


def IsArgumentDefault(Arg, Name, Parser):
    if Parser == None: return False
    return Arg == Parser.get_default(Name)


if __name__ == "__main__":

    parser = setupParser()
    options = parser.parse_args()
    ### Setup the engine for submisison
    cluster_engine = setup_engine(options)
    ### Check first if there is a valid training model
    if len(options.HDF5config) > 1:
        logging.error(
            "You've given more than one HDF5 config to the script. This is not supported because this config is there to setup the most basic prerequsites"
        )
        exit(1)

    mva_config = ResolvePath(options.HDF5config[0])

    if not mva_config:
        logging.error("Please give a valid HDF5 config via --HDF5config.")
        exit(1)
    ### We do not want to read anything from the FileHandler
    #### -> diable its message service
    GetFileHandler().switchOffMsg()
    for t, train_model in enumerate(options.inputConf):
        if not ResolvePath(train_model):
            logging.error("Please parse a valid training model via --inputConf option")
            exit(1)

        ### Set of variables we can use for training
        vars_to_use = standardVariables()
        max_vars = len(vars_to_use)

        var_optimizer = SciKitVariableOptimizer(
            cluster_engine=cluster_engine,
            variables=standardVariables(),
            selections=options.EventSelections,
            mva_config=mva_config,
            max_iterations=options.maxIterations,
            max_vars_to_use=-1,
            seeds=[i for i in options.variableSeed if i < max_vars],
            vars_to_test=[i for i in range(max_vars) if i not in options.variableSeed],
            iteration=options.iteration,
            trainModel=train_model,
            mva_method=options.MVAtoUse,
            lumi=options.lumi,
            bkg_uncert=options.relBkgUncertainty,
            reference_signal=options.ReferenceSignal[t],
            reference_bkg=options.TestOnBackground,
        )

        ### we've assembled the train_cfg files
        if not var_optimizer.prepare_config_files(): exit(1)

        ### Submit the trainings themselves to the cluster
        train_session = SciKitSubmit(
            cluster_engine=cluster_engine,
            input_cfgs=[train_model],
            train_cfgs=var_optimizer.HDF5_configs(),
            hold_jobs=options.HoldJob,
            # Timing information
            runTime=options.RunTime,
            runMem=options.vmem,
            HDF5Time=options.RunHDF5,
            HDF5mem=options.vmemHDF5,
            COVTime=options.RunCOV,
            COVmem=options.vmem,
            DiagTime=options.RunDiag,
            Diagmem=options.vmemDiag,
            PlotTime=options.RunPlots,
            Plotmem=options.vmemDiag,
            isRegression=options.IsRegression,
            submit_meta_jobs=options.noCommonMetaData)
        if not train_session.submit(): exit(1)
        train_names = ReadListFromFile(train_session.tmp_file_name_cfg())
        evaluation_names = []
        evaluation_run_cfgs = []
        evaluation_in_cfgs = []

        ### For proper evaluation we need to copy everything first
        for i, t in enumerate(var_optimizer.scanners()):
            sub_job_name = train_session.get_submitted_hashes()[i]
            if not cluster_engine.submit_copy_job(
                    hold_jobs=[cluster_engine.subjob_name("plotting-%s" % (sub_job_name))],
                    to_copy=[
                        "%s/%s/%s" % (cluster_engine.out_dir(), train_names[i], f) for f in [
                            "Covariance_Matrix.pkl",
                            "correlation_data",
                            "output_scikit_trainCmds.txt",
                            "DiagnosticPlots",
                            #"output_scikit.H5",
                            "uncorrelated_datasets",
                            "Trainings",
                            "output_scikit_diagnostic_paths.txt"
                        ]
                    ],
                    destination=t.train_dir(),
            ):
                exit(1)
            if not var_optimizer.signal_eval() or not var_optimizer.background_eval(): continue
            evaluation_names += [t.Sig_app_file(), t.Bkg_app_file()]
            evaluation_in_cfgs += [var_optimizer.signal_eval(), var_optimizer.background_eval()]
            evaluation_run_cfgs += [t.run_cfg(), t.run_cfg()]

        ### Save all evaluation configurations to disks
        if len(evaluation_names) > 0:
            eval_out_file = "%s/EvalFileNames_%s.conf" % (cluster_engine.config_dir(), id_generator(15))
            eval_cfg_file = "%s/JobConfigs_%s.conf" % (cluster_engine.config_dir(), id_generator(15))
            eval_in_file = "%s/InputConfigs_%s.conf" % (cluster_engine.config_dir(), id_generator(15))
            WriteList(evaluation_names, eval_out_file)
            WriteList(evaluation_run_cfgs, eval_cfg_file)
            WriteList(evaluation_in_cfgs, eval_in_file)

        ### Submit the evalutation of the training to the cluster
        if not cluster_engine.submit_array(script="XAMPPplotting/exeOnBatch.sh",
                                           mem=options.OptimizationVMEM,
                                           run_time=options.OptimizationTime,
                                           hold_jobs=[cluster_engine.subjob_name("Copy")],
                                           env_vars=[
                                               ("HistoCfg", var_optimizer.scanners()[0].histo_cfg()),
                                               ("JobCfg", eval_cfg_file),
                                               ("OutCfg", eval_out_file),
                                               ("InCfg", eval_in_file),
                                               ("Exec", options.HistoExec),
                                           ],
                                           array_size=len(evaluation_names),
                                           sub_job="TrainEval"):
            exit(1)

    ### Finally submit the next iteration
    for m, in_cfg in enumerate(options.inputConf):
        model = in_cfg[in_cfg.rfind("/") + 1:in_cfg.rfind(".")]
        for ev in options.EventSelections:
            channel = ev[ev.rfind("/") + 1:ev.rfind(".")]
            ### If we've more than one event selection
            ### then this job is the first iteration.
            new_jname = options.jobName
            if len(options.inputConf) > 1 or len(options.EventSelections) > 1:
                if len(options.inputConf) > 1: new_jname += model
                if len(options.EventSelections) > 1: new_jname += channel
            if var_optimizer.iteration() > 0:
                new_jname = "%s_%d" % (new_jname[:new_jname.rfind("_")], var_optimizer.iteration() + 1)
            else:
                new_jname += "_%d" % (var_optimizer.iteration() + 1)
            ### Cast the string arguments
            args = [
                " --%s %s" % (arg, getattr(options, arg))
                for arg in vars(options) if not IsArgumentDefault(getattr(options, arg), arg, parser)
                and not isinstance(getattr(options, arg), list) and not isinstance(getattr(options, arg), bool)
            ] + [  ### Then the lists
                " --%s %s" % (arg, " ".join([str(l) for l in getattr(options, arg)]))
                for arg in vars(options) if not IsArgumentDefault(getattr(options, arg), arg, parser)
                and isinstance(getattr(options, arg), list) and len(getattr(options, arg)) > 0
            ] + [  ### Finally the switches
                "--%s" % (arg) for arg in vars(options)
                if isinstance(getattr(options, arg), bool) and not IsArgumentDefault(getattr(options, arg), arg, parser)
            ] + [
                ### Overwrite one option or the other
                "--iteration %d" % (var_optimizer.iteration() + 1),
                "--noBuildJob",
                "--jobName %s" % (new_jname),
                "--variableSeed %s" % (" ".join(["%d" % (k) for k in var_optimizer.fixed_stock()])),
            ] + (["--EventSelections %s" % (ev)] if len(options.EventSelections) > 1 else
                 []) + (["--inputConf %s" %
                         (in_cfg), "--ReferenceSignal " + options.ReferenceSignal[m]] if len(options.inputConf) > 1 else [])

            submit_cmd = "python %s %s" % (cluster_engin.link_to_copy_area("XAMPPtmva/scripts/LepHadStau/variableScan.py"), " ".join(args))
            ### Time for the next iteration...
            cmd_list = "%s/%s.txt" % (cluster_engine.config_dir(), id_generator(43))
            WriteList([submit_cmd], cmd_list)
            cluster_engine.submit_array(script="ClusterSubmission/Run.sh",
                                        mem=options.OptimizationVMEM,
                                        env_vars=[("ListOfCmds", cmd_list)],
                                        hold_jobs=[cluster_engine.subjob_name("Copy"),
                                                   cluster_engine.subjob_name("TrainEval")],
                                        run_time=options.OptimizationTime,
                                        array_size=1,
                                        sub_job="NextItr")
