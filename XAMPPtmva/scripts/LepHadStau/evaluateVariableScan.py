import os, ROOT, math, argparse, threading, random
import os, random, argparse, time, random
from ClusterSubmission.Utils import WriteList, ResolvePath, ExecuteThreads, CreateDirectory, id_generator, ReadListFromFile, FillWhiteSpaces, ClearFromDuplicates
from XAMPPplotting.FileStructureHandler import GetFileHandler
from XAMPPtmva.MVAOptimizationHelpers import SciKitVariableScanner
from XAMPPplotting.PlotUtils import PlotUtils
from variableScan import standardVariables, setupParser
import numpy as np

if __name__ == "__main__":

    parser = setupParser()
    options = parser.parse_args()

    ROOT.gROOT.SetBatch(True)
    ROOT.gROOT.Macro("rootlogon.C")
    ROOT.gROOT.SetStyle("ATLAS")

    GetFileHandler().switchOffMsg()
    vars_to_use = standardVariables()
    max_vars = len(vars_to_use)
    for ev in options.EventSelections:
        selection = ev[ev.rfind("/") + 1:ev.rfind(".")]
        LOG_DIR = "%s/Log_Files/%s/" % (options.BaseFolder, selection)
        if not os.path.isfile("%s/Best_Combinations.txt" % (LOG_DIR)):
            print "WARNING: %s is not from the variale scan" % (LOG_DIR)
            continue
        Best_Comb = ReadListFromFile("%s/Best_Combinations.txt" % (LOG_DIR))
        ### The rule is that the last number corresponds to the tested variable
        trainings = []

        for iteration in Best_Comb:
            itr = [int(i) for i in iteration.split()]
            mva_scanner = SciKitVariableScanner(
                in_cfg=options.inputConf[0],
                base_dir=options.BaseFolder,
                RunCfg=ev,
                MVA=options.MVAtoUse,
                bkg_model_to_test=options.TestOnBackground,
                sig_model_to_test=options.ReferenceSignal,
                lumi=options.lumi,
                topology=selection,
                MVA_TrainCfg="XAMPPtmva/TrainingConf/LepHadStau/TrainConf.conf",  ### What's the MVA training Cfg to use
                set_of_vars=vars_to_use,
                test_var=itr.pop(),
                established_vars=itr,
                bkg_uncert=options.relBkgUncertainty)

            mva_scanner.evaluate_scan()
            trainings += [mva_scanner]

        #ExecuteThreads(trainings)
        ### Check that we've actually found something
        if len(trainings) == 0:
            print "WARNING: Where are my trainings"
            continue
        #### Setup the basic histograms
        print "INFO: Found %d trainings" % (len(trainings))
        ROC_AUC_train = ROOT.TH1D(id_generator(10), "Evaluation of the scan", len(trainings), 0, len(trainings))
        for i, trial in enumerate(trainings):
            ROC_AUC_train.GetXaxis().SetBinLabel(i + 1, "%s (%d)" % (vars_to_use[trial.test_variable()][0], trial.nVars()))

        ROC_AUC_test = ROC_AUC_train.Clone(id_generator(10))
        Kolmog = ROC_AUC_train.Clone(id_generator(10))
        Quality = ROC_AUC_train.Clone(id_generator(10))
        Performance = ROC_AUC_train.Clone(id_generator(10))
        Significance = ROC_AUC_train.Clone(id_generator(10))

        ROC_AUC_train.GetYaxis().SetTitle("ROC-auc (training)")
        ROC_AUC_test.GetYaxis().SetTitle("ROC-auc (testing)")
        Kolmog.GetYaxis().SetTitle("Kolmogorov score")
        Quality.GetYaxis().SetTitle("Quality")
        Significance.GetYaxis().SetTitle("Expected significance")
        for i, trial in enumerate(trainings):
            print ROC_AUC_train.GetXaxis().GetBinLabel(i + 1)
            ROC_AUC_train.SetBinContent(i + 1, trial.ROCAUC_train_default())
            ROC_AUC_test.SetBinContent(i + 1, trial.ROCAUC_test_default())
            Kolmog.SetBinContent(i + 1, trial.kolmogorov_default())
            Quality.SetBinContent(i + 1, trial.training_quality())
            Performance.SetBinContent(i + 1, trial.training_performance())
            Significance.SetBinContent(i + 1, trial.max_signficance_valid())

        plot_names = ["ROC_train", "ROC_test", "Kolmogorov", "Quality", "Performance", "Significance"]

        for i, histo in enumerate([ROC_AUC_train, ROC_AUC_test, Kolmog, Quality, Performance, Significance]):
            pu = PlotUtils(status="Internal", size=24)

            pu.Prepare1PadCanvas(id_generator(5), 800, 600)
            histo.GetXaxis().LabelsOption("d")

            histo.Draw("HIST")
            pu.saveHisto("VarScan_%s_%s" % (selection, plot_names[i]), ["pdf"])
