import sys, os, ROOT, logging
from ClusterSubmission.Utils import WriteList, ReadListFromFile, ResolvePath, RecursiveLS
from XAMPPplotting.FileUtils import ReadInputConfig

### configurable:
summary_points = {
    "LowMass": (0, 200),
    "MidMass": (200, 320),
    "HighMass": (320, 4000),
}

out_path = ResolvePath("XAMPPtmva/InputConf/LepHadStau/TrainModels/")
for vjets in ["PowHeg", "Sherpa"]:
    for fake in ["DD", "MC"]:
        bkg_cfg = "XAMPPtmva/InputConf/LepHadStau/Backgrounds/%s_Fakes_%s.conf" % (fake, vjets)
        if not ResolvePath(bkg_cfg): continue

        sig_cfg_path = "XAMPPplotting/InputConf/LepHadStau/MVA/%s_Fakes/" % (fake)
        sig_cfg_dir = ResolvePath(sig_cfg_path)
        if not sig_cfg_dir: continue

        signals = sorted([s for s in os.listdir(sig_cfg_dir) if s.find("Stau") == 0])

        Signal_Points = {"Inclusive": []}

        for s in signals:
            signal_name = s[s.find("/") + 1:s.rfind(".")]
            stau_mass = float(signal_name.split("_")[1].replace("p", "."))
            lsp_mass = float(signal_name.split("_")[2].replace("p", "."))
            Signal_Points["Inclusive"] += [s]
            try:
                Signal_Points[(stau_mass, lsp_mass)] += [s]
            except:
                Signal_Points[(stau_mass, lsp_mass)] = [s]

            try:
                Signal_Points[(stau_mass, -1)] += [s]
            except:
                Signal_Points[(stau_mass, -1)] = [s]

            for sr_name, rng in summary_points.iteritems():
                if stau_mass >= rng[0] and stau_mass < rng[1]:
                    try:
                        Signal_Points[sr_name] += [s]
                    except:
                        Signal_Points[sr_name] = [s]
        for sig_model, points in Signal_Points.iteritems():
            cfg_name = ""
            if isinstance(sig_model, str):
                cfg_name = "Combined_%s" % (sig_model)
            elif sig_model[1] < 0:
                if len(points) < 2: continue
                cfg_name = "Combined_%.0f" % (sig_model[0])
            else:
                cfg_name = "Stau_%.0f_%.0f" % (sig_model)
            WriteList(["Import %s/%s" % (sig_cfg_path, s)
                       for s in points] + ["Import %s" % (bkg_cfg), "SampleName %s" % (cfg_name)],
                      "%s/%s_Fakes/%s_%s.conf" % (out_path, fake, cfg_name, vjets))
