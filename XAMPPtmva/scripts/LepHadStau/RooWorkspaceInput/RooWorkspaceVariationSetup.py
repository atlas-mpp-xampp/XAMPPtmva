import RooWorkspaceVariations as RWVS


#Note: check on MVAtesting files
# Signal:  python print_root_content.py --file /ptmp/mpp/zenon/Cluster/OUTPUT/2019-03-09/Test_MuTau_SR_0jetA_SysOn_2019_03_09_21_52_57/StauStau_200_1.root
# Bkg: python print_root_content.py --file /ptmp/mpp/zenon/Cluster/OUTPUT/2019-03-09/Test_MuTau_SR_0jetA_SysOn_2019_03_09_21_52_57/Sherpa221_Wjets.root
#
def get_variations():

    variations = RWVS.Variations()
    ### nom
    variations.add("Nominal", "nominal")
    ### e/gamma
    variations.add_double_sided("EG_RESOLUTION_ALL", "egamma_reso")
    variations.add_double_sided("EG_SCALE_AF2", "egamma_atlfast")  #AF2 & Fullsim
    variations.add_double_sided("EG_SCALE_ALL", "egamma_scale")
    ### electron
    variations.add_double_sided("EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR", "ele_id")
    variations.add_double_sided("EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", "ele_iso")
    variations.add_double_sided("EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", "ele_reco")
    variations.add_double_sided("EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR", "ele_trig")
    ### flavor
    variations.add_double_sided("FT_EFF_B_systematics", "flavor_b_sys")
    variations.add_double_sided("FT_EFF_C_systematics", "flavor_c_sys")
    variations.add_double_sided("FT_EFF_Light_systematics", "flavor_light_sys")
    variations.add_double_sided("FT_EFF_extrapolation", "flavor_extrap")
    variations.add_double_sided("FT_EFF_extrapolation_from_charm", "flavor_c_extrap")
    ### jets
    variations.add_double_sided("JET_BJES_Response", "jet_bjes")
    variations.add_double_sided("JET_EffectiveNP_1", "jet_np1")
    variations.add_double_sided("JET_EffectiveNP_2", "jet_np2")
    variations.add_double_sided("JET_EffectiveNP_3", "jet_np3")
    variations.add_double_sided("JET_EffectiveNP_4", "jet_np4")
    variations.add_double_sided("JET_EffectiveNP_5", "jet_np5")
    variations.add_double_sided("JET_EffectiveNP_6", "jet_np6")
    variations.add_double_sided("JET_EffectiveNP_7", "jet_np7")
    variations.add_double_sided("JET_EffectiveNP_8restTerm", "jet_np8")
    variations.add_double_sided("JET_EtaIntercalibration_Modelling", "jet_model")
    variations.add_double_sided("JET_EtaIntercalibration_NonClosure_highE", "jet_nonclosure_high_e")
    variations.add_double_sided("JET_EtaIntercalibration_NonClosure_negEta", "jet_nonclosure_neg_eta")
    variations.add_double_sided("JET_EtaIntercalibration_NonClosure_posEta", "jet_nonclosure_pos_eta")
    #variations.add_double_sided("JET_EtaIntercalibration_TotalStat", "jet_tot_stat") #missing in top 2019-03-13 V19
    variations.add_double_sided("JET_Flavor_Composition", "jet_flavor_comp")
    variations.add_double_sided("JET_Flavor_Response", "jet_flavor_resp")

    variations.add_double_sided("JET_JER_DataVsMC", "jet_jer_data_vs_mc")
    variations.add_double_sided("JET_JER_EffectiveNP_1", "jet_np1")
    variations.add_double_sided("JET_JER_EffectiveNP_2", "jet_np2")
    variations.add_double_sided("JET_JER_EffectiveNP_3", "jet_np3")
    variations.add_double_sided("JET_JER_EffectiveNP_4", "jet_np4")
    variations.add_double_sided("JET_JER_EffectiveNP_5", "jet_np5")
    variations.add_double_sided("JET_JER_EffectiveNP_6", "jet_np6")
    variations.add_double_sided("JET_JER_EffectiveNP_7restTerm", "jet_np7")

    variations.add_double_sided("JET_JvtEfficiency", "jet_jvt")
    variations.add_double_sided("JET_Pileup_OffsetMu", "jet_pu_mu")
    variations.add_double_sided("JET_Pileup_OffsetNPV", "jet_pu_npv")
    variations.add_double_sided("JET_Pileup_PtTerm", "jet_pu_pt")
    variations.add_double_sided("JET_Pileup_RhoTopology", "jet_pu_rho")
    variations.add_double_sided("JET_PunchThrough_AFII", "jet_punch_through_af2")  #AF2 only new
    variations.add_double_sided("JET_PunchThrough_MC16", "jet_punch_through_mc16")  #Fullsim only new
    variations.add_double_sided("JET_RelativeNonClosure_AFII", "jet_rel_non_closure_af2")  #AF2 only new
    variations.add_double_sided("JET_SingleParticle_HighPt", "jet_single_particle")
    variations.add_double_sided("JET_fJvtEfficiency", "jet_fjvt")
    ### met
    variations.add_one_sided_up("MET_SoftTrk_ResoPara", "met_resopara", True)
    variations.add_one_sided_up("MET_SoftTrk_ResoPerp", "met_resoperp", True)
    variations.add_one_sided_down("MET_SoftTrk_ScaleDown", "met_scale", True)
    variations.add_one_sided_up("MET_SoftTrk_ScaleUp", "met_scale", True)
    ### muons weight
    variations.add_double_sided("MUON_EFF_ISO_STAT", "mu_iso_stat")
    variations.add_double_sided("MUON_EFF_ISO_SYS", "mu_iso_sys")
    variations.add_double_sided("MUON_EFF_RECO_STAT_LOWPT", "mu_reco_lowpt_stat")
    variations.add_double_sided("MUON_EFF_RECO_STAT", "mu_reco_stat")
    variations.add_double_sided("MUON_EFF_RECO_SYS_LOWPT", "mu_reco_lowpt_sys")
    variations.add_double_sided("MUON_EFF_RECO_SYS", "mu_reco_sys")
    variations.add_double_sided("MUON_EFF_TTVA_STAT", "mu_ttva_stat")
    variations.add_double_sided("MUON_EFF_TTVA_SYS", "mu_ttva_sys")
    variations.add_double_sided("MUON_EFF_TrigStatUncertainty", "mu_trig_stat")
    variations.add_double_sided("MUON_EFF_TrigSystUncertainty", "mu_trig_sys")
    ## muons kinematic
    variations.add_double_sided("MUON_ID", "mu_id")
    variations.add_double_sided("MUON_MS", "mu_ms")
    variations.add_double_sided("MUON_SAGITTA_RESBIAS", "mu_sagitta_resbias")
    variations.add_double_sided("MUON_SAGITTA_RHO", "mu_sagitta_rho")
    variations.add_double_sided("MUON_SCALE", "mu_scale")
    ### PU
    variations.add_double_sided("PRW_DATASF", "prw")

    ### taus
    variations.add_double_sided("TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU", "tau_trueele_eleolr_stat_highmu")
    variations.add_double_sided("TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU", "tau_trueele_eleolr_stat_lowmu")
    variations.add_double_sided("TAUS_TRUEELECTRON_EFF_ELEOLR_SYST", "tau_trueele_eleolr_stat_syst")

    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL", "tau_eleolr_true_had")

    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025", "tau_id_1p_2025")
    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530", "tau_id_1p_2530")
    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040", "tau_id_1p_3040")
    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40", "tau_id_1p_ge40")
    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030", "tau_id_3p_2030")
    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30", "tau_id_3p_ge30")
    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_JETID_HIGHPT", "tau_id_high_pt")
    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_JETID_SYST", "tau_id_syst")

    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_RECO_HIGHPT", "tau_reco_highpt")
    variations.add_double_sided("TAUS_TRUEHADTAU_EFF_RECO_TOTAL", "tau_reco_total")

    variations.add_double_sided("TAUS_TRUEHADTAU_SME_TES_DETECTOR", "tau_tes_det")
    variations.add_double_sided("TAUS_TRUEHADTAU_SME_TES_INSITU", "tau_tes_insitu")
    variations.add_double_sided("TAUS_TRUEHADTAU_SME_TES_MODEL", "tau_tes_model")

    return variations
