class Asimov(object):
    """
    Asimov model
    """
    def __init__(self, Input=None, Empty=False):
        #basic
        self.__input = Input
        self.__empty = Empty
        self.__asimov_histo = self.get_histo()
        #auxiliary
        self.__is_primary_region = self.__input.is_primary_region if self.__input else False
        self.__is_subsidiary_region = self.__input.is_subsidiary_region if self.__input else False
        self.__asimov_subfolder_final_name = "Data"
        self.__asimov_folder_name = self.__input.folder_name if self.__input else ""
        self.__asimov_subfolder_name = self.__input.subfolder_name if self.__input else ""
        self.__asimov_histo_name = self.__input.histo_name if self.__input else ""
        self.__identity = self.__asimov_folder_name + "_" + self.__asimov_histo_name
        self.__container_names = [self.__input.subfolder_name] if self.__input else []
        self.__container_integrals = [self.__input.histogram.Integral()] if self.__input else []
        self.__region = self.__input.region if self.__input else ""

    def get_histo(self):
        if self.__empty:  #be silent
            return None

        if not self.__input:
            print "Error: Asimov has null input in order to obtain histo..."
            return None

        if not self.__input.histogram:
            print "Error: Asimov - Cannot obtain histo for input: ", self.__input.identity
            return None

        return self.__input.histogram.Clone()

    def __iadd__(self, other):
        if self.__asimov_histo:
            self.__asimov_histo.Add(other.histogram, 1.)
        else:
            self.__asimov_histo = other.histogram
        return self

    @property
    def region(self):
        return self.__region

    @property
    def is_primary_region(self):
        return self.__is_primary_region

    @property
    def is_subsidiary_region(self):
        return self.__is_subsidiary_region

    @property
    def folder_subfolder_histo_name(self):
        return self.__asimov_folder_name, \
            self.__asimov_subfolder_final_name, \
            self.__asimov_histo_name

    @property
    def summary(self):
        report = "Asimov model: %s\n" % (self.identity)

        for counter, name in enumerate(self.__container_names):
            report += "%s %f\n" % (name, self.__container_integrals[counter])

        report += "%s %f\n" % ("Total", self.__asimov_histo.Integral())

        return report

    def __str__(self):
        return self.identity

    @property
    def identity(self):
        return self.__identity

    @property
    def histogram(self):
        return self.__asimov_histo

    @property
    def clone(self):
        return self.__asimov_histo.Clone("clone_" + self.__asimov_histo.GetName())

    def Clone(self, tag="clone"):
        return self.__asimov_histo.Clone(tag + self.__asimov_histo.GetName())

    def equal(self, _input):
        return self.__asimov_folder_name == _input.folder_name \
            and self.__asimov_histo_name == _input.histo_name

    def integral(self):
        return self.__asimov_histo.Integral()

    def update(self, _input=None):
        if not _input:
            print "Error: cannot handle null input in Asimov..."
            return False

        if not _input.histogram:
            print "Error: cannot handle null input histo in Asimov of input %s" % (_input.identity)
            return False

        if not self.__asimov_histo:
            print "Error: cannot add histo to null histo in Asimov of input %s" % (_input.identity)
            return False

        self.__asimov_histo.Add(_input.histogram, 1.0)
        self.__container_names.append(_input.subfolder_name)
        self.__container_integrals.append(_input.histogram.Integral())

        return True


class AsimovRegion(object):
    """
    Asimov model for a given region.
    Combines Asimov samples together.
    """
    def __init__(self, region=""):
        #basic
        self.__region = region
        self.__asimov_signal = Asimov(Empty=True)
        self.__asimov_bkg = Asimov(Empty=True)
        self.__is_primary_region = False
        self.__is_subsidiary_region = False

    def __repr__(self):
        return str(self.__region)

    @property
    def asimov_signal(self):
        return self.__asimov_signal

    @property
    def asimov_bkg(self):
        return self.__asimov_bkg

    @property
    def region(self):
        return self.__region

    @property
    def is_primary_region(self):
        return self.__is_primary_region

    @property
    def is_subsidiary_region(self):
        return self.__is_subsidiary_region

    def signal_events(self):
        return self.__asimov_signal.integral()

    def bkg_events(self):
        return self.__asimov_bkg.integral()

    def update_region(self, asimov=None):

        if not self.__region:
            self.__region = asimov.region
            self.__is_primary_region = asimov.is_primary_region
            self.__is_subsidiary_region = asimov.is_subsidiary_region

        else:
            if asimov.region != self.__region:
                print "Error: AsimovRegion conflicting regions %s with existing %s" % (asimov.region, self.__region)
                return False

        return True

    def add_signal(self, asimov=None):
        if not self.update_region(asimov):
            print "Error: AsimovRegion could not add Asimov %s to region %s" % (asimov.identity, self.__region)
            return False
        else:
            print "Info: AsimovRegion add signal asimov %s in region %s" % (asimov.identity, self.__region)
            self.__asimov_signal += asimov

        return True

    def add_bkg(self, asimov=None):
        if not self.update_region(asimov):
            print "Error: AsimovRegion could not add Asimov %s to region %s" % (asimov.identity, self.__region)
            return False
        else:
            print "Info: AsimovRegion add bkg asimov %s in region %s" % (asimov.identity, self.__region)
            self.__asimov_bkg += asimov

        return True
