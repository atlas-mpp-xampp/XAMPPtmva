import ROOT, os


class Store(object):
    """
    Class to store workspace structure
    """
    def __init__(
            self,
            pathname="RooWorkspaceInput",
            lumi=1.0,  # fb^-1
            do_monitor=False):
        self.__default_filename = "RooWorkspace"
        self.__lumi_pb = lumi * 1e3  # pb^-1
        self.__do_monitor = do_monitor
        #path
        self.__pathname = pathname.replace(" ", "").replace("/", "") + "/"
        self.__dir_created = self.create_dir()
        #RW root file
        self.__filename = self.__default_filename.replace(" ",
                                                          "") + "_input" + ("" if self.__default_filename.endswith(".root") else ".root")
        self.__rootfile = ROOT.TFile(self.__pathname + self.__filename, "RECREATE")
        #monitor root file
        self.__monitor_filename = self.__default_filename.replace(
            " ", "") + "_monitor" + ("" if self.__default_filename.endswith(".root") else ".root")
        self.__monitor_rootfile = ROOT.TFile(self.__pathname + self.__monitor_filename, "RECREATE") if do_monitor else None
        #log
        self.__log_list = []
        self.__log_name = self.__default_filename.replace(" ", "") + "_log" + ("" if self.__default_filename.endswith(".log") else ".log")
        self.__log_file = open(self.__pathname + self.__log_name, "w")
        #optimized bins root file
        self.__monitor_optimization_filename = self.__default_filename.replace(
            " ", "") + "_monitor_optimization" + ("" if self.__default_filename.endswith(".root") else ".root")
        self.__monitor_optimization_rootfile = ROOT.TFile(self.__pathname +
                                                          self.__monitor_optimization_filename, "RECREATE") if do_monitor else None

    def add_to_log(self, line=""):
        self.__log_list.append(line)

    def add_to_optimization(self, obj=None, name=""):
        if not obj:
            print "Error: cannot store null object %s..." % (name)
            return False

        if not issubclass(type(obj), ROOT.TObject):
            print "Error: cannot store non-TObject %s..." % (name)
            return False

        if not self.__monitor_optimization_rootfile:
            print "Error: file %s is null" (self.__monitor_optimization_filename)
            return False

        if not self.__monitor_optimization_rootfile.cd():
            print "Error: couldn't cd to file %s" (self.__monitor_optimization_filename)
            return False

        obj.SetName(name)
        obj.Write()

        return True

    def create_dir(self):
        if not os.path.exists(self.__pathname):
            os.makedirs(self.__pathname)
            return True

        return False

    def finalize(self):
        self.__rootfile.Save()
        self.__rootfile.Close()

        print "Directory:", self.__pathname

        print "Workspace:", self.__filename

        if self.__do_monitor:
            self.__monitor_rootfile.Save()
            self.__monitor_rootfile.Close()
            print "Monitor inputs:", self.__monitor_filename

        if self.__monitor_optimization_rootfile:
            self.__monitor_optimization_rootfile.Save()
            self.__monitor_optimization_rootfile.Close()
            print "Monitor optimization:", self.__monitor_filename

        self.__log_file.writelines("%s\n" % item for item in self.__log_list)
        print "Log file:", self.__log_name

    def is_directory(self, obj=None):
        return obj and issubclass(type(obj), ROOT.TObject) and "TDirectoryFile" in obj.IsA().GetName()

    def keep(self, _any_type=None, suffix=""):

        if not _any_type:
            print "Error: cannot store anything from null object"
            return False

        if not _any_type.histogram:
            print "Error: cannot store with a null  histogram"
            return False

        if not self.__monitor_rootfile:
            print "Error: Root file '%s' is not created" % (self.__monitor_filename)
            return False

        if not self.__monitor_rootfile.cd():
            print "Error: cannot enter root file '%s'" % (self.__monitor_filename)
            return False

        name = _any_type.identity + ("_" + suffix if suffix else "")
        _any_type.histogram.Write(name)

        return True

    def __common_store(self, histo=None, folder="", subfolder="", name="", histo_eps=1e-6):

        if not self.__rootfile.cd():
            print "Error: cannot enter root file '%s'" % (self.__filename)
            return False

        if not histo:
            print "Error: cannt store a null histogram for %s %s %s" % (folder, subfolder, name)
            return False
        else:
            print "Info: storing histogram for %s %s %s" % (folder, subfolder, name)

        integral = histo.Integral()
        sumofweights = histo.GetSumOfWeights()
        if integral < histo_eps or sumofweights < histo_eps:
            print "Warning: storing empty histogram %s %s %s: integral=%f, SumOfWeights=%f" % (folder, subfolder, name, integral,
                                                                                               sumofweights)

        if sumofweights < 0.:
            print "Warning: storing empty histogram %s %s %s with negative SumOfWeights=%f" % (folder, subfolder, name, sumofweights)

        if folder not in self.__rootfile.GetListOfKeys():
            self.__rootfile.mkdir(folder)

        Folder = self.__rootfile.Get(folder)

        if not Folder:
            print "Error: cannot enter directory: '{}'".format(folder)
            return False

        if subfolder not in Folder.GetListOfKeys():
            Folder.mkdir(subfolder)

        Subfolder = Folder.Get(subfolder)

        if not Subfolder:
            print "Error: cannot enter subdirectory: '{}'".format(subfolder)
            return False

        directory = self.__rootfile.GetDirectory("%s/%s" % (folder, subfolder))

        if not self.is_directory(directory):
            print "Error: failed to get path %s/%s" % (folder, subfolder)
            return False

        if not directory.IsWritable():
            print "Error: directory %s/%s is not writeable!" % (folder, subfolder)
            return False

        print "Info: storing object %s in %s/%s" % (name, folder, subfolder)
        #finally store the histo
        directory.WriteObject(histo, name)

        print "Info: storing object %s in %s/%s" % (name, folder, subfolder)

        return True

    def store(self, _input=None):

        if not _input:
            print "Error: cannot store with a null Input class object"
            return False

        histo = _input.histogram
        folder = _input.folder_name
        subfolder = _input.subfolder_name
        name = _input.histo_name

        return self.__common_store(histo, folder, subfolder, name)

    def store_as(self, _asimov=None, process=""):

        if not _asimov:
            print "Error: cannot store with a null Asimov  class object"
            return False

        histo = _asimov.histogram
        folder, subfolder, name = _asimov.folder_subfolder_histo_name

        #as per request
        subfolder = process

        return self.__common_store(histo, folder, subfolder, name)

        return True

    def override(self, _asimov=None):

        if not _asimov:
            print "Error: cannot store with a null Asimov  class object"
            return False

        histo = _asimov.histogram
        folder, subfolder, name = _asimov.folder_subfolder_histo_name

        if not self.__rootfile.cd():
            print "Error: cannot enter root file '%s'" % (self.__filename)
            return False

        if not histo:
            print "Error: cannot store a null histogram..."
            return False

        directory = self.__rootfile.GetDirectory("%s/%s" % (folder, subfolder))

        if not self.is_directory(directory):
            print "Error: Storing %s in %s/%s not possible" % (histo.GetName(), folder, subfolder)
            return False

        existing = directory.Get(name)

        if not existing:
            print "Error: Cannot find %s in %s/%s to override" % (histo.GetName(), folder, subfolder)
            return False
        else:
            print "Info: overriding %s %f (Real) with %s %f (Asimov) for %s" % (existing.GetName(), existing.Integral(), histo.GetName(),
                                                                                histo.Integral(), folder)

        directory.Delete("%s;*" % (name))
        directory.WriteObject(histo, name)

        return True

    def get_folders(self, dire=None):

        folders = []
        for key in dire.GetListOfKeys():
            if "TDirectoryFile" in key.GetClassName():
                folders.append(key)
        return folders

    def store_lumi(self):
        h = ROOT.TH1F("h", "", 1, 0, 1)
        h.Fill(0, self.__lumi_pb)

        if not self.__rootfile.cd():
            print "Error: cannot enter root file '%s' to store lumi..." % (self.__filename)
            return False

        for fold in self.get_folders(ROOT.gDirectory):
            if not ROOT.gDirectory.cd(fold.GetName()):
                print "Error: Cannot enter in {} in order to store the lumi plot..." % (fold.GetName())
                continue
            ROOT.gDirectory.WriteObject(h, "lumiininvpb")
            ROOT.gDirectory.cd("../")

        return True
