import ROOT, math


##
# @class Significance
#
class Significance(object):
    """
    Class providing different definitions for significance estimates
    """
    def __init__(self):
        pass

    def ZN(self, S=0., B=0., Er=0.):

        SpB = S + B
        Er2 = Er**2
        BpEr2 = B + Er2
        B2 = B**2

        T1 = SpB * math.log(SpB * BpEr2 / (B2 + SpB * Er2))
        T2 = B2 / Er2 * math.log(1 + Er2 * S / (B * BpEr2))

        return math.sqrt(2 * (T1 - T2)) if T1 > T2 else 0.

    def Zn(self, S=0., B=0., dBrel=0.):

        Zn = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(S, B, dBrel)

        return 0. if math.isinf(Zn) or math.isnan(Zn) else Zn

    def Asimov(self, s=0., b=0.):

        return 0. if b <= 0. else math.sqrt(2 * ((s + b) * math.log(1 + s / b) - s))

    def SoB(self, s=0., b=0.):

        return s / math.sqrt(b + b_err**2) if b > 0. else 0.

    def SoB_dBrel(self, s=0., b=0., db_rel=0.):

        b_err = db_rel * b

        return s / math.sqrt(b + b_err**2) if b > 0. else 0.
