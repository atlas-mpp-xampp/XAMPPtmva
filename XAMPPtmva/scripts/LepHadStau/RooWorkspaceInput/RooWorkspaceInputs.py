import ROOT
import copy
import RooWorkspaceAsimov as RWA
import RooWorkspaceInputHelper as RWIH
import RooWorkspacePrint as RWP
import RooWorkspaceBins as RWB

from enum import Enum


class Input(RWIH.InputHelper):
    """
    Cache input file and stuff.
    Tasks are done using the InputHelper.
    """
    def __init__(self,
                 filename="",
                 variation=None,
                 channel="",
                 region="",
                 primary_region=False,
                 subsidiary_region=False,
                 classifier="",
                 binning="",
                 lumi=1.0):
        RWIH.InputHelper.__init__(self)
        self.__filename = filename
        self.__variation = variation
        self.__corename = filename.split('/')[-1].replace(".root", "")
        self.__is_data = self.get_is_data(self.__corename)
        self.__is_signal = self.get_is_signal(self.__corename)
        self.__region = region
        self.__is_primary_region = primary_region
        self.__is_subsidiary_region = subsidiary_region
        #names
        self.__region_identifier = "SR" if self.__is_primary_region else "CR" if self.__is_subsidiary_region else "UnknownRegion"
        self.__foldername = self.get_folder_name(channel, self.__region_identifier, self.__region)
        self.__subfoldername = self.get_subfolder_name(self.__corename)
        self.__histoname = self.get_histo_name(variation)
        self.__identity = self.get_identity(self.__foldername, self.__subfoldername, self.__histoname)
        #histo
        self.__histo = self.get_histo(self.__filename, variation, channel, region, classifier, binning)
        self.__histo_status = self.check_histo_validity()
        #lumi
        self.__lumi_fb = lumi  # fb^-1
        self.__scaled = self.scale_histo()

    def update_identity(self):
        self.__identity = self.get_identity(self.__foldername, self.__subfoldername, self.__histoname)

    def check_histo_validity(self):
        if not self.__histo:
            print "Error: histogram %s is not valid..." % (self.__histoname)
            return False

        return True

    def scale_histo(self):
        """
        scale hsito to Lumi
        """
        if self.is_data:
            return True

        if not self.__histo:
            print "Error: cannot scale histogram %s which is null..." % (self.__histoname)
            return False
        else:
            self.__histo.Scale(self.__lumi_fb)

        return True

    def rebin(self, binning_results=RWB.BinningResults()):
        """
        Rebins input histogram based on binning array
        Adjust histogram axis to range with content 
        """
        if not binning_results.optimized:
            print "Error: rebin - no optimization achieved..."
            return False

        if not self.__histo:
            print "Error: rebin - histo is not valid BEFORE rebinning for input", self.indentity
            return False

        self.__histo = RWB.BinsOptimizer("optimized_bins", False).rebin_and_adjust_range(self.__histo, self.__identity, binning_results)

        if not self.__histo:
            print "Error: rebin - histo is not valid AFTER rebinning for input", self.identity
            return False

        #RWP.Print(self.__histo)

        return True

    def truncate(self, histo_side="", histo_bin=0):
        """
        Removes/truncates selected bins from histogram axis
        """

        if not self.__histo:
            print "Error: remove - histo is not valid BEFORE removing bins for input", self.indentity
            return False

        self.__histo = RWB.BinsOptimizer("remove_bins", False).truncate_bins(self.__histo, histo_side, histo_bin)

        if not self.__histo:
            print "Error: rebin - histo is not valid AFTER removing bins for input", self.identity
            return False

        return True

    def histo_is_valid(self):
        return self.__histo != None

    @property
    def region(self):
        return self.__region

    @property
    def is_primary_region(self):
        return self.__is_primary_region

    @property
    def is_subsidiary_region(self):
        return self.__is_subsidiary_region

    @property
    def variation(self):
        return self.__variation

    # Folder
    @property
    def folder_name(self):
        return self.__foldername

    @folder_name.setter
    def folder_name(self, value):
        self.__foldername = value
        self.update_identity()

    #Subfolder
    @property
    def subfolder_name(self):
        return self.__subfoldername

    @subfolder_name.setter
    def subfolder_name(self, value):
        self.__subfoldername = value
        self.update_identity()

    #Histo
    @property
    def histo_name(self):
        return self.__histoname

    @histo_name.setter
    def histo_name(self, value):
        self.__histoname = value
        self.update_identity()

    @property
    def histogram(self):
        return self.__histo

    @property
    def clone(self):
        return self.__histo.Clone("clone_" + self.__histo.GetName())

    def Clone(self, tag="clone"):
        return self.__histo.Clone(tag + self.__histo.GetName())

    @property
    def is_data(self):
        return self.__is_data

    @property
    def identity(self):
        return self.__identity

    @property
    def is_signal(self):
        return self.__is_signal


class RMethod(Enum):
    ALPHA = 1
    BETA = 2
    GAMMA = 3
    DELTA = 4

    def describe(self):
        # self is the member here
        return self.name, self.value

    def __str__(self):
        return 'RMethod {0} ({1})'.format(self.value, self.name)

    @classmethod
    def default(cls):
        # cls here is the enumeration
        return cls.ALPHA


class InputsList(object):
    """
    Cache input file and stuff in a list
    """
    def __init__(
            self,
            files=[],
            variations=None,
            channels=[],
            primary_regions=[],
            subsidiary_regions=[],
            classifier="",
            binning="",
            lumi=1.0,  #fb^-1, 
            asimov=False,
            hybrid=False,
            rebinning=False,
            rmethod=RMethod.default(),
            extra_regions=False,
            store=None):
        self.__files = files
        self.__variations = variations
        self.__channels = channels
        self.__primary_regions = set(primary_regions)
        self.__subsidiary_regions = set(subsidiary_regions)
        self.__regions = self.__primary_regions.union(self.__subsidiary_regions)
        self.__classifier = classifier
        self.__binning = binning
        self.__lumi_fb = lumi  # fb^-1
        self.__asimov = asimov
        self.__hybrid = hybrid
        self.__rebinning = rebinning
        self.__rmethod = rmethod
        self.__extra_regions = extra_regions
        self.__store = store
        #make these lists
        self.__input_list = []
        self.__used_variations = set([])
        self.__unused_variations = set([])
        self.__input_list_created = self.create_input_list()
        #Asimov models
        self.__asimov_S_list = []
        self.__asimov_B_list = []
        self.__asimov_B_plus_S_list = []
        self.__asimov_lists_created = self.create_asimov_lists()
        #hybrid models
        #self.__hybrid_list         = []
        #self.__hybrid_list_created = self.create_hybrid_list()
        #optimizer class
        self.__bins_optimizer = RWB.BinsOptimizer(name="optimized_bins", monitor=True, store=store)
        #rebin & recreate inputs
        self.__rebinned = self.rebin_inputs() if self.__rebinning else False
        #create additional regions based on binned templates
        self.__extra_regions = self.extra_regions() if self.__extra_regions else False
        #finalize
        self._finalize = self.finalize()

    @property
    def inputs_list(self):
        return self.__input_list if self.__input_list_created else []

    def finalize(self):
        print "Info: Asimov models created: "
        print " S   : ", len(self.__asimov_S_list)
        print " B   : ", len(self.__asimov_B_list)
        print " B+S : ", len(self.__asimov_B_plus_S_list)

        print "Info: Templates rebinned:", self.__rebinned

        if len(self.__used_variations):
            print "\nInfo: Used variations:", sorted(self.__used_variations)

        if len(self.__unused_variations):
            print "\nInfo: Unused variations:", sorted(self.__unused_variations)

        return True

    def is_directory(self, obj=None):
        return obj and issubclass(type(obj), ROOT.TObject) and "TDirectoryFile" in obj.IsA().GetName()

    def associates(self, _file, _channel, _region, _variation):

        inputfile = ROOT.TFile.Open(_file)
        if inputfile.IsZombie():
            print "Error: Input file %s is zombie ..." % (_file)
            inputfile.Close()
            return False

        if not inputfile.cd():
            print "Error: cannot enter root file '%s'" % (self.__filename)
            inputfile.Close()
            return False

        for counter, key in enumerate(inputfile.GetListOfKeys()):
            #read off dir
            directory = key.ReadObj()
            if not self.is_directory(directory):
                continue

            #check variation
            if not _variation.fullname in directory.GetName():
                continue

            #print counter, "Associate ", _variation.fullname, "side'", _variation.side, "'to", directory.GetName()

            for skey in directory.GetListOfKeys():
                #read off subdir
                subdirectory = skey.ReadObj()
                if not self.is_directory(subdirectory):
                    continue

                #check channel & region
                if not _channel in subdirectory.GetName():
                    return False

                if not _region in subdirectory.GetName():
                    return False

                #print counter, "From input file", directory.GetName(), subdirectory.GetName(), " for ",_channel, _region, _variation.fullname, _variation.fullnickname
                inputfile.Close()
                return True

        inputfile.Close()
        return False

    def add_to_input_list(self, _input=None):
        if _input:
            self.__input_list.append(_input)
            return True
        return False

    def is_primary_region(self, region=""):
        return region in self.__primary_regions

    def is_subsidiary_region(self, region=""):
        return region in self.__subsidiary_regions

    def create_input_list(self):
        """
        Reads in files with trees.
        Associates stuff and creates Inputs.
        """
        for _file in self.__files:
            for _variation in self.__variations.list:
                #nominal-only output
                if self.__variations.nominal_only:
                    if _variation.is_variation:
                        #print "Info: skipping variation ", _variation.nickname
                        continue
                #look for channels & regions
                for _channel in self.__channels:
                    for _region in self.__regions:
                        #check association with file
                        if self.associates(_file, _channel, _region, _variation):
                            #create input
                            print "Info: RooWorkspaceInputs::create_input_list - create input for file=%s, variation=%s, channel=%s, region=%s" % (
                                _file, _variation, _channel, _region)

                            _input = Input(_file, _variation, _channel, _region, self.is_primary_region(_region),
                                           self.is_subsidiary_region(_region), self.__classifier, self.__binning, self.__lumi_fb)
                            #keep it
                            if self.add_to_input_list(_input):
                                print "Info: appended to input list", _file, _variation.nickname, _channel, _region
                            else:
                                print "Error: error with appending to input list", _file, _variation.nickname, _channel, _region
                                return False

        #check unused variations
        for variation in self.__variations.list:
            used = False
            for _input in self.__input_list:
                if variation == _input.variation:
                    used = True
                    break

            if used:
                self.__used_variations.add(variation.fullname)
            else:
                self.__unused_variations.add(variation.fullname)

        return True

    def asimov_S_existing(self, _input):

        for asimov in self.__asimov_S_list:
            if asimov.equal(_input):
                return True

        return False

    def asimov_B_existing(self, _input):

        for asimov in self.__asimov_B_list:
            if asimov.equal(_input):
                return True

        return False

    def asimov_B_plus_S_existing(self, _input):

        for asimov in self.__asimov_B_plus_S_list:
            if asimov.equal(_input):
                return True

        return False

    def update_asimov_lists(self, _input=None):
        if not _input:
            print "Error: skipping null asimov -- this is bad!"
            return False

        #skip data
        if _input.is_data:
            print "Info: skipping from asimov this list %s" % (_input.identity)
            return True

        #S or B only Asimov
        if _input.is_signal:
            #then consider this S input
            if not self.asimov_S_existing(_input):
                self.__asimov_S_list.append(RWA.Asimov(_input))
            else:
                for asimov in self.__asimov_S_list:
                    if asimov.equal(_input):
                        if not asimov.update(_input):
                            print "Error: update_asimov_lists - cannot update S asimov for  %s" % (_input.identity)
        else:
            #then consider this B input
            if not self.asimov_B_existing(_input):
                self.__asimov_B_list.append(RWA.Asimov(_input))
            else:
                for asimov in self.__asimov_B_list:
                    if asimov.equal(_input):
                        if not asimov.update(_input):
                            print "Error: update_asimov_lists - cannot update B asimov for  %s" % (_input.identity)

        #B+S Asimov
        if not self.asimov_B_plus_S_existing(_input):
            self.__asimov_B_plus_S_list.append(RWA.Asimov(_input))
        else:
            for asimov in self.__asimov_B_plus_S_list:
                if asimov.equal(_input):
                    if not asimov.update(_input):
                        print "Error: update_asimov_lists - cannot update B+S asimov for  %s" % (_input.identity)

        return True

    def clean_asimov_lists(self):
        del self.__asimov_S_list[:]
        del self.__asimov_B_list[:]
        del self.__asimov_B_plus_S_list[:]

        return len(self.__asimov_S_list + self.__asimov_B_list + self.__asimov_B_plus_S_list) == 0

    def create_asimov_lists(self, with_signal=True):
        """
        Creates Asimov entities based on the created Inputs
        """
        if not self.__input_list:
            print "Error: Need to create input samples before constructing an Asimov model..."
            return False

        if not self.clean_asimov_lists():
            print "Error: failed to clean asimov lists..."
            return False

        if self.__asimov or self.__hybrid or self.__rebinning:
            for _input in self.inputs_list:
                #apply asimov only on nominal sample
                if _input.variation.is_variation:
                    continue
                #update list
                if self.update_asimov_lists(_input):
                    print "Info: appended to asimov this list", _input.identity
                else:
                    print "Error: error with appending to asimov list", _input.identity
                    return False

        print "Asimov S"
        for asimov in self.__asimov_S_list:
            print asimov.summary

        print "Asimov B"
        for asimov in self.__asimov_B_list:
            print asimov.summary

        print "Asimov B+S"
        for asimov in self.__asimov_B_plus_S_list:
            print asimov.summary

        return True

    def asimov_B_plus_S_list(self):
        return self.__asimov_B_plus_S_list if self.__asimov_lists_created else []

    def asimov_B_list(self):
        return self.__asimov_B_list if self.__asimov_lists_created else []

    def asimov_S_list(self):
        return self.__asimov_S_list if self.__asimov_lists_created else []

    def get_associated_signal(self, asimov=None):

        for _input in self.inputs_list:
            if not _input.is_signal:
                continue
            if asimov.equal(_input):
                print "Info: Signal input %s associates to Asimov %s" % (_input.identity, asimov.identity)
                return _input

        print "Error: Unable to associate anything to Asimov %s" % (asimov.identity)
        return None

    def rebin_inputs(self):
        """
        Optimizes the binning structure of the fit templates based on bin error and significance
        """
        print "Info: Rebinning starts now."

        if not self.__asimov_lists_created:
            print "Error: rebinning according to Asimov B not possible - Asimov list is not ready..."
            return False

        # Optimization for combined channels.
        # For the bin optimization of multiple channels we should consider now
        # the total bkg and total signal for a combined fit.
        # Must be done per region.

        #1. Create Asimov samples per region
        asimov_regions = []
        for region in self.__regions:
            asimov_regions.append(RWA.AsimovRegion(region))

        #2. Fill in Asimov regions
        for asimov in self.__asimov_S_list:
            for asimov_region in asimov_regions:
                if asimov_region.region == asimov.region:
                    if not asimov_region.add_signal(asimov):
                        print "Error: rebin_inputs - unable to add signal asimov %s in region %s ..." % (asimov.identity,
                                                                                                         asimov_region.region)
                        return False
                    break

        for asimov in self.__asimov_B_list:
            for asimov_region in asimov_regions:
                if asimov_region.region == asimov.region:
                    if not asimov_region.add_bkg(asimov):
                        print "Error: rebin_inputs - unable to add bkg asimov %s in region %s ..." % (asimov.identity, asimov_region.region)
                        return False
                    break

        print "Info: Asimov regions %i: %s" % (len(asimov_regions), ', '.join(str(e) for e in asimov_regions))

        #3. rebin per region
        for asimov_region in asimov_regions:
            print "\nInfo: <><><><><><><> Rebinning of new Region <><><><><><><>"
            region_type = "primary" if asimov_region.is_primary_region else "subsidiary" if asimov_region.is_subsidiary_region else "unknown"
            print "Info: rebinning optimization for combined region %s of type %s" % (asimov_region.region, region_type)

            print "Info: Asimov region %s signal=%f bkg=%f" % (asimov_region.region, asimov_region.signal_events(),
                                                               asimov_region.bkg_events())

            #pick up an appropriate bin method
            rmethod = self.__rmethod if asimov_region.is_primary_region else RMethod.DELTA if asimov_region.is_subsidiary_region else 0

            #obtain the bin results
            if rmethod == RMethod.ALPHA:
                results = self.__bins_optimizer.optimal_binning_alpha(Asimov_B, Asimov_S)  #needs update
            elif rmethod == RMethod.BETA:
                results = self.__bins_optimizer.optimal_binning_beta(asimov_region)
            elif rmethod == RMethod.GAMMA:
                results = self.__bins_optimizer.optimal_binning_gamma(Asimov_B, Asimov_S)  #needs update
            elif rmethod == RMethod.DELTA:
                results = self.__bins_optimizer.optimal_binning_delta(asimov_region)
            else:
                print "Error: undefined rebinning method '%i'" % (rmethod)
                return False

            print "Info rebinning method: %s" % (str(rmethod))
            if not results.optimized:
                print "Info: rebinning optimization failed - not touching the initial templates..."
                return False

            print "\nInfo: rebinning templates started for region %s with binning %s" % (asimov_region.region, ' : '.join(
                str(e) for e in results.bin_array))

            for _input in self.__input_list:
                #pick up inputs from correct region
                if _input.region != asimov_region.region:
                    continue

                #rebin input
                if not _input.rebin(results):
                    print "Error: Failed to rebin", _input.identity
                    return False
                else:
                    print "Info: input %s rebinned according to binning of region %s" % (_input.identity, asimov_region.region)

                #check if input is valid
                if not _input.histo_is_valid():
                    print "Error: Histo is not valid after rebinning for input %s" % (_input.identity)
                    return False

        #4. add extra regions
        input_list_extra_regions = []
        new_region_names = ["low_score", "high_score"]

        #create regions
        for _input in self.__input_list:
            #deep copy instead of shallow (aka = ) which copies only the refs
            for new_region_name in new_region_names:
                _new_input = copy.copy(_input)
                _new_input.folder_name = "%s_%s" % (_input.folder_name, new_region_name)
                print "Info: adding extra region %s" % (_new_input.identity)
                input_list_extra_regions.append(_new_input)

        #select appropriate range for each region
        for _input in input_list_extra_regions:

            if "low" in _input.folder_name:
                histo_bin = 2  #starts from 2nd bin
                histo_side = "high"
            elif "high" in _input.folder_name:
                histo_bin = 1  #up to 1st bin
                histo_side = "low"

            #remove bins
            if not _input.truncate(histo_side, histo_bin):
                print "Error: Failed to remove bins from axis for input %s" % (_input.identity)
                return False
            else:
                print "Info: removing histo bins from input %s " % (_input.identity)

            #check if input is valid
            if not _input.histo_is_valid():
                print "Error: Histo is not valid after removing  for input %s" % (_input.identity)
                return False

        if input_list_extra_regions:
            print "Info: existing histo entries %i. Adding %i extra histo entries" % (len(self.__input_list), len(input_list_extra_regions))
            self.__input_list += input_list_extra_regions
        else:
            print "Warning: list of extra regions is empty..."

        #5. rederive Asimov samples based on new binning
        self.__asimov_list_created = self.create_asimov_lists()

        if not self.__asimov_lists_created:
            print "Error: Failed to derive Asimov lists after bin optimization..."

        return True

    def extra_regions(self):
        """
        Adds extra regions, e.g. control regions, based on the binning of current regions
        """

        if not self.__asimov_lists_created:
            print "Error: rebinning according to Asimov B not possible - Asimov list is not ready..."
            return False

        return True

    def create_hybrid_list(self):

        #if not self.__asimov_list:
        #    print "Error: Need to create input Asimov model before constructing a Hybrid model..."
        #    return False

        return True
