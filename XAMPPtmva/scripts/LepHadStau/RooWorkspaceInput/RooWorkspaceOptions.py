import argparse


class Options(object):
    """
    Options parser class
    """
    def __init__(self, name=""):
        self.__name = name
        self.__parser = None
        self.__options = None
        self.__ready = self.parser()

    def parser(self):
        self.__parser = argparse.ArgumentParser(description='Options for creating the RooWorkspace input for"',
                                                prog=self.__name,
                                                formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        self.__parser.add_argument('--monitor', help='Create monitor histograms', action='store_true', default=False)

        self.__parser.add_argument('--systematics', help='Include systematics', action='store_true', default=False)

        self.__parser.add_argument('--asimov', help='Asimov B+S', action='store_true', default=False)

        self.__parser.add_argument('--rebin', help='Rebin', action='store_true', default=False)

        self.__parser.add_argument('--rebin-algo', help='Score binning algorithm {alpha, beta}', default="beta")

        self.__parser.add_argument('--hybrid',
                                   help='Hybrid: Data in B-dominated region, Asimov B+S in S-dominated region',
                                   action='store_true',
                                   default=False)

        self.__parser.add_argument('--luminosity', help='Luminosity in fb', type=float, default=1.0)

        self.__parser.add_argument('--input-paths', help='Input path of training results', type=str, nargs='+', required=True)

        self.__parser.add_argument('--primary-regions', help='Primary Regions (e.g. Signal Regions)', type=str, nargs='+', required=True)

        self.__parser.add_argument('--subsidiary-regions',
                                   help='Subsidiary Regions (e.g. Control Regions)',
                                   type=str,
                                   nargs='+',
                                   required=True)

        self.__parser.add_argument('--output-path-name', help='Output file name', default="RooWorkspace")

        self.__parser.add_argument('--classifier', help='Classification algorithm', default="AdaBoostBDT")

        self.__parser.add_argument('--binning', help='Score binning pdf', default="050")

        self.__parser.add_argument('--mass', help='Stau mass point', default="120_1")

        self.__options = self.__parser.parse_args()

        if not self.__options:
            self.__parser.print_help()
            return False

        return True

    @property
    def instance(self):
        if self.__ready:
            print "Getting valid options..."
            return self.__options
        print "Invalid options..."
