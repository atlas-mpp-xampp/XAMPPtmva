import ROOT
from decimal import Decimal, ROUND_UP, ROUND_DOWN, ROUND_HALF_UP
import math
import numpy
import array
import RooWorkspacePrint as RWP
import RooWorkspaceSignificance as RWSIG


class HistoBin(object):
    """
    Defines a histo bin 
    """
    def __init__(self, y=0., dy=0., x_low=0., x_high=0.):
        self.__y = y
        self.__dy = dy
        self.__x_low = x_low
        self.__x_high = x_high

    @property
    def y(self):
        return self.__y

    @property
    def dy(self):
        return self.__dy

    @property
    def low_edge(self):
        return self.__x_low

    @property
    def high_edge(self):
        return self.__x_high

    @property
    def x_low(self):
        return self.__x_low

    @property
    def x_high(self):
        return self.__x_high

    def empty(self):
        return math.fabs(self.__y) < 1e-8

    def negative(self):
        return self.__y < 0.


class Bin(object):
    """
    Defines a bin with augmented info
    """
    def __init__(self,
                 bin=0.,
                 lowest_bin=False,
                 middle_bin=False,
                 highest_bin=False,
                 stats_pass=False,
                 stat=0.,
                 sign_pass=False,
                 sign=0.,
                 quantile=0.):
        #
        self.__precision = 2
        self.__tolerance = 1e-5
        #
        self.__lowest_bin = lowest_bin
        self.__middle_bin = middle_bin
        self.__highest_bin = highest_bin
        self.__bin = self.adjust(bin)
        self.__stats_pass = stats_pass
        self.__stat = stat
        self.__sign_pass = sign_pass
        self.__sign = sign
        self.__quantile = quantile

    def __eq__(self, other):
        return math.fabs(self.__bin - other.__bin) < self.__tolerance

    def __ne__(self, other):
        return not self.__eq__(self, other)

    def truncate(self, number, digits):
        stepper = pow(10.0, digits)
        return math.trunc(stepper * number) / stepper

    def round(self, number=0., digits=0, rounding=ROUND_HALF_UP):
        """
        The python's round has imperfections. Use this method instead.
         All your options for rounding:
         ROUND_05UP       ROUND_DOWN       ROUND_HALF_DOWN  ROUND_HALF_UP
         ROUND_CEILING    ROUND_FLOOR      ROUND_HALF_EVEN  ROUND_UP
        """
        value = Decimal(number)
        output = Decimal(value.quantize(Decimal(str(1.0 / 10**digits)), rounding=rounding))
        return float(output)

    def adjust(self, x):

        #if the bin is in the middle of the range: round up half (default)
        if self.__middle_bin:
            return self.round(x, self.__precision)

        # if the bin is negative:
        # round up if lowest
        # round down if highest
        # else: the other way around
        if x < 0:
            if self.__lowest_bin:
                return self.round(x, self.__precision, ROUND_UP)

            if self.__highest_bin:
                return self.round(x, self.__precision, ROUND_DOWN)
        else:
            if self.__lowest_bin:
                return self.round(x, self.__precision, ROUND_DOWN)

            if self.__highest_bin:
                return self.round(x, self.__precision, ROUND_UP)

        #default
        return self.round(x, self.__precision, ROUND_HALF_UP)

    @property
    def bin(self):
        return self.__bin

    @property
    def stats_pass(self):
        return self.__stats_pass

    @property
    def sign_pass(self):
        return self.__sign_pass

    def summary(self):
        return "%f stats=%i (%f) sign=%i (%f) quant=%f" % (self.__bin, self.__stats_pass, self.__stat, self.__sign_pass, self.__sign,
                                                           self.__quantile)

    def __str__(self):
        return self.summary()


class BinningResults(object):
    """
    Keeps outcome from binning algorithms
    """
    def __init__(self, optimized=False, bin_array=[], first_bin_is_empty=False, last_bin_is_empty=False):
        self.optimized = optimized
        self.bin_array = bin_array
        self.first_bin_is_empty = first_bin_is_empty
        self.last_bin_is_empty = last_bin_is_empty


class BinsOptimizer(object):
    """
    Defines rules for bin optimization
    """
    def __init__(self, name="bins_optimizer", monitor=False, store=None):

        #self.__Asimov_sign_bin_threshold = 1.
        #self.__Zn_sign_bin_threshold = 0.5
        #self.__stat_bin_fix_threshold = 0.05
        #self.__stat_bin_threshold = 0.05
        #self.__stat_bin_threshold_bkg = 0.01
        #self.__quantile_threshold = 0.25
        self.__precision = 3
        self.__optimal_binning = []
        #
        self.__store = store
        self._errors = 0

    def error(self):
        self._errors += 1

    def errors(self):
        return self._errors

    def reset_errors(self):
        self._errors = 0

    def log(self, line=""):
        self.__store.add_to_log(line)

    def store(self, obj=None, name=""):
        if not self.__store.add_to_optimization(obj, name):
            print "Error: cannot store %s in BinsOptimizer" % (name)
            return False
        return True

    def append_bin(self, bin=None):
        return self.add_bin(-1, bin)

    def add_bin(self, where=-1, bin=None):

        if not bin:
            print "Error: cannot add null bin to optimal binning..."
            return False
        else:
            for b in self.__optimal_binning:
                if b == bin:
                    return True

        if where > -1:
            self.__optimal_binning.insert(where, bin)
        else:
            self.__optimal_binning.append(bin)

        return True

    def get_sign_bin_threshold(self, use_Zn=True):
        if use_Zn:
            return self.__Zn_sign_bin_threshold

        return self.__Asimov_sign_bin_threshold

    def get_stat_bin_threshold(self, quantile=0.):
        if quantile < self.__quantile_threshold:
            return self.__stat_bin_threshold_bkg

        return self.__stat_bin_threshold

    def get_stat_bin_fix_threshold(self):
        return self.__stat_bin_fix_threshold

    def get_significance(self, definition="", S=0., B=0., dBrel=0.):

        if B <= 0:
            #print "Warning: get_significance - negative or zero background..."
            return 0.

        if definition == "ZN":
            return RWSIG.Significance().ZN(S, B, dBrel)
        elif definition == "Zn":
            return RWSIG.Significance().Zn(S, B, dBrel)
        elif definition == "SoB_dBrel":
            return RWSIG.Significance().SoB_dBrel(S, B, dBrel)
        elif definition == "SoB":
            return RWSIG.Significance().SoB(S, B)
        elif definition == "Asimov":
            return RWSIG.Significance().Asimov(S, B)
        else:
            print "Warning: get_significance - undefined significance..."
            return 0.

    def roundme(self, number=0., digits=0, rounding=ROUND_HALF_UP):
        """
        Rounding options
         ROUND_05UP       ROUND_DOWN       ROUND_HALF_DOWN  ROUND_HALF_UP
         ROUND_CEILING    ROUND_FLOOR      ROUND_HALF_EVEN  ROUND_UP
        """
        value = Decimal(number)
        output = Decimal(value.quantize(Decimal(str(1.0 / 10**digits)), rounding=rounding))
        return float(output)

    def regulate(self, bins=[], first_bin_is_empty=False, last_bin_is_empty=False):

        #if first bin is empty, then round low edge downwards
        #otherwise the rounding up will change this situation
        for i, bin in enumerate(bins):
            if first_bin_is_empty and i == 1:  # i == 0 is the boundary
                if bins[i] > 0:
                    bins[i] = self.roundme(bin, self.__precision, ROUND_DOWN)
                else:
                    bins[i] = self.roundme(bin, self.__precision, ROUND_UP)
            elif last_bin_is_empty and i == len(bins) - 2:  # i == N-1 is the boundary
                if bins[i] > 0:
                    bins[i] = self.roundme(bin, self.__precision, ROUND_UP)
                else:
                    bins[i] = self.roundme(bin, self.__precision, ROUND_DOWN)
            else:

                bins[i] = self.roundme(bin, self.__precision, ROUND_HALF_UP)

        return bins

    def abs_diff(self, x=0., y=0.):
        return math.fabs(math.fabs(x) - math.fabs(y))

    def sliding_window(self, sliding_window_x_size=0.1, Histo_S=None, Histo_B=None, leftmost_bin=0, rightmost_bin=0):

        verbose = False
        use_Zn_significance = True
        sliding_window_x_step_finest = False
        sliding_window_x_step = 0.005
        sliding_window_n_bins_size = int(self.roundme(sliding_window_x_size / Histo_B.GetBinWidth(1), 0, ROUND_HALF_UP))
        sliding_window_n_bins_step = int(self.roundme(sliding_window_x_step / Histo_B.GetBinWidth(1), 0, ROUND_HALF_UP))

        print "\nInfo: SW - number of bins size %i x-size=%f bins-step=%i x-step=%f" % (sliding_window_n_bins_size, sliding_window_x_size,
                                                                                        sliding_window_n_bins_step, sliding_window_x_step)

        sliding_window_right_bin = rightmost_bin
        print "Info: SW - starting from bin %i" % (sliding_window_right_bin)

        max_significance = 0.
        max_significance_low_bin = 0
        max_significance_high_bin = 0
        sliding_window_leftmost_bin_visited = False
        maximum_significance_bin_undefined = True
        while maximum_significance_bin_undefined:

            sliding_window_left_bin = sliding_window_right_bin - sliding_window_n_bins_size

            #do we hit the left edge?
            if sliding_window_left_bin == leftmost_bin:
                print "Info: SW - leftmost bin visited..."
                sliding_window_leftmost_bin_visited = True
                maximum_significance_bin_undefined = False

            if sliding_window_left_bin < leftmost_bin:
                if not sliding_window_leftmost_bin_visited:
                    print "Info: SW - leftmost bin not visited. Restricting SW to leftmost bin..."
                    sliding_window_left_bin = leftmost_bin
                    maximum_significance_bin_undefined = False

            dB = ROOT.Double(0.)
            B = Histo_B.IntegralAndError(sliding_window_left_bin, sliding_window_right_bin, dB)

            ReldB = math.sqrt(dB) / math.fabs(B) if math.fabs(B) > 0. else 0.

            dS = ROOT.Double(0.)
            S = Histo_S.IntegralAndError(sliding_window_left_bin, sliding_window_right_bin, dS)

            xlow = Histo_S.GetBinLowEdge(sliding_window_left_bin)
            xhigh = Histo_S.GetBinLowEdge(sliding_window_right_bin) + Histo_S.GetBinLowEdge(sliding_window_right_bin)

            if use_Zn_significance:
                signifnicance = self.Zn_expected_significance(S, B, ReldB)
            else:
                signifnicance = self.asimov_median_expected_significance(first_bin_S_sum, first_bin_B_sum)

            #sig_rule = signifnicance > self.get_sign_bin_threshold_sliding_window(use_Zn=use_Zn_significance)
            if signifnicance > max_significance:
                max_significance = signifnicance
                max_significance_low_bin = sliding_window_left_bin
                max_significance_high_bin = sliding_window_right_bin

            if verbose:
                print "Bins=%i:%i x=%f:%f B=%f dB=%f ReldB=%f S=%f Sig=%f" % (sliding_window_left_bin, sliding_window_right_bin, xlow,
                                                                              xhigh, B, dB, ReldB, S, signifnicance)

            sliding_window_right_bin -= 1 if sliding_window_x_step_finest else sliding_window_n_bins_step

        #done
        print "Info: results for size=%f: sig=%f low-bin=%f high-bin=%f" % (sliding_window_x_size, max_significance,
                                                                            max_significance_low_bin, max_significance_high_bin)
        return max_significance, max_significance_low_bin, max_significance_high_bin

    def optimal_binning_delta(self, asimov_region=None):  #input must be AsimovRegion
        """
        Creates single bin templates
        """
        empty_bin_maximum_content = 1e-5

        self.log("\nRooWorkspaceBins::optimal_binning_delta")
        self.reset_errors()

        #region
        region = asimov_region.region
        self.log("Region: %s" % (region))

        #clear binning
        del self.__optimal_binning[:]

        #region
        region = asimov_region.region
        print "Info: optimal_binning_delta - Analyzing region %s" % (region)

        #output
        results = BinningResults(optimized=False)

        #get histos here
        Histo = asimov_region.asimov_bkg.clone
        Histo.Add(asimov_region.asimov_signal.clone, 1.)

        N = Histo.GetNbinsX()

        # find first sensible entry from left
        leftmost_bin = 1
        for i in xrange(1, N + 1, 1):  #range [1, N]
            if Histo.GetBinContent(i) > 0:
                leftmost_bin = i
                leftmost_bin_x = Histo.GetBinLowEdge(i)
                self.log("Leftmost bin found %i at %f and added" % (i, leftmost_bin_x))
                self.__optimal_binning.append(leftmost_bin_x)
                break

        # find rightmost bin
        rightmost_bin = N
        for i in range(N, 0, -1):  # range = [N, 1] backwards
            if Histo.GetBinContent(i) > 0:
                rightmost_bin = i
                rightmost_bin_x = Histo.GetBinLowEdge(i) + Histo.GetBinWidth(i)
                self.log("Rightmost bin found %i at %f and added" % (i, rightmost_bin_x))
                self.__optimal_binning.append(rightmost_bin_x)
                break

        if leftmost_bin >= rightmost_bin:
            self.log("ERROR: Couldn't find edges. Abnormal input histos!")
            return results

        #physical edges
        fixed_low_edge = Histo.GetBinLowEdge(1)
        fixed_high_edge = Histo.GetBinLowEdge(N) + Histo.GetBinWidth(N)

        self.__optimal_binning.append(fixed_low_edge)
        self.__optimal_binning.append(fixed_high_edge)

        self.log("Physical Low edge %f" % (fixed_low_edge))
        self.log("Physical High edge %f" % (fixed_high_edge))

        #sort
        self.__optimal_binning.sort()

        #cross-check before rounding
        x_optimal_binning = numpy.array([x for x in self.__optimal_binning])
        Histo_rebinned = Histo.Rebin(len(x_optimal_binning) - 1, "", x_optimal_binning)

        first_bin_is_empty = Histo_rebinned.GetBinContent(1) < empty_bin_maximum_content
        last_bin_is_empty = Histo_rebinned.GetBinContent(Histo_rebinned.GetNbinsX()) < empty_bin_maximum_content

        self.log("Before rounding")
        self.log("N bins: %i" % (Histo_rebinned.GetNbinsX()))
        self.log("Binning: %s" % (' : '.join(str(e) for e in x_optimal_binning)))
        self.log("First/Last bin content %f/%f" %
                 (Histo_rebinned.GetBinContent(1), Histo_rebinned.GetBinContent(Histo_rebinned.GetNbinsX())))
        self.log("First/Last bin is empty %i/%i" % (first_bin_is_empty, last_bin_is_empty))

        #regulate binning - after rounding
        self.__optimal_binning = self.regulate(self.__optimal_binning, first_bin_is_empty, last_bin_is_empty)
        x_optimal_binning = numpy.array([x for x in self.__optimal_binning])
        Histo_rebinned = Histo.Rebin(len(x_optimal_binning) - 1, "", x_optimal_binning)

        first_rounded_bin_is_empty = Histo_rebinned.GetBinContent(1) < empty_bin_maximum_content
        last_rounded_bin_is_empty = Histo_rebinned.GetBinContent(Histo_rebinned.GetNbinsX()) < empty_bin_maximum_content

        self.log("After rounding")
        self.log("N bins: %i" % (Histo_rebinned.GetNbinsX()))
        self.log("Binning: %s" % (' : '.join(str(e) for e in x_optimal_binning)))
        self.log("First/Last bin content %f/%f" %
                 (Histo_rebinned.GetBinContent(1), Histo_rebinned.GetBinContent(Histo_rebinned.GetNbinsX())))
        self.log("First/Last bin is empty %i/%i" % (first_rounded_bin_is_empty, last_rounded_bin_is_empty))

        if first_bin_is_empty != first_rounded_bin_is_empty:
            self.log("ERROR: leftmost bin content changed by rounding")
            self.error()

        if last_bin_is_empty != last_rounded_bin_is_empty:
            self.log("ERROR: rigthmost bin content changed by rounding")
            self.error()

        return BinningResults(True, x_optimal_binning, first_rounded_bin_is_empty, last_rounded_bin_is_empty)

    def optimal_binning_gamma(self, total=None, signal=None):  #inputs can be either Asimov or Input
        """
        Finds bin with maximum significance via a sliding window procedure
        """
        results = BinningResults(optimized=False)
        self.reset_errors()

        #get histos here
        Histo_B = total.clone
        Histo_S = signal.clone

        if not Histo_S:
            print "Error: null signal histo in optimal binning...."
            return results

        if not Histo_B:
            print "Error: null bkg histo in optimal binning...."
            return results

        #no more dependence on input types
        N_B = Histo_B.GetNbinsX()
        N_S = Histo_S.GetNbinsX()
        if N_B != N_S:
            print "Error: rebinning not possible due to unequal B and S templates for Asimov %s" % (total.identity)
            return results
        else:
            print "Info: %i bins found..." % (N_B)

        print "Info: original S %f, original B %f" % (Histo_S.Integral(), Histo_B.Integral())

        # find first sensible entry from left
        leftmost_bin = 1
        for i in xrange(1, N_B + 1, 1):  #range [1, N]
            if Histo_B.GetBinContent(i) > 0:
                leftmost_bin = i
                leftmost_bin_x = Histo_B.GetBinLowEdge(i)
                print "Info: Leftmost bin found %i at %f" % (i, leftmost_bin_x)
                self.__optimal_binning.append(leftmost_bin_x)
                break

        # find rightmost bin
        rightmost_bin = N_B
        for i in range(N_B, 0, -1):  # range = [N, 1] backwards
            if Histo_B.GetBinContent(i) > 0:
                rightmost_bin = i
                rightmost_bin_x = Histo_B.GetBinLowEdge(i) + Histo_B.GetBinWidth(i)
                print "Info: Rightmost bin found %i at %f" % (i, rightmost_bin_x)
                self.__optimal_binning.append(rightmost_bin_x)
                break

        if leftmost_bin >= rightmost_bin:
            print "Error: couldn't find edges. Abnormal input histos..."
            return results

        #physical edges
        fixed_low_edge = Histo_S.GetBinLowEdge(1)
        fixed_high_edge = Histo_S.GetBinLowEdge(N_S) + Histo_S.GetBinWidth(N_S)

        self.__optimal_binning.append(fixed_low_edge)
        self.__optimal_binning.append(fixed_high_edge)

        #sliding window
        minimum_significance = 100.

        optimal_significance = 0.
        optimal_bin_low = 0
        optimal_bin_high = 0
        optimal_size = 0.

        norm_optimal_significance = 0.
        norm_optimal_bin_low = 0
        norm_optimal_bin_high = 0
        norm_optimal_size = 0.

        sizes = numpy.arange(0.01, 0.6, 0.005, 'd')
        significances = numpy.arange(0, 2, 0.005, 'd')
        size_vs_significance = ROOT.TH2D("size_vs_significance", "", len(significances) - 1, significances, len(sizes) - 1, sizes)
        size_vs_norm_significance = ROOT.TH2D("size_vs_norm_significance", "", len(significances) - 1, significances, len(sizes) - 1, sizes)
        for xsize in sizes:
            significance, bin_low, bin_high = self.sliding_window(xsize, Histo_S, Histo_B, leftmost_bin, rightmost_bin)
            size_vs_significance.Fill(significance, xsize)

            norm_significance = significance / xsize
            size_vs_norm_significance.Fill(norm_significance, xsize)

            if significance > optimal_significance:
                optimal_significance = significance
                optimal_bin_low = bin_low
                optimal_bin_high = bin_high
                optimal_size = xsize

            if norm_significance > norm_optimal_significance:
                norm_optimal_significance = norm_significance
                norm_optimal_bin_low = bin_low
                norm_optimal_bin_high = bin_high
                norm_optimal_size = xsize

            if significance < minimum_significance:
                minimum_significance = significance

        #results
        optimal_bin_low_edge = Histo_S.GetBinLowEdge(optimal_bin_low)
        optimal_bin_high_edge = Histo_S.GetBinLowEdge(optimal_bin_high) + Histo_S.GetBinWidth(optimal_bin_high)
        print "\nInfo: optimal sig=%f bins=%i:%i width=%f:%f size=%f" % (optimal_significance, optimal_bin_low, optimal_bin_high,
                                                                         optimal_bin_low_edge, optimal_bin_high_edge, optimal_size)

        norm_optimal_bin_low_edge = Histo_S.GetBinLowEdge(norm_optimal_bin_low)
        norm_optimal_bin_high_edge = Histo_S.GetBinLowEdge(norm_optimal_bin_high) + Histo_S.GetBinWidth(norm_optimal_bin_high)
        print "\nInfo: optimal Norm sig=%f bins=%i:%i width=%f:%f size=%f" % (norm_optimal_significance, norm_optimal_bin_low,
                                                                              norm_optimal_bin_high, norm_optimal_bin_low_edge,
                                                                              norm_optimal_bin_high_edge, norm_optimal_size)

        #visualize
        Histo_S_binned = signal.Clone("binned")
        fixed_granularity = 0.1
        fixed_bins = numpy.arange(
            fixed_low_edge,
            fixed_high_edge + fixed_granularity / 2.,  #tweak to span in the original range
            fixed_granularity)

        for i, element in enumerate(fixed_bins):
            if math.fabs(fixed_bins[i]) < 1e-8:
                fixed_bins[i] = 0.

        print "Binned signal template x-axis", fixed_bins

        fixed_n_bins = len(fixed_bins) - 1

        Histo_S_binned = Histo_S_binned.Rebin(fixed_n_bins, "", fixed_bins)
        max_bin = Histo_S_binned.GetMaximumBin()
        max_bin_content = Histo_S_binned.GetBinContent(max_bin)

        line = ROOT.TLine(optimal_bin_low_edge, max_bin_content / 2., optimal_bin_high_edge, max_bin_content / 2.)

        norm_line = ROOT.TLine(norm_optimal_bin_low_edge, max_bin_content / 3., norm_optimal_bin_high_edge, max_bin_content / 3.)

        ROOT.gROOT.SetBatch(ROOT.kTRUE)

        canvas = ROOT.TCanvas("binning_result", "", 10, 10, 800, 700)
        canvas.cd()
        Histo_S_binned.Draw()
        line.Draw("same")
        # store canvas
        if not self.store(canvas, "binning_result"):
            print "Warning: unable to store canvas with binning result..."

        #store sizes Vs sig
        if not self.store(size_vs_significance, "size_vs_significance"):
            print "Warning: unable to store sliding window size vs significance..."

        #store sizes Vs norm sig
        if not self.store(size_vs_norm_significance, "size_vs_norm_significance"):
            print "Warning: unable to store sliding window size vs norm significance..."

        return results

    def optimal_binning_beta(self, asimov_region=None):  #input must be AsimovRegion
        """
        Finds area with maximum signal and defines first bin there.
        """
        self.log("\nRooWorkspaceBins::optimal_binning_beta")
        self.reset_errors()

        #clear binning
        del self.__optimal_binning[:]

        #region
        region = asimov_region.region
        print "Info: optimal_binning_beta - Analyzing region %s" % (region)

        ########## OPTIONS ###############
        first_bin_fit_n_sigma_range = 2.5
        leftmost_bin_low_content_join_with_next = True
        empty_bin_maximum_content = 1e-5

        # options: Zn, ZN, Asimov
        significance_definition = "ZN"
        significance_threshold = 1.65  #corresponds roughly to p-value=0.05
        if region == "0jetA":
            significance_threshold = 0.75
            right_bins_stat_bin_fix_threshold = 0.05
            left_bins_signal_quantile_threshold = 0.25
            left_bins_stat_bin_fix_threshold_high_signal_quantile = 0.02
            left_bins_stat_bin_fix_threshold_low_signal_quantile = 0.01
        elif region == "0jetB":
            significance_threshold = 0.15
            right_bins_stat_bin_fix_threshold = 0.05
            left_bins_signal_quantile_threshold = 0.25
            left_bins_stat_bin_fix_threshold_high_signal_quantile = 0.05
            left_bins_stat_bin_fix_threshold_low_signal_quantile = 0.01
        elif region == "1jet":
            significance_threshold = 0.25
            right_bins_stat_bin_fix_threshold = 0.02
            left_bins_signal_quantile_threshold = 0.25
            left_bins_stat_bin_fix_threshold_high_signal_quantile = 0.005
            left_bins_stat_bin_fix_threshold_low_signal_quantile = 0.005
        else:
            print "Error: optimal_binning_beta - Unknown region %s" % (region)
            self.log("ERROR: unknown region %s" % (region))
            self.error()

        self.log("Region %s" % (region))
        self.log("Significance definition %s" % (significance_definition))
        self.log("Significance threshold %f" % (significance_threshold))
        self.log("First bin %f x sigma range for fit" % (first_bin_fit_n_sigma_range))
        self.log("Right bins stat error upper threshold %f" % (right_bins_stat_bin_fix_threshold))
        self.log("Left bin signal quantile threshold %f" % (left_bins_signal_quantile_threshold))
        self.log("Left bin low quantile stat upper threshold %f" % (left_bins_stat_bin_fix_threshold_low_signal_quantile))
        self.log("Left bin high quantile stat upper threshold %f" % (left_bins_stat_bin_fix_threshold_high_signal_quantile))
        self.log("Empty bin max content %f" % (empty_bin_maximum_content))
        self.log("Join leftmost bin with next one if its content is low %i" % (leftmost_bin_low_content_join_with_next))

        ########## OPTIMIZATION ###############
        #output
        results = BinningResults(optimized=False)

        #get original histos here
        Histo_B = asimov_region.asimov_bkg.clone
        Histo_S = asimov_region.asimov_signal.clone
        #to be binned
        Histo_B_binned = asimov_region.asimov_bkg.Clone("binned")
        Histo_S_binned = asimov_region.asimov_signal.Clone("binned")

        if not Histo_S:
            print "Error: null signal histo in optimal binning...."
            return results

        if not Histo_B:
            print "Error: null bkg histo in optimal binning...."
            return results

        #no more dependence on input types
        N_B = Histo_B.GetNbinsX()
        N_S = Histo_S.GetNbinsX()
        if N_B != N_S:
            print "Error: rebinning not possible due to unequal B and S templates for Asimov %s" % (total.identity)
            return results
        else:
            print "Info: %i bins found..." % (N_B)

        self.log("Original S %f, original S from clone %f, original B %f" %
                 (Histo_S.Integral(), Histo_S_binned.Integral(), Histo_B.Integral()))

        # find first sensible entry from left
        leftmost_bin = 1
        for i in xrange(1, N_B + 1, 1):  #range [1, N]
            if Histo_B.GetBinContent(i) > 0:
                leftmost_bin = i
                leftmost_bin_x = Histo_B.GetBinLowEdge(i)
                self.log("Leftmost bin found %i at %f and added" % (i, leftmost_bin_x))
                self.__optimal_binning.append(leftmost_bin_x)
                break

        # find rightmost bin
        rightmost_bin = N_B
        for i in range(N_B, 0, -1):  # range = [N, 1] backwards
            if Histo_B.GetBinContent(i) > 0:
                rightmost_bin = i
                rightmost_bin_x = Histo_B.GetBinLowEdge(i) + Histo_B.GetBinWidth(i)
                self.log("Rightmost bin found %i at %f and added" % (i, rightmost_bin_x))
                self.__optimal_binning.append(rightmost_bin_x)
                break

        if leftmost_bin >= rightmost_bin:
            self.log("ERROR: Couldn't find edges. Abnormal input histos!")
            return results

        #physical edges
        fixed_low_edge = Histo_S_binned.GetBinLowEdge(1)
        fixed_high_edge = Histo_S_binned.GetBinLowEdge(N_S) + Histo_S_binned.GetBinWidth(N_S)

        self.__optimal_binning.append(fixed_low_edge)
        self.__optimal_binning.append(fixed_high_edge)

        self.log("Physical Low edge %f" % (fixed_low_edge))
        self.log("Physical High edge %f" % (fixed_high_edge))

        self.log("Effective Low edge %f" % (leftmost_bin_x))
        self.log("Effective High edge %f" % (rightmost_bin_x))

        ### find highest-Sig bin -- get an idea about the location of the maximum signal
        self.log("Searching for 1st bin with a fitting procedure")
        fixed_granularity = 0.1
        fixed_bins = numpy.arange(
            fixed_low_edge,
            fixed_high_edge + fixed_granularity / 2.,  #tweak to span in the original range
            fixed_granularity)

        for i, element in enumerate(fixed_bins):
            if math.fabs(fixed_bins[i]) < 1e-8:
                fixed_bins[i] = 0.

        self.log("Binned signal template x-axis %s" % (' : '.join(str(e) for e in fixed_bins)))

        fixed_n_bins = len(fixed_bins) - 1

        Histo_S_binned = Histo_S_binned.Rebin(fixed_n_bins, "", fixed_bins)
        Histo_B_binned = Histo_B_binned.Rebin(fixed_n_bins, "", fixed_bins)

        self.log("Original Ns %i, binned Ns %i" % (Histo_S.GetNbinsX(), Histo_S_binned.GetNbinsX()))
        self.log("Original S %f, binned S %f" % (Histo_S.Integral(), Histo_S_binned.Integral()))

        # store binned pdfs
        if not self.store(Histo_S_binned, "signal_rebinned_%i_%s" % (fixed_n_bins, region)):
            print "Warning: unable to store rebinned signal histo template to find maximum..."

        if not self.store(Histo_B_binned, "bkg_rebinned_%i_%s" % (fixed_n_bins, region)):
            print "Warning: unable to store rebinned bkg histo template to find maximum..."

        # now do a fit
        mean = Histo_S_binned.GetMean()
        rms = Histo_S_binned.GetRMS()
        xstart = mean - rms * first_bin_fit_n_sigma_range
        xend = mean + rms * first_bin_fit_n_sigma_range
        self.log("Fit gauss in range %f (%f) %f" % (xstart, mean, xend))
        Fit = ROOT.TF1("Fit", "gaus", xstart, xend)
        ROOT.gROOT.SetBatch(False)
        Histo_S_binned.Fit("Fit", "R Q N")
        param0 = Fit.GetParameter(0)
        param1 = Fit.GetParameter(1)
        param2 = Fit.GetParameter(2)
        self.log("Fit parameters ampl=%f  mean=%f  err=%f" % (param0, param1, param2))

        if not self.store(Fit, "fit_%s" % (region)):
            print "Warning: unable to store fit result..."

        # no fit
        nofit_max_bin = Histo_S_binned.GetMaximumBin()

        nofit_max_bin_content = Histo_S_binned.GetBinContent(nofit_max_bin)

        nofit_max_bin_x = Histo_S_binned.GetBinCenter(nofit_max_bin)

        nofit_max_bin_original = Histo_S.FindBin(nofit_max_bin_x)

        nofit_max_bin_x_original = Histo_S.GetBinCenter(nofit_max_bin_original)

        self.log(
            "No-fit - maximum signal %f in bin %i  with bin center %f. Original bin %i %f:%f" %
            (nofit_max_bin_content, nofit_max_bin, nofit_max_bin_x, nofit_max_bin_original, Histo_S.GetBinLowEdge(nofit_max_bin_original),
             Histo_S.GetBinLowEdge(nofit_max_bin_original) + Histo_S.GetBinWidth(nofit_max_bin_original)))

        self.log("Maximum signal is found at %f of bin %i" % (nofit_max_bin_x_original, nofit_max_bin_original))

        max_bin_original = Histo_S.FindBin(param1)

        max_bin_x_original = Histo_S.GetBinCenter(max_bin_original)

        self.log("Maximum signal from fit is found at %f of bin %i" % (max_bin_x_original, max_bin_original))

        self.log("Best bin from fit corresponds to original bin %i %f:%f" %
                 (max_bin_original, Histo_S.GetBinLowEdge(max_bin_original),
                  Histo_S.GetBinLowEdge(max_bin_original) + Histo_S.GetBinWidth(max_bin_original)))

        if max_bin_original <= leftmost_bin or max_bin_original >= rightmost_bin:
            self.log("ERROR: Bin %i with maximum significance coincides with edge bins left %i or right %i" %
                     (max_bin_original, leftmost_bin, rightmost_bin))
            self.error()

        if math.fabs(max_bin_original - leftmost_bin) < math.fabs(max_bin_original - rightmost_bin):
            self.log("ERROR: Bin %i with maximum significance is closer to the leftmost bin %i where most of bkg is " %
                     (max_bin, leftmost_bin))
            self.error()

        #take this bin as starting point for the binning algorithm. Create the first bin:
        first_bin_undefined = True
        first_bin_hits_high_edge = False
        first_bin_hits_low_edge = False
        first_bin_S_sum = 0.
        first_bin_B_sum = 0.
        first_bin_dB2_sum = 0.
        first_bin_low_edge = 0.
        first_bin_high_edge = 0.
        ibin = max_bin_original
        ibin_left = max_bin_original
        ibin_right = max_bin_original
        go_bin_right = True
        self.log("Searching for first bin (highest Sig)")
        while first_bin_undefined:

            if ibin < leftmost_bin:
                self.log("ERROR: bin  %i went below lowest edge bin %i, started from %i. Max significance %f" %
                         (ibin, leftmost_bin, max_bin_original, significance))
                first_bin_hits_low_edge = True
                self.error()
                break

            if ibin > rightmost_bin:
                self.log("ERROR: bin  %i went above highest edge bin %i, started from %i. Max significance %f" %
                         (ibin, rightmost_bin, max_bin_original, significance))
                first_bin_hits_high_edge = True
                self.error()
                break

            #read off bin info
            y_B = Histo_B.GetBinContent(ibin)
            dy_B = Histo_B.GetBinError(ibin)
            y_S = Histo_S.GetBinContent(ibin)
            x_low = Histo_S.GetBinLowEdge(ibin_left)
            x_high = Histo_S.GetBinLowEdge(ibin_right) + Histo_S.GetBinWidth(ibin_right)

            #overall measures
            first_bin_S_sum += y_S
            first_bin_B_sum += y_B
            first_bin_dB2_sum += dy_B**2

            rel_dy_B = math.sqrt(first_bin_dB2_sum) / math.fabs(first_bin_B_sum) if math.fabs(first_bin_B_sum) > 0. else 0.

            if x_low > max_bin_x_original:
                self.log("ERROR: bin center %f violated by low=%f at bin %i" % (max_bin_x_original, x_low, ibin))
                self.error()
                break

            if x_high < max_bin_x_original:
                self.log("ERROR: bin center %f violated by high=%f at bin %i" % (max_bin_x_original, x_high, ibin))
                self.error()
                break

            significance = self.get_significance(significance_definition, first_bin_S_sum, first_bin_B_sum, rel_dy_B)

            sig_rule = significance > significance_threshold

            #print "\nBin=%i binL=%i binR=%i S=%f B=%f Sig=%f xlow=%f xhigh=%f" % (ibin, ibin_left, ibin_right, first_bin_S_sum,
            #                                                                      first_bin_B_sum, significance, x_low, x_high)

            if sig_rule:
                first_bin_high_edge = x_high
                first_bin_low_edge = x_low
                first_bin_undefined = False
            else:
                #go right
                if go_bin_right:
                    ibin_right += 1
                    ibin = ibin_right
                    go_bin_right = False
                    #print "R", ibin
                #go left
                else:
                    ibin_left -= 1
                    ibin = ibin_left
                    go_bin_right = True
                    #print "L", ibin

        #done, report results
        if first_bin_undefined:
            self.log("ERROR: first bin is undefined")
            self.error()
        else:
            self.log("First bin is defined %f:%f width=%f Sig=%f" %
                     (first_bin_low_edge, first_bin_high_edge, first_bin_high_edge - first_bin_low_edge, significance))

        self.log("Adding first bin left=%f right=%f" % (first_bin_low_edge, first_bin_high_edge))
        self.__optimal_binning.append(first_bin_low_edge)
        self.__optimal_binning.append(first_bin_high_edge)

        self.log("First bin with maximum signal %f : %f width=%f Sig=%f" %
                 (first_bin_low_edge, first_bin_high_edge, math.fabs(math.fabs(first_bin_high_edge) - math.fabs(first_bin_low_edge)),
                  significance))
        self.log("First bin with maximum signal hits the low edge?  %i %s" %
                 (first_bin_hits_low_edge, "<====" if first_bin_hits_low_edge else "OK"))
        self.log("First bin with maximum signal hits the high edge? %i %s" %
                 (first_bin_hits_high_edge, "<====" if first_bin_hits_high_edge else "OK"))

        #find right bins
        self.log("Searching for right bins")
        sum_y_S = 0.
        sum_y_B = 0.
        sum_dy_B2 = 0.
        for i in xrange(ibin_right + 1, rightmost_bin + 1, 1):  #range (right bin, right most]

            #bkg
            y_B = Histo_B.GetBinContent(i)
            dy_B = Histo_B.GetBinError(i)
            x_low = Histo_B.GetBinLowEdge(i)
            x_high = x_low + Histo_B.GetBinWidth(i)

            #signal
            y_S = Histo_S.GetBinContent(i)
            sum_y_S += y_S

            #update stats & sum up contributions
            #consider negative bins in the integral
            sum_y_B += y_B
            sum_dy_B2 += dy_B**2
            #for the rel. error estimate consider the abs value of total bkg
            rel_dy_B = math.sqrt(sum_dy_B2) / math.fabs(sum_y_B) if math.fabs(sum_y_B) > 0. else 0.

            #signifnicance depending on cumulative S and B for every new bin
            significance = self.get_significance(significance_definition, sum_y_S, sum_y_B, rel_dy_B)

            #optimization rules
            pos_bkg_rule = sum_y_B > 0.

            stat_rule = rel_dy_B < right_bins_stat_bin_fix_threshold and rel_dy_B > 0.

            sig_rule = significance > significance_threshold

            yields_rule = pos_bkg_rule and (stat_rule or sig_rule)

            bound_rule = i == rightmost_bin

            total_rule = yields_rule or bound_rule

            #if rule is satisfied
            if total_rule:
                right_bin = Histo_B.GetBinLowEdge(i) + Histo_B.GetBinWidth(i)
                #bound is added anyway
                if not bound_rule:
                    self.__optimal_binning.append(right_bin)
                    self.log("Right bin %i found at %f Sig=%f reldB=%f" % (i, right_bin, significance, rel_dy_B))
                else:
                    self.log("Rightmost bin at %f found visited but not added (added already)" % (right_bin))
                #reset
                sum_y_B = sum_dy_B2 = sum_y_S = 0.

        #find left bins
        self.log("Searching for left bins")
        sum_y_S = 0.
        sum_y_B = 0.
        sum_dy_B2 = 0.
        total_signal = Histo_S.Integral()
        signal_quantile = 0.
        signal_cumulative = 0.
        for i in xrange(ibin_left - 1, leftmost_bin - 1, -1):  #range (left bin, left most]

            #bkg
            y_B = Histo_B.GetBinContent(i)
            dy_B = Histo_B.GetBinError(i)
            x_low = Histo_B.GetBinLowEdge(i)
            x_high = x_low + Histo_B.GetBinWidth(i)

            #signal
            y_S = Histo_S.GetBinContent(i)
            sum_y_S += y_S
            signal_cumulative += y_S
            signal_quantile = (total_signal - signal_cumulative) / total_signal

            #update stats & sum up contributions
            #consider negative bins in the integral
            sum_y_B += y_B
            sum_dy_B2 += dy_B**2
            #for the rel. error estimate consider the abs value of total bkg
            rel_dy_B = math.sqrt(sum_dy_B2) / math.fabs(sum_y_B) if math.fabs(sum_y_B) > 0. else 0.

            #signifnicance depending on cumulative S and B for every new bin
            significance = self.get_significance(significance_definition, sum_y_S, sum_y_B, rel_dy_B)

            #optimization rules
            pos_bkg_rule = sum_y_B > 0.

            if signal_quantile > left_bins_signal_quantile_threshold:
                stat_rule = rel_dy_B < left_bins_stat_bin_fix_threshold_high_signal_quantile and rel_dy_B > 0.
            else:
                stat_rule = rel_dy_B < left_bins_stat_bin_fix_threshold_low_signal_quantile and rel_dy_B > 0.

            sig_rule = significance > significance_threshold

            yields_rule = pos_bkg_rule and (stat_rule or sig_rule)

            bound_rule = i == leftmost_bin

            total_rule = yields_rule or bound_rule

            #if rule is satisfied
            if total_rule:
                left_bin = Histo_B.GetBinLowEdge(i)
                #bound is added anyway
                if not bound_rule:
                    self.__optimal_binning.append(left_bin)
                    self.log("Left bin %i found at %f Sig=%f reldB=%f quantile=%f" % (i, left_bin, significance, rel_dy_B, signal_quantile))
                else:
                    self.log("Leftmost bin at %f found visited but not added (added already)" % (left_bin))

                #reset
                sum_y_B = sum_dy_B2 = sum_y_S = 0.

        #sort
        self.__optimal_binning.sort()

        #cross-check before rounding
        x_optimal_binning = numpy.array([x for x in self.__optimal_binning])
        Histo_B_rebinned = Histo_B.Rebin(len(x_optimal_binning) - 1, "", x_optimal_binning)
        #RWP.Print(Histo_B_rebinned)

        first_bin_is_empty = Histo_B_rebinned.GetBinContent(1) < empty_bin_maximum_content
        last_bin_is_empty = Histo_B_rebinned.GetBinContent(Histo_B_rebinned.GetNbinsX()) < empty_bin_maximum_content

        self.log("Before rounding")
        self.log("N bins: %i" % (Histo_B_rebinned.GetNbinsX()))
        self.log("Binning: %s" % (' : '.join(str(e) for e in x_optimal_binning)))
        self.log("First/Last bin content %f/%f" %
                 (Histo_B_rebinned.GetBinContent(1), Histo_B_rebinned.GetBinContent(Histo_B_rebinned.GetNbinsX())))
        self.log("First/Last bin is empty %i/%i" % (first_bin_is_empty, last_bin_is_empty))

        #regulate binning - after rounding
        self.__optimal_binning = self.regulate(self.__optimal_binning, first_bin_is_empty, last_bin_is_empty)
        x_optimal_binning = numpy.array([x for x in self.__optimal_binning])
        Histo_B_rebinned = Histo_B.Rebin(len(x_optimal_binning) - 1, "", x_optimal_binning)

        first_rounded_bin_is_empty = Histo_B_rebinned.GetBinContent(1) < empty_bin_maximum_content
        last_rounded_bin_is_empty = Histo_B_rebinned.GetBinContent(Histo_B_rebinned.GetNbinsX()) < empty_bin_maximum_content

        self.log("After rounding")
        self.log("N bins: %i" % (Histo_B_rebinned.GetNbinsX()))
        self.log("Binning: %s" % (' : '.join(str(e) for e in x_optimal_binning)))
        self.log("First/Last bin content %f/%f" %
                 (Histo_B_rebinned.GetBinContent(1), Histo_B_rebinned.GetBinContent(Histo_B_rebinned.GetNbinsX())))
        self.log("First/Last bin is empty %i/%i" % (first_rounded_bin_is_empty, last_rounded_bin_is_empty))

        if first_bin_is_empty != first_rounded_bin_is_empty:
            self.log("ERROR: leftmost bin content changed by rounding")
            self.error()

        if last_bin_is_empty != last_rounded_bin_is_empty:
            self.log("ERROR: rigthmost bin content changed by rounding")
            self.error()

        #join leftmost bins? boundary bin is not empty
        self.log("Merging of the left boundary bins")
        for i in xrange(0, len(self.__optimal_binning) - 1):
            current = self.__optimal_binning[i]
            next = self.__optimal_binning[i + 1]
            size = math.fabs(math.fabs(next) - math.fabs(current))
            self.log(" %i:%i %f:%f d=%f" % (i, i + 1, current, next, size))

        if not first_rounded_bin_is_empty and leftmost_bin_low_content_join_with_next and len(self.__optimal_binning) > 2:  #need 2 bins
            if self.abs_diff(self.__optimal_binning[0], self.__optimal_binning[1]) < self.abs_diff(
                    self.__optimal_binning[1], self.__optimal_binning[2]):
                self.log("Leftmost bins merged including boundary (bins 0-1 vs 1-2)")
                self.log("Optimal binning before merging leftmost bins: %s" % (' : '.join(str(e) for e in self.__optimal_binning)))
                del self.__optimal_binning[1]  #this joins the 1st with 2nd bin
                self.log("Optimal binning after merging leftmost bins: %s" % (' : '.join(str(e) for e in self.__optimal_binning)))
            else:
                self.log("Non-empty leftmost bin: No merging needed (bins 0-1 vs 1-2)")
        #join leftmost bins? boundary bin is  empty
        elif first_rounded_bin_is_empty and leftmost_bin_low_content_join_with_next and len(self.__optimal_binning) > 3:  #need 3 bins
            if self.abs_diff(self.__optimal_binning[1], self.__optimal_binning[2]) < self.abs_diff(
                    self.__optimal_binning[2], self.__optimal_binning[3]):
                self.log("Leftmost bins merged excluding boundary (bins 1-2 vs 2-3)")
                self.log("Optimal binning before merging leftmost bins: %s" % (' : '.join(str(e) for e in self.__optimal_binning)))
                del self.__optimal_binning[2]  #this joins the 2nd with 3rd bin
                self.log("Optimal binning after merging leftmost bins: %s" % (' : '.join(str(e) for e in self.__optimal_binning)))
            else:
                self.log("Empty leftmost bin: No merging needed (bins 1-2 vs 2-3)")
        else:
            self.log("No merging of leftmost bins")

        x_optimal_binning = numpy.array([x for x in self.__optimal_binning])

        self.log("Final optimal binning: %s" % (' : '.join(str(e) for e in x_optimal_binning)))
        self.log("Final N bins: %i" % (len(x_optimal_binning) - 1))
        self.log("ERRORS during optimization: %i" % (self.errors()))

        return BinningResults(True, x_optimal_binning, first_rounded_bin_is_empty, last_rounded_bin_is_empty)

    def optimal_binning_alpha(self, total=None, signal=None):  #inputs can be either Asimov or Input

        results = BinningResults(optimized=False)

        #signifnicance depending on cumulative S and B for every new bin
        use_Zn_significance = True

        #get histos here
        Histo_B = total.histogram
        Histo_S = signal.histogram
        if not Histo_S:
            print "Error: null signal histo in optimal binning...."
            return results

        if not Histo_B:
            print "Error: null bkg histo in optimal binning...."
            return results

        #no more dependence on input types
        N_B = Histo_B.GetNbinsX()
        N_S = Histo_S.GetNbinsX()
        if N_B != N_S:
            print "Error: rebinning not possible due to unequal B and S templates for Asimov %s" % (total.identity)
            return results
        else:
            print "Info: %i bins found..." % (N_B)

        print "Info: original S %f, original B %f" % (Histo_S.Integral(), Histo_B.Integral())

        #normal loop -> find first sensible entry
        leftmost_bin = 1
        for i in xrange(1, N_B + 1, 1):  #range [1, N]
            if Histo_B.GetBinContent(i) > 0:
                leftmost_bin = i
                print "Info: Leftmost bin found", leftmost_bin
                break

        #reverse loop
        sum_y_S = 0.
        sum_y_B = 0.
        sum_dy_B2 = 0.
        total_signal = Histo_S.Integral()
        signal_quantile = 0.
        signal_cumulative = 0.
        high_edge_found = False
        for i in range(N_B, 0, -1):  # range = [N, 1] backwards
            #bkg
            y_B = Histo_B.GetBinContent(i)
            dy_B = Histo_B.GetBinError(i)
            x_low = Histo_B.GetBinLowEdge(i)
            x_high = x_low + Histo_B.GetBinWidth(i)

            #signal
            y_S = Histo_S.GetBinContent(i)
            sum_y_S += y_S
            signal_cumulative += y_S
            signal_quantile = (total_signal - signal_cumulative) / total_signal

            #keep anyway the high edge bin and continue
            if not high_edge_found:
                if y_B > 0:
                    high_edge_found = True
                    self.add_bin(0, Bin(bin=x_high, highest_bin=True))
                    print "Info: Righmost bin found %i with low edge %f high edge %f content %f ..." % (i, x_low, x_high, y_B)

            #catch first leftmost empty bin and force optimization to stop there
            if i == leftmost_bin:
                print "Info: Leftmost bin catched %i with low edge %f content %f ..." % (i, x_low, y_B)
                self.add_bin(0, Bin(bin=x_low, lowest_bin=True))
                #here we stop the scan
                break

            #update stats & sum up contributions
            #consider negative bins in the integral
            sum_y_B += y_B
            sum_dy_B2 += dy_B**2
            #for the rel. error estimate consider the abs value of total bkg
            rel_dy_B = math.sqrt(sum_dy_B2) / math.fabs(sum_y_B) if math.fabs(sum_y_B) > 0. else 0.

            if use_Zn_significance:
                signifnicance = self.Zn_expected_significance(sum_y_S, sum_y_B, rel_dy_B)
            else:
                signifnicance = self.asimov_median_expected_significance(sum_y_S, sum_y_B)

            #optimization rules
            pos_bkg_rule = sum_y_B > 0.

            stat_rule = rel_dy_B < self.get_stat_bin_threshold(signal_quantile) and rel_dy_B > 0.

            sig_rule = signifnicance > self.get_sign_bin_threshold(use_Zn=use_Zn_significance)

            total_rule = pos_bkg_rule and (stat_rule or sig_rule)

            #if rule is satisfied
            if total_rule:
                #store position
                self.add_bin(
                    0,
                    Bin(bin=x_low,
                        middle_bin=True,
                        stats_pass=stat_rule,
                        stat=rel_dy_B,
                        sign_pass=sig_rule,
                        sign=signifnicance,
                        quantile=signal_quantile))
                print "Info: appended new bin found at", x_low
                #reset everything and restart optimizing the next bin
                sum_y_B = sum_dy_B2 = 0.
                sum_y_S = 0.
            #done here

        #TH1::Rebin requires the original boundaries to be there
        high_boundary = Histo_B.GetBinLowEdge(N_B) + Histo_B.GetBinWidth(N_B)
        print "Info: attaching high boundary", high_boundary
        self.append_bin(Bin(high_boundary))

        low_boundary = Histo_B.GetBinLowEdge(1)
        print "Info: attaching low boundary", low_boundary
        if leftmost_bin != 1:  #avoid the physical left boundary to be inserted twice
            self.add_bin(0, Bin(low_boundary))

        #print outcome
        x_optimal_binning = numpy.array([x.bin for x in self.__optimal_binning])
        print "Info: optimal binning array (alpha)", x_optimal_binning

        #cross-check here
        Histo_B_rebinned = Histo_B.Rebin(len(x_optimal_binning) - 1, "", x_optimal_binning)
        print "Info: cross-check bin stats for %i bins" % (Histo_B_rebinned.GetNbinsX())
        RWP.Print(Histo_B_rebinned)

        print "Info: first bin content", Histo_B_rebinned.GetBinContent(1)
        print "Info: last bin content", Histo_B_rebinned.GetBinContent(Histo_B_rebinned.GetNbinsX())
        first_bin_is_empty = Histo_B_rebinned.GetBinContent(1) < 1e-5
        last_bin_is_empty = Histo_B_rebinned.GetBinContent(Histo_B_rebinned.GetNbinsX()) < 1e-5

        print "Info: bin details"
        for bin in self.__optimal_binning:
            print bin.summary()

        print "Info: number of bins %i (initially %i)" % (len(self.__optimal_binning), N_B)

        return BinningResults(True, x_optimal_binning, first_bin_is_empty, last_bin_is_empty)

    def rebin_and_adjust_range(self, histo=None, identity="", binning_results=BinningResults()):

        bin_array = binning_results.bin_array
        first_bin_is_empty = binning_results.first_bin_is_empty
        last_bin_is_empty = binning_results.last_bin_is_empty

        if not len(bin_array):
            print "Error: empty bin array - nothing to rebin:", bin_array
            return None

        if not histo:
            print "Error: null input histo - nothing to rebin..."
            return None

        #rebin
        print "\nInfo: treating", identity
        print "Info: input bin array", bin_array
        print "Info: first bin is empty", first_bin_is_empty
        print "Info: last bin is empty", last_bin_is_empty

        rebinned = histo.Rebin(len(bin_array) - 1, "", bin_array)

        #remove empty edges - use the rebinned!
        histo_bins = []
        N = rebinned.GetNbinsX()
        for i in range(1, N + 1):  # range [1, N]
            y = rebinned.GetBinContent(i)
            dy = rebinned.GetBinError(i)
            x_low = rebinned.GetBinLowEdge(i)
            x_high = x_low + rebinned.GetBinWidth(i)

            if i == 1 and not first_bin_is_empty:
                histo_bins.append(HistoBin(y, dy, x_low, x_high))

            if i == N and not last_bin_is_empty:
                histo_bins.append(HistoBin(y, dy, x_low, x_high))

            if i > 1 and i < N:
                histo_bins.append(HistoBin(y, dy, x_low, x_high))
                if y < 0.:
                    print "Warning: Do you want to add a negative bin? bin = %i, y = %f" % (i, y)

        print "Info: rearrange - rebinned histo %i bins, new rearranged histo %i bins" % (N, len(histo_bins))

        #control potential leakage of signal (basically)
        y = rebinned.GetBinContent(N)
        dy = rebinned.GetBinError(N)
        x_low = rebinned.GetBinLowEdge(N)
        x_high = x_low + rebinned.GetBinWidth(N)
        overflow_bin = HistoBin(y, dy, x_low, x_high)
        if overflow_bin.empty():
            print "Info: Last bin is empty for", histo.GetName()
        else:
            print "Info: Last bin is NOT empty for %s y=%f" % (histo.GetName(), overflow_bin.y)

        #get out the EFFECTIVE array of bins
        bins_array = []
        for histobin in histo_bins:
            bins_array.append(histobin.low_edge)

        #append also high edge as requested by TH1::Rebin
        bins_array.append(histo_bins[-1].high_edge)

        print "Info: rearrange - bin array for TH1 will have %i bins" % (len(bins_array) - 1)
        print "Info: array for TH1", bins_array
        rearranged = ROOT.TH1D(histo.GetName() + identity + "_rebinned", histo.GetTitle(),
                               len(bins_array) - 1, array.array('d', bins_array))

        rearranged.SetDirectory(0)

        for counter, bin in enumerate(histo_bins):
            i = counter + 1

            x_low = rearranged.GetBinLowEdge(i)
            x_high = x_low + rearranged.GetBinWidth(i)

            if not (math.fabs(bin.x_low - x_low) < 1e-5 and math.fabs(bin.x_high - x_high) < 1e-5):
                print "Error: oops - bin array and new histogram do not match in x-axis for bin %i ..." % (i)
                print "\tCreated histo: ", x_low, x_high
                print "\tBin from array:", bin.x_low, bin.x_high
                print "\tBins array used for histo:", bins_array
                return None

            rearranged.SetBinContent(i, bin.y)
            rearranged.SetBinError(i, bin.dy)

        #include leaks in last bin
        if not overflow_bin.empty():
            NR = rearranged.GetNbinsX()
            high_edge = rearranged.GetBinLowEdge(NR) + rearranged.GetBinWidth(NR)

            overflow_low_edge = overflow_bin.low_edge

            if math.fabs(overflow_low_edge - high_edge) > 1e-8:
                print "Error: compatibility error of overflow bin with upper edge..."
            else:
                y = rearranged.GetBinContent(NR)
                dy = rearranged.GetBinError(NR)

                Y = y + overflow_bin.y
                dY = math.sqrt(dy**2 + overflow_bin.dy**2)

                rearranged.SetBinContent(NR, Y)
                rearranged.SetBinError(NR, dY)

                print "Info: overflow bin included %f vs old %f" % (y, Y)

        return rearranged

    def truncate_bins(self, _histo=None, _side="", _histo_bin=0):
        """
        Truncates a histo bin range. By default, 0, "" is doing nothing.
        Possibilities:  
         "low", N --> truncates up to Nth bin
         "high", N --> truncates from N to end
        """
        allowed_side_values = ["low", "high"]

        if not _histo:
            print "Error: null input histo - nothing to truncate..."
            return None

        if _side not in ["low", "high"]:
            print "Error: undefined side- nothing to truncate..."
            return _histo

        # array of bins
        bins_n = _histo.GetNbinsX()
        bins_array = array.array('d', [0] * (bins_n))  #book an array of zeros
        _histo.GetXaxis().GetLowEdge(bins_array)

        #range checks
        if _histo_bin <= 0:
            print "Info: null bin %i - nothing to truncate..." % (_histo_bin)
            return _histo
        elif _histo_bin == bins_n and _side == "low":
            print "Error: asking to truncate all bins side=%s bin=%i" % (_side, _histo_bin)
            return _histo
        elif _histo_bin == 1 and _side == "high":
            print "Error: asking to truncate all bins side=%s bin=%i" % (_side, _histo_bin)
            return _histo
        else:
            print "Info: about to truncate  bins side=%s bin=%i" % (_side, _histo_bin)

        #prepare range for array
        if _side == "low":
            _array_bin_first = 0
            _array_bin_last = _histo_bin
        elif _side == "high":
            _array_bin_first = _histo_bin
            _array_bin_last = bins_n
        else:
            print "Error: undefined side- nothing to truncate..."
            return _histo

        #new bin array
        print "Info: truncate histo range %i %s, array range %i:%i out of 0:%i" % (_histo_bin, _side, _array_bin_first, _array_bin_last,
                                                                                   len(bins_array))

        print "Info: truncate - OLD BINS", bins_array
        bins_array_new = bins_array
        del bins_array_new[_array_bin_first:_array_bin_last]
        print "Info: truncate - NEW BINS", bins_array_new

        #new histo
        _histo_new = ROOT.TH1D(_histo.GetName() + "_truncated", _histo.GetTitle(), len(bins_array_new) - 1, bins_array_new)
        _histo_new.SetDirectory(0)

        epsilon = 1e-6
        for i in xrange(1, _histo_new.GetNbinsX() + 1):  #[1, N]
            bin_low_edge_new = _histo_new.GetBinLowEdge(i)
            for j in xrange(1, _histo.GetNbinsX() + 1):  #[1, M]
                bin_low_edge = _histo.GetBinLowEdge(j)
                if math.fabs(bin_low_edge_new - bin_low_edge) < epsilon:
                    _histo_new.SetBinContent(i, _histo.GetBinContent(j))
                    _histo_new.SetBinError(i, _histo.GetBinError(j))
                    break
        #for i, bin in enumerate(bins_array_new):
        #    print "truncate", i, bin

        _histo_new.SetName(_histo.GetName())

        return _histo_new
