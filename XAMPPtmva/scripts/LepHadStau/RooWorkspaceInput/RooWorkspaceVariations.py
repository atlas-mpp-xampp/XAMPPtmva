import RooWorkspaceVariation as RWV


class Variations(object):
    """
    Class to cache a list of variations
    """
    def __init__(self):
        self.__variations = []
        self.__nominal_only = False

    def exists(self, variation=None):
        for v in self.__variations:
            if v.fullname == variation.fullname:
                return True

        return False

    def add(self, name="", nickname=""):
        variation = RWV.Variation(name, nickname, "")
        if not self.exists(variation):
            self.__variations.append(variation)

    def add_one_sided(self, name="", nickname="", side="", special=False):
        variation = RWV.Variation(name, nickname, side, special)
        if not self.exists(variation):
            self.__variations.append(variation)
        else:
            print "Warning: %s duplicated and this skipped..." % (variation.identity)

    def add_one_sided_up(self, name="", nickname="", special=False):
        self.add_one_sided(name, nickname, "1up", special)

    def add_one_sided_down(self, name="", nickname="", special=False):
        self.add_one_sided(name, nickname, "1down", special)

    def add_double_sided(self, name="", nickname="", special=False):
        self.add_one_sided_up(name, nickname, special)
        self.add_one_sided_down(name, nickname, special)

    @property
    def list(self):
        return self.__variations

    def show(self):
        for v in self.__variations:
            print v.fullname, v.nickname

    @property
    def nominal_only(self):
        return self.__nominal_only

    @nominal_only.setter
    def nominal_only(self, value):
        self.__nominal_only = value
