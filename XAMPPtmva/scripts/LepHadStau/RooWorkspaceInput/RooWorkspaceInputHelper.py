import ROOT, sys
import RooWorkspacePrint as RWP


class InputHelper(object):
    """
    Give proper names to tree branches, obtain stuff etc
    """
    def __init__(self):
        pass

    def generator(self, name=""):
        if "SHERPA" in name.upper():
            return "Sherpa"

        if "POWHEG" in name.upper():
            return "Powheg"

        return "Gen"

    def process(self, name=""):
        if "DATA" in name.upper():
            return "Data"

        if "ZLL" in name.upper():
            return "Zll"

        if "ZTAUTAU" in name.upper() or "ZTT" in name.upper():
            return "Ztautau"

        if "TOP" in name.upper():
            return "Top"

        if "WJETS" in name.upper():
            return "Wjets"

        if "STAU" in name.upper():
            return "Signal"

        if "MULTIV" in name.upper():
            return "MultiV"

        if "TTV" in name.upper():
            return "ttV"

        if "HIGGS" in name.upper():
            return "Higgs"

        print "Error: RooWorkspaceInputHelper - sample name '%s' not understood. Bailing out." % (name)
        sys.exit(1)

    def get_identity(self, folder="", subfolder="", name=""):
        return folder + "_" + subfolder + "_" + name

    def get_folder_name(self, channel="", region_type="", region=""):
        channel = channel.replace("#tau", "had").replace("e", "ele").replace("#mu", "mu")
        return channel + "_" + region_type + "_" + region

    def get_subfolder_name(self, name=""):
        name = self.generator(name) + "_" + self.process(name)
        return "Data" if "DATA" in name.upper() else name

    def get_histo_name(self, Variation=None):
        return Variation.fullnickname

    def get_histo(self, filename="", Variation=None, channel="", region="", classifier="", binning=""):
        variation = Variation.fullname
        inputfile = ROOT.TFile.Open(filename)

        if inputfile.IsZombie():
            print "Warning: RooWorkspaceInputHelper::get_histo- Input file %s is zombie ..." % (filename)
            return None

        histoname = "StauMVA_%s/%s_%s/%sScore%s" % (variation, channel, region, classifier, binning)

        histo = inputfile.Get(histoname)

        if not histo:
            print "Warning: RooWorkspaceInputHelper::get_histo - Input histo=%s of file=%s is null for variation=%s channel=%s region=%s..." % (
                histoname, filename, variation, channel, region)
            return None

        if not (issubclass(type(histo), ROOT.TObject) and "TH" in histo.IsA().GetName()):
            print "Warning: RooWorkspaceInputHelper::get_histo - Input histo %s of %s is not a TH class..." % (filename)
            return None

        #necessary
        histo.SetDirectory(0)

        return histo

    def get_is_data(self, name=""):
        return name.upper() in self.process(name).upper()

    def get_is_signal(self, name=""):
        return "STAU" in name.upper()
