##
## Reads MVA test outputs
## Creates the input for HistFactory
## 1st implementation: July 2018
## systematics: Aug 2018
## Asimov B+S: Sept 2018
## Asimov B or B+S choice: Oct 2018

import ROOT
import sys, os, glob
from decimal import Decimal, ROUND_UP, ROUND_DOWN, ROUND_HALF_UP

import RooWorkspaceOptions as RWO
import RooWorkspaceStore as RWS
import RooWorkspaceInputs as RWI
import RooWorkspaceVariationSetup as RWVarSet
from RooWorkspaceInputs import RMethod as RM


def main(argv):

    options = RWO.Options("Workspace options").instance

    veto_samples = ["PowHegPy8_Wjets", "PowHegPy8_Zll", "PowHegPy8_Ztautau"]

    signal_tag = "StauStau"

    files = []
    for path in options.input_paths:
        files.extend(glob.glob(os.path.join(path, "*.root")))

    if not files:
        print "Error: No files loaded from '%s'" % (path)
        return

    for vsample in veto_samples:
        for ifile in files:
            if vsample in ifile:
                files.remove(ifile)

    for ifile in files:
        if signal_tag in ifile:
            if not options.mass in ifile:
                files.remove(ifile)

    signal_included = 0
    for ifile in files:
        if signal_tag in ifile:
            signal_included += 1

    if not signal_included:
        print "Error: no signal selected..."
        return
    else:
        print "Info: signals selected %i" % (signal_included)

    print "Info: selected files"
    for ifile in files:
        print ifile

    #variations
    variations = RWVarSet.get_variations()
    if options.systematics:
        variations.show()
    else:
        variations.nominal_only = True

    primary_regions = options.primary_regions

    subsidiary_regions = options.subsidiary_regions

    channels = ["e#tau", "#mu#tau"]

    classifier = options.classifier

    binning = options.binning

    asimov = options.asimov

    hybrid = False

    rebinning = options.rebin

    lumi = options.luminosity

    rmethod = RM.GAMMA if options.rebin_algo == "gamma" else RM.BETA if options.rebin_algo == "beta" else RM.ALPHA

    extra_regions = True  #creates low-Score CRs

    store = RWS.Store(options.output_path_name, lumi, options.monitor)

    #create inputs and rebin
    inputs = RWI.InputsList(files=files,
                            variations=variations,
                            channels=channels,
                            primary_regions=primary_regions,
                            subsidiary_regions=subsidiary_regions,
                            classifier=classifier,
                            binning=binning,
                            lumi=lumi,
                            asimov=asimov,
                            hybrid=hybrid,
                            rebinning=rebinning,
                            rmethod=rmethod,
                            extra_regions=extra_regions,
                            store=store)

    #normal inputs stored in structure
    for _input in inputs.inputs_list:
        if not store.store(_input):
            print "Eror: Fail to store input", _input.identity

        if options.monitor:
            if not store.keep(_input):
                print "Error: Fail to store input monitor", _input.identity

    #lumi info
    if not store.store_lumi():
        print "Error: Failed to store lumi info ..."

    #asimov models: B+S
    for _asimov in inputs.asimov_B_plus_S_list():

        #only if asked
        if asimov:
            if not store.override(_asimov):
                print "Error: Fail to override with asimov", _asimov.identity

        #keep for cross-checks
        if not store.store_as(_asimov, "B_plus_S"):
            print "Error: Fail to insert asimov", _asimov.identity

        #monitor stuff
        if options.monitor:
            if not store.keep(_asimov, "B_plus_S"):
                print "Error: Fail to store asimov monitor", _asimov.identity

    #asimov models: B
    for _asimov in inputs.asimov_B_list():

        #keep for cross-checks
        if not store.store_as(_asimov, "B_only"):
            print "Error: Fail to insert asimov", _asimov.identity

        #monitor stuff
        if options.monitor:
            if not store.keep(_asimov, "B_only"):
                print "Error: Fail to store asimov monitor", _asimov.identity

    #asimov models: S
    for _asimov in inputs.asimov_S_list():

        #keep for cross-checks
        if not store.store_as(_asimov, "S_only"):
            print "Error: Fail to insert asimov", _asimov.identity

        #monitor stuff
        if options.monitor:
            if not store.keep(_asimov, "S_only"):
                print "Error: Fail to store asimov monitor", _asimov.identity

    #close storage
    store.finalize()

    print "Done!"


##
# @brief execute main
##
if __name__ == "__main__":

    main(sys.argv[1:])
