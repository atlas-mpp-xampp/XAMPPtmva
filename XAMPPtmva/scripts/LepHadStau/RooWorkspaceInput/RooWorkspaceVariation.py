class Variation(object):
    """
    Class to cache a variation
    """
    def __init__(
            self,
            name="",
            nickname="",
            side="",  #empty=nominal, 1up, 1down
            special=False,  #some funny ATLAS systs miss the __1up, __1down
    ):
        self.__name = name
        self.__nickname = nickname
        self.__side = side
        self.__special = special
        #derive these
        self.__np_side = self.get_np_side()  # empty, `low`, `up`
        self.__fullname = self.get_fullname()  #from input tree structure
        self.__fullnickname = self.get_fullnickname()  #used in the output tree structure
        self.__is_nominal = self.get_is_nominal()

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, self.__class__):
            return self.__fullname == other.__fullname and \
                self.__fullnickname == other.__fullnickname

        return False

    def get_np_side(self):
        return "low" if self.__side == "1down" else "high" if self.__side == "1up" else ""

    def get_fullname(self):

        if self.__special:
            return self.__name

        return self.__name + "__" + self.__side if self.__side else self.__name

    def get_fullnickname(self):
        return self.__nickname + "_" + self.__np_side if self.__np_side else self.__nickname

    def get_is_nominal(self):
        return not self.__side

    @property
    def name(self):
        return self.__name

    @property
    def fullname(self):
        return self.__fullname

    @property
    def nickname(self):
        return self.__nickname

    @property
    def fullnickname(self):
        return self.__fullnickname

    @property
    def side(self):
        return self.__side

    @property
    def is_nominal(self):
        return self.__is_nominal

    @property
    def is_variation(self):
        return not self.__is_nominal
