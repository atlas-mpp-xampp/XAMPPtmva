import ROOT


class Print():
    """
    Prints objects
    """
    def __init__(self, _object):
        self.__object = _object
        self.__shown = self.show()

    def is_1d_histogram(self):
        return issubclass(type(self.__object), ROOT.TObject) and "TH1" in self.__object.IsA().GetName()

    def show(self):
        if self.is_1d_histogram():
            self.histo()
            return True

        print "Warning: cannot print unknown object ", self.__object
        return False

    def histo(self):

        if not self.__object:
            print "Error: cannot print out a null histo..."

        print "Info: Summary %s with %i bins" % (self.__object.GetName(), self.__object.GetNbinsX())
        for i in xrange(1, self.__object.GetNbinsX() + 1, 1):
            y = self.__object.GetBinContent(i)
            dy = self.__object.GetBinError(i)
            x_low = self.__object.GetBinLowEdge(i)
            x_high = x_low + self.__object.GetBinWidth(i)
            rel_dy = dy / y if dy > 0. else 0.
            print i, x_low, ":", x_high, "(", x_high - x_low, ")", y, rel_dy
