from variableScan import standardVariables
from ClusterSubmission.Utils import id_generator, WriteList
import os

model_path = "XAMPPtmva/InputConf/LepHadStau/TrainModels/Reco_Signals/all/PowHegReco-MidMass.conf"
model_path = "XAMPPtmva/InputConf/LepHadStau/TrainModels/Truth_Signals/all/PowHegReco-MidMass.conf"

train_path = "XAMPPtmva/TrainingConf/LepHadStau/TrainConf.conf"
run_config = "XAMPPtmva/RunConf/LepHadStau/MVA_Regions/MuoTau_SR_0jet.conf"

train_cfg = "/ptmp/mpp/junggjo9/test_stau_var_scan/training.conf"
WriteList(["Import " + train_path, "RunConfig " + run_config] +
          ["""
    New_MVAVar
        Name %s
        %s
    End_MVAVar
""" % (name, reader) for name, reader in standardVariables()], train_cfg)

cmd = "TrainSciKitLearn -i %s -t %s -d %s" % (model_path, train_cfg, train_cfg[:train_cfg.rfind("/")])
os.system(cmd)
