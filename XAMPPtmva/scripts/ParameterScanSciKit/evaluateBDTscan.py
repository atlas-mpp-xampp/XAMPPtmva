#! /usr/bin/env python
import numpy as np
import os, ROOT, math, logging

from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser
from XAMPPtmva.MVAOptimizationHelpers import SciKitMVAPerformance
from XAMPPplotting.FileStructureHandler import GetFileHandler
from ClusterSubmission.Utils import ClearFromDuplicates
from XAMPPtmva.SciKitCorrelationMatrix import createColourPalette

from XAMPPplotting.PlotUtils import *

from pprint import pprint
from itertools import product, permutations


class SciKitBDTParameters(SciKitMVAPerformance):
    def __init__(self, nominal_file, cross_valid_file):

        self.__nominal_training = nominal_file
        self.__cross_valid = cross_valid_file

        ####################################################
        #    Meta  data about the  training parameters     #
        ####################################################
        ### Name = "BDT%s_%d_%d_%.2f_%.2f_%.2f"%( (""if len(extra_name) == 0 else "_"+extra_name), nEstimators, maxDepth, minNodeSize, impuritySplit, learningRate)
        file_name_no_dir = nominal_file[nominal_file.rfind("/") + 1:nominal_file.rfind(".")]
        self.__train_name = file_name_no_dir

        self.__group_name = "_".join(sorted([fragment for fragment in file_name_no_dir.split("_") if fragment.isalpha()]))
        params = [fragment for fragment in file_name_no_dir.split("_") if not fragment.isalpha()]
        if len(params) > 4:
            self.__nEstimators = int(params[0])
            self.__max_depth = int(params[1])
            self.__min_node = float(params[2])
            self.__impurity_split = float(params[3])
            self.__learning_rate = float(params[4])
        else:
            nominal_file = cross_valid_file = ""
        SciKitMVAPerformance.__init__(self, default_file_path=nominal_file, cross_valid_file_path=cross_valid_file)

    def name(self):
        return self.__train_name

    def group(self):
        return self.__group_name

    def nTrees(self):
        return self.__nEstimators

    def minNodeSize(self):
        return self.__min_node

    def maximumDepth(self):
        return self.__max_depth

    def learningRate(self):
        return self.__learning_rate

    def impuritySplit(self):
        return self.__impurity_split

    def trainFile(self):
        return self.__nominal_training  #.GetName()

    def validiationFile(self):
        return self.__cross_valid  #.GetName()


def drawScan(Plotoptions, group_name, trainings, param_getters, param_names, slice_in_space):
    free_params = tuple(i for i in range(len(slice_in_space)) if slice_in_space[i] == -1)
    fixed_params = tuple(i for i in range(len(slice_in_space)) if slice_in_space[i] != -1)
    matching_trainings = [
        t for t in trainings if len([i for i in fixed_params if param_getters[i](t) == slice_in_space[i]]) == len(fixed_params)
    ]

    if len(matching_trainings) == 0: return False

    range_x = ClearFromDuplicates([param_getters[free_params[0]](t) for t in matching_trainings])
    range_y = ClearFromDuplicates([param_getters[free_params[1]](t) for t in matching_trainings])
    if len(range_x) <= 1 or len(range_y) <= 1: return False
    ### Let's find out if there are crosses
    y_values = []
    for x in range_x:
        y_in_x = len([param_getters[free_params[1]](t) for t in matching_trainings if param_getters[free_params[0]](t) == x])
        if y_in_x > 1: y_values += [y_in_x]
        if len(y_values) > 2: break

    if len(y_values) < 2: return False
    max_x = max(range_x)
    max_y = max(range_y)

    min_x = min(range_x)
    min_y = min(range_y)

    ### Create the histograms
    Train_Quality = ROOT.TH2D("train_quality", "Quality  of the training", len(range_x), min_x, max_x, len(range_y), min_y, max_y)

    Train_ROC_Auc = Train_Quality.Clone("train_roc_aucs")
    Test_ROC_Auc = Train_Quality.Clone("test_roc_aucs")
    Kolm_Auc = Train_Quality.Clone("kolmogorov")

    Train_Quality.GetZaxis().SetTitle("train-quality")
    Train_ROC_Auc.GetZaxis().SetTitle("ROC-AUC (training)")
    Test_ROC_Auc.GetZaxis().SetTitle("ROC-AUC (testing)")
    Kolm_Auc.GetZaxis().SetTitle("Kolmogorov score")

    ###It's super slow but I'm to lazy to write a method
    max_roc = max_quality = max_kol = -1.e12
    min_roc = min_quality = min_kol = 1.e12
    ### fill the histograms
    for t in matching_trainings:
        B = Train_Quality.FindBin(param_getters[free_params[0]](t), param_getters[free_params[1]](t))

        Train_Quality.SetBinContent(B, t.training_quality())
        Train_ROC_Auc.SetBinContent(B, t.default_perf_object().train_ROCAUC())
        Test_ROC_Auc.SetBinContent(B, t.default_perf_object().test_ROCAUC())
        Kolm_Auc.SetBinContent(B, t.default_perf_object().Kolmogorov())

        if max_roc < t.default_perf_object().train_ROCAUC(): max_roc = t.default_perf_object().train_ROCAUC()
        if max_roc < t.default_perf_object().test_ROCAUC(): max_roc = t.default_perf_object().test_ROCAUC()
        if max_quality < t.training_quality(): max_quality = t.training_quality()
        if max_kol < t.default_perf_object().Kolmogorov(): max_kol = t.default_perf_object().Kolmogorov()

        if min_roc > t.default_perf_object().train_ROCAUC(): min_roc = t.default_perf_object().train_ROCAUC()
        if min_roc > t.default_perf_object().test_ROCAUC(): min_roc = t.default_perf_object().test_ROCAUC()
        if min_quality > t.training_quality(): min_quality = t.training_quality()
        if min_kol > t.default_perf_object().Kolmogorov(): min_kol = t.default_perf_object().Kolmogorov()

    Train_Quality.GetZaxis().SetRangeUser(min_quality, max_quality)

    Train_ROC_Auc.GetZaxis().SetRangeUser(min_roc, max_roc)
    Test_ROC_Auc.GetZaxis().SetRangeUser(min_roc, max_roc)
    Kolm_Auc.GetZaxis().SetRangeUser(min_kol, max_kol)
    for H in [Train_Quality, Train_ROC_Auc, Test_ROC_Auc, Kolm_Auc]:
        pu = PlotUtils(status=Plotoptions.label, size=24)
        ### prepare the canvas
        CanvasName = "BDTScan_%s_%s-%s" % (group_name, H.GetName(), "_".join(
            ["%s(%.2f)" % (param_names[i], slice_in_space[i]) for i in fixed_params]))
        pu.Prepare1PadCanvas(CanvasName, 800, 600)
        pu.GetCanvas().SetRightMargin(0.18)
        pu.GetCanvas().SetLeftMargin(0.12)
        pu.GetCanvas().SetTopMargin(0.15)

        H.GetXaxis().SetTitle(param_names[free_params[0]])
        H.GetYaxis().SetTitle(param_names[free_params[1]])
        H.GetZaxis().SetTitleOffset(1.2 * H.GetZaxis().GetTitleOffset())
        H.GetYaxis().SetTitleOffset(0.8 * H.GetYaxis().GetTitleOffset())

        H.SetContour(1500)
        H.Draw("colz")

        if not options.noATLAS: pu.DrawAtlas(pu.GetCanvas().GetLeftMargin(), 0.92)
        pu.DrawSqrtS(pu.GetCanvas().GetLeftMargin(), 0.87)
        param_string = "".join(["%s=%.2f, " % (param_names[i], slice_in_space[i]) for i in fixed_params])
        if len(param_string) > 0: param_string = param_string[:param_string.rfind(",")]
        pu.DrawTLatex(0.4, 0.87, param_string, size=18)
        if len(options.regionLabel) > 0: pu.DrawTLatex(0.4, 0.92, options.regionLabel, size=20)

        if not Plotoptions.summaryPlotOnly: pu.saveHisto("%s/%s" % (Plotoptions.output, CanvasName), ["pdf"])
        pu.saveHisto("%s/AllEvaluationPlots%s" % (options.output, "" if len(group_name) == 0 else "_" + group_name), ["pdf"])
    return True


if __name__ == "__main__":

    parser = setupSciKitPlotParser(
        prog='createBDTscanConfig',
        description='This script generates a training config file varing all training parameters and books the methods')
    parser.set_defaults(
        inputPath="/ptmp/mpp/junggjo9/Cluster/OUTPUT/2020-06-10/LepHad_BDT/Combined_LowMass_PowHeg-BasicVariables_MuoTau_PreSel/Trainings/")
    parser.set_defaults(output="BDTScan/")

    options = parser.parse_args()
    setupPlottingEnviroment(options)

    trainings = []
    train_grps = []

    n_found = 0

    best_training = None
    largest_roc = None

    for train in os.listdir(options.inputPath):
        if not train.endswith(".root"): continue
        if train.rfind("_swapped") != -1: continue
        cross_valid = "%s/%s_swapped.root" % (options.inputPath, train[:train.rfind(".")])
        if not os.path.exists(cross_valid): continue

        if n_found % 50 == 0:
            GetFileHandler().closeAll()
            logging.info("%d trainings were loaded %d have been skipped due to too small ROC-AUC" %
                         (len(trainings), n_found - len(trainings)))

            if best_training:
                logging.info("     Best training thus far %s with %.4f ROC-AUC, %.4f kolmogorov and %.4f similarity." %
                             (best_training.name(), best_training.default_perf_object().train_ROCAUC(),
                              best_training.default_perf_object().Kolmogorov(), best_training.default_perf_object().delta_ROCAUC()))

                logging.info("     %s is the training with the largest ROC-AUC %.4f ROC-AUC, %.4f kolmogorov and %.4f similarity." %
                             (largest_roc.name(), largest_roc.default_perf_object().train_ROCAUC(),
                              largest_roc.default_perf_object().Kolmogorov(), largest_roc.default_perf_object().delta_ROCAUC()))

        ParamHelper = SciKitBDTParameters("%s/%s" % (options.inputPath, train), cross_valid)
        n_found += 1
        if ParamHelper.default_perf_object().test_ROCAUC() < 0.5 or ParamHelper.valid_perf_object().test_ROCAUC() < 0.5:
            logging.warning(
                "The training %s seems to be broken by a lot. Since it has a ROC AUC of %f and %f, respectively" %
                (ParamHelper.trainFile(), ParamHelper.default_perf_object().test_ROCAUC(), ParamHelper.valid_perf_object().test_ROCAUC()))
            continue
        trainings += [ParamHelper]
        if ParamHelper.group() not in train_grps: train_grps += [ParamHelper.group()]
        if not best_training or best_training.training_quality() < ParamHelper.training_quality(): best_training = ParamHelper
        if not largest_roc or largest_roc.default_perf_object().test_ROCAUC() < ParamHelper.default_perf_object().test_ROCAUC():
            largest_roc = ParamHelper

    logging.info("Found %d out of %d trainings which seem to be valid" % (len(trainings), n_found))
    train_param_fkt = [
        SciKitBDTParameters.nTrees, SciKitBDTParameters.minNodeSize, SciKitBDTParameters.maximumDepth, SciKitBDTParameters.learningRate,
        SciKitBDTParameters.impuritySplit
    ]
    train_param_name = ["nTrees", "minNodeSize", "maximumDepth", "learningRate", "impuritySplit"]

    ### define a scan bit matrix
    bit_matrix = ClearFromDuplicates([t for t in permutations(tuple([1, 1] + [0 for i in range(len(train_param_fkt) - 2)]))])

    for grp in train_grps:
        createColourPalette()

        ### Filter the trainings first
        filtered_trainings = [t for t in trainings if t.group() == grp]
        hyper_parameters = []
        for t in filtered_trainings:
            hyper_param = tuple(fkt(t) for fkt in train_param_fkt)
            for permut in bit_matrix:
                grid_scan = tuple(hyper_param[i] if permut[i] != 1 else -1. for i in range(len(hyper_param)))
                if grid_scan not in hyper_parameters: hyper_parameters += [grid_scan]

        ### We've found all of our different parameter settings in the BDT.
        ### Let's draw it
        DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
        DummyCanvas.SaveAs("%s/AllEvaluationPlots%s.pdf[" % (options.output, "" if len(grp) == 0 else "_" + grp))
        SavedOnce = False
        for choice in hyper_parameters:
            SavedOnce = drawScan(
                options, grp, filtered_trainings, param_getters=train_param_fkt, param_names=train_param_name,
                slice_in_space=choice) or SavedOnce

        DummyCanvas.SaveAs("%s/AllEvaluationPlots%s.pdf]" % (options.output, "" if len(grp) == 0 else "_" + grp))
        if not SavedOnce: os.system("rm %s/AllEvaluationPlots%s.pdf" % (options.output, "" if len(grp) == 0 else "_" + grp))

        if best_training:
            logging.info("     Best training thus far %s with %.4f ROC-AUC, %.4f kolmogorov and %.4f similarity." %
                         (best_training.name(), best_training.default_perf_object().train_ROCAUC(),
                          best_training.default_perf_object().Kolmogorov(), best_training.default_perf_object().delta_ROCAUC()))

            logging.info("     %s is the training with the largest ROC-AUC %.4f ROC-AUC, %.4f kolmogorov and %.4f similarity." %
                         (largest_roc.name(), largest_roc.default_perf_object().train_ROCAUC(),
                          largest_roc.default_perf_object().Kolmogorov(), largest_roc.default_perf_object().delta_ROCAUC()))
