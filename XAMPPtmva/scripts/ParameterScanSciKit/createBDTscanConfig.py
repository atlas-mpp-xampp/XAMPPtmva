#! /usr/bin/env python
import argparse
import numpy as np
from ClusterSubmission.Utils import WriteList, ResolvePath, ClearFromDuplicates
from XAMPPtmva.SciKitBDTClassifier import setupBDTParser


def generateTraining(nEstimators, maxDepth, minNodeSize, impuritySplit, learningRate, extra_args=[], extra_name=""):
    Name = "BDT%s_%d_%d_%.5f_%.5f_%.1f" % (
        ("" if len(extra_name) == 0 else "_" + extra_name), nEstimators, maxDepth, minNodeSize, impuritySplit, learningRate)
    Training = """

New_SciKitMethod
    Name %s
    Module BDTTraining
    Option --nEstimators %d --maxDepth %d --minNodeSize %f --minImpurityDecrease %f --learningRate %f  %s
End_SciKitMethod

BookMethod %s
""" % (Name, nEstimators, maxDepth, minNodeSize, impuritySplit, learningRate, " ".join(extra_args), Name)
    return Training


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='createBDTscanConfig',
        description='This script generates a training config file varing all training parameters and books the methods')
    parser.add_argument("--outDir", help="Where to store the file", default="XAMPPtmva/TrainingConf/")
    parser.add_argument("--nEstimators",
                        help="Maximum number of estimators at which boosting is terminated",
                        type=int,
                        nargs=3,
                        default=[100, 1200, 50])
    parser.add_argument("--maxDepth", help="How many nodes should be created at each tree", type=int, default=6)
    parser.add_argument("--minNodeSize",
                        help="What is the minimal amount of events needed to build a node",
                        type=float,
                        nargs=3,
                        default=[0., 0.1, 0.0025])
    parser.add_argument("--minImpurityDecrease",
                        help="A node will be split if this split induces a decrease of the impurity greater than or equal to this value.",
                        type=float,
                        nargs=3,
                        default=[0., 0.1, 0.0025])
    parser.add_argument("--learningRate", help="Learning rate of the AdaBoost classifier", type=float, nargs=3, default=[0.1, 2, 0.1])
    parser.add_argument("--useEntropySplit",
                        help="Use the 'entropy' algorithm as criterion of the best split instead of gini in the TreeClassisfier",
                        action='store_true',
                        default=False)
    parser.add_argument("--useRealBoostingAlg",
                        help="Use the 'SAMME' algorithm in the AdaBoostClassfier else 'SAMME.R'",
                        action='store_true',
                        default=False)
    parser.add_argument("--decorrelate", help="Decorrelate the training", action='store_true', default=False)
    parser.add_argument("--useRandomSplit", help="Split the features randomly", action='store_true', default=False)

    Options = parser.parse_args()
    trainings = ["ifdef isSciKitTraining"]

    extra_args = []
    extra_name = ""
    if Options.useEntropySplit:
        extra_args.append("--useEntropySplit")
        extra_name += "entropy"
    if Options.useRealBoostingAlg:
        extra_args.append("--useRealBoostingAlg")
        extra_name += "rAda"
    if Options.decorrelate:
        extra_args.append("--transform_data decorrelation")
        extra_name += "noCorr"
    if Options.useRandomSplit:
        extra_args.append("--useRandomSplit")
        extra_name += "random"

    ##
    ##
    ##
    for estimators in range(Options.nEstimators[0], Options.nEstimators[1], Options.nEstimators[2]):
        for depth in range(1, Options.maxDepth + 1):
            trainings.append(
                generateTraining(nEstimators=estimators,
                                 maxDepth=depth,
                                 minNodeSize=setupBDTParser().get_default("minNodeSize"),
                                 impuritySplit=setupBDTParser().get_default("minImpurityDecrease"),
                                 learningRate=setupBDTParser().get_default("learningRate"),
                                 extra_args=extra_args,
                                 extra_name=extra_name))

        for size in np.arange(Options.minNodeSize[0], Options.minNodeSize[1], Options.minNodeSize[2]):
            trainings.append(
                generateTraining(nEstimators=estimators,
                                 maxDepth=setupBDTParser().get_default("maxDepth"),
                                 minNodeSize=size,
                                 impuritySplit=setupBDTParser().get_default("minImpurityDecrease"),
                                 learningRate=setupBDTParser().get_default("learningRate"),
                                 extra_args=extra_args,
                                 extra_name=extra_name))

        for split in np.arange(Options.minImpurityDecrease[0], Options.minImpurityDecrease[1], Options.minImpurityDecrease[2]):
            trainings.append(
                generateTraining(nEstimators=estimators,
                                 maxDepth=setupBDTParser().get_default("maxDepth"),
                                 minNodeSize=setupBDTParser().get_default("minNodeSize"),
                                 impuritySplit=split,
                                 learningRate=setupBDTParser().get_default("learningRate"),
                                 extra_args=extra_args,
                                 extra_name=extra_name))

        for rate in np.arange(Options.learningRate[0], Options.learningRate[1], Options.learningRate[2]):
            trainings.append(
                generateTraining(nEstimators=estimators,
                                 maxDepth=setupBDTParser().get_default("maxDepth"),
                                 minNodeSize=setupBDTParser().get_default("minNodeSize"),
                                 impuritySplit=setupBDTParser().get_default("minImpurityDecrease"),
                                 learningRate=rate,
                                 extra_args=extra_args,
                                 extra_name=extra_name))

    for depth in range(1, Options.maxDepth + 1):
        for size in np.arange(Options.minNodeSize[0], Options.minNodeSize[1], Options.minNodeSize[2]):
            trainings.append(
                generateTraining(nEstimators=setupBDTParser().get_default("nEstimators"),
                                 maxDepth=depth,
                                 minNodeSize=size,
                                 impuritySplit=setupBDTParser().get_default("minImpurityDecrease"),
                                 learningRate=setupBDTParser().get_default("learningRate"),
                                 extra_args=extra_args,
                                 extra_name=extra_name))

        for split in np.arange(Options.minImpurityDecrease[0], Options.minImpurityDecrease[1], Options.minImpurityDecrease[2]):
            trainings.append(
                generateTraining(nEstimators=setupBDTParser().get_default("nEstimators"),
                                 maxDepth=depth,
                                 minNodeSize=setupBDTParser().get_default("minNodeSize"),
                                 impuritySplit=split,
                                 learningRate=setupBDTParser().get_default("learningRate"),
                                 extra_args=extra_args,
                                 extra_name=extra_name))

        for rate in np.arange(Options.learningRate[0], Options.learningRate[1], Options.learningRate[2]):
            trainings.append(
                generateTraining(nEstimators=setupBDTParser().get_default("nEstimators"),
                                 maxDepth=depth,
                                 minNodeSize=setupBDTParser().get_default("minNodeSize"),
                                 impuritySplit=setupBDTParser().get_default("minImpurityDecrease"),
                                 learningRate=rate,
                                 extra_args=extra_args,
                                 extra_name=extra_name))

    for size in np.arange(Options.minNodeSize[0], Options.minNodeSize[1], Options.minNodeSize[2]):
        for split in np.arange(Options.minImpurityDecrease[0], Options.minImpurityDecrease[1], Options.minImpurityDecrease[2]):
            trainings.append(
                generateTraining(nEstimators=setupBDTParser().get_default("nEstimators"),
                                 maxDepth=setupBDTParser().get_default("maxDepth"),
                                 minNodeSize=size,
                                 impuritySplit=split,
                                 learningRate=setupBDTParser().get_default("learningRate"),
                                 extra_args=extra_args,
                                 extra_name=extra_name))

        for rate in np.arange(Options.learningRate[0], Options.learningRate[1], Options.learningRate[2]):
            trainings.append(
                generateTraining(nEstimators=setupBDTParser().get_default("nEstimators"),
                                 maxDepth=setupBDTParser().get_default("maxDepth"),
                                 minNodeSize=size,
                                 impuritySplit=setupBDTParser().get_default("minImpurityDecrease"),
                                 learningRate=rate,
                                 extra_args=extra_args,
                                 extra_name=extra_name))

    for split in np.arange(Options.minImpurityDecrease[0], Options.minImpurityDecrease[1], Options.minImpurityDecrease[2]):
        for rate in np.arange(Options.learningRate[0], Options.learningRate[1], Options.learningRate[2]):
            trainings.append(
                generateTraining(nEstimators=setupBDTParser().get_default("nEstimators"),
                                 maxDepth=setupBDTParser().get_default("maxDepth"),
                                 minNodeSize=setupBDTParser().get_default("minNodeSize"),
                                 impuritySplit=split,
                                 learningRate=rate,
                                 extra_args=extra_args,
                                 extra_name=extra_name))

    WriteList(
        ClearFromDuplicates(trainings) + ["endif"],
        "%s/SciKit_BDT_Scan%s.conf" % (ResolvePath(Options.outDir), "" if len(extra_name) == 0 else "_" + extra_name))
    print "INFO: A scan containing %d points has been prepared" % (len(trainings) - 1)
