#!/bin/bash
if [  "${SLURM_JOB_USER}" == ""  ];then
	ID=${SGE_TASK_ID}
else
	echo "ID=${SLURM_ARRAY_TASK_ID} + ${IdOffSet}"
	ID=$((SLURM_ARRAY_TASK_ID+IdOffSet))
fi

echo "###############################################################################################"
echo "					 Enviroment variables"
echo "###############################################################################################"
export
echo "###############################################################################################"
echo " "
if [ -z "${ATLAS_LOCAL_ROOT_BASE}" ];then
	echo "###############################################################################################"
	echo "					Setting up the enviroment"
	echo "###############################################################################################"
	echo "cd ${TMPDIR}"
	cd ${TMPDIR}
	echo "Setting Up the ATLAS Enviroment:"
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" 
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    echo "Setup athena:"
    echo "asetup ${OriginalProject},${OriginalPatch}"
    asetup ${OriginalProject},${OriginalPatch}
    #Check whether we're in release 21
    if [ -f ${OriginalArea}/../build/${BINARY_TAG}/setup.sh ]; then
        echo "source ${OriginalArea}/../build/${BINARY_TAG}/setup.sh"
        source ${OriginalArea}/../build/${BINARY_TAG}/setup.sh
        WORKDIR=${OriginalArea}/../build/${BINARY_TAG}/bin/
    elif [ -f ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh ];then
        echo "source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh"
        source ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh
        WORKDIR=${OriginalArea}/../build/${WorkDir_PLATFORM}/bin/
    elif [ -f ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh ];then
            echo "source ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh"
            source ${OriginalArea}/../build/${LCG_PLATFORM}/setup.sh
            WORKDIR=${OriginalArea}/../build/${LCG_PLATFORM}/bin/		    
    elif [ -z "${CMTBIN}" ];then
        source  ${OriginalArea}/../build/x86_64*/setup.sh
        if [ $? -ne 0 ];then
            echo "Something strange happens?!?!?!"
            export
            echo " ${OriginalArea}/../build/${BINARY_TAG}/setup.sh"
            echo " ${OriginalArea}/../build/${WorkDir_PLATFORM}/setup.sh"            
            scontrol requeuehold ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}        
            exit 100
        fi
    fi
fi
echo "cd ${OriginalArea}"
cd ${OriginalArea}
echo "python XAMPPtmva/python/SciKitLearn/Utils/RunDiagnostics.py --runVariablesOnly --HDF5File ${HDF5Dir}/output_scikit.H5  --outDir ${HDF5Dir}/DiagnosticVariables/ --nAuxillaryThreads 1 --dataPreProcessing ${HDF5Dir}/Covariance_Matrix.pkl "
python XAMPPtmva/python/SciKitLearn/Utils/RunDiagnostics.py --runVariablesOnly --HDF5File ${HDF5Dir}/output_scikit.H5  --outDir ${HDF5Dir}/DiagnosticVariables/ --nAuxillaryThreads 1 --dataPreProcessing ${HDF5Dir}/Covariance_Matrix.pkl
if [ $? -eq 0 ]; then
	echo "###############################################################################################"
	echo "						SciKitLearn terminated successfully"
	echo "###############################################################################################"
else
	echo "###############################################################################################"
	echo "					SciKitLearn job has experienced an error"
	echo "###############################################################################################"
	scontrol requeuehold ${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}		
	exit 100
fi

echo "python XAMPPtmva/python/SciKitPlotting/SciKitFastPlots.py --inputPath ${HDF5Dir}/DiagnosticVariables/ --output ${HDF5Dir}/DiagnosticPlots/ --nThreads 1"
python XAMPPtmva/python/SciKitPlotting/SciKitFastPlots.py  --inputPath ${HDF5Dir}/DiagnosticVariables/ --output ${HDF5Dir}/DiagnosticPlots/ --nThreads 1 
 


