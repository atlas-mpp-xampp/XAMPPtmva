#! /usr/bin/env python
from XAMPPplotting.PlotUtils import *
from scipy.integrate import simps
from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser


def IntegrateTGraph(mygraph):
    xCleaned = [round(mygraph.GetX()[i], 7) for i in range(mygraph.GetN()) if i == 0 or mygraph.GetX()[i - 1] != mygraph.GetX()[i]]
    yCleaned = [round(mygraph.GetY()[i], 7) for i in range(mygraph.GetN()) if i == 0 or mygraph.GetX()[i - 1] != mygraph.GetX()[i]]
    simpsonIntegral = simps(y=xCleaned, x=xCleaned)
    myIntegral = 0.
    ### What is this for?
    for idx, x in enumerate(xCleaned):
        if x == xCleaned[-1]: break
        # 1/2 * (x2-x1) * (y1+y2)
        myIntegral += (xCleaned[idx + 1] - x) * 0.5 * (yCleaned[idx + 1] + yCleaned[idx])
    return myIntegral


def RenameNLayers(label):
    if label == '10010': return '100 10'
    elif label == '1005010': return '100 50 10'
    elif label == '15010': return '150 10'
    elif label == '1505010': return '150 50 10'
    elif label == '5010': return '50 10'
    elif label == '50105': return '50 10 5'
    elif label == '10010': return '100 10'
    elif label == '50105': return '50 10 5'
    else: return label


def RenameAxis(histo):
    for i in range(1, histo.GetNbinsX() + 1):
        histo.GetXaxis().SetBinLabel(i, RenameNLayers(histo.GetXaxis().GetBinLabel(i)))


if __name__ == "__main__":
    parser = setupSciKitPlotParser(prog='GetRocAndOvertrain',
                                   description='This script makes plots of the ROC curves and the Overtrain Plot done by SciKit learn')
    parser.set_defaults(output="ROCCurves")
    parser.add_argument('--noSingleCurves', help='Disable the drawing of single ROC curves', action='store_true', default=False)
    parser.add_argument('--xLabelMethods',
                        help='Specify an identifier for the methods to be drawn on the x-axis (in case of more than one training to plot)',
                        default='')
    parser.add_argument('--yLabelMethods',
                        help='Specify an identifier for the methods to be drawn on the y-axis (in case of more than one training to plot)',
                        default='')
    parser.add_argument('--plotName', help='name of plot for ROCACU summary', default='')

    Options = parser.parse_args()
    setupPlottingEnviroment(Options)

    ROOTFiles = {}
    for myfile in os.listdir(Options.inputPath):
        if not '.root' in myfile: continue
        ROOTFiles[myfile.replace('.root', '')] = Options.inputPath + '/' + myfile

    if len(ROOTFiles) == 0:
        print "ERROR: No .root files found at given path %s, exiting..." % Options.inputPath
        sys.exit(1)

    pu = PlotUtils(status=Options.label, size=24)
    pu.Prepare1PadCanvas("ROC", 800, 600)

    pu = PlotUtils(status=Options.label, size=24)
    Red = array('d', [0.8, 0.8, 0.9, 0.3])
    Green = array('d', [0., 0.6, 0.9, 0.8])
    Blue = array('d', [0., 0., 0., 0.])
    Length = array('d', [0., 0.33, 0.66, 1.])
    ROOT.TColor.CreateGradientColorTable(4, Length, Red, Green, Blue, 1500)

    ROCAUCs = {}
    ROCAUCsTest = {}
    allvalues = []
    for method, ifile in ROOTFiles.iteritems():
        InputFile = ROOT.TFile.Open(ifile)

        if not InputFile.Get('Diagnostics/MVA_roc_Testing'):
            print 'Did not find testing ROC curve, exiting...'
            sys.exit(1)

        if not 'beta095' in method: continue
        # if 'beta095' in method: continue

        if Options.xLabelMethods != '' and not Options.xLabelMethods in ifile: continue
        if Options.yLabelMethods != '' and not Options.yLabelMethods in ifile: continue

        ROCGraphTest = InputFile.Get('Diagnostics/MVA_roc_Testing')
        ROCAUCTest = IntegrateTGraph(ROCGraphTest)
        ROCGraphTest.SetTitle("Testing - AUC: %.6f" % ROCAUCTest)
        ROCGraphTest.SetMarkerStyle(ROOT.kOpenSquare)
        ROCGraphTest.SetLineColor(ROOT.kRed)
        ROCGraphTest.SetMarkerColor(ROOT.kRed)
        ROCGraphTrain = InputFile.Get('Diagnostics/MVA_roc_Train')
        ROCAUCTrain = IntegrateTGraph(ROCGraphTrain)
        ROCGraphTrain.SetTitle("Training - AUC: %.6f" % ROCAUCTrain)
        ROCGraphTrain.SetMarkerStyle(ROOT.kOpenCircle)
        allvalues.append(ROCAUCTest)
        allvalues.append(ROCAUCTrain)

        ROCAUCs[method] = ROCAUCTrain
        ROCAUCsTest[method] = ROCAUCTest

        ROCHisto = ROCGraphTrain.GetHistogram()

        if not Options.noSingleCurves:

            ROCHisto.GetXaxis().SetRangeUser(0, 1)
            ROCHisto.GetYaxis().SetRangeUser(0, 1)
            ROCHisto.GetYaxis().SetTitle("Background rejection")

            ROCHisto.GetXaxis().SetTitle("Signal efficiency")

            ROCHisto.Draw()
            ROCGraphTrain.Draw("sameP")
            ROCGraphTest.Draw("sameP")

            pu.CreateLegend(0.2, 0.2, 0.35, 0.35)
            pu.AddToLegend([ROCGraphTrain, ROCGraphTest], Style="PL")
            pu.DrawLegend()

            pu.GetCanvas().SaveAs("%sROCCurve.pdf" % method)

    histoTest = ROOT.TH1F("h_ROCAUCTest", "Testing", len(ROCAUCsTest), 0, len(ROCAUCsTest))
    histoTest.SetLineColor(ROOT.kRed)
    histoTest.SetLineStyle(ROOT.kDashed)
    bin = 1
    for m in ROCAUCsTest.iterkeys():
        histoTest.SetBinContent(bin, ROCAUCsTest[m])
        histoTest.GetXaxis().SetBinLabel(bin, m)
        bin += 1
    histo = ROOT.TH1F("h_ROCAUCT", "Training", len(ROCAUCs), 0, len(ROCAUCs))
    histo.GetYaxis().SetTitle("ROC AUC")
    histo.GetXaxis().SetLabelSize(.045)
    bin = 1
    for m in sorted(ROCAUCs.iterkeys()):
        histo.SetBinContent(bin, ROCAUCs[m])
        histo.GetXaxis().SetBinLabel(bin, m.replace('_swapped', ' (sw.)'))
        bin += 1

    pu.Prepare1PadCanvas("ROC_all", len(ROCAUCs) * 18, 600)
    pu.GetCanvas().GetCanvas().cd()
    pu.GetCanvas().SetBottomMargin(0.35)
    histo.GetXaxis().LabelsOption("v")
    histo.GetYaxis().SetRangeUser(0.85, 0.95)
    histo.Draw()
    histoTest.Draw("same")

    pu.CreateLegend(0.19, 0.8, 0.4, 0.9)
    pu.AddToLegend([histo, histoTest], Style="L")
    pu.DrawLegend()
    if Options.plotName != '': pu.GetCanvas().SaveAs("%s1D.pdf" % Options.plotName)
    else: pu.GetCanvas().SaveAs("%sROCCurve1D.pdf" % method)

    if Options.xLabelMethods != '' and Options.yLabelMethods != '':
        print 'Selected to create 2D ROCAUC plot with drawing all methods containing %s on the x-axis while drawing all methods containing %s on the y-axis' % (
            Options.xLabelMethods, Options.yLabelMethods)
        xbins = []
        ybins = []
        values = []
        for m in ROCAUCsTest.iterkeys():
            if '_swapped' in m: continue
            if Options.xLabelMethods in m:
                x = m.split(Options.xLabelMethods)[1].split('_')[0]
                if not x in xbins: xbins.append(x)
            if Options.yLabelMethods in m:
                y = m.split(Options.yLabelMethods)[1].split('_')[0]
                if not y in ybins: ybins.append(y)

        if len(xbins) == 0:
            print 'ERROR: no methods found containing %s' % Options.xLabelMethods
        if len(ybins) == 0:
            print 'ERROR: no methods found containing %s' % Options.yLabelMethods

        pu.Prepare1PadCanvas("ROC_all2D", (len(xbins) * 18 if len(xbins) > 10 else 800), (len(ybins) * 18 if len(ybins) > 8 else 600))
        pu.GetCanvas().GetCanvas().cd()
        pu.GetCanvas().SetRightMargin(0.18)

        histo2DTest = ROOT.TH2F("h_ROCAUCTest2D", "Testing", 2 * len(xbins), 0, 2 * len(xbins), 2 * len(ybins), 0, 2 * len(ybins))
        histo2DTest.GetXaxis().SetTitle(Options.xLabelMethods)
        histo2DTest.GetYaxis().SetTitle(Options.yLabelMethods)
        histo2DTest.GetXaxis().SetLabelSize(.03)
        histo2DTest.GetYaxis().SetLabelSize(.03)
        for i, x in enumerate(sorted(xbins)):
            histo2DTest.GetXaxis().SetBinLabel((i + 1) * 2 - 1, x)
            histo2DTest.GetXaxis().SetBinLabel((i + 1) * 2, x + ' (sw.)')
        for i, y in enumerate(sorted(ybins)):
            histo2DTest.GetYaxis().SetBinLabel((i + 1) * 2 - 1, y)
            histo2DTest.GetYaxis().SetBinLabel((i + 1) * 2, y + ' (sw.)')

        histo2DTestTrain = histo2DTest.Clone("%s_train" % histo2DTest.GetName())

        # for i,x in enumerate(sorted(xbins)):
        #     xlab = histo2DTest.GetXaxis().GetBinLabel((i+1)*2-1)
        #     for iy,y in enumerate(sorted(ybins)):
        #         ylab = histo2DTest.GetYaxis().GetBinLabel((iy+1)*2-1)
        #         histo2DTest.SetBinContent((i+1)*2-1,(iy+1)*2-1,ROCAUCsTest['NN_%s%s_%s%s'%(Options.xLabelMethods,xlab,Options.yLabelMethods,ylab)])
        #         histo2DTest.SetBinContent((i+1)*2,(iy+1)*2,ROCAUCsTest['NN_%s%s_%s%s_swapped'%(Options.xLabelMethods,xlab,Options.yLabelMethods,ylab)])

        #         histo2DTestTrain.SetBinContent((i+1)*2-1,(iy+1)*2-1,ROCAUCs['NN_%s%s_%s%s'%(Options.xLabelMethods,xlab,Options.yLabelMethods,ylab)])
        #         histo2DTestTrain.SetBinContent((i+1)*2,(iy+1)*2,ROCAUCs['NN_%s%s_%s%s_swapped'%(Options.xLabelMethods,xlab,Options.yLabelMethods,ylab)])

        # minimum = min(allvalues)
        # histo2DTest.GetZaxis().SetRangeUser(minimum*0.9, 1.001)
        # histo2DTestTrain.GetZaxis().SetRangeUser(minimum*0.9, 1.001)

        # histo2DTest.Draw("colz")
        # if Options.plotName!='': pu.GetCanvas().SaveAs("%s2DTesting.pdf"%Options.plotName)
        # else: pu.GetCanvas().SaveAs("ROCAUC2DTesting.pdf")

        # histo2DTestTrain.Draw("colz")
        # if Options.plotName!='': pu.GetCanvas().SaveAs("%s2DTraining.pdf"%Options.plotName)
        # else: pu.GetCanvas().SaveAs("ROCAUC2DTraining.pdf")

        histo2DTestNoSwap = ROOT.TH2F("h_ROCAUCTest2DNoSwap", "TestingNoSwap", len(xbins), 0, len(xbins), len(ybins), 0, len(ybins))
        histo2DTestNoSwap.GetXaxis().SetTitle(Options.xLabelMethods)
        histo2DTestNoSwap.GetYaxis().SetTitle(Options.yLabelMethods)
        histo2DTestNoSwap.GetXaxis().SetTitle("Neurons in n-th hidden layer")
        histo2DTestNoSwap.GetYaxis().SetTitle("Maximum number of iterations")
        histo2DTestNoSwap.GetYaxis().SetTitleOffset(histo2DTestNoSwap.GetYaxis().GetTitleOffset() * 1.2)
        histo2DTestNoSwap.GetXaxis().SetTitleOffset(histo2DTestNoSwap.GetXaxis().GetTitleOffset() * 1.2)
        histo2DTestNoSwap.GetXaxis().SetLabelSize(.03)
        histo2DTestNoSwap.GetYaxis().SetLabelSize(.03)
        histo2DTestNoSwap.GetXaxis().SetLabelSize(.07)
        histo2DTestNoSwap.GetYaxis().SetLabelSize(.08)
        xbins.sort(key=lambda s: len(s))

        for i, x in enumerate(xbins):
            histo2DTestNoSwap.GetXaxis().SetBinLabel((i + 1), x)
        for i, y in enumerate(sorted(ybins)):
            histo2DTestNoSwap.GetYaxis().SetBinLabel((i + 1), y)

        histo2DTrainNoSwap = histo2DTestNoSwap.Clone("%s_train" % histo2DTestNoSwap.GetName())
        histo2DTrainSwap = histo2DTrainNoSwap.Clone("%s_doSwap" % histo2DTrainNoSwap.GetName())
        histo2DTestSwap = histo2DTestNoSwap.Clone("%s_doSwap" % histo2DTestNoSwap.GetName())
        for i in range(histo2DTestNoSwap.GetNbinsX()):
            xlab = histo2DTestNoSwap.GetXaxis().GetBinLabel((i + 1))
            for iy in range(histo2DTestNoSwap.GetNbinsY()):
                ylab = histo2DTestNoSwap.GetYaxis().GetBinLabel((iy + 1))
                histo2DTestNoSwap.SetBinContent(
                    (i + 1), (iy + 1), ROCAUCsTest['NN_%s%s_beta095_%s%s' % (Options.xLabelMethods, xlab, Options.yLabelMethods, ylab)])
                histo2DTrainNoSwap.SetBinContent(
                    (i + 1), (iy + 1), ROCAUCs['NN_%s%s_beta095_%s%s' % (Options.xLabelMethods, xlab, Options.yLabelMethods, ylab)])

                histo2DTestSwap.SetBinContent(
                    (i + 1), (iy + 1),
                    ROCAUCsTest['NN_%s%s_beta095_%s%s_swapped' % (Options.xLabelMethods, xlab, Options.yLabelMethods, ylab)])
                histo2DTrainSwap.SetBinContent(
                    (i + 1), (iy + 1), ROCAUCs['NN_%s%s_beta095_%s%s_swapped' % (Options.xLabelMethods, xlab, Options.yLabelMethods, ylab)])

                # histo2DTestNoSwap.SetBinContent((i+1),(iy+1),ROCAUCsTest['NN_%s%s_%s%s'%(Options.xLabelMethods,xlab,Options.yLabelMethods,ylab)])
                # histo2DTrainNoSwap.SetBinContent((i+1),(iy+1),ROCAUCs['NN_%s%s_%s%s'%(Options.xLabelMethods,xlab,Options.yLabelMethods,ylab)])

                # histo2DTestSwap.SetBinContent((i+1),(iy+1),ROCAUCsTest['NN_%s%s_%s%s_swapped'%(Options.xLabelMethods,xlab,Options.yLabelMethods,ylab)])
                # histo2DTrainSwap.SetBinContent((i+1),(iy+1),ROCAUCs['NN_%s%s_%s%s_swapped'%(Options.xLabelMethods,xlab,Options.yLabelMethods,ylab)])

        for h in [histo2DTestNoSwap, histo2DTrainNoSwap, histo2DTestSwap, histo2DTrainSwap]:
            RenameAxis(h)
            h.GetZaxis().SetRangeUser(0.9205, 0.93)
            h.GetZaxis().SetTitle("ROC AUC")
            h.GetZaxis().SetTitleOffset(h.GetZaxis().GetTitleOffset() * 1.4)

        # histo2DTestNoSwap.GetZaxis().SetRangeUser(minimum*0.9, 1.001)
        # histo2DTrainNoSwap.GetZaxis().SetRangeUser(minimum*0.9, 1.001)

        # histo2DTestSwap.GetZaxis().SetRangeUser(minimum*0.9, 1.001)
        # histo2DTrainSwap.GetZaxis().SetRangeUser(minimum*0.9, 1.001)

        histo2DTestNoSwap.SetContour(2000)
        histo2DTestNoSwap.Draw("colztext")
        pu.DrawTLatex(0.16, 0.96, "Nominal", size=20)
        if Options.plotName != '': pu.GetCanvas().SaveAs("%s2DTesting_noSwap.pdf" % Options.plotName)
        else: pu.GetCanvas().SaveAs("ROCAUC2DTesting_noSwap.pdf")

        histo2DTrainNoSwap.SetContour(2000)
        histo2DTrainNoSwap.Draw("colztext")
        pu.DrawTLatex(0.16, 0.96, "Nominal", size=20)
        if Options.plotName != '': pu.GetCanvas().SaveAs("%s2DTraining_noSwap.pdf" % Options.plotName)
        else: pu.GetCanvas().SaveAs("ROCAUC2DTraining_noSwap.pdf")

        histo2DTestSwap.SetContour(2000)
        histo2DTestSwap.Draw("colztext")
        pu.DrawTLatex(0.16, 0.96, "Cross-validation", size=20)
        if Options.plotName != '': pu.GetCanvas().SaveAs("%s2DTesting_Swap.pdf" % Options.plotName)
        else: pu.GetCanvas().SaveAs("ROCAUC2DTesting_Swap.pdf")

        histo2DTrainSwap.SetContour(2000)
        histo2DTrainSwap.Draw("colztext")
        pu.DrawTLatex(0.16, 0.96, "Cross-validation", size=20)
        if Options.plotName != '': pu.GetCanvas().SaveAs("%s2DTraining_Swap.pdf" % Options.plotName)
        else: pu.GetCanvas().SaveAs("ROCAUC2DTraining_Swap.pdf")
