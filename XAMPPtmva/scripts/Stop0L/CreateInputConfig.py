#!/usr/bin/env python
import os
import sys
import commands


def WriteToFile(options, files, configname):
    with open(options.outdir + '/' + configname, "w") as fout:
        for f in sorted(files):
            fout.write("Input %s\n" % (f))
        if options.background != "":
            fout.write("\n# import the background input config\nImport %s\n" % (options.background))
        if options.IsHistFitter:
            fout.write("\nIsHistFitter\n")


if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description="Script for creating XAMPPtmva input configs from a folder with files containing trees",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', dest='indir', help='Folder with files', required=True)
    parser.add_argument('-o', '--output', dest='outdir', help='Output directory to put input configs into', required=True)
    parser.add_argument('-n', '--name', help='name of input configs', default="")
    parser.add_argument('-p', '--pattern', help='pattern in filenames to filter on', nargs='+', default=[], type=str)
    parser.add_argument('-b', '--background', help='background input config to include into input configs', default="")
    parser.add_argument('--mergeSignals', help='merge all signals found in indir into one input config', action='store_true', default=False)
    parser.add_argument('--IsHistFitter', help='add "IsHistFitter" flag to input configs', action='store_true', default=False)
    options = parser.parse_args()

    os.system("mkdir -p %s" % options.outdir)

    files = os.listdir(options.indir)

    if len(files) == 0:
        print 'No files found, exiting...'
        sys.exit(1)

    FilesForInputConfig = []
    for f in files:
        foundPattern = True
        for p in options.pattern:
            if not p in f: foundPattern = False
        if foundPattern: FilesForInputConfig.append(options.indir + '/' + f)

    if options.mergeSignals:
        if options.name != "": configname = options.name
        else: configname = "mergedInputConfig.conf"
        if not configname.endswith('.conf'): configname += ".conf"
        WriteToFile(options, FilesForInputConfig, configname)
    else:
        for f in FilesForInputConfig:
            configname = f.split('/')[-1].replace(".root", ".conf")
            if not configname.endswith('.conf'): configname += ".conf"
            WriteToFile(options, [f], configname)
