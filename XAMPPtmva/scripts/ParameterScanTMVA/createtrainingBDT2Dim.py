from ClusterSubmission.Utils import FillWhiteSpaces, WriteList, ResolvePath
Cfg_Methods = []
Method_Names = []

### Max depth against the n-trees
for i in range(10):
    for j in range(21):
        Cfg_Methods += [
            "New_TMVAMethod",
            "    Name BDT-MaxDepth=%d_nTrees=%d" % ((i + 1), (j * 100 + 50)), "    Type BDT",
            "    Option !H:!V:NTrees=%d:MinNodeSize=2.5%%:MaxDepth=%d:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
            % ((j * 100 + 50), (i + 1)), "End_TMVAMethod"
        ]
        Method_Names += ["BDT-MaxDepth=%d_nTrees=%d" % ((i + 1), (j * 100 + 50))]

##### Max Depth against the MinNode size

for i in range(10):
    for j in range(25):
        Cfg_Methods += [
            "New_TMVAMethod",
            "    Name BDT-MaxDepth=%d_MinNodeSize=%.1f" % ((i + 1), (2 * j + 1) / float(10)), "    Type BDT",
            "    Option !H:!V:NTrees=850:MinNodeSize=%.1f%%:MaxDepth=%d:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
            % ((2 * j + 1) / float(10), (i + 1)), "End_TMVAMethod"
        ]
        Method_Names += ["BDT-MaxDepth=%d_MinNodeSize=%.1f" % ((i + 1), (2 * j + 1) / float(10))]

### Max depth against the number of cuts
for i in range(10):
    for j in range(20):
        Cfg_Methods += [
            "New_TMVAMethod",
            "    Name BDT-MaxDepth=%d_BDTnCuts=%d" % (i + 1, 5 * (j + 1)), "    Type BDT",
            "    Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=%d:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=%d"
            % (i + 1, 5 * (j + 1)), "End_TMVAMethod"
        ]
        Method_Names += ["BDT-MaxDepth=%d_BDTnCuts=%d" % (i + 1, 5 * (j + 1))]

### number of trees against the min node size
for i in range(21):
    for j in range(25):
        Cfg_Methods += [
            "New_TMVAMethod",
            "   Name BDT-nTrees=%d_MinNodeSize=%.1f" % (i * 100 + 50, (2 * j + 1) / float(10)), "   Type BDT",
            "   Option !H:!V:NTrees=%d:MinNodeSize=%.1f%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
            % (i * 100 + 50, (2 * j + 1) / float(10)), "End_TMVAMethod"
        ]
        Method_Names += ["BDT-nTrees=%d_MinNodeSize=%.1f" % (i * 100 + 50, (2 * j + 1) / float(10))]

### number of trees against the number of cuts on the interval
for i in range(21):
    for j in range(20):
        Cfg_Methods += [
            "New_TMVAMethod",
            "    Name BDT-nTrees=%d_BDTnCuts=%d" % (i * 100 + 50, 5 * j + 5), "    Type BDT",
            "    Option !H:!V:NTrees=%d:MinNodeSize=2.5%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=%d"
            % (i * 100 + 50, 5 * j + 5), "End_TMVAMethod"
        ]
        Method_Names += ["BDT-nTrees=%d_BDTnCuts=%d" % (i * 100 + 50, 5 * j + 5)]

for i in range(25):
    for j in range(20):
        Cfg_Methods += [
            "New_TMVAMethod",
            "    Name BDT-MinNodeSize=%.1f_BDTnCuts=%d" % ((2 * i + 1) / float(10), 5 * j + 5), "    Type BDT",
            "    Option !H:!V:NTrees=850:MinNodeSize=%.1f%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=%d"
            % ((2 * i + 1) / float(10), 5 * j + 5), "End_TMVAMethod"
        ]
        Method_Names += ["BDT-MinNodeSize=%.1f_BDTnCuts=%d" % ((2 * i + 1) / float(10), 5 * j + 5)]

WriteList(Cfg_Methods + [FillWhiteSpaces(10, "\n"), "ifdef iSTMVATraining"] + ["BookMethod %s" % (M) for M in Method_Names] + ["endif"],
          "%s/TMVA_BDT2DScan.conf" % (ResolvePath("XAMPPtmva/TrainingConf")))
