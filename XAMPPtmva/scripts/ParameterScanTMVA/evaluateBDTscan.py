#! /usr/bin/env python
import numpy as np
import os, ROOT, math

from XAMPPtmva.SciKitTrainingVariables import setupPlottingEnviroment, setupSciKitPlotParser
from XAMPPtmva.SciKitCorrelationMatrix import createColourPalette
from XAMPPtmva.SciKitOvertrainPlot import KolmogorovSmirnovScore
from XAMPPtmva.GetROCCurve import generateROC
from XAMPPplotting.FileStructureHandler import GetFileHandler
from ClusterSubmission.Utils import ClearFromDuplicates
from XAMPPtmva.MVAOptimizationHelpers import TMVAPerformance

from XAMPPplotting.PlotUtils import *

from pprint import pprint
from itertools import product, permutations


class TMVABDTParameters(TMVAPerformance):
    def __init__(self, nominal_file, cross_valid_file, method):
        ####################################################
        #    Meta  data about the  training parameters     #
        ####################################################
        ### Name = "BDT%s_%d_%d_%.2f_%.2f_%.2f"%( (""if len(extra_name) == 0 else "_"+extra_name), nEstimators, maxDepth, minNodeSize, impuritySplit, learningRate)
        TMVAPerformance.__init__(self,
                                 nominal_file=nominal_file,
                                 cross_valid_file=cross_valid_file,
                                 method=method,
                                 over_training_tolerance=0.01)

        file_name_no_dir = method
        ### First set the standard training parameters
        self.__nEstimators = 850
        self.__max_depth = 3
        self.__min_node = 2.5
        self.__n_cuts = 20

        error_passed = False

        nTrees = len("nTrees=")
        pos_nTrees = file_name_no_dir.find("nTrees=")

        nCuts = len("nCuts=")
        pos_nCuts = file_name_no_dir.find("nCuts=")

        MaxDepth = len("MaxDepth=")
        pos_max = file_name_no_dir.find("MaxDepth=")

        NodeSize = len("MinNodeSize=")
        pos_size = file_name_no_dir.find("MinNodeSize=")

        if pos_nTrees != -1:
            num_pos = file_name_no_dir.find("_", pos_nTrees)
            try:
                self.__nEstimators = int(file_name_no_dir[pos_nTrees + nTrees:num_pos if num_pos > -1 else len(file_name_no_dir)])
            except:
                error_passed = True

        if pos_nCuts != -1:
            num_pos = file_name_no_dir.find("_", pos_nCuts)
            try:
                self.__n_cuts = int(file_name_no_dir[pos_nCuts + nCuts:num_pos if num_pos > -1 else len(file_name_no_dir)])
            except:
                error_passed = True

        if pos_max != -1:
            num_pos = file_name_no_dir.find("_", pos_max)
            try:
                self.__max_depth = int(file_name_no_dir[pos_max + MaxDepth:num_pos if num_pos > -1 else len(file_name_no_dir)])
            except:
                error_passed = True

        if pos_size != -1:
            num_pos = file_name_no_dir.find("_", pos_size)
            try:
                self.__min_node = float(file_name_no_dir[pos_size + NodeSize:num_pos if num_pos > -1 else len(file_name_no_dir)])
            except:
                error_passed = True

        self.__training = file_name_no_dir if not error_passed else ""

    def training(self):
        return self.__training

    def nTrees(self):
        return self.__nEstimators

    def minNodeSize(self):
        return self.__min_node

    def maximumDepth(self):
        return self.__max_depth

    def nCuts(self):
        return self.__n_cuts


def drawScan(PlotOptions, group_name, trainings, param_getters, param_names, slice_in_space):
    createColourPalette()
    free_params = tuple(i for i in range(len(slice_in_space)) if slice_in_space[i] == -1)
    fixed_params = tuple(i for i in range(len(slice_in_space)) if slice_in_space[i] != -1)
    matching_trainings = [
        t for t in trainings if len([i for i in fixed_params if param_getters[i](t) == slice_in_space[i]]) == len(fixed_params)
    ]

    if len(matching_trainings) == 0: return False

    range_x = ClearFromDuplicates([param_getters[free_params[0]](t) for t in matching_trainings])
    range_y = ClearFromDuplicates([param_getters[free_params[1]](t) for t in matching_trainings])
    if len(range_x) <= 1 or len(range_y) <= 1: return False
    max_x = max(range_x)
    max_y = max(range_y)

    min_x = min(range_x)
    min_y = min(range_y)

    ### Create the histograms
    Train_Quality = ROOT.TH2D("train_quality", "Quality  of the training", len(range_x), min_x, max_x, len(range_y), min_y, max_y)

    Train_ROC_Auc = Train_Quality.Clone("train_roc_aucs")
    Test_ROC_Auc = Train_Quality.Clone("test_roc_aucs")
    Kolm_Auc = Train_Quality.Clone("kolmogorov")

    Train_Quality.GetZaxis().SetTitle("train-quality")
    Train_ROC_Auc.GetZaxis().SetTitle("ROC-AUC (training)")
    Test_ROC_Auc.GetZaxis().SetTitle("ROC-AUC (testing)")
    Kolm_Auc.GetZaxis().SetTitle("Kolmogorov score")
    ### fill the histograms
    for t in matching_trainings:
        B = Train_Quality.FindBin(param_getters[free_params[0]](t), param_getters[free_params[1]](t))

        Train_Quality.SetBinContent(B, t.training_quality())
        Train_ROC_Auc.SetBinContent(B, t.default_perf_object().train_ROCAUC())
        Test_ROC_Auc.SetBinContent(B, t.default_perf_object().test_ROCAUC())
        Kolm_Auc.SetBinContent(B, t.default_perf_object().Kolmogorov())

    for H in [Train_Quality, Train_ROC_Auc, Test_ROC_Auc, Kolm_Auc]:
        pu = PlotUtils(status=PlotOptions.label, size=24)
        ### prepare the canvas
        CanvasName = "BDTScan_%s_%s-%s" % (group_name, H.GetName(), "_".join(
            ["%s(%.2f)" % (param_names[i], slice_in_space[i]) for i in fixed_params]))
        pu.Prepare1PadCanvas(CanvasName, 800, 600)
        pu.GetCanvas().SetRightMargin(0.18)
        pu.GetCanvas().SetLeftMargin(0.12)
        pu.GetCanvas().SetTopMargin(0.15)

        H.GetXaxis().SetTitle(param_names[free_params[0]])
        H.GetYaxis().SetTitle(param_names[free_params[1]])
        H.GetZaxis().SetTitleOffset(1.2 * H.GetZaxis().GetTitleOffset())
        H.GetYaxis().SetTitleOffset(0.8 * H.GetYaxis().GetTitleOffset())

        H.SetContour(1500)
        #        H.SetMinimum(0.5 if )
        H.Draw("colz")
        if not PlotOptions.summaryPlotOnly: pu.saveHisto("%s/%s" % (PlotOptions.output, CanvasName), ["pdf"])
        pu.saveHisto("%s/AllEvaluationPlots" % (PlotOptions.output), ["pdf"])
    return True


if __name__ == "__main__":

    parser = setupSciKitPlotParser(
        prog='createBDTscanConfig',
        description='This script generates a training config file varing all training parameters and books the methods')
    parser.set_defaults(
        inputPath="/ptmp/mpp/junggjo9/Cluster/Output/2018-07-23/TMVA_BDTScan/PowHegReco-Inclusive/EleTau_PreSel-TauLeptonVar/")
    parser.set_defaults(output="BDTScanTMVA/")

    options = parser.parse_args()
    setupPlottingEnviroment(options)
    createColourPalette()

    trainings = []
    n_found = 0
    best_training = None
    largest_roc = None

    train_param_fkt = [
        TMVABDTParameters.nTrees,
        TMVABDTParameters.minNodeSize,
        TMVABDTParameters.maximumDepth,
        TMVABDTParameters.nCuts,
    ]
    train_param_name = ["nTrees", "minNodeSize", "maximumDepth", "nCuts"]

    hyper_parameters = []
    for train in os.listdir(options.inputPath):
        if not train.endswith(".root"): continue
        if train.rfind("_swapped") != -1: continue
        cross_valid = "%s/%s_swapped.root" % (options.inputPath, train[:train.rfind(".")])
        nominal = "%s/%s" % (options.inputPath, train)
        if not os.path.exists(cross_valid): continue

        n_found += 1
        if n_found % 100 == 0:
            GetFileHandler().closeAll()
            print "INFO: %d trainings were loaded %d have been skipped due to too small ROC-AUC" % (len(trainings),
                                                                                                    n_found - len(trainings))
            if best_training:
                print "     Best training thus far %s with %.4f ROC-AUC, %.4f kolmogorov and %.4f similarity." % (
                    best_training.training(), best_training.default_perf_object().train_ROCAUC(),
                    best_training.default_perf_object().Kolmogorov(), best_training.default_perf_object().delta_ROCAUC())

                print "     %s is the training with the largest ROC-AUC %.4f ROC-AUC, %.4f kolmogorov and %.4f similarity." % (
                    largest_roc.training(), largest_roc.default_perf_object().train_ROCAUC(),
                    largest_roc.default_perf_object().Kolmogorov(), largest_roc.default_perf_object().delta_ROCAUC())

        Std_Dir_Default = GetFileHandler().LoadFile(nominal).GetDirectory("TrainingFiles/")
        Std_Dir_Valid = GetFileHandler().LoadFile(cross_valid).GetDirectory("TrainingFiles/")
        if not Std_Dir_Default or not Std_Dir_Valid:
            print "WARNING: Not a TMVA training"
            continue

        Default_Methods = [
            key.GetName()[key.GetName().find("_") + 1:] for key in Std_Dir_Default.GetListOfKeys()
            if key.ReadObj().InheritsFrom("TDirectory") and key.GetName().find("Method") != -1
        ] if Std_Dir_Default else []
        for M in Default_Methods:
            ParamHelper = TMVABDTParameters(nominal, cross_valid, M)
            if ParamHelper.default_perf_object().train_ROCAUC() < 0.5 or ParamHelper.default_perf_object().test_ROCAUC() < 0.5:
                print "WARNING: The training %s seems to be broken by a lot. Since it has a ROC AUC of %f and %f, respectively" % (
                    ParamHelper.training(), ParamHelper.default_perf_object().train_ROCAUC(),
                    ParamHelper.default_perf_object().test_ROCAUC())
                continue
            trainings += [ParamHelper]
            hyper_parameters += [tuple(fkt(ParamHelper) for fkt in train_param_fkt)]
            if not best_training or best_training.training_quality() < ParamHelper.training_quality(): best_training = ParamHelper
            if not largest_roc or largest_roc.default_perf_object().train_ROCAUC() < ParamHelper.default_perf_object().train_ROCAUC():
                largest_roc = ParamHelper
    GetFileHandler().closeAll()
    print "INFO: Found %d out of %d trainings which seem to be valid" % (len(trainings), n_found)

    scan_to_perform = []
    ### define a scan bit matrix
    bit_matrix = ClearFromDuplicates([t for t in permutations(tuple([1, 1] + [0 for i in range(len(train_param_fkt) - 2)]))])

    for h in hyper_parameters:
        for b in bit_matrix:
            choice = tuple(h[i] if b[i] == 0 else -1 for i in range(len(b)))
            if not choice in scan_to_perform: scan_to_perform += [choice]

    ### Release the hyper  parameters
    hyper_parameters = []

    ### We've found all of our different parameter settings in the BDT.
    ### Let's draw it
    DummyCanvas = ROOT.TCanvas("dummy", "dummy", 800, 600)
    DummyCanvas.SaveAs("%s/AllEvaluationPlots.pdf[" % (options.output))
    SavedOnce = False
    for choice in scan_to_perform:
        SavedOnce = drawScan(
            options, "TMVA_BDT", trainings, param_getters=train_param_fkt, param_names=train_param_name, slice_in_space=choice) or SavedOnce

    DummyCanvas.SaveAs("%s/AllEvaluationPlots.pdf]" % (options.output))
    if not SavedOnce: os.system("rm %s/AllEvaluationPlots.pdf" % (options.output))

    if best_training:
        print "     Best training thus far %s with %.4f ROC-AUC, %.4f kolmogorov and %.4f similarity." % (
            best_training.training(), best_training.default_perf_object().train_ROCAUC(), best_training.default_perf_object().Kolmogorov(),
            best_training.default_perf_object().delta_ROCAUC())

        print "     %s is the training with the largest ROC-AUC %.4f ROC-AUC, %.4f kolmogorov and %.4f similarity." % (
            largest_roc.training(), largest_roc.default_perf_object().train_ROCAUC(), largest_roc.default_perf_object().Kolmogorov(),
            largest_roc.default_perf_object().delta_ROCAUC())
