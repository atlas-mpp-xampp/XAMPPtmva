from ClusterSubmission.Utils import WriteList, ResolvePath, FillWhiteSpaces
Cfg_Methods = []
Method_Names = []

for i in range(21):
    Cfg_Methods += [
        "New_TMVAMethod",
        "    Name BDT-nTrees=%d" % (i * 100 + 50), "    Type BDT",
        "    Option !H:!V:NTrees='%d':MinNodeSize=2.5%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
        % (i * 100 + 50), "End_TMVAMethod\n\n"
    ]
    Method_Names += ["BDT-nTrees=%d" % (i * 100 + 50)]

for i in range(51):
    Cfg_Methods += [
        "New_TMVAMethod",
        "    Name BDT-MinNodeSize=%.1f" % ((i + 1) / float(10)), "    Type BDT",
        "    Option !H:!V:NTrees=850:MinNodeSize=%.1f%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
        % ((i + 1) / float(10)), "End_TMVAMethod"
    ]
    Method_Names += ["BDT-MinNodeSize=%.1f" % ((i + 1) / float(10))]

for i in range(10):
    Cfg_Methods += [
        "New_TMVAMethod",
        "    Name BDT-MaxDepth=%d" % (i + 1), "    Type BDT ",
        "    Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=%d:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
        % (i + 1), "End_TMVAMethod"
    ]
    Method_Names += ["BDT-MaxDepth=%d" % (i + 1)]

for i in ["RealAdaBoost", "Bagging", "AdaBoostR2", "Grad"]:
    Cfg_Methods += [
        "New_TMVAMethod",
        "    Name BDTBoostType=%s" % (i), "    Type BDT",
        "    Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=3:BoostType=%s:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
        % (i), "End_TMVAMethod"
    ]

    Method_Names += ["BDTBoostType=%s" % (i)]
for i in ["AdaBoost"]:  #########mit AdaBoostBeta
    Cfg_Methods += [
        "New_TMVAMethod",
        "    Name BDTBoostType-%s" % (i), "    Type BDT",
        "    Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=3:BoostType=%s:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
        % (i), "End_TMVAMethod"
    ]
    Method_Names += ["BDTBoostType-%s" % (i)]

for i in range(9):
    Cfg_Methods += [
        "New_TMVAMethod",
        "    Name BDTAdaBoostBeta=%.1f" % ((i + 1) / float(10)), "    Type BDT",
        "    Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=%.1f:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20"
        % ((i + 1) / float(10)), "End_TMVAMethod"
    ]
    Method_Names += ["BDTAdaBoostBeta=%.1f" % ((i + 1) / float(10))]

Cfg_Methods += [
    "New_TMVAMethod", "    Name BDTUseBaggedBoost", "    Type BDT",
    "    Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20",
    "End_TMVAMethod"
]

Cfg_Methods += [
    "New_TMVAMethod", "    Name BDTNotUseBaggedBoost", "    Type BDT",
    "    Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20",
    "End_TMVAMethod"
]

Method_Names += ["BDTUseBaggedBoost", "BDTNotUseBaggedBoost"]

for i in range(9):
    Cfg_Methods += [
        "New_TMVAMethod",
        "   Name BDTBaggedSampleFraction=%.1f" % ((i + 1) / float(10)), "   Type BDT",
        "   Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=%.1f:SeparationType=GiniIndex:nCuts=20"
        % ((i + 1) / float(10)), "End_TMVAMethod"
    ]

    Method_Names += ["BDTBaggedSampleFraction=%.1f" % ((i + 1) / float(10))]

for i in ["CrossEntropy", "GiniIndex", "GiniIndexWithLaplace", "MisClassificationError", "SDivSqrtSPlusB", "RegressionVariance"]:
    Cfg_Methods += [
        "New_TMVAMethod",
        "   Name BDTSeparationType=%s" % (i), "   Type BDT",
        "   Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=%s:nCuts=20"
        % (i), "End_TMVAMethod"
    ]

    Method_Names += ["BDTSeparationType=%s" % (i)]

for i in range(20):
    Cfg_Methods += [
        "New_TMVAMethod",
        "   Name BDTnCuts=%d" % (5 * i + 5), "   Type BDT",
        "   Option !H:!V:NTrees=850:MinNodeSize=2.5%%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=%d"
        % (i * 5 + 5), "End_TMVAMethod"
    ]

    Method_Names += ["BDTnCuts=%d" % (5 * i + 5)]

WriteList(["ifdef iSTMVATraining"] + Cfg_Methods + [FillWhiteSpaces(10, "\n")] + ["BookMethod %s" % (M) for M in Method_Names] + ["endif"],
          "%s/TMVA_BDT1DScan.conf" % (ResolvePath("XAMPPtmva/TrainingConf")))
